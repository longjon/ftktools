/********************************************************/
/*                                                      */
/********************************************************/

#ifndef SPYBUFFERGETTER_H
#define SPYBUFFERGETTER_H 


#include <vector>
#include <map>
#include <string>


enum FTKBoard {FTK=0, IM=1, DF=2, AUX=3, AMB=4, SSB=5, FLIC=6};

class SpyBufferGetter{
  	
  	public: //Constructor and destructor
	  	
  	SpyBufferGetter();
  	~SpyBufferGetter();
		
	
  		
	std::vector <unsigned int> getSpyBufferFromSourceID(unsigned int sourceID)
	{
		return m_robSourceIDtoData->at(sourceID);
	};
	
	std::vector<unsigned int> *getSourceIDs(){
		return m_index;
	};
	
	std::string getStringBoardTypeFromUInt(uint32_t board){
		switch(board){
			case 0: return "FTK";
			case 1: return "IM";
			case 2: return "DF";
			case 3: return "AUX";
			case 4: return "AMB";
			case 5: return "SSB";
			case 6: return "Flic";
		}
		return 0;			
	};
	
	std::vector<unsigned int> *getBoardSpecificSourceIDs(FTKBoard board){
		switch(board){
			case FTK : return m_index;
			case IM  : return m_index_IM;
			case DF  : return m_index_DF;
			case AUX : return m_index_AUX;
			case AMB : return m_index_AMB;
			case SSB : return m_index_SSB;
			case FLIC: return m_index_FLIC;
		}
		return 0;		
	};
	
	std::string getStringBoardType(FTKBoard board){
		switch(board){
			case FTK : return "FTK";
			case IM  : return "IM";
			case DF  : return "DF";
			case AUX : return "AUX";
			case AMB : return "AMB";
			case SSB : return "SSB";
			case FLIC: return "Flic";
		}
		return 0;		
	};

	
    struct detailed_information_struct {
        float num_float;
        int num_int;
        bool err;
        std::string error_message;
        std::string additional_info;
    };
    

	void setupMapFromPartition(std::string partition_name);
	void setupMapFromDirectory(std::string dir);
	// void setupMapFromRawData(std::string file);
	
	void saveMapToDirectory(std::string dir, FTKBoard board);
	// void saveMapToRawDataFile(std::string dir);
	
    //void createMapforPostProcessing(FTKBoard board);
    
    std::map <std::string , std::map <unsigned int, detailed_information_struct > > createMapforPostProcessing(FTKBoard board);
    
    void printMapforPostProcessing(std::map <std::string , std::map <unsigned int, detailed_information_struct > > post_processing_information_map_input);
    
    //std::set <std::string> getCategoryList();
    detailed_information_struct getMapElement(std::map <std::string , std::map <unsigned int, detailed_information_struct > > post_processing_information_map_input, const char * category, unsigned int board_unique_ID);
    
    std::map <std::string , std::map <unsigned int, detailed_information_struct > > insertMapElement(std::map <std::string , std::map <unsigned int, detailed_information_struct > > post_processing_information_map_input, const char * category, unsigned int board_unique_ID, float struct_float, int struct_int, bool struct_error, std::string struct_error_message, std::string struct_add_info);
    
		
  	private: //Common variables
  	
    std::map <unsigned int, std::vector <unsigned int>>* m_robSourceIDtoData; //map source IDs (int) with the datawords --> vector of the datawords
	
	std::vector<unsigned int>* m_index;
	std::vector<unsigned int>* m_index_FTK;
	std::vector<unsigned int>* m_index_IM;
	std::vector<unsigned int>* m_index_DF;
	std::vector<unsigned int>* m_index_AUX;
	std::vector<unsigned int>* m_index_AMB;
	std::vector<unsigned int>* m_index_SSB;
	std::vector<unsigned int>* m_index_FLIC;
    

    // helper functions to re-dump spyfiles from another directory:
    
    std::vector<std::string> getSpyFilesFromDirectory(const std::string& name);
    std::vector<int> spyfileToVector(std::string spyfile);
    unsigned int hexStringToInt(std::string hexString);
    std::string getSourceIDFromFilename(std::string filename);
    int getAbsoluteChannelFromSourceID(std::string sourceID);

	
};

#endif // SPYBUFFERGETTER_H
