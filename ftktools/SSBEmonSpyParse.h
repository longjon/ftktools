#ifndef SSBEMONSPYPARSE_H
#define SSBEMONSPYPARSE_H

#include <string>

#include <ftkcommon/SourceIDSpyBuffer.h>

#include <ftktools/SpyBufferGetter.h>

#include <ssb/ssb_srsb.h>

namespace daq
{
    namespace ftk
    {
        class ssb_emon_spyparse
        {
        public:

            ssb_emon_spyparse(SpyBufferGetter* getter);

            void printSSBHistosToFile(std::string filePath, std::string base_file_name, int nBLT=SSB_HISTO_BLT, int nWords=SSB_HISTO_NWORDS, int nSPY=1);
            void printSSBSRSBsToFile(std::string filePath);
            void printSSBSpybuffersToFile(std::string filePath, std::string base_file_name, int nBLT=SSB_SPYBUFFER_BLT, int nWords=SSB_SPYBUFFER_NWORDS, int nSPY=SSB_EXTF_NSPY, bool hmode = false);

            std::vector<std::vector<uint32_t>>& getOutspys();

        private:
            void get_ssb_histos();
            void get_ssb_srsbs();
            void get_ssb_spybuffers();

            SpyBufferGetter*  m_getter;
            std::vector <unsigned int> *m_available_sourceIDs;
            
            std::map< uint32_t, std::vector<uint>> m_spybuffers;
            std::map< uint32_t, std::vector<uint>> m_histos;
            std::map< uint32_t, daq::ftk::ssb_srsb> m_srsbs;        
            
        };
        
    }
}


#endif

