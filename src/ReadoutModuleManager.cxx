/*****************************************************************/
/*                                                               */
/*                     ReadoutModule_Manager                     */
/*                  Author: Simone Sottocornola                  */
/*                  simone.sottocornola@cern.ch                  */
/*                          17/05/2018                           */
/*                                                               */
/*****************************************************************/

#include <iostream>
#include <stdlib.h>
#include <sys/syscall.h>
#include <memory>               //shared_ptr
#include <utility>              //pair
#include <ctime>
#include <thread>

// OKS, RC and IS
#include "rc/DAQApplicationInfoNamed.h"
#include "ftktools/ReadoutModuleManager.h"
#include "ftktools/dal/ReadoutModule_Manager.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "dal/Segment.h"
#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "is/info.h"
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"
#include "ftkcommon/Utils.h"    //getDFOHNameString

namespace daq
{
    namespace ftk
    {
        ReadoutModule_Manager::ReadoutModule_Manager():
            m_configuration(0),
            m_monitorCheck(true),
            m_running(false),
            m_recovery_ongoing(false),
            m_recovery_failed(false)
        {
            ERS_LOG("ReadoutModule_Manager::constructor: Entered"       << std::endl
                    << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl);
            if (daq::ftk::isLoadMode())
            { ERS_LOG("Loading mode"); }
            ERS_LOG("ReadoutModule_Manager::constructor: Done");
        }

        ReadoutModule_Manager::~ReadoutModule_Manager() noexcept
        {
            ERS_LOG("ReadoutModule_Manager::destructor: Start");
            ERS_LOG("ReadoutModule_Manager::destructor: Done");
        }


        void ReadoutModule_Manager::setup(DFCountedPointer<ROS::Config> configuration)
        {
            ERS_LOG("ReadoutModule_Manager::setup: Entered");

            // Get online objects from daq::rc::OnlineServices
            m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
            m_appName = daq::rc::OnlineServices::instance().applicationName();
            auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
            Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
            auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );

            // Read some info from the DB
            //const ftk::dal::ReadoutModule_Manager * module_dal = conf->get<ftk::dal::ReadoutModule_Manager>(configuration->getString("UID"));
            const dal::ReadoutModule_Manager * module_dal = conf->get<dal::ReadoutModule_Manager>(configuration->getString("UID"));
            m_name    				 = module_dal->UID();
            m_debugMode 			 = module_dal->get_DebugMode();
            m_perform_recovery = module_dal->get_performRecovery();
            m_isServerName  	 = readOutConfig->get_ISServerName(); 

            // Creating the ISInfo object
            std::string isProvider = m_isServerName + "." + m_name; 
            ERS_LOG("IS: publishing in " << isProvider);
            try 
            { 
              m_managerNamed = std::make_unique<managerNamed>( m_ipcpartition, isProvider.c_str()); 
            } 
            catch( ers::Issue &ex)
            {
                daq::ftk::ISException e(ERS_HERE, "Error while creating managerNamed object", ex);  // NB: append the original 
                // exception (ex) to the new one 
                ers::warning(e);  //or throw e;  
            }
            ERS_LOG("ReadoutModule_Manager::setup: Done");
        }

        void ReadoutModule_Manager::configure(const daq::rc::TransitionCmd& cmd)
        { 
            ERS_LOG(cmd.toString());
            ERS_LOG(cmd.toString() << " done"); 
        }


        void ReadoutModule_Manager::unconfigure(const daq::rc::TransitionCmd& cmd)
        {
            ERS_LOG(cmd.toString());
            ERS_LOG(cmd.toString() << " done");
        }



        void ReadoutModule_Manager::connect(const daq::rc::TransitionCmd& cmd)
        {
            ERS_LOG(cmd.toString());
            ERS_LOG(cmd.toString() << " done");
        }



        void ReadoutModule_Manager::disconnect(const daq::rc::TransitionCmd& cmd)
        {
            ERS_LOG(cmd.toString());
            ERS_LOG(cmd.toString() << " done");
        }


        void ReadoutModule_Manager::prepareForRun(const daq::rc::TransitionCmd& cmd)
        {
            ERS_LOG(cmd.toString());
            m_running=true;
            ERS_LOG(cmd.toString() << " Done");
        }


        void ReadoutModule_Manager::stopRecording(const daq::rc::TransitionCmd& cmd)
        {
            ERS_LOG(cmd.toString());
            m_running=false;
            ERS_LOG(cmd.toString() << " done");
        }


        void ReadoutModule_Manager::publish()
        {
            ERS_LOG("Publishing...");    
            ERS_LOG( " Done");
        }


        void ReadoutModule_Manager::publishFullStats()
        {
            ERS_LOG("Publishing Full Stats...");
            ERS_LOG("Publishing Full Stats... Done");
        }

        void ReadoutModule_Manager::resynch(const daq::rc::ResynchCmd& cmd)
        {
          ERS_LOG(cmd.toString());
          std::vector<std::string> param = cmd.arguments();
          ERS_LOG("Received the Resynch request from CHIP. Arguments are:");
          for (int i=0; i<param.size(); i++)
            ERS_LOG("Param " << i << " = " << param[i].c_str() );
          //Taking proper actions if needed (e.g. send a last transition to the boards..)
          ERS_LOG("Rising the HardwareRecovered issue; Application to be notified: " + m_recovery_params[1]);
          daq::rc::HardwareRecovered recovery( ERS_HERE , m_recovery_params[0].c_str() , m_recovery_params[1].c_str());
          ers::error(recovery);
          ERS_LOG(cmd.toString() << " done");
        }

        void ReadoutModule_Manager::InitializeMap()
        {
          //Reading the Configuration..Collecting all the active RMs from the FTK segment in a list 
          Configuration& confi=daq::rc::OnlineServices::instance().getConfiguration();
          const daq::core::Partition & partition = daq::rc::OnlineServices::instance().getPartition();
          const daq::core::Segment & segment = daq::rc::OnlineServices::instance().getSegment();
          m_segment = segment.UID();
          std::string last_UID;
          std::vector<const daq::core::ResourceBase*> rb = segment.get_Resources();
          std::vector<const daq::core::Component*> disabledComponents = partition.get_Disabled();
          if (m_debugMode)
          {
            ERS_LOG( "Got " << disabledComponents.size() << " disabled applications");
            ERS_LOG( "Got " << rb.size() << " applications from segment.get_Resources()");
          }
          //Performing the check on all found applications what are the RM and what are the active ones
          for (std::vector<const daq::core::ResourceBase*>::iterator it = rb.begin(); it != rb.end(); it++) 
          {
            const daq::df::RCD* rcdAppl = confi.cast<daq::df::RCD>(*it);
						if ( rcdAppl == 0 ) continue;
						if (rcdAppl->disabled(partition))
						{
              if (m_debugMode){
                ERS_LOG( "ReadoutApplication " << rcdAppl->UID()<< " is disabled!\n Skipping it..");
              }
              continue;
            }
						//Check the state and the membership of the application
						DAQApplicationInfoNamed info(m_ipcpartition, "RunCtrl.Supervision." + rcdAppl->UID());
            info.checkout();                                                                               
            bool isIN = info.membership;
						std::string stat = info.status;
						if (stat!="UP")
						{
							ERS_LOG("Application " << rcdAppl->UID() << " is not in UP state. Current state = " << stat <<  ". \nSkipping it..");
							continue;
						}
						else if (!isIN)
						{
							ERS_LOG("Application RCD " << rcdAppl->UID() << " is out of membership! \nSkipping it.."); 
							continue;
						}
            std::vector<const daq::core::ResourceBase*> RMs = rcdAppl->get_Contains();
            for (std::vector<const daq::core::ResourceBase*>::iterator cit = RMs.begin(); cit != RMs.end(); cit++)
            {
              const daq::df::ReadoutModule* roModule = confi.cast<daq::df::ReadoutModule, daq::core::ResourceBase> (*cit);
              if (m_debugMode)
              {
                std::cout << "Current ReadoutModule UID: " << roModule->UID()<< "\n";
                std::cout << "Current ReadoutModule class: " << roModule->class_name() << "\n";
              }
              if (roModule==0) //check if the object is a RM
              {
                if (m_debugMode){
                  ERS_LOG( "ReadoutApplication " << rcdAppl->UID() << " Contains relationship to something (" 
                            << roModule->class_name() << ") that is not a ReadoutModule.. Skipping it!");
                }
                continue;
              }
              else if ((*cit)->disabled(partition)) //check if the RM is enabled
              {
                if (m_debugMode){
                  ERS_LOG( "ReadoutModule " << roModule->UID()<< " is disabled!\n Skipping it.."); 
                }
                continue;
              }
              else if (roModule->UID()== m_name)  //check if the RM is the manager Application
              {                                   
                if (m_debugMode){                    
                  ERS_LOG( "ReadoutModule " << roModule->UID()<< " is the Manager application!\n Going in the ready state..");  
                }                                 
                m_state.AddRM(roModule->UID(), 0);
                continue;                         
              }
              else //Adding the RM in the map (with the state -1: not defined)
              {
                ERS_LOG("Adding RM " + roModule->UID() + " to the RM list.." );
                m_state.AddRM(roModule->UID(), -1);
                last_UID=roModule->UID();
              }
            }
          }
          m_state.PrintContent();
          if (m_debugMode)
          {
            //Try to change the state of the map
            ERS_LOG("Trying all_equal..PS: last RM is " << last_UID);
            ERS_LOG("The common state is " << m_state.which_state());
            ERS_LOG("Changing the state of the last RM");
            m_state.updateState(last_UID, -8);
            m_state.PrintContent();
            ERS_LOG("The common state is " << m_state.which_state());
          }
          ERS_LOG("The map has been initialized");  
        }

        void ReadoutModule_Manager::user(const daq::rc::UserCmd& cmd) 
        {
          const std::string& cmdName = cmd.commandName();
          ERS_LOG("Received user command \"" + cmdName + "\" while in state \"" + cmd.currentFSMState() + "\"");
          if (cmdName == "FTKRecovery")
          {
            if (!isRunning())
             return;
            ERS_LOG("Starting the recovery procedure for FTK..");
						daq::ftk::ftkException ex(ERS_HERE, m_name, "Recovery procedure for FTK started!");
						ers::warning(ex);
            m_recovery_ongoing=true;
            //The FTKRecovery command shuld be sent with the parameters defining the ROLs to be recovered and the app name to be informed.
            m_recovery_params = cmd.commandParameters();
            m_recovery_params.push_back(m_appName);
            //Inizializing the map. The initialization is done here in order to create a map of the active RM at this time!
            //e.g. to solve the problem of a RM put out of the partition
            if (m_monitorCheck)
              InitializeMap();
            //making the call for the first transitions
            sendTrasnition(0);
            //going to spawn the thread that will manage the future calls to the FTK internal transitions
            if (m_monitorCheck)
            {
              m_monitorCheck = false;
              std::thread Recovery_monitor(&ReadoutModule_Manager::RecoveryMonitor, this);
              Recovery_monitor.detach();
            }
          }
          if (cmdName == "ACK")
          {
            if (m_recovery_ongoing==false)
            {
              ERS_LOG("Received an ACK when the recovery prodecure is not going/has been blocked. Ignoring it.." );
              return;
            }
            std::vector<std::string> param = cmd.commandParameters();
            ERS_LOG("Received the ACK from board: \n" +  cmd.toString() );
            //Parameters n°=4. Command type (user), command name (ACK), passed parameters: UID, state;
            if (m_debugMode)
						{
              for(int i = 0; i < param.size(); i++) 
              {
                ERS_LOG("Parameter " << i << ": " << param[i] );
              }
						}
						//check in the map to which integer refers to the given trasnition
						std::map<int,std::string>::iterator it;
						for (it = m_transitions.begin(); it != m_transitions.end(); ++it )
            {
    				  if (param[1] == "ERROR")
              {
                //Check for errors occured during the recovery transition execution
                m_recovery_failed=true;
                ERS_LOG("Received ERROR state from bord " + param[0]);
                std::stringstream message;
                message << "The recovery application failed! Transition not completed for board " << param[0];
                FTKRecoveryIssue err(ERS_HERE, name_ftk(), message.str());
                ers::error(err);
              }	
              else if (it->second == param[1])
							{
            		//going to chage the state of the RM in the map.
            		m_state.updateState(param[0],it->first);
							}
            } 
            //Notify the thred to start back
            std::unique_lock<std::mutex> lck(mtx);
            m_cvar.notify_one();
            if (m_debugMode)
              m_state.PrintContent();
          }
        }

        void ReadoutModule_Manager::user_sender(const std::string cmd_name, const std::vector<std::string>& cmd_params)
        {
          //Sending the Recovery usercommand to FTK segment
          ERS_LOG("Sending the command " << cmd_name << " to the " << m_segment << " segment");
          rc::UserCmd command(cmd_name, cmd_params);
          rc::UserBroadcastCmd usrbc(command);
          rc::CommandSender sendr(m_ipcpartition.name(), m_appName);
          sendr.makeTransition(m_segment, usrbc);
          ERS_LOG("Usercommand " + cmd_name + " sent to the FTK segment...");
        }

        void ReadoutModule_Manager::sendTrasnition(int curr_state)
        {
          const std::string cmd_name = "Recovery";
          std::vector<std::string> cmd_params;
					std::map<int,std::string>::iterator it;
					it=m_transitions.find(curr_state);
					if (it==m_transitions.end())
					{
						std::stringstream message;
            message << "Critical recovery error: (sendTrasnition) Unable to find transition " << curr_state;
            daq::ftk::FTKRecoveryIssue ex(ERS_HERE, name_ftk(), message.str());
            ers::error(ex);
					}
					else
          	cmd_params.push_back(it->second);
          ERS_LOG("Sending the command " << cmd_name);
          user_sender(cmd_name, cmd_params);
        }

        void ReadoutModule_Manager::RecoveryMonitor()
        {
          ERS_LOG("Entered RecoveryMonitor method.. Start monitoring the RMs states.. When ready the next internal transition will be called");
          //loop over the possible transitions. starting from 1 because trasnition 0 is called by the user function
          for (int curr_state=1; curr_state< m_transitions.size(); curr_state++)
          {
            while(m_state.which_state() == -99)
            {
              if (!isRunning() || m_recovery_failed)
              {
                ERS_LOG("Stopping the Recovery procedure..");
                ERS_LOG("Clearing the map to be ready for a possible new Recovery call..");
                m_recovery_ongoing=false;
                m_recovery_failed=false;
                m_state.mapClear();
                m_monitorCheck=true;
                return;
              }
              ERS_LOG("Transition not completed.. waiting..");
              std::unique_lock<std::mutex> lck(mtx);
              m_cvar.wait(lck);
              ERS_LOG("Thread waked up.. Checking the states..");
            }
            ERS_LOG("Transition " << m_state.which_state() << " completed.. \nCalling transition " << curr_state );
						std::stringstream msg;
						msg << "Transition " << m_state.which_state() << " completed.. \nCalling transition " << curr_state;
            daq::ftk::ftkException ex(ERS_HERE, m_name, msg.str());
						ers::info(ex);
						//move the state of the Manager to the next transition..To waits for that trasnition to occur
            m_state.updateState(m_name,curr_state);
            sendTrasnition(curr_state);
          }
          while(m_state.which_state() == -99)
          {
            if (!isRunning() || m_recovery_failed)
            { 
              ERS_LOG("Stopping the Recovery procedure..");
              ERS_LOG("Clearing the map to be ready for a possible new Recovery call..");
              m_recovery_failed=false;
              m_recovery_ongoing=false;
              m_state.mapClear();
              m_monitorCheck=true;
              return;
            }
            ERS_LOG("Transition not completed.. waiting..");
            std::unique_lock<std::mutex> lck(mtx);
            m_cvar.wait(lck);
            ERS_LOG("Thread waked up.. Checking the states..");
          }
          if (m_state.which_state() == (m_transitions.size()-1)) 
          {
            m_recovery_ongoing=false;
            ERS_LOG("FTK recovery of the HW completed succesfully! Going to start the Stopless-recovery procedure..");
						std::stringstream msg;
						msg << "FTK recovery of the HW completed succesfully! Going to start the Stopless-recovery procedure..";
            daq::ftk::ftkException ex(ERS_HERE, m_name, msg.str());                                                               
            ers::warning(ex);
            if(m_perform_recovery)
            {
              ERS_LOG("Rising the ReadyForHardwareRecovery issue; Application to be notified: " + m_recovery_params[1]);
              daq::rc::ReadyForHardwareRecovery recovery( ERS_HERE , m_recovery_params[0].c_str() , m_recovery_params[1].c_str());
              ers::error(recovery);
            }
          }
          else
          {
            std::stringstream message;
            message << "ERROR: Recovery failed! current state " << m_state.which_state() 
                    << " while final state should be " << m_transitions.size()-1;
            daq::ftk::FTKRecoveryIssue ex(ERS_HERE, name_ftk(), message.str());
            ers::error(ex);
          }
          ERS_LOG("Clearing the map to be ready for a possible new Recovery call..");
          m_state.mapClear();
          m_recovery_ongoing=false;
          m_monitorCheck=true;
        }
  
        bool ReadoutModule_Manager::isRunning()
        {
          if (!m_running)
          {
           std::stringstream message;
           message << "ERROR: not in RUNNING state!";
           daq::ftk::FTKRecoveryIssue ex(ERS_HERE, name_ftk(), message.str());
           ers::error(ex);  
           return false;
          }
          else 
            return true;
        }
  
        //FOR THE PLUGIN FACTORY
        extern "C"
        {
            extern ROS::ReadoutModule* createReadoutModule_Manager();
        }

        ROS::ReadoutModule* createReadoutModule_Manager()
        {
            return (new ReadoutModule_Manager());
        }

        /// Constructor of ::TSafeMap class
        ReadoutModule_Manager::TSafeMap::TSafeMap()
        {
          ERS_LOG("TSafeMap constructor entered.");
          m_accessMutex = DFFastMutex::Create();
          ERS_LOG("TSafeMap constructor complete.");
        }

        /// Destructor of FtkEMonDataOut::TSafeMap class
        ReadoutModule_Manager::TSafeMap::~TSafeMap()
        {
          ERS_LOG("TSafeMap destructor entered.");
          m_accessMutex->destroy();
          ERS_LOG("TSafeMap destructor complete.");
        }

        /// AddSpyBuff method of FtkEMonDataOut::TSafeMap class
        void ReadoutModule_Manager::TSafeMap::AddRM(std::string roModule, int state )
        {
          m_accessMutex->lock();
          //If the RM is already in the map, rise an error  
          if(m_data.count(roModule))
          {
            std::stringstream message;
            message << "ERROR: trying to insert the RM "<< roModule <<" which is already present in the map!";
            daq::ftk::FTKRecoveryIssue ex(ERS_HERE, name_ftk(), message.str());
            ers::error(ex);
          }
          m_data.insert(pair <std::string, int> (roModule, state));
          m_accessMutex->unlock();
        }

        void ReadoutModule_Manager::TSafeMap::updateState(std::string roModule, int state)
        {
          m_accessMutex->lock();
          std::map<std::string, int>::iterator it = m_data.find(roModule); 
          if (it != m_data.end())
          {
            it->second = state;
          }
          else{
            std::stringstream message;
            message << "ERROR: trying to update the state of a RM missing! "<< roModule <<" is not in the map!";
            daq::ftk::FTKRecoveryIssue ex(ERS_HERE, name_ftk(), message.str());
            ers::error(ex);
          }
          m_accessMutex->unlock();
        }

        //Function that checks that all the states in the map are the same
        //NB: the mutex in this function is not locked! you should lock before entering it!
        bool ReadoutModule_Manager::TSafeMap::all_equal()
        {
          // the lambda will only get called when the map is not empty
          // so we can safely access begin()->second
          auto const cmpWithFirst = [&](std::pair<std::string,int> const& i)
          {
            return m_data.begin()->second == i.second;
          };
          bool out=std::all_of(m_data.begin(), m_data.end(), cmpWithFirst);
          return out; 
        }

        //checks that all the states in the maps are the same and returns the state. 
        //returns -99 if they are not all the same!
        int ReadoutModule_Manager::TSafeMap::which_state()
        {
          int out;
          m_accessMutex->lock();
          if (all_equal())
            out = m_data.begin()->second;
          else
            out = -99;
          m_accessMutex->unlock();
          return out;
        }

        void ReadoutModule_Manager::TSafeMap::mapClear()
        {
          m_accessMutex->lock();
          m_data.clear();
          m_accessMutex->unlock();
        }
      
        void ReadoutModule_Manager::TSafeMap::PrintContent()
        {
          ERS_LOG("map.size = " << m_data.size());
          m_accessMutex->lock();
          for( auto it = m_data.cbegin(); it != m_data.cend(); ++it)
          {
            std::cout << "Key: " << it->first << " \t\t State: " << it->second << " \n";
          }
          m_accessMutex->unlock();
        }

    }   //namespace ftk

}   //namespace daq
