#include "ftktools/SSBEmonSpyParse.h"

#include "ssb/ssb_defines.h"

#include <boost/filesystem.hpp>

namespace daq {
    namespace ftk {


        // Constructor
        //
        ssb_emon_spyparse::ssb_emon_spyparse(SpyBufferGetter* getter)
        {
            m_getter = getter;
            m_available_sourceIDs = getter->getBoardSpecificSourceIDs(FTKBoard::SSB);
            
            get_ssb_histos();
            get_ssb_srsbs();
            get_ssb_spybuffers();            
        }


        //
        //
        void ssb_emon_spyparse::get_ssb_histos()
        {
            // Loop over all source IDs, skip if it's not an SRSB
            for (auto it_sid = m_available_sourceIDs->begin(); it_sid!=m_available_sourceIDs->end(); it_sid++)
            {
                SourceIDSpyBuffer sid = daq::ftk::decode_SourceIDSpyBuffer(*it_sid);
                int n_fpga = -1;
                int board_number = sid.boardNumber;
                
                // EXTF
                if (sid.position == daq::ftk::Position::IN)
                {    
                    // Skip everything but histos            
                    if (sid.boardInternal == SSB_SOURCEID_SRSB + 0 || 
                        sid.boardInternal == SSB_SOURCEID_SRSB + 1 || 
                        sid.boardInternal == SSB_SOURCEID_SRSB + 2 || 
                        sid.boardInternal == SSB_SOURCEID_SRSB + 3 ||
                        sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 0 || 
                        sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 1 || 
                        sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 2 || 
                        sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 3 ) continue;
                                        
                } // HW 
                else if (sid.position == daq::ftk::Position::OUT)
                {
                    // Skip everything but histos
                    if (sid.boardInternal == SSB_SOURCEID_SRSB || 
                        sid.boardInternal == SSB_SOURCEID_SPYBUFFER) continue;
                    n_fpga = 4;
                                   
                }
                
                
                std::vector <unsigned int> histo = m_getter->getSpyBufferFromSourceID(*it_sid);
                
                m_histos[*it_sid] = histo;    
                
            }
        }

        //
        //
        void ssb_emon_spyparse::get_ssb_srsbs()
        {

            // Loop over all source IDs, skip if it's not an SRSB
            for (auto it_sid = m_available_sourceIDs->begin(); it_sid!=m_available_sourceIDs->end(); it_sid++)
            {
                SourceIDSpyBuffer sid = daq::ftk::decode_SourceIDSpyBuffer(*it_sid);
                int n_fpga = -1;
                int board_number = sid.boardNumber;

                // EXTF
                if (sid.position == daq::ftk::Position::IN)
                {                    
                    if (sid.boardInternal == SSB_SOURCEID_SRSB + 0) n_fpga = 0;
                    else if (sid.boardInternal == SSB_SOURCEID_SRSB + 1) n_fpga = 1;
                    else if (sid.boardInternal == SSB_SOURCEID_SRSB + 2) n_fpga = 2;
                    else if (sid.boardInternal == SSB_SOURCEID_SRSB + 3) n_fpga = 3;
                    else continue;                    
                } // HW 
                else if (sid.position == daq::ftk::Position::OUT)
                {
                    if (sid.boardInternal == SSB_SOURCEID_SRSB) n_fpga = 4;
                    else continue;                                        
                }
                

                std::vector <unsigned int> srsb_raw = m_getter->getSpyBufferFromSourceID(*it_sid);
                daq::ftk::ssb_srsb srsb(srsb_raw);
                m_srsbs[*it_sid] = srsb;    
                   

            }  // end loop

        }


        //
        //
        void ssb_emon_spyparse::get_ssb_spybuffers()
        {

            // Loop over all spybuffers, skip if it's not an SRSB
            for (auto it_sid = m_available_sourceIDs->begin(); it_sid!=m_available_sourceIDs->end(); it_sid++)
            {
                SourceIDSpyBuffer sid = daq::ftk::decode_SourceIDSpyBuffer(*it_sid);
                int n_fpga = -1;
                int board_number = sid.boardNumber;

                // EXTF
                if (sid.position == daq::ftk::Position::IN)
                {                    
                    if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 0) n_fpga = 0;
                    else if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 1) n_fpga = 1;
                    else if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 2) n_fpga = 2;
                    else if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 3) n_fpga = 3;
                    else continue;                    
                } // HW 
                else if (sid.position == daq::ftk::Position::OUT)
                {
                    if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER) n_fpga = 4;
                    else continue;                                        
                }
                
                


                std::vector <unsigned int> spyBuffer = m_getter->getSpyBufferFromSourceID(*it_sid);
                m_spybuffers[*it_sid] = spyBuffer;


            }  // end loop


        }


        //
        //
        std::vector<std::vector<uint32_t>>& ssb_emon_spyparse::getOutspys()
        {

            int nBLT = SSB_SPYBUFFER_BLT;
            int nWords = SSB_SPYBUFFER_NWORDS;
            int nSPY = SSB_HW_NSPY;
               

            std::vector<std::vector<uint32_t>> output_spybuffers;

            for (auto const& it_map : m_spybuffers)
            {

                SourceIDSpyBuffer sid = daq::ftk::decode_SourceIDSpyBuffer(it_map.first);
                int n_fpga = -1;
                int board_number = sid.boardNumber;
                
                // Pick out HW spybuffer
                if (sid.position == daq::ftk::Position::OUT && sid.boardInternal == SSB_SOURCEID_SPYBUFFER)
                {
                    std::vector<uint32_t> temp_spy;
                    // Loop over words in spybuffer
                    for(u_int i=0; i<nBLT*nWords; i++)
                    {
                        for(u_int ispy=0; ispy<nSPY; ispy++)
                        {
                            // Pick only the final output column.  This part depends on the formatting of the spybuffers out of the firmware!
                            if (it_map.second[i+ispy*nBLT*nWords] & 0xf0000000 == 0xf0000000 )
                            {
                                temp_spy.push_back(it_map.second[i]);
                                break; // only grab the first 0xf column
                            }                        
                        }
                    }
                    output_spybuffers.push_back(temp_spy);
                }              
            }


            return output_spybuffers;
        }


        //
        //
        void ssb_emon_spyparse::printSSBHistosToFile(std::string filePath, std::string base_file_name, int nBLT, int nWords, int nSPY)
        {

            printSSBSpybuffersToFile(filePath, base_file_name, nBLT, nWords, nSPY, true );

        }
        
        
        //
        //
        void ssb_emon_spyparse::printSSBSRSBsToFile(std::string filePath)
        {
            
            
        }


        //
        //
        void ssb_emon_spyparse::printSSBSpybuffersToFile(std::string filePath, std::string base_file_name, int nBLT, int nWords, int nSPY, bool hmode)
        {


            std::map< uint32_t, std::vector<uint>> m_loop;
            if (hmode) m_loop =  m_histos;
            else       m_loop = m_spybuffers;

            for (auto const& it_map : m_loop)
            {
                
                SourceIDSpyBuffer sid = daq::ftk::decode_SourceIDSpyBuffer(it_map.first);
                int n_fpga = -1;
                int board_number = sid.boardNumber;

                // Get FPGA number for spybuffers, doesn't work for histos
                // EXTF
                if (sid.position == daq::ftk::Position::IN)
                {                    
                    if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 0) n_fpga = 0;
                    else if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 1) n_fpga = 1;
                    else if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 2) n_fpga = 2;
                    else if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER + 3) n_fpga = 3;
                } // HW 
                else if (sid.position == daq::ftk::Position::OUT)
                {
                    if (sid.boardInternal == SSB_SOURCEID_SPYBUFFER) n_fpga = 4;
                }
                

                boost::filesystem::create_directories(filePath);

                stringstream filename;                
                // Name the histogram output files in a human readable way
                if (hmode) 
                {
                    histo_names hnames;
                    try // hitwarrior first
                    {
                        filename << filePath << "/" << base_file_name << "_" << hnames.hw_name_map.at(sid.boardInternal) << "_" << board_number << "_FPGA4.log";
                    } catch (const std::out_of_range& oor)
                    {
                        // loop over n_extf to find which one this is for
                        for (uint i=0; i <SSB_N_EXTF_FPGA; i++)
                        {
                            try
                            {
                                
                                filename << filePath << "/" << base_file_name << "_" << hnames.extf_name_map.at(sid.boardInternal - i) << "_" << board_number<< "_FPGA"<<i<<".log";
                                break;
                            } catch (const std::out_of_range& oor)
                            { // if we didn't find a name:
                                if (i == SSB_N_EXTF_FPGA-1) filename << filePath << "/" << base_file_name << "_" << sid.boardInternal << "_"<<board_number<< ".log";                            
                            }
                        }
                    }

                } else filename << filePath << "/" << base_file_name << n_fpga << "_"<<board_number<< ".log";

                
                ofstream out_file;
                out_file.open(filename.str());

                if(out_file.is_open())
                {
                    // This particular histogram has a different size
                    if (hmode & (sid.boardInternal == SSB_SOURCEID_LAYERMAP_HISTO + 0x0 ||
                                 sid.boardInternal == SSB_SOURCEID_LAYERMAP_HISTO + 0x1 ||
                                 sid.boardInternal == SSB_SOURCEID_LAYERMAP_HISTO + 0x2 ||
                                 sid.boardInternal == SSB_SOURCEID_LAYERMAP_HISTO + 0x3
                                 ) ) nBLT = 256;

                    // Write to the file
                    for(u_int i=0; i<nBLT*nWords; i++)
                    {
                        for(u_int ispy=0; ispy<nSPY; ispy++)
                        {

                            if (hmode)
                            {
                                out_file<< "word " << std::setw(4) << std::setfill('0') << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << it_map.second[i];

                            } else
                            {
                                out_file<< "word " << std::setw(4) << std::setfill('0') << std::dec << i <<": 0x" << std::setw(8) << std::setfill('0') << std::right <<std::hex << it_map.second[i+ispy*nBLT*nWords] <<"  ";
                            }
                        }                    
                        out_file<<std::endl;
                    }

                    out_file.close();
                    
                } else // file not open
                {

                    std::cout<<"Failed to open file: "<<filename.str()<<std::endl;
                }


            }  // end loop            
            
        }



    }
}
