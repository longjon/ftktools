#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <tuple>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <sys/stat.h>

//histograms
#include <TFile.h>
#include <TH1F.h>

// real
#include "ftkcommon/EventFragmentHitClusters.h"
#include "ftkcommon/Utils.h"
#include "ftkcommon/Cluster.h"
#include "ftktools/DFSpyHandler.h"
#include "ftktools/Utils_dfspy.h"

// DFConstants (separate class) (include DF CONSTANTS!)
const int NUMBER_OF_FTK_TOWERS = 64;
const int INSPY_QUEUE_MAX_LENGTH = 250;
const int OUTSPY_QUEUE_MAX_LENGTH = 20;

/*
 * Constructor
 * ----
 * Requires three config files from FTKSpyHandler. Builds LUTs.
 */

DFSpyHandler::DFSpyHandler(std::string multiboard, std::string modulelist, std::string system_config) {
    buildLUTs(multiboard, modulelist, system_config);
    expectedModulesInspyCount = 0, totalModulesInspy = 0;
    expectedModulesOutspyCount = 0, totalModulesOutspy = 0;
}

// Destructor
//DFSpyHandler::~DFSpyHandler() {
//    // Not yet implemented
//}

// ======= Public functions:

void DFSpyHandler::performBitlevelChecks( const std::map< daq::ftk::SourceIDSpyBuffer, std::shared_ptr<daq::ftk::SpyBuffer> >& bufferMap) {
    // Not yet implemented, but has very simple implementation:
    // Iterate through keys of bufferMap, call parseNewInputBuffer or parseNewOutputBuffer on the vector of EventFragmentHitClusters!
    // Tester below uses text files to generate these vectors -- this is the function for the final product.
    return;
}

void DFSpyHandler::performBitLevelChecksTESTER(std::string dumpdir, bool parseDirectory, bool makeFrags, bool doParsing) {
    
    if(parseDirectory) {
        size_t sumFrags = 0, maxFrags = 0, minFrags = SIZE_MAX;
        std::vector<std::string> vec = getSpyFilesFromDirectory(dumpdir);
        std::vector<Buffer> buffers;
        for(std::vector<std::string>::iterator itvec = vec.begin(); itvec != vec.end(); ++itvec) {
            std::cout << "\n NEXT SPYFILE \n" << std::endl;
            std::cout << "Path:         " << *itvec << std::endl;
            std::string filename = itvec->substr(itvec->find("DF_"));
            std::cout << "Filename:     " << filename << std::endl;
            std::string sourceID = getSourceIDFromFilename(filename);
            unsigned int intSourceID = dfspy::hexStringToInt(sourceID);
            std::cout << "SourceID:     " << sourceID << std::endl;
            int board = daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber;
            std::cout << "Board #:      " << daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber << std::endl;
            int absChannel = getAbsoluteChannelFromSourceID(sourceID);
            int lane;
            bool isInspy;
            if(absChannel < 36) {
                isInspy = false;
                lane = absChannel;
            } else {
                isInspy = true;
                lane = absChannel - 36;
            }
            std::cout << "Lane:         " << lane << ((isInspy) ? " (Inspy)" : " (Outspy)") << std::endl;
            if(makeFrags) {
                // We're making 4tups: (eventFrags, board, lane, isInspy)
                unsigned int ctrlPos = 16;
                std::vector<unsigned int> data = spyfileToVector(*itvec);
                std::cout << "data size:    " << data.size() << std::endl;
                std::cout << "== Errors/notices in EventFragment parsing of spyfile: ==" << std::endl;
                std::vector<daq::ftk::EventFragmentHitClusters*> frags = FTKPacket_parseFragments<daq::ftk::EventFragmentHitClusters>(data, ctrlPos);
                std::cout << "== End errors/notices ==" << std::endl;
                std::cout << "# of frags:   " << frags.size() << std::endl;
                
                // Update frag stats:
                sumFrags += frags.size();
                if(frags.size() > maxFrags) maxFrags = frags.size();
                if(frags.size() < minFrags) minFrags = frags.size();
                // Prepare buffer vector:
                board = daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber;
                std::sort(frags.begin(), frags.end(), dfspy::compareByL1ID);
                Buffer buffer = {frags, board, lane, isInspy, filename, sourceID};
                buffers.push_back(buffer);
            }
        }
        std::cout << "\n \n DIRECTORY PARSING COMPLETE" << std::endl;
        std::cout << "         STATISTICS             " << std::endl;
        std::cout << "Spyfiles parsed:              " << buffers.size() << std::endl;
        std::cout << "Average frags per buffer:     " << sumFrags/vec.size() << std::endl;
        std::cout << "Maximum frags:                " << maxFrags << std::endl;
        std::cout << "Minimum frags:                " << minFrags << std::endl;
        
        
        if(doParsing) {
            std::cout << "Preparing to parse " << buffers.size() << " buffers..." << std::endl;
            //INSPY THEN OUTSPY
            for(int i = 0; i < 52; i++) {
                if(i < 36) continue;
                Buffer buffer = buffers[i];
                std::string btype;
                buffer.isInspy ? btype = "INSPY" : btype = "OUTSPY";
                unsigned int intSourceID = dfspy::hexStringToInt(buffer.sourceID);
                std::cout << "\n|------------------------------------------------------------------------------------------------------------|" << std::endl;
                std::cout << "|                                         NEXT BUFFER CHANNEL                                                |" << std::endl;
                std::cout << "|------------------------------------------------------------------------------------------------------------|" << std::endl;
                std::cout << "Buffer Channel:   " << btype << " | Board Number: " << daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber << " | Lane " << buffer.lane << " | frags: " << buffer.frags.size() << " | Filename:  " << buffer.filename  << std::endl;
                
                if(!buffer.isInspy) {
                    parseNewOutputBuffer(buffer.frags, daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber, buffer.lane);
                } else {
                    parseNewInputBuffer(buffer.frags, daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber  , buffer.lane);
                }
            }
            
            for(int i = 0; i < 52; i++) {
                if (i > 35) continue;
                Buffer buffer = buffers[i];
                std::string btype;
                buffer.isInspy ? btype = "INSPY" : btype = "OUTSPY";
                unsigned int intSourceID = dfspy::hexStringToInt(buffer.sourceID);
                std::cout << "\n|------------------------------------------------------------------------------------------------------------|" << std::endl;
                std::cout << "|                                         NEXT BUFFER CHANNEL                                                |" << std::endl;
                std::cout << "|------------------------------------------------------------------------------------------------------------|" << std::endl;
                std::cout << "\n Buffer Channel:   " << btype << " | Board Number: " << daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber << " | Lane " << buffer.lane << " | frags: " << buffer.frags.size() << " | Filename:  " << buffer.filename  << std::endl;
                
                if(!buffer.isInspy) {
                    parseNewOutputBuffer(buffer.frags, daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber , buffer.lane);
                } else {
                    parseNewInputBuffer(buffer.frags, daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardNumber  , buffer.lane);
                }
            }
            
            
            // this part of the code displays the parsing statistics
            
            std::cout << "\n\n\n\n BUFFER PARSING STATISTICS \n\n";
            double percentageOutspy = ((double)expectedModulesOutspyCount/totalModulesOutspy) * 100;
            std::cout << "OUTSPY:     " << percentageOutspy << "% (" << expectedModulesOutspyCount << " / " << totalModulesOutspy << ") of modules were successfully checked against config." << std::endl;
            double percentageInspy = ((double)expectedModulesInspyCount/totalModulesInspy) * 100;
            std::cout << "INSPY:      " << percentageInspy << "% (" << expectedModulesInspyCount << " / " << totalModulesInspy << ") of modules were successfully checked against config. \n" << std::endl;
            
            std::cout << "All unique modules inspy:         " << uniqueModulesInspy.size() << std::endl;
            std::cout << "All unique modules outspy (also non LUT expected):        " << uniqueModulesOutspy.size() << std::endl;
            std::cout << "EXPECTED unique outspy modules (found in in the LUT): " << uniqueModulesOutspy_inLUT.size() << std::endl;
            
            
            // part of the code to investigate missing outspy modules:

            if (percentageOutspy != 100){
            if (uniqueModulesOutspy.size() != uniqueModulesOutspy_inLUT.size()){
                std::set <int> unexpected_outspy;
                std::set_difference(uniqueModulesOutspy.begin(), uniqueModulesOutspy.end(), uniqueModulesOutspy_inLUT.begin(), uniqueModulesOutspy_inLUT.end(), std::inserter(unexpected_outspy, unexpected_outspy.begin()));
                std::cout << "\nNumber of UNEXPECTED outspy modules (not found in LUT, but found in the SpyBuffers): " << unexpected_outspy.size() << std::endl;
                std::cout << "These are: " << std::endl;
                for(std::set<int>::iterator it_unexpected_outspy = unexpected_outspy.begin(); it_unexpected_outspy != unexpected_outspy.end(); ++it_unexpected_outspy ){
                }
                std::string modulelist = "/afs/cern.ch/work/s/ssevova/public/FTK_DF/TrigFTKSimTools/integration/df-spy-parse/DF-2-04_2PU_config_files/moduleList_Data2018_64T.fixed.txt";
                mapBoardAndLaneModIDsOutspy_tower_for_unexpected_modules(modulelist, unexpected_outspy );
            }
            
            // printing the unique modules that are missing in the outspy
            std::cout << "\nUnique modules MISSING in outspy: " << std::endl;
            
            std::set <int> diff;
            std::set_difference(uniqueModulesInspy.begin(), uniqueModulesInspy.end(), uniqueModulesOutspy.begin(), uniqueModulesOutspy.end(), std::inserter(diff, diff.begin()));
            std::cout << "\nNumber of unique modules not found in the outspy SpyBuffers but found in the inspy: " << diff.size() << std::endl;
            
            // creating a set of missing modules that are found in the LUT i.e. they correspond to a correct tower for a given board, they are found in inspy but not in outspy
            std::set <int> missing_modules_inLUT;
            missing_modules_inLUT.clear();
            for (auto i : diff){
                for(std::map<std::vector<int>, std::set<int> >::iterator it = boardAndLaneToModIDsOutspy.begin(); it != boardAndLaneToModIDsOutspy.end(); ++it) {
                    std::vector<int> boardLane = it->first;
                    std::set<int> modSet = it->second;
                    
                    for(std::set<int>::iterator iter = modSet.begin(); iter != modSet.end(); ++iter){
                        if (i == *iter){
                            missing_modules_inLUT.insert(i);
                        }
                    }
                }
            }
            // printing missing modules found in LUT
            if (missing_modules_inLUT.size() != 0){
                std::cout << "\nTruly missing modules - missing BUT found in the LUT, missing modules for all L1IDs recorded: " << std::endl;
                std::cout << "Found " << missing_modules_inLUT.size() << " of such modules. " << std::endl;
                std::cout << "These are: " << std::endl;
                for(std::set<int>::iterator itmissLUT = missing_modules_inLUT.begin();itmissLUT != missing_modules_inLUT.end(); ++itmissLUT ){
                    for (std::map<std::vector<int>, std::set<int> >::iterator it_tofind_outlane = boardAndLaneToModIDsOutspy.begin();it_tofind_outlane != boardAndLaneToModIDsOutspy.end(); ++it_tofind_outlane  ){
                        std::vector<int> boardandlaneVEC = it_tofind_outlane ->first;
                        std::set<int> moduleIDsOutspySET = it_tofind_outlane -> second;
                        
                        for (std::set<int>::iterator iter_moduleIDsOutspySET = moduleIDsOutspySET.begin(); iter_moduleIDsOutspySET != moduleIDsOutspySET.end(); ++iter_moduleIDsOutspySET ){
                            
                            if ( *itmissLUT == *iter_moduleIDsOutspySET){
                                std::cout << "0x8" << std::setfill('0') << std::setw(7) << std::hex << *itmissLUT << std::dec << ", which corresponds to outlane: " << boardandlaneVEC[1]  << std::endl ;
                                
                            }
                        }
                    }
                    
                }
            }
            
            // printing all the missing modules that were not found in LUT because they correspond to different towers than the tower corresponding to a given board
            std::cout << "\n Missing modules BUT not found in the LUT, coming from unexpected towers: " << std::endl;
            std::set <int> missing_but_unexpected_modules;
            std::set_difference(diff.begin(), diff.end(), missing_modules_inLUT.begin(), missing_modules_inLUT.end(), std::inserter(missing_but_unexpected_modules, missing_but_unexpected_modules.begin()));
            std::cout << "Found " << missing_but_unexpected_modules.size() << " of such modules." << std::endl;
            std::string modulelist = "/afs/cern.ch/work/s/ssevova/public/FTK_DF/TrigFTKSimTools/integration/df-spy-parse/DF-2-04_2PU_config_files/moduleList_Data2018_64T.fixed.txt";
            mapBoardAndLaneModIDsOutspy_tower_for_unexpected_modules(modulelist, missing_but_unexpected_modules );
            
            
            std::cout << "\nMISSING MODULES (found in inspy and not in outspy). Missing per L1ID and per outlane. " << std::endl;
            
            std::cout << "\nOrganised by the outlane: " << std::endl;
            displayMissingModules_perLane();
            
            std::cout << "\nOrganised by the L1ID: " << std::endl;
            displayMissingModules_pereventfrag();
            }
            
            std::cout << "\nUNEXPECTED MODULES (found in outspy but not in inspy): " << std::endl;
            displayUnexpectedModules();
            displayUnexpectedModules_modules();
        }
    }
}


// ======= Private functions:
/* ########################################
 * #        Main Parsing Functions        #
 * ######################################## */
void DFSpyHandler::parseNewInputBuffer(std::vector<daq::ftk::EventFragmentHitClusters*> eventFragments, int board, int inlane) {
    // Loop over event fragments:
    std::set<int> uniqueBufferModules;
    uniqueBufferModules.clear();
    std::cout << "\nL1IDs found for this inlane: " <<std::endl;
    for(size_t e = 0; e < eventFragments.size(); e++) {
        daq::ftk::EventFragmentHitClusters* eventFragment = eventFragments[e];
        std::set<ThreeTuple> packetsToRequest;
        bool packetInOutspy = false;
        unsigned int L1ID = eventFragment->getL1ID();
        
        std::cout << std::hex  << L1ID << std::dec << std::endl;
        
        unsigned int nModules = eventFragment->getNModules();
        // Loop over all modules in each eventFragment
        std::set<int> expectedModules = boardAndLaneToModIDsInspy[dfspy::make_tuple(board, inlane)];
        
        for(unsigned int m = 0; m < nModules; m++) {
            daq::ftk::ModuleHitClusters* mhc = eventFragment->getModule(m);
            int modNum = mhc->getModuleNumber();
            
            if(mhc->getModuleType() == 1) modNum |= (0x1 << 15);
            uniqueBufferModules.insert(modNum);
            uniqueModulesInspy.insert(modNum);
            totalModulesInspy++;
            
            // == Inspy vs. Config == Check if this module is in the set of expected modules:
            if(expectedModules.find(modNum) != expectedModules.end()) {
                expectedModulesInspyCount++;
                // == Inspy vs. Outspy ==
                // For this modID, we have to get all of its outlanes (in form of (board, outlane) pairs:
                std::set<std::vector<int> > boardOutlanes = modID2BoardandLaneOutspy[modNum];
                // Construct a set of 5-tuples based on this:
                std::set<FiveTuple*> fivetuples;
                for(auto BLpair : boardOutlanes) {
                    int outlane = BLpair[1];
                    FiveTuple* fivetuple = new FiveTuple(modNum, L1ID, board, outlane, mhc);
                    fivetuples.insert(fivetuple);
                }
                // Loop over the fivetuples pointers in the set
                for(auto ft : fivetuples) {
                    ThreeTuple packet = {ft->boardNumber, ft->outputLane, ft->L1ID};
                    // Now, check if this packet has been "seen" by the output buffer parser.
                    packetInOutspy = (pointerMapOutspy.count(packet) > 0);
                    if(packetInOutspy) {
                        performModAndHitClusterCheckInspy(packet, modNum, mhc);
                    } else {
                        registerPacketAsRequest(packet, packetsToRequest, ft);
                    }
                }
            } else {
                std::cout << "\n**** EventFragment in **Board " << board << " **Inlane " << inlane << " **L1ID " << std::hex  << L1ID << std::dec << std::endl;
                std::cout << "(X) INSPY: L1ID: " << std::hex  << L1ID << " -- Found module " << std::dec << modNum << " in INSPY board " << board
                << " and inlane " << inlane << ". Module was not expected (Inspy vs. Config failed)." << std::endl;
            }
        } // End module loop
    } // End event fragment loop
    
    std::cout << "\nnumber of unique inspy modules per lane: " << uniqueBufferModules.size() << std::endl;
} //end of parsing for a given spybuffer



void DFSpyHandler::parseNewOutputBuffer(std::vector<daq::ftk::EventFragmentHitClusters*> eventFragments, int board, int outlane) {
    
    std::set<int> uniqueBufferModules;
    std::set <int> modNum_notInspy_set_lane;
    std::set <int> uniqueMod_lane;
    unsigned int unexpected_nModules = 0;
    
    std::cout << "\nL1IDs found for this outlane: " << std::endl;
    for(auto eventFragment : eventFragments) {
        unsigned int L1ID = eventFragment->getL1ID();
        std::cout << std::hex << L1ID << std::dec << std::endl;
    }
    
    
    for(auto eventFragment : eventFragments) {
        
        std::set<FiveTuple> checkedFiveTuples;
        checkedFiveTuples.clear();
        std::set <int> modNum_notInspy_set;
        modNum_notInspy_set.clear();
        
        unsigned int L1ID = eventFragment->getL1ID();
        std::cout << "\n**** Analyzing EventFragment in **Board " << board << " **Outlane " << outlane << " **L1ID " << std::hex << L1ID << std::dec << std::endl;
        ThreeTuple packet = {board, outlane, L1ID}; // Packet identifier for this loop
        bool packetInInspy = false;
        if(setMapInspy.count(packet) > 0) {
            packetInInspy = true; // Determine if module check necessary
        }
        unsigned int nModules = eventFragment->getNModules();
        // Check 1: Check if this module is in the set of expected modules:
        std::set<int> expectedModules = boardAndLaneToModIDsOutspy[dfspy::make_tuple(board, outlane)];
        
        for(unsigned int m = 0; m < nModules; m++) {
            daq::ftk::ModuleHitClusters* mhc = eventFragment->getModule(m);
            
            int modNum = mhc->getModuleNumber();
            if(mhc->getModuleType() == 1) {
                modNum |= (0x1 << 15);
            }
            uniqueModulesOutspy.insert(modNum); // uniqueness, if inserted here: we get all unique modules, also the ones that were not found in inspy and also the ones not found in the config
            totalModulesOutspy++;
            
            if(expectedModules.find(modNum) != expectedModules.end()) {
                expectedModulesOutspyCount++;
                
                // === Outspy vs. Inspy === (if possible)
                
                if(packetInInspy) {
                    if(setMapInspy[packet].size() != 0) {
                        // Do module check - i.e. check if this module is actually showing up where it should:
                        for(FiveTuple ft : setMapInspy[packet]) {
                            
                            if(ft.moduleID == modNum) {
                                
                                auto iter1 = modNum_notInspy_set.find(modNum);
                                
                                if(iter1 != modNum_notInspy_set.end()) {
                                    modNum_notInspy_set.erase(iter1);
                                }
                                
                                std::cout << "Module " << std::hex << modNum << std::dec << " was found in outspy function and was in the inspy as well." <<std::endl;
                                
                                auto iter = setMapInspy[packet].find(ft);
                                setMapInspy[packet].erase(iter);
                                
                                // Hit Clusters check:
                                if(compareModuleHitClusters(ft.mHC, mhc)) {
                                } else {
                                    std::cout << "(HC-) Hit clusters check failed for comparison of module " << modNum << " in inspy and outspy!" << std::endl;
                                }
                                
                                checkedFiveTuples.insert(ft); // FiveTuples of setMapInspy[packet] that have been checked
                                uniqueModulesOutspy_inLUT.insert(modNum); //moved to this loop by  Marta: now the unique modules are only the unique modules that were also found in inspy --> in a given five tuple
                                uniqueMod_lane.insert(modNum); //unique modules per lane but only the ones that are found in inspy already
                                
                                break;
                                
                            } else {
                                modNum_notInspy_set.insert(modNum);
                            }
                        }
                    } else {
                        modNum_notInspy_set.insert(modNum);
                    }
                }
            } else {
                std::cout << "(X) OUTSPY: Found module " << std::hex <<  modNum <<std::dec << "     in OUTSPY board " << board <<
                " and outlane " << outlane << ". Module was not expected (Outspy vs. Config failed)." << std::endl;
            }
            
        }    // End module loop for this event fragment
        
        
        //If the packet is not yet registered in the inspy, we must add to SEEN data structures --> this is for the recognition of the unexpected modules in the outspy since all the inspy spybuffers were parsed first
        
        if(!packetInInspy) {
            std::string ots;
            auto result = pointerMapOutspy.insert(std::pair<ThreeTuple, daq::ftk::EventFragmentHitClusters* >(packet, eventFragment));
            
            result.second ? ots = "YES: " : ots = "NO:  ";
            std::cout << "ALL MODULES FROM PACKET: " << makePacketString(packet, true) << std::hex << packet.L1ID << ")" << std::dec  << " not expected " << std::endl;
            unexpected_nModules = eventFragment->getNModules();
            std::cout << "Number of unexpected modules for this outlane and L1ID (in outspy but not in inspy): " << unexpected_nModules << std::endl;
            std::cout << "These are: " << std::endl;
            for (unsigned int m1 = 0; m1 < unexpected_nModules; m1++) {
                daq::ftk::ModuleHitClusters* mhc1 = eventFragment->getModule(m1);
                int modNum1 = mhc1->getModuleNumber();
                if(mhc1->getModuleType() == 1) {
                    modNum1 |= (0x1 << 15);
                }
                std::cout << std::hex << modNum1 << std::dec << std::endl;
            }
            
            TwoTuple boardLane = {board, outlane};
            safePushOutspy(boardLane, packet);
        }
        
        //If the packet was registered as requested in the inspy, now that the checks are done, we can remove the inspy data structures.
        TwoTuple boardLane = {board, outlane};
        
        if(packetInInspy) {
            // Clean the queue for this board and lane
            
            while(setMapInspy[packet].find(queueMapInspy[boardLane].front()) != setMapInspy[packet].end()) {
                FiveTuple popped = queueMapInspy[boardLane].front();
                int size = queueMapInspy[boardLane].size();
                queueMapInspy[boardLane].pop();
                if(queueMapInspy[boardLane].size() == 0) break;
            }
        }
        
        
        if (modNum_notInspy_set.size() != 0) {
            
            std::cout << "Some modules from this packet were found in inspy but: " << modNum_notInspy_set.size() <<  " modules from this packet were found ONLY in outspy and not in inspy: " << std::endl;
            auto result1 = pointerMapOutspy_modules.insert(std::pair<ThreeTuple, std::set <int> >(packet, modNum_notInspy_set));
            std::cout << "These modules are: " <<std::endl;
            for (std::set<int>::iterator iter_mod_not_insp = modNum_notInspy_set.begin(); iter_mod_not_insp != modNum_notInspy_set.end(); ++iter_mod_not_insp ){
                std::cout << std::hex << *iter_mod_not_insp << std::dec <<std::endl;
            }
        }
        
        modNum_notInspy_set_lane.insert(modNum_notInspy_set.begin(), modNum_notInspy_set.end());
        
    } // End EventFragment loop
    
    std::cout << "\nnumber of unique modules per outlane: " << uniqueMod_lane.size() <<std::endl;
}



/* ########################################
 * #      Look Up Table Construction      #
 * ######################################## */

void DFSpyHandler::buildLUTs(std::string multiboard, std::string modulelist, std::string system_config) {
    std::cout << "\n\n\n\n START: \n\n\n\n";
    std::cout << "======== CONFIG FILES ========" << std::endl;
    std::cout << "MODULE LIST:  " << modulelist << std::endl;
    std::cout << "MULITBOARD:   " << multiboard << std::endl;
    std::cout << "SYSTEM:       " << system_config << std::endl;
    std::cout << "==============================\n" << std::endl;
    std::cout << "======== JUMP TO CONTENTS ========" << std::endl;
    std::cout << "[P1] BOARD -> ENABLED?" << std::endl;
    std::cout << "[P2] ROBID -> (Board, Lane)" << std::endl;
    std::cout << "[P3] (Board, Lane) -> {inspy modules}" << std::endl;
    std::cout << "[P4] (Tower, Plane) -> (Board, Lane)" << std::endl;
    std::cout << "[P5] (Board, Lane) -> {outspy modules}" << std::endl;
    std::cout << "[P6] Outspy module # -> {(Board, Lane)}" << std::endl;
    std::cout << "==============================" << std::endl;
    
    std::cout << "\n\n\n\n NEXT: [P1] BOARD -> ENABLED? \n\n\n\n";
    boardEnabled = mapInspyBoardEnable(multiboard);
    std::cout << "\n\n\n\n NEXT: [P2] ROBID -> (Board, Lane) \n\n\n\n";
    ROBIDtoBoardAndLane = mapROBIDtoBoardAndLane(multiboard);
    std::cout << "\n\n\n\n NEXT: [P3] (Board, Lane) -> {inspy modules} \n\n\n\n";
    boardAndLaneToModIDsInspy = mapBoardAndLaneToModIDsInspy(modulelist);
    std::cout << "\n\n\n\n NEXT: [P4] (Tower, Plane) -> (Board, Lane) \n\n\n\n";
    towerPlaneToBoardLane = mapTowerPlaneToBoardLane(system_config);
    towerPlaneToBoardLane1 = mapTowerPlaneToBoardLane1(system_config);
    std::cout << "\n\n\n\n NEXT: [P5] (Board, Lane) -> {outspy modules} \n\n\n\n";
    boardAndLaneToModIDsOutspy = mapBoardAndLaneModIDsOutspy(modulelist);
    std::cout << "\n\n\n\n NEXT: [P6] Outspy module # -> {(Board, Lane)} \n\n\n\n";
    modID2BoardandLaneOutspy = mapModID2BoardAndLaneOutspy();
    std::cout << "LUT Table Construction Complete" << std::endl;
}

/*
 * Function: mapInspyBoardEnable
 * -----
 * Returns Board -> Enabled? (bool)
 */
std::map<int, bool> DFSpyHandler::mapInspyBoardEnable(std::string multiboardFile) {
    std::map<int, bool> boardEnabled;
    std::string outString;
    std::string line;
    std::ifstream myfile(multiboardFile);
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            std::vector<std::string> tokens = dfspy::split(line);
            if(tokens[0] == "boardEnable") {
                int boardNum = std::stoi(tokens[1], nullptr);
                if(tokens[2] == "1") {
                    auto result = boardEnabled.insert(std::pair<int, bool> (boardNum, true));
                    result.second ? outString = "SUCCESS:    " : outString = "FAIL:     ";
                    std::cout << outString << "Board# -> Enabled?: Inserted key [ " << boardNum << " ] with value TRUE" << std::endl;
                } else {
                    auto result = boardEnabled.insert(std::pair<int, bool> (boardNum, false));
                    result.second ? outString = "SUCCESS:    " : outString = "FAIL:     ";
                    std::cout << outString << "Board# -> Enabled?: Inserted key [ " << boardNum << " ] with value FALSE" << std::endl;
                }
            }
        }
        myfile.close();
    } else {
        std::cerr << "Unable to open Global CF (multiboard) file" << std::endl;
    }
    return boardEnabled;
}

/*
 * Function: mapROBIDtoBoardAndLane
 * -----
 * Returns ROBID -> (Board, Inlane)
 */
std::map<int, std::vector<int> > DFSpyHandler::mapROBIDtoBoardAndLane(std::string multiboardfile) {
    std::map<int, std::vector<int> > m;
    std::string outString;
    std::string line;
    std::ifstream myfile(multiboardfile);
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            std::vector<std::string> tokens = dfspy::split(line);
            if(tokens[0] == "rodToBoard") {
                int robID = dfspy::hexStringToInt(tokens[1]);
                int boardNum = std::stoi(tokens[2], nullptr);
                int IMLane = std::stoi(tokens[3], nullptr);
                auto result = m.insert(std::pair<int, std::vector<int> > (robID, dfspy::make_tuple(boardNum, IMLane)));
                result.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "ROBID -> (Board, Lane): Inserted key [ " << robID << " ] with tuple " << "( "
                << boardNum << " , " << IMLane << " )" << std::endl;
            }
        }
        myfile.close();
    } else {
        std::cerr << "Unable to open Global CF (multiboard) file" << std::endl;
    }
    return m;
}

/*
 * Function: mapTowerPlaneToBoardLane
 * -----
 * Returns (Tower, Plane) -> (Board, Outlane)
 */
std::map<std::vector<int>, std::vector<int> > DFSpyHandler::mapTowerPlaneToBoardLane(std::string sysFile) {
    // Declare maps:
    std::map< std::vector<int>, std::vector<int> > towerPlaneToBoardLane;
    std::map<int, int> towerToBoard;
    std::map<int, bool> towerToTop;
    std::map<int, int> planeToOutbit;
    std::string outString;
    std::string outString1;
    // Parse file:
    std::string line;
    std::ifstream myfile(sysFile);
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            std::vector<std::string> tokens = dfspy::split(line);
            if(tokens.size() < 3) continue;
            // Sort by Key (tokens[0]):
            if(tokens[0] == "BoardNToTopTower") {
                int boardNum = std::stoi(tokens[1], nullptr);
                int towerNum = std::stoi(tokens[2], nullptr);
                // Tower to Top/Bottom (bool)
                auto result = towerToTop.insert(std::pair<int, bool> (towerNum, true));
                result.second ? outString =  "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "Tower -> Board#: Adding key Tower " << towerNum << " with value TRUE" << std::endl;
                // Tower to Board#
                auto result2 = towerToBoard.insert(std::pair<int, int> (towerNum, boardNum));
                result2.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "Tower -> Board#: Adding key Tower " << towerNum << " with value Board " << boardNum << std::endl;
                
            } else if(tokens[0] == "BoardNToBotTower") {
                int boardNum = std::stoi(tokens[1], nullptr);
                int towerNum = std::stoi(tokens[2], nullptr);
                // Tower to Top/Bottom (bool)
                auto result = towerToTop.insert(std::pair<int, bool> (towerNum, false));
                result.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "Tower -> Board#: Adding key Tower " << towerNum << " with value FALSE" << std::endl;
                // Tower to Board#
                auto result2 = towerToBoard.insert(std::pair<int, int> (towerNum, boardNum));
                result2.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "Tower -> Board#: Adding key Tower " << towerNum << " with value Board " << boardNum << std::endl;
                
            } else if(tokens[0] == "PlaneToOutBit") {
                int planeNum = std::stoi(tokens[1], nullptr);
                int outBit = std::stoi(tokens[2], nullptr);
                auto result = planeToOutbit.insert(std::pair<int, int> (planeNum, outBit));
                result.second ? outString = "SUCCESS:   " : outString = "FAIL:        ";
                std::cout << outString << "Plane -> Outbit: Adding key plane " << planeNum << " with value outbit " << outBit << std::endl;
            }
        }
        // Now construct map
        std::cout << "\n TOWER PLANE TO BOARD LANE \n" << std::endl;
        int lane = 0;
        for(int tower = 0; tower <= 63; tower++) {
            int board = towerToBoard[tower];
            if(boardEnabled.find(board) == boardEnabled.end() || !boardEnabled[board]) {
                continue;
            }
            
            for(int plane = 0; plane <= 11; plane++) {
                bool topTowerBool = towerToTop[tower];
                int outbit = planeToOutbit[plane];
                if(topTowerBool) {
                    if(outbit == 8) {        // SSB
                        lane = 34;
                    } else {                 // AUX
                        lane = outbit + 8;
                    }
                } else if(!topTowerBool) {
                    if(outbit == 8) {        // SSB
                        lane = 35;
                    } else {                 // AUX
                        lane = outbit + 26;
                    }
                }
                
                std::vector<int> towerPlane = dfspy::make_tuple(tower, plane);
                std::vector<int> boardLane0  = dfspy::make_tuple(board, lane);
                auto result = towerPlaneToBoardLane.insert(std::pair<std::vector<int> , std::vector<int> > (towerPlane, boardLane0));
                result.second ? outString = "SUCCESS:   " : outString = "FAIL:     ";
                std::cout << outString << "(Tower, Plane) -> (Board, Lane): Added (Tower, Plane) key " <<
                get2TupleString(towerPlane) << " with (Board, Lane) value " << get2TupleString(boardLane0) << std::endl;
                
            }
        }
        
    } else {
        std::cerr << "Unable to open system config file" << std::endl;
    }
    //return towerPlaneToBoardLane;
    return towerPlaneToBoardLane;
}

/*
 * Function: mapTowerPlaneToBoardLane
 * -----
 * Returns (Tower, Plane) -> (Board, Outlane)
 */
std::map<std::vector<int>, std::vector<int> > DFSpyHandler::mapTowerPlaneToBoardLane1(std::string sysFile) {
    // Declare maps:
    std::map< std::vector<int>, std::vector<int> > towerPlaneToBoardLane1;
    std::map<int, int> towerToBoard;
    std::map<int, bool> towerToTop;
    std::map<int, int> planeToOutbit;
    std::string outString;
    std::string outString1;
    // Parse file:
    std::string line;
    std::ifstream myfile(sysFile);
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            std::vector<std::string> tokens = dfspy::split(line);
            if(tokens.size() < 3) continue;
            // Sort by Key (tokens[0]):
            if(tokens[0] == "BoardNToTopTower") {
                int boardNum = std::stoi(tokens[1], nullptr);
                int towerNum = std::stoi(tokens[2], nullptr);
                // Tower to Top/Bottom (bool)
                auto result = towerToTop.insert(std::pair<int, bool> (towerNum, true));
                result.second ? outString =  "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "Tower -> Board#: Adding key Tower " << towerNum << " with value TRUE" << std::endl;
                // Tower to Board#
                auto result2 = towerToBoard.insert(std::pair<int, int> (towerNum, boardNum));
                result2.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "Tower -> Board#: Adding key Tower " << towerNum << " with value Board " << boardNum << std::endl;
                
            } else if(tokens[0] == "BoardNToBotTower") {
                int boardNum = std::stoi(tokens[1], nullptr);
                int towerNum = std::stoi(tokens[2], nullptr);
                // Tower to Top/Bottom (bool)
                auto result = towerToTop.insert(std::pair<int, bool> (towerNum, false));
                result.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "Tower -> Board#: Adding key Tower " << towerNum << " with value FALSE" << std::endl;
                // Tower to Board#
                auto result2 = towerToBoard.insert(std::pair<int, int> (towerNum, boardNum));
                result2.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
                std::cout << outString << "Tower -> Board#: Adding key Tower " << towerNum << " with value Board " << boardNum << std::endl;
                
            } else if(tokens[0] == "PlaneToOutBit") {
                int planeNum = std::stoi(tokens[1], nullptr);
                int outBit = std::stoi(tokens[2], nullptr);
                auto result = planeToOutbit.insert(std::pair<int, int> (planeNum, outBit));
                result.second ? outString = "SUCCESS:   " : outString = "FAIL:        ";
                std::cout << outString << "Plane -> Outbit: Adding key plane " << planeNum << " with value outbit " << outBit << std::endl;
            }
        }
        // Now construct map
        std::cout << "\n TOWER PLANE TO BOARD LANE \n" << std::endl;
        int lane1 = 0;
        for(int tower = 0; tower <= 63; tower++) {
            int board = towerToBoard[tower];
            if(boardEnabled.find(board) == boardEnabled.end() || !boardEnabled[board]) {
                continue;
            }
            
            for(int plane = 0; plane <= 11; plane++) {
                bool topTowerBool = towerToTop[tower];
                int outbit = planeToOutbit[plane];
                if(topTowerBool) {
                    if(outbit == 8) {        // SSB
                        lane1 = 16;
                    } else {                 // AUX
                        lane1 = outbit;
                    }
                } else if(!topTowerBool) {
                    if(outbit == 8) {        // SSB
                        lane1 = 17;
                    } else {                 // AUX
                        lane1 = outbit + 18;
                    }
                }
                
                std::vector<int> towerPlane = dfspy::make_tuple(tower, plane);
                std::vector<int> boardLane1  = dfspy::make_tuple(board, lane1);
                
                auto result1 = towerPlaneToBoardLane1.insert(std::pair<std::vector<int> , std::vector<int> > (towerPlane, boardLane1));
                result1.second ? outString1 = "SUCCESS:   " : outString1 = "FAIL:     ";
                std::cout << outString1 << "(Tower, Plane) -> (Board, Lane): Added (Tower, Plane) key " <<
                get2TupleString(towerPlane) << " with (Board, Lane) value " << get2TupleString(boardLane1) << std::endl;
                
            }
        }
        
    } else {
        std::cerr << "Unable to open system config file" << std::endl;
    }
    return towerPlaneToBoardLane1;
}


/*
 * Function:
 * ----
 * Returns (Board, Inlane) -> {mod IDs}
 */
std::map<std::vector<int>, std::set<int> > DFSpyHandler::mapBoardAndLaneToModIDsInspy(std::string moduleListFile) {
    std::map<std::vector<int>, std::set<int> > boardAndLaneToModIDs;
    std::string outString;
    std::string line;
    std::ifstream myfile(moduleListFile);
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            outString = "";
            std::vector<std::string> tokens = dfspy::split(line);
            int robID = dfspy::hexStringToInt(tokens[0]);
            int modID = std::stoi(tokens[1], nullptr);
            if(robID >= 0x200000) modID |= (0x1 << 15); // Add extra isSCT for sct modules
            if(ROBIDtoBoardAndLane.find(robID) == ROBIDtoBoardAndLane.end()) {
                //Modulelist contains all robIDs, so of course many will not be included in the multiboard file.
                continue;
                
            }
            std::vector<int> boardAndLane = ROBIDtoBoardAndLane[robID];
            auto result = boardAndLaneToModIDs[boardAndLane].insert(modID);
            std::string boardLaneString = get2TupleString(boardAndLane);
            result.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
            std::cout << outString << "(Board, Lane) -> {mod IDs}: Found ROBID " << robID << " which maps to (Board, Lane) "
            << boardLaneString << " which maps to mod ID " << std::hex << modID << std::dec << std::endl;
        }
        
    } else {
        std::cerr << "Unable to open module list file" << std::endl;
    }
    
    // Condensed format:
    std::cout << " \n CONDENSED FORMAT (BoardAndLane -> ModIDs Inspy) \n " << std::endl;
    for(std::map<std::vector<int>, std::set<int> >::iterator itkey = boardAndLaneToModIDs.begin(); itkey != boardAndLaneToModIDs.end(); ++itkey) {
        std::vector<int> key = itkey->first;
        std::cout << "For board " << key[0] << " and inlane " << key[1] << " we have " << boardAndLaneToModIDs[key].size() << " mods: " << std::endl;
        for(int mod : boardAndLaneToModIDs[key]) {
            std::cout << std::hex << mod << std::dec << ", ";
        }
        std::cout << "and STOP." << std::endl;
        
    }
    return boardAndLaneToModIDs;
}


/*
 * Function: mapBoardAndLaneModIDsOutspy
 * ---
 * Returns (Board, Outlane) -> {mod IDs}
 */
std::map<std::vector<int>, std::set<int> > DFSpyHandler::mapBoardAndLaneModIDsOutspy(std::string moduleListFile) {
    
    std::map<std::vector<int>, std::set<int> > boardAndLaneToModIDsOutspy;
    std::string outString;
    std::string outString1;
    std::string line;
    std::ifstream myfile(moduleListFile);
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            outString = "";
            std::vector<std::string> tokens = dfspy::split(line);
            int robID = dfspy::hexStringToInt(tokens[0]);
            int modID = std::stoi(tokens[1], nullptr);
            if(robID >= 0x200000) modID |= (0x1 << 15); // Add extra isSCT bit for sct modules
            unsigned long long tower_0_31_bits = dfspy::hexStringToULL(tokens[2]);
            unsigned long long tower_32_63_bits = dfspy::hexStringToULL(tokens[3]);
            
            
            int plane = std::stoi(tokens[4], nullptr);
            // Utilize tower bits
            uint64_t tow_all_bits = (tower_0_31_bits) | (tower_32_63_bits << 32);
            
            
            for(int tow = 0; tow < NUMBER_OF_FTK_TOWERS; tow++) {
                if((1ull << tow) & tow_all_bits) {
                    std::vector<int> towerPlane = dfspy::make_tuple(tow, plane);
                    if(towerPlaneToBoardLane.find(towerPlane) != towerPlaneToBoardLane.end()) { //doesn't work for certain towers and hence some modules are not registered in LUT
                        std::vector<int> boardLane = towerPlaneToBoardLane[towerPlane];
                        auto result = boardAndLaneToModIDsOutspy[boardLane].insert(modID);
                        
                        result.second ? outString = "SUCCESS:   " : "FAIL:     ";
                        std::cout << outString << "Added (Board, Lane) pair " << get2TupleString(boardLane) << " with value mod ID:  " << std::hex << modID << std::dec << ". Tower: " << tow << std::endl;
                        
                    }
                    
                    if(towerPlaneToBoardLane1.find(towerPlane) != towerPlaneToBoardLane1.end()) { //doesn't work for certain towers and hence some modules are not registered in LUT
                        std::vector<int> boardLane1 = towerPlaneToBoardLane1[towerPlane];
                        
                        auto result1 = boardAndLaneToModIDsOutspy[boardLane1].insert(modID);
                        
                        result1.second ? outString1 = "SUCCESS:   " : "FAIL:     ";
                        std::cout << outString1 << "Added (Board, Lane) pair " << get2TupleString(boardLane1) << " with value mod ID:  " << std::hex << modID << std::dec << ". Tower: " << tow << std::endl;
                    }
                    
                    
                }
            }
        }
        
    } else {
        std::cerr << "Unable to open module list file" << std::endl;
    }
    // Print keys
    for(std::map<std::vector<int>, std::set<int> >::iterator itkey = boardAndLaneToModIDsOutspy.begin(); itkey != boardAndLaneToModIDsOutspy.end(); ++itkey) {
        std::vector<int> key = itkey->first;
        std::cout << get2TupleString(key) << std::endl;
        
    }
    return boardAndLaneToModIDsOutspy;
}

/*
 * Function: mapModID2BoardAndLaneOutspy()
 * ----
 * Returns map: modID -> {(Board, Outlane)}
 */
std::map<int, std::set< std::vector<int> > > DFSpyHandler::mapModID2BoardAndLaneOutspy() {
    std::map<int, std::set<std::vector<int> > > modID2BoardAndLaneOutspy;
    std::string outString = "";
    for(std::map<std::vector<int>, std::set<int> >::iterator it = boardAndLaneToModIDsOutspy.begin(); it != boardAndLaneToModIDsOutspy.end(); ++it) {
        std::vector<int> boardLane = it->first;
        std::set<int> modSet = it->second;
        for(std::set<int>::iterator itset = modSet.begin(); itset != modSet.end(); ++itset) {
            auto result = modID2BoardAndLaneOutspy[*itset].insert(boardLane);
            result.second ? outString = "SUCCESS:   " : outString = "FAIL:      ";
            std::cout << outString << "For module " << std::hex << *itset << std::dec << " (Board, Lane) pair " << get2TupleString(boardLane)
            << " was added to set. Set is now size: " << modID2BoardAndLaneOutspy[*itset].size() << std::endl;
        }
    }
    return modID2BoardAndLaneOutspy;
}

/* ########################################
 * #        Parsing Helper Functions      #
 * ######################################## */

void DFSpyHandler::printBonusBufferStats(int board, int lane, std::set<int>& uniqueBufferModules) {
    std::cout << "\nBUFFER MODULES" << std::endl;
    std::cout << uniqueBufferModules.size() << " unique modules:   " << std::endl;
    for(int mod : uniqueBufferModules) {
        std::cout << "0x8" << std::hex << std::setfill('0') << std::setw(7) << mod << std::dec << std::endl;
    }
    std::cout << std::endl;
}

void DFSpyHandler::safePushInspy(TwoTuple &boardLane, FiveTuple &ft)
{
    if(queueMapInspy[boardLane].size() >= INSPY_QUEUE_MAX_LENGTH) {
        // Then, pop and check.
        bool warning = false;
        while(warning == false) {
            FiveTuple popped = queueMapInspy[boardLane].front();
            ThreeTuple packetFromFT = {popped.boardNumber, popped.outputLane, popped.L1ID};
            if(setMapInspy.count(packetFromFT) > 0) {
                // When something is inserted into the setMapInspy, it is also inserted into the queueMapInspy.
                // When Inspy vs. Outspy occurs, the packet key in setMapInspy is definitely erased, but the corresponding FiveTuples may remain in the queueMapInspy.
                // Thus, if the packet to be popped exists in setMapInspy and pointerMapOutspy, we should perform an Inspy vs. Outspy check and shorten the queue as much as possible.
                
                // Check if the packet to be popped exists in the pointerMapOutspy
                if(pointerMapOutspy.count(packetFromFT) > 0) {
                    performModAndHitClusterCheckInspy(packetFromFT, popped.moduleID, popped.mHC);
                } else {
                    warning = true;
                }
            }
            queueMapInspy[boardLane].pop();
        }
    }
    queueMapInspy[boardLane].push(ft);
}

void DFSpyHandler::safePushOutspy(TwoTuple &boardLane, ThreeTuple &packet) {
    if(queueMapOutspy[boardLane].size() >= OUTSPY_QUEUE_MAX_LENGTH) {
        ThreeTuple popped = queueMapOutspy[boardLane].front();
        // Delete popped from pointerMapOutspy
        if(pointerMapOutspy.count(popped) > 0) pointerMapOutspy.erase(popped);
        // Now pop from the queue
        queueMapOutspy[boardLane].pop();
    }
    queueMapOutspy[boardLane].push(packet);
}

void DFSpyHandler::performModAndHitClusterCheckInspy(ThreeTuple &packet, int modNum, daq::ftk::ModuleHitClusters* &mhc) {
    // Module check
    unsigned int nPacketModules = pointerMapOutspy[packet]->getNModules();
    bool moduleFound = false;
    for(unsigned int pm = 0; pm < nPacketModules; pm++) {
        daq::ftk::ModuleHitClusters* pmhc = pointerMapOutspy[packet]->getModule(pm);
        int checkMod = pmhc->getModuleNumber();
        if(pmhc->getModuleType() == 1) checkMod |= (0x1 << 15);
        if(checkMod == modNum) {
            moduleFound = true;
            std::cout << "(P +) INSPY VS. OUTSPY: Module " << modNum << " from packet " << makePacketString(packet, true) << std::hex << packet.L1ID << ")" << std::dec  << " registered as requested in inspy was successfully found in pointerMapOutspy." << std::endl;
            // Hit Clusters check:
            if(compareModuleHitClusters(mhc, pmhc)) {
                std::cout << "(P HC+) Hit clusters match for module ID " << modNum << "!" << std::endl;
            } else {
                std::cout << "(P HC-) hit clusters do not match between the ModuleHitCluster objects with the same module ID: " << modNum << std::endl;
            }
            break;
        }
    }
    if(moduleFound == false) {
        std::cout << "(P -) INSPY VS. OUTSPY: For L1ID " << packet.L1ID << ", output lane " << packet.laneNumber
        << " should have had module " << modNum
        << ", but didn't!" << std::endl;
    }
}

void DFSpyHandler::registerPacketAsRequest(ThreeTuple &packet, std::set<ThreeTuple> &packetsToRequest, FiveTuple* &ft) {
    // If the packet is not in the pointerMapOutspy, then it and its set of FiveTuples must be registed as a request.
    if(packetsToRequest.find(packet) == packetsToRequest.end()) {
        packetsToRequest.insert(packet);
    }
    // Now, add fivetuple to the queue and register its existence in the set map.
    TwoTuple boardLane = {ft->boardNumber, ft->outputLane};
    int size = queueMapInspy[boardLane].size();
    safePushInspy(boardLane, *ft);
    auto res = setMapInspy[packet].insert(*ft);
}

// QUEUE MAP OUTSPY CLEANER FOR FIRST DUMP: This is more of a cleanup for the queueMapOutspy. It continually looks for the packet or packets of smaller L1ID to clear from the queueMapOutspy.
// The reason to clear packets of smaller L1ID initially is because in that board&outlane in the queueMapOutspy, no L1IDs will be coming in that are
// smaller than that one.
void DFSpyHandler::initialOutspyCleaner(TwoTuple &boardLane, ThreeTuple &packet)
{
    // std::cout << "Starting one-time outspy cleaner function" << std::endl;
    while(queueMapOutspy[boardLane].size() != 0 && (queueMapOutspy[boardLane].front() == packet || packet.L1ID > queueMapOutspy[boardLane].front().L1ID)) {
        ThreeTuple popped = queueMapOutspy[boardLane].front();
        int size = queueMapOutspy[boardLane].size();
        queueMapOutspy[boardLane].pop();
        pointerMapOutspy.erase(popped);
    }
}

/* ########################################
 * #       Check-Specific Helpers         #
 * ######################################## */
bool DFSpyHandler::compareModuleHitClusters(daq::ftk::ModuleHitClusters* & mhc1, daq::ftk::ModuleHitClusters* & mhc2) {
    if(mhc1->getModuleType() != mhc2->getModuleType()) return false;
    if(mhc1->getNClusters() != mhc2->getNClusters()) {
        std::cout << "Unequal amount of clusters. Hit cluster check failed. ";
        return false;
    }
    for(unsigned int i = 0; i < mhc1->getNClusters(); i++) {
        if(mhc1->getModuleType()) {
            if(!(compareSCTClusters(mhc1->getSctCluster(i), mhc2->getSctCluster(i)))) return false;
        } else {
            if(!(comparePixelClusters(mhc1->getPixelCluster(i), mhc2->getPixelCluster(i)))) return false;
        }
    }
    return true;
}

bool DFSpyHandler::compareSCTClusters(daq::ftk::SCTCluster* a, daq::ftk::SCTCluster* b) {
    return (a->hit_coord() == b->hit_coord() &&
            a->hit_coord() == b->hit_coord());
}

bool DFSpyHandler::comparePixelClusters(daq::ftk::PixelCluster* a, daq::ftk::PixelCluster* b) {
    return  (a->column_width()  == b->column_width()  &&
             a->column_coord()  == b->column_coord()  &&
             a->split_cluster() == b->split_cluster() &&
             a->row_width()     == b->row_width()     &&
             a->row_coord()     == b->row_coord());
}



/* ########################################
 * #               Viewing                #
 * ######################################## */
std::string DFSpyHandler::makePacketString(ThreeTuple& t, bool isOutlane) {
    std::string p;
    std::string l;
    isOutlane ? l = "outlane" : l = "inlane";
    p = "(board " + std::to_string(t.boardNumber) + ", " + l + " " + std::to_string(t.laneNumber) + ", L1ID ";
    return p;
}

/* ########################################
 Functions to display missing modules
 ########################################*/

void DFSpyHandler::displayMissingModules_pereventfrag(){ //display all missing modules and organise them by L1ID and then by outlane
    
    std::cout << "\nMISSING MODULES PER EVENT FRAGMENT " << std::endl;
    
    std::set<int> L1IDset;
    for(std::map<ThreeTuple, std::set<FiveTuple>> ::iterator it = setMapInspy.begin(); it != setMapInspy.end(); ++it){
        ThreeTuple pack = it-> first;
        L1IDset.insert(pack.L1ID);
    }
    
    for(std::set<int>::iterator it1 = L1IDset.begin(); it1 != L1IDset.end(); ++it1){
        std::cout << "\n***For the event fragment, L1ID: " << std::hex << *it1 << std::dec << " ***" << std::endl;
        missingmodule_count = 0; //renew missing module count per event fragment
        
        
        std::set<int> laneset;
        laneset.clear();
        std::set<int> boardset;
        boardset.clear();
        
        for(std::map<ThreeTuple, std::set<FiveTuple> >::iterator it10 = setMapInspy.begin(); it10 != setMapInspy.end(); ++it10) {
            ThreeTuple pack = it10->first;
            
            if (pack.L1ID == *it1)
                laneset.insert(pack.laneNumber);
            boardset.insert(pack.boardNumber);
        }
        
        for(std::set<int>::iterator it12 = boardset.begin(); it12 != boardset.end(); ++it12){
            std::cout << "\nCouldn't find outspy \nboard: " << *it12 << std::endl;
            for(std::set<int>::iterator it11 = laneset.begin(); it11 != laneset.end(); ++it11){
                std::cout << "outlane: " << *it11 << std::endl;
                
                for(std::map<ThreeTuple, std::set<FiveTuple> >::iterator it = setMapInspy.begin(); it != setMapInspy.end(); ++it) {
                    std::set<FiveTuple> my_set = it->second;
                    
                    for(std::set<FiveTuple>::iterator iter = my_set.begin(); iter != my_set.end(); ++iter) {
                        std::set<int> missing_modID_set; //set to store all module ID's
                        missing_modID_set.insert(iter->moduleID); //set of the modIDs taken from the final setMapInspy -> all modIDs that are missing
                        
                        for (std::map<std::vector<int>, std::set<int>>::iterator it2 = boardAndLaneToModIDsInspy.begin(); it2 != boardAndLaneToModIDsInspy.end(); ++it2){
                            
                            std::set<int> setModIDfromcongif = it2->second;
                            std::vector<int> vector_board_lane = it2->first;
                            
                            for (std::set<int>::iterator it3 = setModIDfromcongif.begin(); it3 != setModIDfromcongif.end(); ++it3) {
                                
                                for (std::set<int>::iterator it4 = missing_modID_set.begin(); it4 != missing_modID_set.end(); ++it4){
                                    if (*it4 == *it3) {
                                        if (iter->L1ID == *it1){
                                            if (iter->outputLane == *it11){
                                                std::cout  << std::dec << "where module: " <<"0x8" << std::setfill('0') << std::setw(7)  << std::hex << iter->moduleID << std::dec <<  " from inlane: " << vector_board_lane[1] << " is supposed to go. " <<std::endl;
                                                missingmodule_count++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}






void DFSpyHandler::displayMissingModules_perLane() { //missing modules per lane version
    
    std::cout << "\nMISSING MODULES PER OUTLANE " << std::endl;
    
    std::set<int> laneset;
    std::set<int> boardset;
    std::set<int> L1IDset;
    
    for(std::map<ThreeTuple, std::set<FiveTuple> >::iterator it10 = setMapInspy.begin(); it10 != setMapInspy.end(); ++it10) {
        ThreeTuple pack = it10->first;
        
        laneset.insert(pack.laneNumber);
        boardset.insert(pack.boardNumber);
        L1IDset.insert(pack.L1ID);
        
    }
    
    for(std::set<int>::iterator it12 = boardset.begin(); it12 != boardset.end(); ++it12){
        std::cout << "\nCouldn't find outspy \nboard: " << *it12 << std::endl;
        for(std::set<int>::iterator it11 = laneset.begin(); it11 != laneset.end(); ++it11){
            
            missingmodule_count = 0; //renew missing module count per lane
            std::cout << "\noutlane: " << *it11 << std::endl;
            
            std::set<int> missingmodule_lane;
            missingmodule_lane.clear();
            
            for (std::set<int>::iterator it_L1IDset = L1IDset.begin(); it_L1IDset != L1IDset.end(); ++ it_L1IDset ) {
                std::cout << "L1ID: " << std::hex << *it_L1IDset << std::dec << std::endl;
                
                for(std::map<ThreeTuple, std::set<FiveTuple> >::iterator it = setMapInspy.begin(); it != setMapInspy.end(); ++it) {
                    std::set<FiveTuple> my_set = it->second;
                    
                    
                    for(std::set<FiveTuple>::iterator iter = my_set.begin(); iter != my_set.end(); ++iter) {
                        
                        std::set<int> missing_modID_set; //set to store all module ID's
                        missing_modID_set.insert(iter->moduleID); //set of the modIDs taken from the final setMapInspy -> all modIDs that are missing
                        
                        for (std::map<std::vector<int>, std::set<int>>::iterator it2 = boardAndLaneToModIDsInspy.begin(); it2 != boardAndLaneToModIDsInspy.end(); ++it2){
                            
                            std::set<int> setModIDfromcongif = it2->second;
                            std::vector<int> vector_board_lane = it2->first;
                            
                            for (std::set<int>::iterator it3 = setModIDfromcongif.begin(); it3 != setModIDfromcongif.end(); ++it3) {
                                
                                for (std::set<int>::iterator it4 = missing_modID_set.begin(); it4 != missing_modID_set.end(); ++it4){
                                    
                                    
                                    if (*it4 == *it3) {//*it4 - element of a set of missing modIDs, *it3: element of the set of modID, don't compare element by element, find the value in the whole set
                                        if (iter->outputLane == *it11){
                                            if (iter->L1ID == *it_L1IDset){
                                                
                                                int unique_mod_ID = iter->moduleID;
                                                
                                                std::cout  << std::dec << "where module: " <<"0x8" << std::setfill('0') << std::setw(7)  << std::hex << unique_mod_ID << std::dec <<  " from inlane: " << vector_board_lane[1] << " is supposed to go. " << std::endl;
                                                
                                                missingmodule_count++;
                                            }
                                            
                                            missingmodule_lane.insert(iter->moduleID);
                                        }
                                    }
                                }
                                
                            }
                        }
                        
                    }
                    
                }
            }
            std::cout << "missing modules per lane: " << missingmodule_count << std::endl;
            std::cout << "missing UNIQUE modules per lane: " << missingmodule_lane.size() << std::endl;
        }
        
    }
}

/*########################################
 Functions to display unexpected modules
 ########################################*/

void DFSpyHandler::displayUnexpectedModules(){ //normal version --> missing modules organised per event fragment and per outlane but only considering the events that were fully unexpected --> all modules for a given event fragment was not expected
    std::set<int> L1IDset;
    for(std::map<ThreeTuple, daq::ftk::EventFragmentHitClusters* > ::iterator it = pointerMapOutspy.begin(); it != pointerMapOutspy.end(); ++it){
        ThreeTuple pack = it-> first;
        L1IDset.insert(pack.L1ID);
    }
    
    for(std::set<int>::iterator it1 = L1IDset.begin(); it1 != L1IDset.end(); ++it1){
        std::cout << "\n\n****** For the event fragment, L1ID: " << std::hex << *it1 << std::dec << " ******" << std::endl;
        
        std::set<int> boardset;
        for(std::map<ThreeTuple, daq::ftk::EventFragmentHitClusters* > ::iterator it = pointerMapOutspy.begin(); it != pointerMapOutspy.end(); ++it){
            ThreeTuple pack1 = it-> first;
            if (pack1.L1ID == *it1)
                boardset.insert(pack1.boardNumber);}
        
        for(std::set<int>::iterator it12 = boardset.begin(); it12 != boardset.end(); ++it12){
            
            std::cout << "For: board " << *it12 << std::endl;
            
            for(std::map<ThreeTuple, daq::ftk::EventFragmentHitClusters* > ::iterator it = pointerMapOutspy.begin(); it != pointerMapOutspy.end(); ++it){
                std::set<int> unexpectedModules;
                ThreeTuple pack = it-> first;
                daq::ftk::EventFragmentHitClusters* my_pointer = it->second;
                if(pack.L1ID == *it1)
                    //std::cout << "outlane: " << pack.laneNumber << ", is: " << std::hex << my_pointer << std::dec << std::endl;
                    std::cout << "outlane: " << pack.laneNumber << std::endl;
                unsigned int nModules = my_pointer->getNModules();
                
                for(unsigned int m = 0; m < nModules; m++) {
                    daq::ftk::ModuleHitClusters* mhc = my_pointer->getModule(m);
                    int modNum = mhc->getModuleNumber();
                    if(mhc->getModuleType() == 1) {
                        modNum |= (0x1 << 15);
                    }
                    unexpectedModules.insert(modNum);
                }
                if (pack.L1ID == *it1){
                    std::cout << "unexpected modules (found in outspy and not in inspy) are:  " << std::endl;
                    for( int mod : unexpectedModules){
                        std::cout<< std::hex <<"0x8" << std::setfill('0') << std::setw(7) << mod << std::dec << " ";
                    }
                    std::cout << std::endl;
                }
            }
        }
    }
}


void DFSpyHandler::displayUnexpectedModules_modules() { //display modules that were unexpected in the event fragment in which some modules were expected
    
    std::set<int> L1IDset;
    for(std::map<ThreeTuple, std::set<int>> ::iterator it = pointerMapOutspy_modules.begin(); it != pointerMapOutspy_modules.end(); ++it){
        ThreeTuple pack = it-> first;
        L1IDset.insert(pack.L1ID);
    }
    
    for(std::set<int>::iterator it1 = L1IDset.begin(); it1 != L1IDset.end(); ++it1){
        std::cout << "\n***For the event fragment, L1ID: " << std::hex << *it1 << std::dec << " ***" << std::endl;
        
        std::set<int> laneset;
        std::set<int> boardset;
        
        for(std::map<ThreeTuple, std::set<int> >::iterator it10 = pointerMapOutspy_modules.begin(); it10 != pointerMapOutspy_modules.end(); ++it10) {
            ThreeTuple pack = it10->first;
            
            if (pack.L1ID == *it1)
                laneset.insert(pack.laneNumber);
            boardset.insert(pack.boardNumber);
        }
        
        for(std::set<int>::iterator it12 = boardset.begin(); it12 != boardset.end(); ++it12){
            std::cout << "For board: " << *it12 << std::endl;
            for(std::set<int>::iterator it11 = laneset.begin(); it11 != laneset.end(); ++it11){
                std::cout << "outlane: " << *it11 << std::endl;
                std::cout << "Unexpected modules (found in outspy and not in inspy) are: " << std::endl;
                
                for(std::map<ThreeTuple, std::set<int> >::iterator it = pointerMapOutspy_modules.begin(); it != pointerMapOutspy_modules.end(); ++it) {
                    std::set<int> setModID_notinspy = it->second;
                    ThreeTuple pack1 = it->first;
                    
                    for (std::set<int>::iterator iter = setModID_notinspy.begin(); iter != setModID_notinspy.end(); ++iter) {
                        if (pack1.L1ID == *it1)
                            if (pack1.laneNumber == *it11 )
                                std::cout  << "0x8" << std::setfill('0') << std::setw(7) << std::hex << *iter << std::dec << " ";
                    }
                    
                }
                std::cout << std::endl;
            }
        }
    }
}

/* ########################################
 Other display helper functions
 ########################################*/



void DFSpyHandler::displayContentsQueueMapOutspy() {
    for(std::map<TwoTuple, std::queue<ThreeTuple> >::iterator it = queueMapOutspy.begin(); it != queueMapOutspy.end(); ++it) {
        TwoTuple boardOutlane = it->first;
        std::string bl =  "(Board " + std::to_string(boardOutlane.boardNumber) + ", Lane " + std::to_string(boardOutlane.laneNumber) + ")";
        std::cout << bl << "  queue size: " << queueMapOutspy[boardOutlane].size() << std::endl;
    }
}



void DFSpyHandler::displayContentsQueueMapInspy() {
    for(std::map<TwoTuple, std::queue<FiveTuple> >::iterator it = queueMapInspy.begin(); it != queueMapInspy.end(); ++it) {
        TwoTuple bl = it->first;
        std::cout << "[" << "Board " << bl.boardNumber << ", Lane " << bl.laneNumber << ")  queue size: " << queueMapInspy[bl].size() << std::endl;
    }
}



std::string DFSpyHandler::get2TupleString(std::vector<int> t) {
    std::string tuple2 = " ";
    tuple2 = "( "  + std::to_string(t[0]) + ", " + std::to_string(t[1]) + " )";
    return tuple2;
    
}

/* ########################################
 * #               Testing                #
 * ######################################## */
// Returns absolute path to each spyfile
std::vector<std::string> DFSpyHandler::getSpyFilesFromDirectory(const std::string& name) {
    std::vector<std::string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        if(dp->d_name[0] == 'D') {
            // Construct absPath
            std::string absPath = name;
            if(name[name.size()-1] != '/') {
                absPath += "/";
            }
            absPath += dp->d_name;
            v.push_back(absPath);
        }
    }
    closedir(dirp);
    return v;
}

// Convert single spyfile to vector of unsigned ints (representing data)
std::vector<unsigned int> DFSpyHandler::spyfileToVector(std::string spyfile) {
    std::vector<unsigned int> data;
    std::string line;
    std::ifstream myfile(spyfile);
    std::stringstream ss;
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            unsigned int dataword = dfspy::hexStringToInt(line);
            data.push_back(dataword);
        }
    } else {
        std::cerr << "Could not open spyfile." << std::endl;
    }
    return data;
}

// Returns string hex source ID sans the 0x.
std::string DFSpyHandler::getSourceIDFromFilename(std::string filename) {
    return filename.substr(filename.find("0x") + 2, filename.find(".txt") - 5);
}

// Returns board number from string hex source ID
int DFSpyHandler::getBoardFromSourceID(std::string sourceID) {
    return dfspy::hexStringToInt(sourceID.substr(6, 8));
}

int DFSpyHandler::getAbsoluteChannelFromSourceID(std::string sourceID) {
    return dfspy::hexStringToInt(sourceID.substr(0,2));
}


// Function to match towers to the Outspy Module IDs that were missing in the LUT -> to investigate why some modules are missing

std::map<std::vector<int>, std::set<int> > DFSpyHandler::mapBoardAndLaneModIDsOutspy_tower_for_unexpected_modules(std::string moduleListFile, std::set <int> missing_LUTmods) {
    
    std::set<int> towers;
    std::string outString;
    std::string line;
    std::ifstream myfile(moduleListFile);
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            outString = "";
            std::vector<std::string> tokens = dfspy::split(line);
            int robID = dfspy::hexStringToInt(tokens[0]);
            int modID = std::stoi(tokens[1], nullptr);
            
            if(robID >= 0x200000) modID |= (0x1 << 15); // Add extra isSCT bit for sct modules
            unsigned long long tower_0_31_bits = dfspy::hexStringToULL(tokens[2]);
            unsigned long long tower_32_63_bits = dfspy::hexStringToULL(tokens[3]);
            
            // Utilize tower bits
            uint64_t tow_all_bits = (tower_0_31_bits) | (tower_32_63_bits << 32);
            for(int tow = 0; tow < NUMBER_OF_FTK_TOWERS; tow++) {
                if((1ull << tow) & tow_all_bits) {
                    
                    for (std::set <int>::iterator iterator_LUT = missing_LUTmods.begin(); iterator_LUT != missing_LUTmods.end(); ++iterator_LUT ){
                        if (modID == *iterator_LUT){
                            towers.insert(tow);
                            std::cout << "0x8" << std::setfill('0') << std::setw(7) << std::hex <<  modID << std::dec << " tower: " << tow << " rob ID: " << std::hex << robID << std::dec  << std::endl;
                        }
                    }
                }
            }
        }
        std::cout << "\nAll towers for these modules are: " << std::endl;
        for(std::set<int>::iterator towers_it = towers.begin(); towers_it != towers.end(); ++towers_it){
            std::cout << *towers_it << std::endl;
        }
        
    } else {
        std::cerr << "Unable to open module list file" << std::endl;
    }
    
    return boardAndLaneToModIDsOutspy;
}
