#include "ftktools/SpyBufferGetter.h"

#include "ipc/partition.h"
#include "ipc/exceptions.h"
#include "ipc/core.h"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <emon/EventSampler.h>
#include <emon/EventIterator.h>
#include <emon/SelectionCriteria.h>
#include <emon/EventIterator.h>

#include <eformat/SourceIdentifier.h>
#include <eformat/eformat.h>
#include <eformat/write/eformat.h>

#include <ftkcommon/SourceIDSpyBuffer.h>
#include <ftkcommon/EventFragment.h>
#include <ftkcommon/SpyBuffer.h>
#include <ftkcommon/EventFragmentFTKPacket.h>
#include "ftkcommon/Utils.h"

#include "EventStorage/pickDataReader.h"

#include <dirent.h>



SpyBufferGetter::SpyBufferGetter(){
	m_robSourceIDtoData= new std::map <unsigned int, std::vector <unsigned int>>;
    
	m_index= new std::vector <unsigned int>;
	
	m_index_IM   =new std::vector <unsigned int>;
	m_index_DF   =new std::vector <unsigned int>;
	m_index_AUX  =new std::vector <unsigned int>;
	m_index_AMB  =new std::vector <unsigned int>;
	m_index_SSB  =new std::vector <unsigned int>;
	m_index_FLIC =new std::vector <unsigned int>;
	m_index_FTK  =new std::vector <unsigned int>;
}

SpyBufferGetter::~SpyBufferGetter(){
	delete m_robSourceIDtoData;
	m_robSourceIDtoData=0;
	
	delete m_index;
	m_index=0;
	
	delete m_index_FTK;
	m_index_FTK =0;
	
	delete m_index_IM;
	m_index_IM  =0;
	
	delete m_index_DF;
	m_index_DF  =0;
	
	delete m_index_AUX;
	m_index_AUX =0;
	
	delete m_index_AMB;
	m_index_AMB =0;
	
	delete m_index_SSB;
	m_index_SSB =0;
	
	delete m_index_FLIC;
	m_index_FLIC=0;
}

void SpyBufferGetter::setupMapFromPartition(std::string partition_name){
	
	std::string type  = "ReadoutApplication";
	int timeout  = 1000;
	int maxEvts  = 1;
	int dTime    = 0;
	int freezeMask = -1;
	
	//Open IPC partition:
	const IPCPartition   partition( (const char*)partition_name.data() );
	
	//Define a list of the samplers that will be used:
	std::vector<std::string> samplers_name;
	std::map<std::string,EventMonitoring::EventSampler_var> samplers;
    partition.getObjects<EventMonitoring::EventSampler,ipc::no_cache,ipc::non_existent>( samplers );
	std::map<std::string,EventMonitoring::EventSampler_var>::iterator it = samplers.begin();
      
    for ( ; it != samplers.end( ); ++it ) {
    
		//Skip the non interesting samplers.
		if(it->first.find(type+":")==std::string::npos)continue;
		if(!(it->first.find("ROS")==std::string::npos))continue;
		if(!(it->first.find("Flic")==std::string::npos))continue;
        if(!(it->first.find("Manager")==std::string::npos))continue;
        if(!(it->first.find("ReadoutApplicationDebug")==std::string::npos))continue;
        
        samplers_name.push_back(it->first.substr(it->first.find(":")+1) );
        std::cout<<"\nUsing samplers:"<<it->first.substr(it->first.find(":")+1)<<std::endl;
	}
  
	if(samplers_name.size()==0){
		std::cout<<"No sampler found" <<std::endl;
		return ;
	}
    
	//Access the sampler:
    for(std::vector<std::string>::iterator iter_samp = samplers_name.begin(); iter_samp != samplers_name.end(); ++iter_samp ){
        
        std::cout << "\nName of the sampler: " << *iter_samp << std::endl;
        const EventMonitoring::SamplingAddress address  = emon::SamplingAddress( type, *iter_samp);

	// Selection based on freeze status
	int freezeVal=0;
	bool freezeIgnore = true;

	if (freezeMask < 0 ){
		freezeVal=freezeMask;
		freezeIgnore=false;
	}
	 
	emon::L1TriggerType freezeSelection = emon::L1TriggerType(freezeVal, freezeIgnore);
        
    daq::ftk::BoardType BoardType = daq::ftk::BoardType(0); //BoardTypes = {'FTK': 0, 'IM': 1, 'DF': 2, 'AUX': 3, 'AMB': 4, 'SSB': 5, 'FLIC': 6, 'OTHER': 0xf}
        
    uint8_t boardNumber=0xFF;
	uint8_t boardInternal=0;
	daq::ftk::Position Position=daq::ftk::Position(3);	// Position   = {'IN': 0, 'OUT': 1, 'OTHER_POSITION': 3}

	uint8_t res=0x0;
	uint32_t sourceID = daq::ftk::encode_SourceIDSpyBuffer(BoardType, boardNumber, boardInternal, Position,res);

    std::cout<<"Source ID: "<<std::hex<<" 0x"<<sourceID<<std::endl;
	emon::StatusWord boardSelection=emon::StatusWord( sourceID, false);
	emon::SmartBitValue lvl1_trigger_info=emon::SmartBitValue();
	emon::SmartStreamValue stream_tags=emon::SmartStreamValue();
		
	const emon::SelectionCriteria criteria=emon::SelectionCriteria(
    freezeSelection,
	lvl1_trigger_info,
	stream_tags,
	boardSelection);
	int evt=0;
	bool retryOnce=false;
	emon::EventIterator *eventIterator=new emon::EventIterator( partition, address, criteria);
    
    
    const eformat::read::FullEventFragment* fe=0;
    maxEvts=1;
	while (evt < maxEvts){
		evt += 1;	
		emon::Event event;
		try {
			event = eventIterator->nextEvent(timeout);
		}
		catch(emon::NoMoreEvents){
			std::cout<<"No new events available, will try once to sleep 60s and check if an event appear..."<<std::endl;
			if(!retryOnce){
				sleep(60);
				evt=0;
				retryOnce=true;
			}
			continue;
		}
        
		fe= new eformat::read::FullEventFragment(event.data());
        std::vector<eformat::read::ROBFragment> rob;
        fe->robs(rob);
        
        for (unsigned int count1=0; count1<rob.size(); count1++){
            std::vector <unsigned int> vec_rod_data;
            for (unsigned int data=0; data < rob[count1].rod_ndata(); data++){
                vec_rod_data.push_back(rob[count1].rod_data()[data]);
            }
            m_robSourceIDtoData->insert(std::pair <unsigned int, std::vector <unsigned int>> (rob[count1].rob_source_id(), vec_rod_data));
			m_index->push_back(rob[count1].rob_source_id());
			if(daq::ftk::decode_SourceIDSpyBuffer(rob[count1].rob_source_id()).boardType==1)m_index_IM->push_back(rob[count1].rob_source_id());
			if(daq::ftk::decode_SourceIDSpyBuffer(rob[count1].rob_source_id()).boardType==2)m_index_DF->push_back(rob[count1].rob_source_id());
			if(daq::ftk::decode_SourceIDSpyBuffer(rob[count1].rob_source_id()).boardType==3)m_index_AUX->push_back(rob[count1].rob_source_id());
			if(daq::ftk::decode_SourceIDSpyBuffer(rob[count1].rob_source_id()).boardType==4)m_index_AMB->push_back(rob[count1].rob_source_id());			
			if(daq::ftk::decode_SourceIDSpyBuffer(rob[count1].rob_source_id()).boardType==5)m_index_SSB->push_back(rob[count1].rob_source_id());
			if(daq::ftk::decode_SourceIDSpyBuffer(rob[count1].rob_source_id()).boardType==6)m_index_FLIC->push_back(rob[count1].rob_source_id());
        }
        
        if (dTime != 0) sleep(dTime/1000.);
	
	  }
	}
}

void SpyBufferGetter::setupMapFromDirectory(std::string dir){
    
    unsigned int hexStringToInt(std::string hexString);
    std::vector<std::string> getSpyFilesFromDirectory(const std::string& name);
    std::vector<unsigned int> spyfileToVector(std::string spyfile);
    std::string getSourceIDFromFilename(std::string filename);
    std::string getStringBoardType(uint32_t boardType);
    
    std::vector<std::string> vec = getSpyFilesFromDirectory(dir);
    std::vector<unsigned int> vec_of_source_IDs;
    std::map < std::string, std::vector <unsigned int>> robSourceIDtoData; //declaring a map of source IDs to data
    
    for(std::vector<std::string>::iterator itvec = vec.begin(); itvec != vec.end(); ++itvec) {
        std::string filename = itvec->substr(itvec->find("_"));
        std::string sourceID = getSourceIDFromFilename(filename);
        unsigned int intSourceID = hexStringToInt(sourceID);
        std::vector<unsigned int> data_words_vec = spyfileToVector(*itvec);
        
        m_robSourceIDtoData->insert(std::pair <unsigned int, std::vector <unsigned int>> (intSourceID, data_words_vec));
        m_index->push_back(intSourceID);
        if(daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardType==1)m_index_IM->push_back(intSourceID);
        if(daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardType==2)m_index_DF->push_back(intSourceID);
        if(daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardType==3)m_index_AUX->push_back(intSourceID);
        if(daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardType==4)m_index_AMB->push_back(intSourceID);
        if(daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardType==5)m_index_SSB->push_back(intSourceID);
        if(daq::ftk::decode_SourceIDSpyBuffer(intSourceID).boardType==6)m_index_FLIC->push_back(intSourceID);
    }
    
}


void SpyBufferGetter::saveMapToDirectory(std::string dir, FTKBoard board){
    
	
	std::vector<unsigned int>* idx=this->getBoardSpecificSourceIDs(board);
    std::string afile       = "afile.txt";
    std::string adir     = dir;
    std::string DirFile="";
    
    if(adir!=""){
        time_t rawtime;
        struct tm * timeinfo;
        char buffer [80];
        time (&rawtime);
        timeinfo = localtime (&rawtime);
        strftime (buffer,80,"SpyDump_%Y%m%d_%H%M%S",timeinfo);
        
        DirFile=std::string(adir)+buffer;
        std::cout<<"Will dump spy buffers into: "<<DirFile<<std::endl;
        if(!boost::filesystem::create_directories(DirFile)){
            std::cout<<"Fatal Error could not create directory: "<<DirFile<<std::endl;
            std::cout<<"End"<<DirFile<<std::endl;
        }
    }
    
    std::string empty_dir = DirFile;
    std::string filename;
    std::ofstream myfile;
    
	
	for (unsigned int i=0;i<idx->size();i++){
		unsigned int sourceID=idx->at(i);
		uint32_t boardTypeint = daq::ftk::decode_SourceIDSpyBuffer(sourceID).boardType;
	    std::vector <unsigned int> data_vector = this->getSpyBufferFromSourceID(sourceID);

	    DirFile = empty_dir;
	    std::stringstream ss;
	    ss << std::setfill('0') << std::setw(8) << std::hex << sourceID;
	    filename = getStringBoardTypeFromUInt(boardTypeint);
	    filename += "_0x"+ss.str();
	    DirFile+="/"+std::string(filename);
	    DirFile+=".txt";
        
	    myfile.open (DirFile);

	    if(afile!=""){
	         for(std::vector <unsigned int>::iterator itvec_data = data_vector.begin(); itvec_data != data_vector.end(); itvec_data ++){
	               myfile << "0x" << std::hex << std::setfill ('0') <<std::setw (8) << *itvec_data << "\n";
	          }
	        myfile.close();

	    }
	}
    
}



//void SpyBufferGetter::createMapforPostProcessing(FTKBoard board) {

std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > > SpyBufferGetter::createMapforPostProcessing(FTKBoard board) {
    
    /* the map will allow any of the categories from the set (defined by their name as a string) to be mapped to a map of a unique board (identified by type and logical number) mapped to a struct that will have all necessary information for the given category object
    
     the categories are (same as in the SoloRun PostProcessing) :
     
        * Firmware of included boards -> firmware
        * Link status for included boards -> link_status
        * Freezes in the system -> freezes
        * Back-pressure in the system -> back_pres
        * FIFO overflows in the system -> fifo_overflow
        * Any additional errors in the system -> other_err
        * Rates measured by different boards -> rate_measured
        * Events through each board -> events_through
        * Additional information -> extra_info

     */
   
    //pointer to the vector with source IDs for the given board, FTK means all of the boards otherwise the enum is like this: enum FTKBoard {FTK=0, IM=1, DF=2, AUX=3, AMB=4, SSB=5, FLIC=6};
    std::vector<unsigned int>* idx=this->getBoardSpecificSourceIDs(board);
    
    // declare set of the category strings
    std::set <std::string> category_string = {"firmware", "link_status", "freezes", "back_pres", "fifo_overflow", "other_err", "rate_measured", "events_through", "extra_info"};
    std::map <std::string , std::map <unsigned int, detailed_information_struct > > post_processing_information_map;
    std::map <unsigned int, detailed_information_struct > board_to_detailed_information_map;
    
    std::set <unsigned int> unique_board_identifier_set;
    
    // get a set of unique board identifiers from all source IDs: includes information on both type of FTK board (possible options: IM, DF, AUX, AMB, SSB, Flic) and logical board's number
    
    for (unsigned int i=0;i<idx->size();i++){
        unsigned int sourceID = idx->at(i);
        
        Int_tf unique_board_identifier = daq::ftk::getBitRange(sourceID, 0, 11); //includes both the type of board and board number --> should be a unique identifier
        unique_board_identifier_set.insert(unique_board_identifier);
    }
    
    // loop over a set of strings that are names of the categories

    for (std::set <std::string>::iterator category_it = category_string.begin(); category_it != category_string.end(); ++category_it){
        
        detailed_information_struct category_struct;
        for (std::set <unsigned int>::iterator uniqueID_it = unique_board_identifier_set.begin(); uniqueID_it != unique_board_identifier_set.end(); uniqueID_it ++ ){
            
                board_to_detailed_information_map.insert(std::pair <unsigned int, detailed_information_struct> (*uniqueID_it, category_struct));
            
                Int_tf type_of_board = daq::ftk::getBitRange(*uniqueID_it, 8, 11);
                Int_tf number_of_board = daq::ftk::getBitRange(*uniqueID_it, 0, 7);
            }
            post_processing_information_map.insert(std::pair <std::string, std::map <unsigned int, detailed_information_struct > > (*category_it , board_to_detailed_information_map ));
    }
    return post_processing_information_map;
}

void SpyBufferGetter::printMapforPostProcessing(std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > > post_processing_information_map_input) {
    
    for ( std::map <std::string , std::map <unsigned int, detailed_information_struct > >::iterator it = post_processing_information_map_input.begin(); it != post_processing_information_map_input.end(); it++  ){
        
        std::string category = it->first;
        std::map < unsigned int, detailed_information_struct > value_map = it->second;
        
        std::cout << "\nCATEGORY: " << category << std::endl;
        for (  std::map < unsigned int, detailed_information_struct >::iterator it1 = value_map.begin(); it1 != value_map.end(); it1++){
            
            unsigned int boardID = it1->first;
            detailed_information_struct category_struct = it1 -> second;
            
            Int_tf type_of_board = daq::ftk::getBitRange(boardID, 8, 11);
            Int_tf number_of_board = daq::ftk::getBitRange(boardID, 0, 7);
            
            std::cout  << std::dec << " Type of board: " << getStringBoardTypeFromUInt(type_of_board) << ", Board's logical number: " << number_of_board  << ", Board's unique identifier: "  <<  boardID << std::endl;
            
            std::cout << " Floating number: " << category_struct.num_float << ", Integer number: " << category_struct.num_int << ", Error (bool): " << category_struct.err << ", Error message: " << category_struct.error_message << ", Additional information: " << category_struct.additional_info << std::endl;
            
        }
    }
}

SpyBufferGetter::detailed_information_struct SpyBufferGetter::getMapElement(std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > > post_processing_information_map_input, const char * category, unsigned int board_unique_ID){
    
    std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > >::iterator it;
    
    it = post_processing_information_map_input.find(category);
    if (it != post_processing_information_map_input.end()) {
        
        // Access the Value from iterator
        std::map < unsigned int, SpyBufferGetter::detailed_information_struct > value_map = it->second;
        
        std::map < unsigned int, SpyBufferGetter::detailed_information_struct >::iterator it1;
        it1 = value_map.find(board_unique_ID);
        if (it1 != value_map.end()) {
            detailed_information_struct val_struct = it1->second;
        return val_struct;
        }
    }
}

std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > > SpyBufferGetter::insertMapElement(std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > > post_processing_information_map_input, const char * category, unsigned int board_unique_ID, float struct_float, int struct_int, bool struct_error, std::string struct_error_message, std::string struct_add_info){

    std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > > modified_map = post_processing_information_map_input;
    std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > >::iterator it;
    
    it = modified_map.find(category);
    if (it != modified_map.end()) {
        
        // Access the Value from iterator
        std::map < unsigned int, SpyBufferGetter::detailed_information_struct > value_map = it->second;
        
        std::map < unsigned int, SpyBufferGetter::detailed_information_struct >::iterator it1;
        it1 = value_map.find(board_unique_ID);
        if (it1 != value_map.end()) {
            detailed_information_struct val_struct = it1->second;
            
            
            val_struct.num_float = struct_float;
            val_struct.num_int = struct_int;
            val_struct.err = struct_error;
            val_struct.error_message = struct_error_message;
            val_struct.additional_info = struct_add_info;
            
            
            value_map[board_unique_ID] = val_struct;
            modified_map[category] = value_map;
            
            }
    }
    return modified_map;
}


// helper functions

// ====================================================================================================================================================================
// Function to take the data from the directory (same as in DFSpyHandler tool)
// ====================================================================================================================================================================

// Returns absolute path to each spyfile
std::vector<std::string> getSpyFilesFromDirectory(const std::string& name) {
    std::vector<std::string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        if(dp->d_name[2] == '_'){
            // Construct absPath
            std::string absPath = name;
            if(name[name.size()-1] != '/') {
                absPath += "/";
            }
            absPath += dp->d_name;
            v.push_back(absPath);
        }
        if(dp->d_name[3] == '_'){
            std::string absPath = name;
            if(name[name.size()-1] != '/') {
                absPath += "/";
            }
            absPath += dp->d_name;
            v.push_back(absPath);
        }
        if(dp->d_name[4] == '_'){
            std::string absPath = name;
            if(name[name.size()-1] != '/') {
                absPath += "/";
            }
            absPath += dp->d_name;
            v.push_back(absPath);
        }
    }
    closedir(dirp);
    return v;
}


// ====================================================================================================================================================================
// Convert single spyfile to vector of unsigned ints (representing data)
// ====================================================================================================================================================================

std::vector<int> spyfileToVector(std::string spyfile) {
    unsigned int hexStringToInt(std::string hexString);
    std::vector<int> data;
    std::string line;
    std::ifstream myfile(spyfile);
    std::stringstream ss;
    if(myfile.is_open()) {
        while(std::getline(myfile, line) ) {
            int dataword = hexStringToInt(line);
            data.push_back(dataword);
        }
    } else {
        std::cerr << "Could not open spyfile." << std::endl;
    }
    
    return data;
}

// ====================================================================================================================================================================
// Convert hex string to the unsigned integer
// ====================================================================================================================================================================
unsigned int hexStringToInt(std::string hexString) {
    unsigned int x;
    std::stringstream ss;
    ss << std::hex << hexString;
    ss >> x;
    return x;
}

// ====================================================================================================================================================================
// Get SourceID from the filename
// ====================================================================================================================================================================

std::string getSourceIDFromFilename(std::string filename) {
    return filename.substr(filename.find("0x") + 2, filename.find(".txt") - 5);
}
