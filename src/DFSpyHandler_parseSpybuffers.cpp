#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <queue>
#include <fstream>
#include <sstream>
#include <string>
#include <tuple>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <stdlib.h>
#include <algorithm>

#include "ftkcommon/EventFragment.h"
#include "ftkcommon/EventFragmentFTKPacket.h"
#include "ftkcommon/EventFragmentHitClusters.h"
#include "ftkcommon/Utils.h"
#include "ftkcommon/Cluster.h"

#include "ftktools/DFSpyHandler.h"


int main() {

// define the configuration files needed and the directory with the Spybuffers
    
    // Lab 4 good files:
    
//    std::string modulelist = "/tbed/ftk/condDB/latest/DataFormatter/ver1.5/config/modulelist_Data2018_64T.txt"; // should be correct for TVs
//    std::string system_config = "/tbed/ftk/condDB/latest/DataFormatter/ver1.5/config/df_system_config_64towers.txt";    // config from Stany
//
//    std::string multiboard = "/tbed/ftk/condDB/latest/DataFormatter/ver1.5/config/DF17_tower22_bitlevelchecks/Tower22_config_EnhancedSlice.txt"; //TVs 1-2, tower 22
    
//    std::string multiboard = "/afs/cern.ch/user/m/mczurylo/public/ftk-04-00-01/ftktools/share/Tower40_config_EnhancedSlice.txt"; //CORRECT: TVs 3-6, tower 40 that is working after removing empty lines


    // Slice A-2PU
    std::string modulelist = "/afs/cern.ch/work/s/ssevova/public/FTK_DF/TrigFTKSimTools/integration/df-spy-parse/DF-2-04_2PU_config_files/moduleList_Data2018_64T.fixed.txt";
    std::string system_config = "/afs/cern.ch/work/s/ssevova/public/FTK_DF/TrigFTKSimTools/integration/df-spy-parse/DF-2-04_2PU_config_files/df_system_config.txt";
    std::string multiboard = "/afs/cern.ch/user/m/mczurylo/public/ftk-04-00-01/ftktools/share/Slice2_04_2PU_config_Data2018_wIBL_EnhancedSlice_useCh13.txt";
    
    std::string dumpdir = "/afs/cern.ch/user/m/mczurylo/public/ftk-04-00-01/install/ftktools/dir/SpyDump_SliceA2PU_20190805_152055"; // Slice A-2PU collected on 5th of August

    DFSpyHandler* dfspy = new DFSpyHandler(multiboard, modulelist, system_config);
    dfspy->performBitLevelChecksTESTER(dumpdir, true, true, true);
    return 0;
}
