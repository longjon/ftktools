
tdaq_package()

############################################################
# DAL: generation of C++ OKS interface from xml schema file 
#####
tdaq_generate_dal( schema/ReadoutModule_Manager.schema.xml
  NAMESPACE daq::ftk::dal
  INCLUDE ftktools/dal 
  INCLUDE_DIRECTORIES dal DFConfiguration
  CPP_OUTPUT dal_cpp_srcs
  TARGET ReadoutModule_Manager_dal
  )

tdaq_add_library( managerdal
 	DAL
  ${dal_cpp_srcs}
  DEFINITIONS WORD64BIT
  INCLUDE_DIRECTORIES dal DFConfiguration 
  LINK_LIBRARIES ftkcommon ROOT::Core ROOT::Tree Boost::program_options Boost::iostreams Boost::filesystem
                 tdaq::ROSCore tdaq-common::ers tdaq-common::eformat tdaq-common::eformat_write
                 ROOT::Hist tdaq::rdbconfig tdaq::oks tdaq::rdb tdaq::daq-df-dal)

############################################################
# IS generation
############################################################
tdaq_generate_isinfo(manager_is_ISINFO
  schema/manager_is.schema.xml
  NAMED
  NAMESPACE daq::ftk
  OUTPUT_DIRECTORY ftktools/dal
  CPP_OUTPUT cpp_srcs
  JAVA_OUTPUT is_java_srcs
  NOINSTALL
  )

tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/ftktools/info  DESTINATION ftktools)
tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/ftktools/dal  DESTINATION ftktools)

###########################################################
# Libraries
###########
tdaq_add_library( ReadoutModule_Manager
  tmp.cpp/ReadoutModule_Manager.cpp
  src/ReadoutModuleManager.cxx
  DEFINITIONS WORD64BIT
  INCLUDE_DIRECTORIES dal DFConfiguration ftkcommon
  LINK_LIBRARIES  managerdal manager_is_ISINFO ROOT::Core ROOT::Tree Boost::program_options Boost::iostreams 
									Boost::filesystem tdaq::ROSCore tdaq-common::ers tdaq-common::eformat tdaq-common::eformat_write
                  ROOT::Hist tdaq::rdbconfig tdaq::oks tdaq::rdb tdaq::daq-df-dal
                  ReadoutModule_FTK ReadoutModule_FTK_dal )

tdaq_add_library( dfspy
  src/Utils_dfspy.cxx 
  LINK_LIBRARIES ftkcommon Boost::iostreams Boost::filesystem tdaq-common::ers tdaq::omnithread )

tdaq_add_library( DFSpyHandler
  src/DFSpyHandler.cpp
  LINK_LIBRARIES managerdal ftkcommon SpyBuffer Boost::iostreams Boost::filesystem tdaq-common::ers tdaq::omnithread tdaq::omniORB dfspy ROOT)

tdaq_add_library( SSBEmonSpyParse
                  src/SSBEmonSpyParse.cxx
                  DEFINITIONS HAVE_BOOST_IOSTREAMS                  
                  INCLUDE_DIRECTORIES ftkcommon ssb ../ftkcommon/variant/include
                  LINK_LIBRARIES ssb Boost::iostreams Boost::system Boost::filesystem)
  
tdaq_add_library( SpyBufferGetter
   ${dal_cpp_srcs}
   src/SpyBufferGetter.cxx
   DEFINITIONS WORD64BIT
   INCLUDE_DIRECTORIES dal DFConfiguration ftkcommon
   LINK_LIBRARIES  tdaq::omnithread tdaq::omniORB ftkcommon SpyBuffer tdaq-common::DataWriter  tdaq-common::DataReader tdaq-common::RawFileName tdaq-common::compression Boost::iostreams Boost::filesystem tdaq::ipc tdaq::is tdaq-common::eformat tdaq-common::ers tdaq::emon  tdaq::cmdline tdaq::daq-df-dal)

###########################################################
# Executables 
##############
tdaq_add_executable(FTKSpyDump
  src/FTKSpyDump.cxx
    LINK_LIBRARIES    tdaq::omnithread tdaq::omniORB ftkcommon SpyBuffer tdaq-common::DataWriter Boost::iostreams Boost::filesystem tdaq::ipc tdaq::is tdaq-common::eformat tdaq-common::ers tdaq::cmdline tdaq::emon tdaq::RawFileName tdaq::DataWriter tdaq::DWCBcout SpyBufferGetter)

tdaq_add_executable (DFSpyHandler_parseSpybuffers
  src/DFSpyHandler_parseSpybuffers.cpp
LINK_LIBRARIES SpyBuffer DFSpyHandler  ftkcommon Boost::iostreams Boost::filesystem  tdaq-common::ers tdaq::omnithread  dfspy tdaq::rdbconfig tdaq::oks tdaq::rdb tdaq::daq-df-dal )

tdaq_add_executable(SSBSpyDump
  src/SSBSpyDump.cxx
  DEFINITIONS HAVE_BOOST_IOSTREAMS      
  LINK_LIBRARIES SpyBufferGetter SSBEmonSpyParse FtkCool Boost::iostreams Boost::system Boost::filesystem)

#######################################################
#  Installing schema
###################################################
tdaq_add_schema(schema/ReadoutModule_Manager.schema.xml)

#######################################################
#  Installing header
###################################################
tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/ftktools/dal PATTERN *.h DESTINATION ftktools/dal)

###########################################################
#Installing 
#############

tdaq_add_scripts(python/ftkmaker/ftk_maker.py)
tdaq_add_python_package(ftkmaker)
tdaq_add_data(python/ftkmaker/checksums/data/*json DESTINATION checksums/data)
tdaq_add_data(python/ftkmaker/checksums/mc/*json DESTINATION checksums/mc)
tdaq_add_data(python/ftkmaker/dfDict/dfDict.json DESTINATION dfDict)

tdaq_add_scripts(python/FTK_Solo_run/FTK_Solo.sh)
tdaq_add_scripts(python/FTK_Solo_run/getLogFiles.sh)
tdaq_add_scripts(python/FTK_Solo_run/SaveAllSpyISEMon_IMDF_v1.sh)
tdaq_add_scripts(python/FTK_Solo_run/checkerrors-DF.sh)
tdaq_add_scripts(python/FTK_Solo_run/SoloRun_post.py)
tdaq_add_python_package(FTK_Solo_run)

tdaq_add_scripts(scripts/ftk_emon_task.py)
tdaq_add_scripts(scripts/ftk_spy_dump.py)
tdaq_add_scripts(scripts/transition.py)
tdaq_add_scripts(scripts/TransMonitoringResources.py)
tdaq_add_scripts(scripts/rcdLogAnalyzer.py)
