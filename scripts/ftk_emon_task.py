#!/usr/bin/env tdaq_python


"""Event monitoring service

This is the test for Event Monitoring Service (EMON) 
package of the ATLAS TDAQ system.
"""

__author__ = "Serguei Kolos (Serguei.Kolos@cern.ch)"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2013/06/05 21:57:20 $"

import ers
import ipc
import emon
import sys
import getopt
import eformat
import eformat.dump
import libpyeformat
import libpyeformat_helper
import time

def Usage():
  print "Get events from emon and dump the data"
  print "    -h               Help"
  print "                   Selection criteria"
  print "    -p part          Partition name"
  print "    -t type          Sampler type"
  print "    -T stype         StreamTag type (physics, calibration, monitoring)"
  print "    -N sname         StreamTag name"
  print "                   Configuration"
  print "    -D ms            Event timeout (def=1000ms)"
  print "    -e evts          Number of events to sample"
  print "    -m n             Number of samplers"
  print "                   Output options"
  print "    -s              Dump streamtags"
  print "    -r               Dump robs ids"
  print "    -R               Dump raw data words"
  print "    -P               Eformat pretty print: 1=dump full event header, 2=dump robs header"
  print "    -S               Only print the sampler names"
  print "    -f fname         To choose  the name of the output file"
  print "    -w delay-time    Delay in ms, the monitor should wait for each"
  print "                               event to simulate processing (default 0)"

  sys.exit(2)


pname    = 'ATLAS'
type     = 'dcm'
names    = []
stype    = ''
sname    = [] 
l1bits   = []
timeout  = 1000
dStags   = False
dRobs    = False
dRaw     = False
dPretty  = 0
maxEvts  = 1
onlySamp = False
nSamplers= 1
afile	   = False
dTime    = 0

#Read command line parameters
try:
  opts, args = getopt.getopt(sys.argv[1:], "hsrRSP:p:T:N:D:e:t:n:m:f:w:")
except getopt.GetoptError, err:
  print str(err)
  Usage()
  sys.exit(2)
try:
  if not len(opts): Usage()
  for o,a in opts:
    if    o == "-h": Usage()
    elif  o == "-T": stype    = a
    elif  o == "-t": type     = a
    elif  o == "-m": nSamplers= int(a)
    elif  o == "-N": sname.append(a)
    elif  o == "-D": timeout  = int(a)
    elif  o == "-s": dStags   = True
    elif  o == "-r": dRobs    = True
    elif  o == "-R": dRaw     = True
    elif  o == "-S": onlySamp = True
    elif  o == "-P": dPretty  = int(a)
    elif  o == "-e": maxEvts  = int(a)
    elif  o == "-p": pname    = a
    elif  o == "-f": afile    = a
    elif  o == "-w": dTime    = int(a)

except(AttributeError): pass

if afile:
  # Create the output file
  out = eformat.ostream(".", afile)

if __name__ == "__main__":
  # Create the partition
  partition = ipc.IPCPartition(pname)

  # Find the available samplers
  try:
    samplers = emon.getEventSamplers(partition)
    # Either dump the samplers
    if onlySamp:
      print "The samplers are:"
      for s in samplers:
        print s
      sys.exit(0)
    # Or find the samplers of the right type  
    for s in samplers:
      beg=type+":"
      if s.startswith(type+":"):
        names.append(s[len(beg):])
    names = names[:nSamplers]    
    print "Using samplers:", names    
  except ers.Issue, e:
    ers.log( e )

  address  = emon.SamplingAddress( type, names)
  criteria = emon.SelectionCriteria(
  	emon.L1TriggerType(0, True),
        emon.L1TriggerBits([], emon.Logic.OR, emon.Origin.AFTER_VETO),
        emon.StreamTags(stype, sname, emon.Logic.AND),
        emon.StatusWord(0, True)
        )
  
  evt = 0
  try:
    with emon.EventIterator(partition, address, criteria) as iterator:
      while evt < maxEvts:
        evt += 1
        event = iterator.nextEvent(timeout)
        fe = libpyeformat.FullEventFragment(event)  
        stags = fe.stream_tag()
        if dTime != 0:
          time.sleep(dTime/1000.)

	# Adding event to the out file
	if afile:
	  new_ev = eformat.write.FullEventFragment(fe)
	  out.write(new_ev.__raw__())

        # Pretty print
        if dPretty > 0:
          eformat.dump.fullevent_handler(fe)
          if dPretty >1:
            for rob in fe:
              eformat.dump.rob_handler(rob)
        # Hexdump printout
        if dRaw:
          cnt = 0
          for w in event:
            print "[%6d] 0x%08x" % (cnt, w)
            cnt = cnt + 1
        # Print generic informations
        print "L1id/Gid=%8d / %8d, zlib=%d sz=%6d/%6dB, #stags=%2d, #ROBs=%3d, #status=%2d" % (
          fe.lvl1_id(), fe.global_id(), fe.compression_type(), fe.fragment_size_word()*4, fe.readable_payload_size_word()*4, len(stags), fe.nchildren(), len(fe.status()))
        # Dump StreamTags
        if dStags:
          str="\tStags: "
          for s in stags:
            sR =  "".join('x%08x, ' % i for i in s.robs)
            sD =  "".join('x%08x, ' % i for i in s.dets)
            str+="%s@%s [{%s},{%s}]; " % (s.name, s.type, sR, sD)
          print str
        # Dump the ROBs
        if dRobs:
          str="\tROBs: "
          for r in fe.children():
            str+="0x%08x " % r.source_id()
          print str

  except ers.Issue, e:
    ers.log( e )
