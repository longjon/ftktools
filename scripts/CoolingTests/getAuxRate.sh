#rate for one of the hit links
period=120000
freq=150000000
o=$(aux_read_main --slot $1 --regaddr 0x560310 --fpga 3)
cs=$(cut -d'x' -f3 <<< $o)
dec=$((16#0003897f))
rate=$(($dec/$period/$freq/1000))
#echo $dec
echo "AUX rate: $rate"
