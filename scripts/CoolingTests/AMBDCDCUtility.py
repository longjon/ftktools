#!/usr/bin/env tdaq_python

# Author: G. Volpi, control of the DCDC converters voltage and working conditions

import sys
import subprocess
import math
import argparse

from time import sleep

verb = 0
isAMBv4 = False
VOUT_SCALE=1.6666666666666667 # value for boards with 1.000 nominal tesion

PMBUSWrite = 0x00008000
PMBUSBarracuda = 0x00002900
PMBUSFPGA = 0x00000a00         
PMBUSLambs = [0x00001000, 0x00000e00, 0x00001100, 0x00000f00]
PMBUSCmd = 0x00002160
PMBUSData = 0x00002164

def VMEPeek(slot, regaddr, delay=0.001):
  """Do a vme_peek for a specific address, a delay can be specified, default .1"""
  vmeaddr = (slot<<27) | regaddr
  args = ["vme_peek",hex(vmeaddr)]
  if verb :
    print slot, regaddr, args
  res = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
  
  sleep(delay)
  return int(res,16)

def VMEPoke(slot, regaddr, value, delay=.001):
  """Do a vme_poke for a specific address and value, a delay can be specified, default .1"""
  vmeaddr = (slot<<27) | regaddr
  args = ["vme_poke",hex(vmeaddr), hex(value)]
  if verb :
   print slot, regaddr, value, args
  res = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
  
  sleep(delay)
  return None

def PMBusRead(slot, device, addr, delay=.001):
  """Perform a basic read of PMBus register. An integer is rturned"""
  VMEPoke(slot, PMBUSCmd, device | addr, delay)
  return VMEPeek(slot, PMBUSData)

def compl2(x,n):
  """Return the complement to 2 integer"""
  if x >> n-1 == 1:
    return x - ( 1 << n)
  return x

def DCDCValueM11E5(val):
  """Convert a DCDC value from PMBUS into a float, the value is supposed to have
  11-bit mantissa, 5-bit exponent"""
  maskMantissa = 0x7ff
  mantissaLen  = 11
  expLen       = 5

  exp      = compl2(val >> mantissaLen, expLen)
  mantissa = compl2(val & maskMantissa, mantissaLen)
  
  return math.pow(2,exp)*mantissa

def DCDCValueM16EU(val, exp):
  """Return the floaing point DCDC value when the mantissa is 16-bit number
  encoded as complement 2 and the exponent value is known"""  
  mantissa=  compl2(val, 16)

  return math.pow(2,exp)*mantissa  


def DCDCValueUM16EU(val, exp): 
  """Convert an integer in a floating point. The mantissa is 16
  bits, representing unsigned number, the exponent of 2 is passed
  as parameter"""
  
  mantissa = val&0xffff
  return math.pow(2, exp)*mantissa
 
def SetOverThreshold(slot) :
  """Set the voltage threshold value for the DCDC converters""" 
  if isAMBv4 :
    print "Reset voltage to 1.15 V"
    cmdword = 0x40 | PMBUSWrite | 0x15350000
  else :
    print "Reset voltage to 1.00 V"
    cmdword = 0x40 | PMBUSWrite | 0x18f60000

  print "Reference word:", hex(cmdword)      
  for laddr in PMBUSLambs :
    VMEPoke(slot, PMBUSCmd, cmdword | laddr)
  return None

def SetVoltage(slot) :
  """"Set the voltage provided by the DCDC converter related to the 
  LAMBs"""
  if isAMBv4 :
    print "Set correct voltage to 1.15 V"
    cmdword = 0x22 | PMBUSWrite | 0x00c40000
  else :
    if refValue == 1. : 
      print "Set correct voltage to 1 V"
      cmdword = 0x22 | PMBUSWrite | 0x00000000
    else : 
      print "Set correct voltage to 1.15 V"
      cmdword = 0x22 | PMBUSWrite | 0x02e30000
  print "Reference word:", hex(cmdword)    
  for laddr in PMBUSLambs :
    VMEPoke(slot, PMBUSCmd, cmdword | laddr)

  return None
  
def ReadDCDCInfo(slot, printSummary=False) :
  """Read the information from the DCDC converters through the PMBUS, if the the
  printSummary variable is True the method will also print a single line summary
  with the current format:
  TODO
  """
  
  summary = []
  
  # read the Barracuda
  status_word = PMBusRead(slot, PMBUSBarracuda, 0x79)&0xffff
  
  print "\nBarracuda, (%04x)" % status_word
  VinAD = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0x88))
  VinOff = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0xd4))  
  VinGain = DCDCValueUM16EU(PMBusRead(slot, PMBUSBarracuda, 0xd3), 0)
  Vin = VinGain/8192*VinAD+VinOff
  
  VoutAD = DCDCValueUM16EU(PMBusRead(slot, PMBUSBarracuda, 0x8b), -12)
  VoutOff = DCDCValueM16EU(PMBusRead(slot, PMBUSBarracuda, 0xd2), -12)
  Vout = VoutAD+VoutOff
   
  IoutAD = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0x8c))
  IoutGain = DCDCValueUM16EU(PMBusRead(slot, PMBUSBarracuda, 0xd6), 0)
  IoutOff = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0xd7))
  Iout = IoutGain/8192*IoutAD+IoutOff 
  
  # Barracuada power
  Pout = Iout*Vout
  
  Temp1 = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0x8d))
  
  #print "Vin =", Vin, "V"
  #print "Vout =", Vout, "V", "Iout=", Iout, "A, Power =", Pout, "W"
  print "Power = ", Pout, "W \t\t\t Temperature:", Temp1
    
  summary += [Vin, Vout, Iout, Temp1]
  
  # Read status of the DCDC that cotnrols the FPGA, it soudl have 2 pages  
  VMEPoke(slot, PMBUSCmd, PMBUSWrite | PMBUSFPGA | 0x00000000)  
  status_word = PMBusRead(slot, PMBUSFPGA, 0x79)&0xffff
  print "FPGA DCDC, page 0 (%04x)" % status_word
  
  Vout = DCDCValueUM16EU(PMBusRead(slot, PMBUSFPGA, 0x8b), -9)
  Iout = DCDCValueM11E5(PMBusRead(slot, PMBUSFPGA, 0x8c))  
  
  print "Power =", Vout*Iout, " W"
  #print "Vout =", Vout, "V, Iout =", Iout, " A, Power =", Vout*Iout, " W"
  
  
  summary += [Vout, Iout]

  VMEPoke(slot, PMBUSCmd, PMBUSWrite | PMBUSFPGA | 0x00010000)  
  status_word = PMBusRead(slot, PMBUSFPGA, 0x79)&0xffff
  print "FPGA DCDC, page 1 (%04x)" % status_word  
  
  Vout = DCDCValueUM16EU(PMBusRead(slot, PMBUSFPGA, 0x8b), -9)
  Iout = DCDCValueM11E5(PMBusRead(slot, PMBUSFPGA, 0x8c))
  
  print "Power =", Vout*Iout, " W\n"
  #print "Vout =", Vout, "V, Iout =", Iout, " A, Power =", Vout*Iout, " W"
  
  
  summary += [Vout, Iout]

  for ilamb, laddr in enumerate(PMBUSLambs) : # loop over the LAMbs
    status = PMBusRead(slot, PMBUSBarracuda, 0x79)&0xffff
    print "LAMB", ilamb, "(%04x)" % status
    # read the input voltage
    Vin = DCDCValueM11E5(PMBusRead(slot, laddr, 0x88))
    
    #read Vout
    Vout = DCDCValueM16EU(PMBusRead(slot, laddr, 0x8b), -13)*VOUT_SCALE
  
    # read the out current  
    IoutAD = DCDCValueM11E5(PMBusRead(slot, laddr, 0x8c))
    IoutGain = DCDCValueM11E5(PMBusRead(slot, laddr, 0x38))  
    IoutOff = DCDCValueM11E5(PMBusRead(slot, laddr, 0x39))
    ## Iout=((IoutGAINL0)*IoutADL0)+IoutOFFsetL0
    Iout = IoutGain*IoutAD+IoutOff

    Temp1 = DCDCValueM11E5(PMBusRead(slot, laddr, 0x8d))
    Temp2 = DCDCValueM11E5(PMBusRead(slot, laddr, 0x8e))
    
    Power= Vout*Iout

    #print "Vin =", Vin, "V"
    #print "Vout =", Vout, "V, ", "Iout=", Iout, "A, Power =", Power, "W"
    print "Power =", Power, "W \t\t\t Temperatures: %f C, %f C" % (Temp1, Temp2)
    summary += [Vin, Vout, Iout, Power, Temp1, Temp2] 
  
  if printSummary :
    strsum = [str(v) for v in summary]
    print " ".join(strsum)
  return summary


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument("-C", "--ChangeVoltages", dest="Change", action="store_true",
                    help="Change the value, if not used the tool do nothing", default=False)
  parser.add_argument("-O", "--OldAMBv4", help="The board is an AMBv4 or AMBv5 proto with 1.15 V by defeault",
                    dest="isAMBv4", action="store_true", default=False)
  parser.add_argument("--ref1V", help="Set the reference voltage to 1 volt, only for AMBv5",
                    dest='ref1V', action="store_true", default=False)
  parser.add_argument("--Summary", dest="Summary", action="store_true",
                      help="Print a single summary line at the end", default=False)
  parser.add_argument("-v", "--verbose", dest="verbose", action="count",
                      help="Verbosity level", default=0)
  parser.add_argument("slot",help="Slot number", type=int)
  opts = parser.parse_args()

  #global slot, verb, VOUT_SCALE, isAMBv4
  
  print "AMB Voltage setting utility\n"
  
  verb =  opts.verbose 
  slot = opts.slot
  print "Checking board in slot:", slot
  isAMBv4 = opts.isAMBv4
  if opts.ref1V :
    refValue = 1.
  else :
    refValue = 1.15   
  
  if not isAMBv4 :
    VOUT_SCALE=1.6666666666666667 # value for boards with 1.000 nominal tesion
  else :
    VOUT_SCALE=1.9216666666666667 # value for boards with 1.145 nominal tesion
  #print "Vout scale =", VOUT_SCALE
  
  if opts.Change :
    SetOverThreshold(slot)
    SetVoltage(slot)
  ReadDCDCInfo(slot, opts.Summary)

if __name__ == "__main__" :
  main()
