hexString=$(aux_read_main --slot $1 --regaddr 0x110 --fpga $2 | awk '{print $NF}')
echo $(( 16#${hexString:6:2} - 128 )) 
