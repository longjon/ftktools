#!/usr/bin/env tdaq_python
import sys
import time
import collections
import re
import getopt
import array
import ROOT
from datetime import datetime
from datetime import date
from datetime import timedelta
#import numpy  # Not supported in tdaq_python

# Global variables
dbMakeTrans    = collections.OrderedDict()  # <Key, ersLine>, with key=boardName.TransitionName.functionName
makeTransition = []                         # Array of Key and Delta time
operations     = []                         # Store begin and end of operations
t0             = None                       # Define t0
resources      = []                         # tuple: (time, cpu, mem)

# Dump usage
def Usage():
  print 'Parse and process FTK RCD output file'
  print 'Usage:', sys.argv[0], '[options]'
  print '   -f file   Input file'
  print '   -P val    Just filter the input removing useless data'
  print '   -m        Process MAKE_TRANSITION label'
  print '   -p        Process do_publish()'
  print '   -C        Check the configuration'
  print '   -r        Dump resources: time, cpu, mem'
  sys.exit(2)


# Class for operations
class operation:
  def __init__( self, key, time, delta ):
    self.key   = key
    self.time  = time
    self.delta = delta

# Class to parse ers lines
# Extract: time, tid, board name, function, message
class ersLine:
  def __init__( self, line ):
    global t0
    global makeTransition
    self.line  = line
    self.dtime = datetime(1970, 1, 1)
    self.tid   = 0
    self.board = "none"
    self.func  = "none"
    self.msg   = "none"
    self.funcName = "none"

    # Parse the line to extract date, tid, board, function, message
    if line[25:28] == 'LOG':  # Filter out no LOG messages
      date=line[0:24]
      try: self.dtime = datetime.strptime(date, '%Y-%b-%d %H:%M:%S,%f')
      except(ValueError): pass
      try: self.tid = int(line.replace("}","{").split("{")[1])
      except(IndexError): pass
      try: self.msg = line.replace("}","{").split("{")[2][:-1]
      except(IndexError): pass
      try: self.board = line.replace("]","[").split("[")[3]
      except(IndexError): pass
      try: self.func  = line.replace("]","[").split("[")[1]
      except(IndexError): pass
      if t0 is None:
        t0 = self.dtime
  
      # Calculate "minimal" function name
      self.funcName = self.func.split("(")[0] + "()"
      x = self.funcName.find("daq::ftk")
      if x > 0:
        self.funcName=self.funcName[x:]

      # If message contain "MAKE_TRANSITION" analize it
      if self.msg.find("MAKE_TRANSITION")>0:
        self.analizeMakeTrans()
      
      # If message contain "do_monitor" analize it
      if "Resource monitor:" in self.msg:
        self.parseResource()
      
      # If message contain "do_monitor" analize it, TODO replace with special tag
      if self.msg.find("do_publish")>0:
        self.analizePublish()

  # Just schrink the line for better readability
  def purge(self):
    l = re.sub("at /afs.*releases/", ".", self.line)
    return l[:-1]

  # Summary of the line: time, board, tid
  def brief(self):
    s = "%s" % self.dtime
    return "%-24s, %-16s, %5d" % (s[:-3], self.board, self.tid)
  
  # Summary of the line: time, board, tid
  def summary(self):
    t1 = 0
    if t0 is not None:
      t1 = timedelta_total_seconds(self.dtime - t0)
    s = "%s" % self.dtime
    fn = self.funcName.replace("daq::ftk::","")[:38]
    return "%10.3f, %-14s, %5d, %-40s %s" % (t1, self.board, self.tid, fn, self.msg)

  # Analyze lines containing the string MAKE_TRANSITION (set by the Giuseppe's RC base class)
  # Fill lists:
  #  - makeTransition 
  #  - dbMakeTrans:     <Key, ersLine>, with key=boardName.TransitionName.functionName 
  def analizeMakeTrans(self):
    global dbMakeTrans
    spl  = self.msg.split()
    tra  = spl[1]
    if self.msg.find("SUB_TRANSITION")>0:
      tra = "%s_%s" % (spl[1], spl[2])
    key = "%s.%s.%s" % (self.board, tra, self.funcName)
    if key in dbMakeTrans:
      if self.msg.lower().find("done") < 0:
        print "WARNING: not coupled SUB_TRANSITION message. \n\t --> ", self.line
      prev = dbMakeTrans.pop(key)  
      delta = timedelta_total_seconds(self.dtime - prev.dtime)
      t1    = timedelta_total_seconds(prev.dtime - t0)
      s = "%-80s %6.1f s" % (key, delta)
      makeTransition.append(s)
      operations.append(operation(key, t1, delta))
    else:
      dbMakeTrans[key]=self
 
  # Analyze lines containing publish methods
  # TODO: da fare!
  def analizePublish(self):
    pass
    #print self.purge()

  # Parse resource line
  def parseResource(self):
    t1  = timedelta_total_seconds(self.dtime - t0)
    cpu = float(self.msg.split()[3])
    mem = float(self.msg.split()[5])
    resources.append( (t1, cpu, mem) )
    
# Function to calculate (in python 2.6) the delta time in seconds
def timedelta_total_seconds(timedelta):
    return ( timedelta.microseconds + 0.0 +
                    (timedelta.seconds + timedelta.days * 24 * 3600) * 10 ** 6) / 10 ** 6

# Missing numpy :(
def mean(l):
  return sum(l)/len(l) 

# Missing numpy :(
def std(l):
  c = mean(l)
  n = len(l)
  sd = sum((x-c)**2 for x in l)
  p = sd/(n-1)
  return p**0.5

# Plot a Graph of operation lifespan vs time
def doGraph(operations, tMax):
  colors = [1,2,4,6,7,8,9,28,38,46]  # Color to use in round robin
  noper = len(operations)

  # Create an empty Graph using max x and y 
  x = array.array("d",[0,tMax+tMax/3.])
  y = array.array("d",[0, noper])
  c = ROOT.TCanvas( "c", "Transition Graph", 900, 700)
  gr = ROOT.TGraph(2, x, y)
  gr.Draw( 'AP' )
  gr.SetTitle("Graph of Transitions")
  gr.GetXaxis().SetTitle( 'Time [s]' )
  gr.GetYaxis().SetTitle( 'Transition' )
  gr.GetYaxis().SetRangeUser(-noper/12, noper+noper/12)

  # Loop over operations
  tmp = []; tmp1 = [] # To store TLine and TText 
  cnt = 1   # Operation counter
  prev = "" # To identify "change" of transition
  for o in operations:
    tra = o.key[o.key.find(".")+1:]             # Remove board id
    tra = tra.replace("daq::ftk::","")          # Some cleanup
    #print "%-80s %4d %6.2f %6.2f" % (o.key, cnt, o.time, o.time + o.delta)
    
    # If change of operation write the TText
    if tra != prev:
      prev = tra
      st = tra.split(".")[1]  # Just the function
      t = ROOT.TText(o.time, cnt-6, " "+st)
      t.SetTextColor(12);
      t.SetTextSize(0.02)
      t.Draw()
      tmp1.append(t)

    # Draw the TLines (operation lifetimes)
    delta = o.delta if o.delta>1 else 1
    l =  ROOT.TLine(o.time, cnt , o.time + delta, cnt)
    l.SetLineWidth(3)
    l.SetLineColor(colors[cnt%len(colors)])
    l.Draw()
    tmp.append(l)
    cnt += 1

    # TODO
    # Use vector resources to superimpose cpu and mem
    
  c.Update()
  raw_input("Wait ")
  time.sleep(3)
  c.SaveAs("TransitionGraph.pdf")


def isThere(vec, string):
  for v in vec:
    if v in string:
      return True
  return False    

def extractConfiguration(filename):
  valid = ["Parallel", "Protect", "Separate", "Serialize", "ThreadLimit"]
  confDB={}
  with open(fileName) as fp:
    line = fp.readline()
    block = False
    classname = ""
    while line:
      if line.find("daq::ftk::dal object:")>0:
        block = True
      if line[25:28] == 'LOG' or line[25:29] == 'INFO':
        block = False
      if block:
        if "class name" in line:
          classname = line.split("'")[3]
        if isThere(valid, line):
          key = "%s.%s" % ( classname,  line.split(":")[0].replace(" ","") )
          val = line.split(":")[1]
          confDB.setdefault(key,[]).append(int(val))
          #print line[:-1]
      line = fp.readline()
 
  print "-----------------"
  print "OKS configuration"
  print "-----------------"
  for key in sorted(confDB.keys()) : 
    print "%-50s = %s" % (key, confDB[key])
   

#############################################################
# MAIN
if __name__ == '__main__':

  # Read command line parameters
  purge     = 0
  fileName  = "" 
  makeTra   = False
  publish   = False
  checkConf = False
  dumpRes   = False
  try:
    opts, args = getopt.getopt(sys.argv[1:], "HprmCP:f:")
  except getopt.GetoptError, err:
    print str(err)
    Usage()
    sys.exit(2)

  try:
    for o,a in opts:
      if    o == "-H": Usage()
      elif  o == "-m": makeTra   = True
      elif  o == "-p": publish   = True
      elif  o == "-P": purge     = int(a)
      elif  o == "-f": fileName  = a
      elif  o == "-C": checkConf = True
      elif  o == "-r": dumpRes   = True
  except(AttributeError): pass
  if len(fileName) < 4:
    print "Missing mandatory parameter -f filename"
    Usage()
    sys.exit(2)

  # If checkConf extract configuration info
  if checkConf:
    extractConfiguration(fileName)
    sys.exit(0)

  # Main loop
  with open(fileName) as fp:
    line = fp.readline()
    while line:
      l = ersLine(line)
      if purge == 1: print l.purge()
      if purge >  1: print l.summary()
      line = fp.readline()

  # Analize transitions
  # i.e. process makeTransition and dbMakeTrans lists
  if makeTra:
    table = collections.OrderedDict()   # The final table
    maxT = 0
    for o in operations:
      # Remove the board name 
      n = o.key[o.key.find(".")+1:]
      t = o.delta
      if o.time > maxT: 
        maxT=o.time
      if n in table:
        v = table[n]
        v.append(t)
        table[n] = v
      else:
        table[n] = [t]
    
    # Sanity check
    if len(dbMakeTrans) > 0:
      print "(WW) WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"
      print "(WW) WARNING: There are", len(dbMakeTrans), "unpaired transitions "
      for i in dbMakeTrans.iterkeys(): 
        print "(WW)\t", i
      print "(WW) WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW\n"

    # Print the table
    print "Time 0 =", t0
    for x, y in table.items():
      print "%-80s sec: %8.1f +- %4.1f, max %8.1f" % (x , mean(y), std(y), max(y))

    # Graph
    doGraph(operations, maxT)

  if dumpRes:
    for i in resources:
      print "%12.3f %8.1f %10d" % (i[0], i[1], i[2])



