#!/usr/bin/env tdaq_python

"""
  Python script to measure the partition transitions time and the RCD memory usage
  Author: Simone Sottocornola
          simone.sottocornola@cern.ch
"""

import os,sys
from ispy import IPCPartition, ISInfoIterator, ISInfoAny, ISCriteria, ISServerIterator
import re
import getopt
import time, datetime

def Usage():
  print 'Python script for the measure of the memory usage and transition times'
  print 'Usage:', sys.argv[0], " -[options]"
  print '    -h        Help'
  print '    -o  val   "Memory usage only" mode'
  print '    -f  val   output file'
  print '    -P  val   PID of the ReadoutApplication to which monitor the memory usage'
  print '    -p  val   Name of the partition to test'
  print '    -a  val   Name of the RC_app to which send the command [def=RootController]'
  print '    -n  val   Number of complete cycle of transition to be done for the test [def=2]'
  print '    -m        Enter "manual change of transition" mode'
  sys.exit(2)

# Default parameters
app_name  = 'RootController'
part_name = ''
cycle     = 2
pid       = 5
manual    = False
mem_only  = False
out_file  = '/tmp/mem_out.txt'

#Read command line parameters
try:
  opts, args = getopt.getopt(sys.argv[1:], "hmop:a:n:f:P:")
except getopt.GetoptError, err:
  print str(err)
  Usage()
  sys.exit(2)
try:
  if not len(opts): Usage()
  for o,a in opts:
    if    o == "-h": Usage()
    if    o == "-o": mem_only     = True
    if    o == "-f": out_file     = a
    if    o == "-p": part_name    = a
    if    o == "-a": app_name     = a
    if    o == "-n": cycle        = int(a)
    if    o == "-P": pid          = int(a)
    if    o == "-m": manual       = True
except(AttributeError): pass


#############################################################################################################
#                                            Memory check                                                   #
#############################################################################################################
def mem(data):
  regexp = re.compile(r'VmRSS.*?([0-9.-]+)')
  with open(data) as f:
    for line in f:
        match = regexp.match(line)
        if match:
            return match.group(1)

#############################################################################################################
#                                             Transition                                                    #
#############################################################################################################
def transition(Command, status):
  done  = True
  start = time.time()
  os.system('rc_sender -p %s -n %s -c %s' % (part_name, app_name, Command))
  while(done):
    time.sleep(0.01)
    it = ISInfoIterator(p, 'RunCtrl', ISCriteria(app_name))
    while it.next():
      # Read the IS server
      rm = ISInfoAny()
      it.value(rm)
      rm._update(it.name(), p)
      state = rm.state
      if state == status:
        stop = time.time()
        times[Command]=times[Command]+(stop-start)
        print 'Time required for the  %s transition= \t\t%ss' %(Command, stop-start)
        done = False
    

#############################################################################################################
#                                               Main                                                        #
#############################################################################################################

os.system('clear')

if __name__ == '__main__':

  data= '/proc/%s/status' %pid
  Mem=0

  ############# MEM ONLY #############
  if mem_only==True:

    if pid==0:
      print 'Please insert the pid of the process!!'
      sys.exit(1)

    while os.path.isfile(data):
      Mem=mem(data)
      print "Memory used: %sKB" %Mem
      f = open(out_file, 'ab+')
      f.write(Mem + "\n")
      f.close()
      time.sleep(5)
    sys.exit(0)

  ############# Transitions #############
  if mem_only==False:
    print 'Partition =', part_name
    p = IPCPartition(part_name)
    if not p.isValid():
      print 'Partition', part_name, 'is not valid, abort.'
      sys.exit(2)
    
    Command = ["INITIALIZE", "CONFIGURE", "CONNECT", "START", "STOP", "DISCONNECT", "UNCONFIGURE"]
    Status  = ["INITIAL", "CONFIGURED", "CONNECTED", "RUNNING", "CONNECTED", "CONFIGURED", "INITIAL"]
    times   = dict((el,0) for el in Command)
    Mem     = []

    for x in range (0, cycle):
      for a,b in zip(Command,Status): 
        if manual:
          raw_input('Press enter to proceed with the next transition...')
        transition(a, b)
        if not pid == 0:
          Mem.append(mem(data))
          print "Memory used: %sKB" %mem(data)
        time.sleep(2)
      print "Finished cycle ", x
  
    #Printing the mean transition times
    for key, value in times.iteritems():
    # if key == "START" or key == "STOP":
    #   value = value/(cycle*2)
    #   print '\033[91m',"Mean required time for transition %s: %s s" %(key,value), '\033[0m'
    # else:
      value = value/cycle
      print '\033[91m',"Mean required time for transition %s: %s s" %(key,value), '\033[0m'  

    #Printing the memory usage
    if not pid == 0:
      final_mem = float(Mem[-2])-float(Mem[2])
      percent_mem= ((float(Mem[-2])*100)/float(Mem[2]))-100
      print '\033[91m',"Memory usage first CONFIGURED: \t %sKB" %(Mem[2]), '\033[0m'
      print '\033[91m',"Memory usage (first-last CONFIGURED): \t %sKB" %(final_mem), '\033[0m'
      print '\033[91m',"Memory usage in percentage (first-last CONFIGURED): \t + %s %%" %(percent_mem), '\033[0m'

    sys.exit(0)
