# Script for copying directories to P1
# Goals are to have this done consistently, and to leave a record of how things were copied

# Usage:
# bash copy_to_p1.sh <DIR_ON_LXPLUS> <DIR_AT_P1>

# Define colors
RED='\033[1;31m'
YELLOW='\033[1;33m'
CYAN='\033[0;36m' 
NC='\033[0m'

# Define destinations
FIRST_DEST_DIR=/shared/data

if [ -z "$1" ] || [ "$1" = "--help" ] || [ "$1" = "-h" ] || [ "$1" = "--h" ]
then
  echo "Copy things to p1."
  echo "First argument is the directory to copy."
  echo "Second is the target destination."
  echo "Third is optional, and the name of the destination directory (uses original name by default)."
  exit 1
fi

ORIG_DIR=$1
TARGET_DEST_DIR=$2
#ORIG_DIR=/eos/atlas/atlascerngroupdisk/det-ftk/tvlibrary/data18_pp_2017LowDF/TestDirForCopying
#TARGET_DEST_DIR=/det/ftk/repo/condDB/pseudodata

COPIER_NAME=copier.sh

if [ -z "$3" ]
then
  NAME=`basename $ORIG_DIR`
else
  NAME=$3
fi
DATE=`date '+%m/%d/%Y %H:%M:%S'`

# figure out if a relative or absolute path has been provided
# if it's absolute, then record that directory directly
# if it's relative, without a leading slash, add the current directory to it
if [[ $ORIG_DIR == /* ]] ;
then
  ORIG_FULL=$ORIG_DIR
else
  ORIG_FULL=$(pwd)/$ORIG_DIR
fi

echo 
echo "Copying pseudodata from directory $ORIG_FULL" 
echo "Copying to $FIRST_DEST_DIR at P1."
echo "Ultimate destination is $TARGET_DEST_DIR at P1."
echo "New directory will be called $NAME."
echo

read -p "Continue? (y/n) " -n 1 -r
echo 
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo
    echo "Copying!"

    # Make file that will do the copying at P1
    # Also stores information about where it's from
    cat <<EOF > $ORIG_DIR/$COPIER_NAME
# Copied on $DATE
# Copied from $ORIG_FULL
# Temporary directory: $FIRST_DEST_DIR
# Target directory: $TARGET_DEST_DIR

# Script for copying to final destination
if [ -d "$TARGET_DEST_DIR/$NAME" ]; then
  echo -e "${RED}Problem copying! Directory already exists:${NC}"
  echo "$TARGET_DEST_DIR/$NAME"
else
  cp -r  $FIRST_DEST_DIR/$NAME $TARGET_DEST_DIR/.
  rm -rf  $FIRST_DEST_DIR/$NAME
fi
EOF

    # Copy files and script over
    scp -r $ORIG_DIR atlasgw:$FIRST_DEST_DIR/$NAME
    
    # Remove script
    rm $ORIG_DIR/$COPIER_NAME

    # Instructions at P1
    echo
    echo -e "${YELLOW}To complete the copying process, log into atlasgw and do${NC}"
    echo -e "${CYAN}cd $FIRST_DEST_DIR${NC}"
    echo -e "${CYAN}source $NAME/$COPIER_NAME${NC}"

else
    echo
    echo "Not doing anything!"
fi

