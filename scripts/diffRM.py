# Script to diff two RMs in two xml files

import xml.etree.ElementTree as ET
import argparse

# Define colors
RED='\033[1;31m'
YELLOW='\033[1;33m'
CYAN='\033[0;36m'
GREY='\033[0;37m'
NC='\033[0m'

#f1 = '/tbed/oks/tdaq-07-01-00/FTK/FTK-04-00-02/ftk/segments/FTK-RCD_vme.data.xml'
#f2 = '/tbed/oks/tdaq-07-01-00/FTK/FTK-04-00-02/ftk/segments/FTK-RCD_vme.data.xml'
#rm1_name = 'PU-2-15'
#rm2_name = 'PU-2-19-for-2AUX'

#f1 = '/tbed/oks/tdaq-07-01-00/FTK/FTK-04-00-02/ftk/segments/FTK-segment.data.xml'
#f2 = '/tbed/oks/tdaq-07-01-00/FTK/FTK-04-00-02/ftk/segments/FTK-segment.data.xml'
#rm1_name = 'DF17'
#rm2_name = 'DF17-for-2AUX'

# Handle options
parser = argparse.ArgumentParser()
parser.add_argument('--obj1', dest='obj1', required=True, help='Name of the first XML object you want to look at')
parser.add_argument('--obj2', dest='obj2', required=True, help='Name of the second XML object you want to look at')
parser.add_argument('--file1', dest='file1', required=True, help='File the first XML object can be found in')
parser.add_argument('--file2', dest='file2', help='File the second XML object can be found in')
parser.add_argument('--verbose', '-v', dest='verbose', action='count', help='In verbose mode, print out all object attributes, not just differing ones.')
args = parser.parse_args()

f1 = args.file1
if args.file2 is None:
    print "%sNo second file set; assuming you're comparing two RMs in first file.%s"%(YELLOW,NC)
    f2 = args.file1
else: f2 = args.file2
rm1_name = args.obj1
rm2_name = args.obj2
verbose = args.verbose

# Find Object with a given ID
def getRM(fname, rmname):

    elements = ET.parse(f1)
    for e in elements.getiterator('obj'):
        if e.attrib['id'] == rmname: return e
    print "Couldn't find RM %s in file %s!"%(rmname, fname)
    exit()

# Find object attribute (<attr>) with a given name
def getChild(rm, name):

    for child in rm:
        #print child.get("name"), child.text
        if child.get("name") == name: return child
    #print "\t%20s: \t Does not exist in RM %s"%(name, rm.attrib['id'])
    return 1

# Print attribute values in a way that multi-line stuff doesn't suck
def printValue(text):
    if text==None: text = "Not defined."
    lines = text.split("\n")
    if len(lines) == 1:
        print "\t", lines[0], NC
    else:
        counter=0
        for line in lines:
            counter+=1
            if line.strip()=="": continue
            if counter==2: print "     %s"%line
            if counter==len(lines)-1:
                print "\t%45s"%"", line, NC
            else: print "\t%45s"%"", line

rm1 = getRM(f1, rm1_name)
rm2 = getRM(f2, rm2_name)

print "\nComparing 2 RMs:"
print "%sRM1:"%GREY, rm1.attrib, "in file", f1, NC
print "RM2:", rm2.attrib, "in file", f2

print "\nSummary of differences found:"
for c1 in rm1:
    c2 = getChild(rm2, c1.get("name"))
    if c2 == 1: c2_text = "Not found in RM"
    else: c2_text = c2.text
    if not c1.text == c2_text or verbose:
        print "%s\t%25s: "%(CYAN, c1.get("name")),
        color = GREY
        if verbose and not c1.text == c2_text: color = RED
        print "%s\t in RM1:"%color,
        printValue(c1.text)
        print "\t%25s\t in RM2:"%"",
        printValue(c2_text)

# Also check for stuff that's only in the 2nd file
for c2 in rm2:
    c1 = getChild(rm1, c2.get("name"))
    if c1 == 1: c1_text = "Not found in RM"
    else: c1_text = c1.text
    if c1 == 1:
        print "%s\t%25s: "%(CYAN, c2.get("name")),
        color = GREY
        if verbose and not c1_text == c2.text: color = RED
        print "%s\t in RM1:"%color,
        printValue(c1_text)
        print "\t%25s\t in RM2:"%"",
        printValue(c2.text)




