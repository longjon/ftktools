#!/usr/bin/env tdaq_python

"""
  Python script to measure the partition transitions time and the RCD memory usage
  Author: Simone Sottocornola
          simone.sottocornola@cern.ch
"""

import os,sys
from ispy import IPCPartition, ISInfoIterator, ISInfoAny, ISCriteria, ISServerIterator
import re
import getopt
import time, datetime
import threading
import array as arr
import signal
import sys
import subprocess
import socket
from ROOT import *


ThreadRun = True

def Usage():
  print 'Python script for the measure of the memory usage and transition times'
  print '    -H        Help'
  print '    -M        Manual mode'
  print '    -o        "Memory usage only" mode'
  print '    -t  val   Title of test '
  print '    -p  val   Name of the partition to test'
  print '    -a  val   Name of the RCD to which send the command. If default is used, subtransition are not considered  [default=RootController]. '
  print '    -n  val   Number of complete cycle of transition to be done for the test [def=2]'
  print '    -r  val   Number of seconds in running state'
  print '    -m  str   Few words describing the test'
  sys.exit(2)


# Default parameters
app_name    = 'RootController'
part_name   = ''
cycle       = 2
pid         = 0
manual      = False
mem_only    = False
out_file    = 'default_test'
node        = "pc-tbed-pub-23.cern.ch"
runTime     = 60
description = ""
ThreadRun   = True
p1ArchPath  = '/data/SoloRun/moveToEOS/scalability'
p1ArchNode  = 'pc-tdq-calib-13.cern.ch'
eosArchPath = '/eos/atlas/atlascerngroupdisk/det-ftk/SoloRun/scalability'

# Data arrays 
cpu_vec           = arr.array('d',[])
mem_vec           = arr.array('d',[])
resTime_vec       = arr.array('d',[])
traTime_vec       = arr.array('d',[])
time_transition   = arr.array('d',[])
x_time_transition = arr.array('d',[])
which_transition  = []

# Set reference time t0      
time0 = time.time()


#Read command line parameters
try:
  opts, args = getopt.getopt(sys.argv[1:], "HMop:a:n:t:r:m:")
except getopt.GetoptError, err:
  Usage()
  sys.exit(2)
try:
  if not len(opts): Usage()
  for o,a in opts:
    if    o == "-H": Usage()
    if    o == "-o": mem_only     = True
    if    o == "-t": out_file     = a
    if    o == "-p": part_name    = a
    if    o == "-a": app_name     = a
    if    o == "-n": cycle        = int(a)
    if    o == "-M": manual       = True
    if    o == "-r": runTime      = int(a)
    if    o == "-m": description  = a
except(AttributeError): pass

if len(description)==0:
  print "\nERROR Missing mandatory parameter -m \n"
  Usage()
if len(part_name)==0:
  print "\nERROR Missing mandatory parameter -p \n"
  Usage()



#############################################################################################################
#                                             Transition                                                    #
#############################################################################################################
def transition(Command, Subtransition, CommandNumber):
  done  = True
  time_needed = 0
  start = time.time()
  if Subtransition==0:
    print "Sending command " + Command
    os.system('rc_sender -p %s -n %s -c %s NO_SUBTR' % (part_name, "RootController", Command))
  elif Subtransition!=0 and app_name=='RootController':
    done=False
    print "Skipping subtransition " + Command
  elif Subtransition==1:
    CommandSub = "SUB_TRANSITION " + Command + " START"
    print "Sending command " + Command
    os.system('rc_sender -p %s -n %s -c %s' % (part_name, app_name, CommandSub))
    
  elif Subtransition==-1:
    CommandSub = "SUB_TRANSITION " + Command + " CONFIGURE"
    print "Sending command " + Command
    os.system('rc_sender -p %s -n %s -c %s' % (part_name, app_name, CommandSub))

  while(done):
    time.sleep(5)
    it = ISInfoIterator(p, 'RunCtrl', ISCriteria(app_name))
    while it.next():
    # Read the IS server
      rm = ISInfoAny()
      it.value(rm)
      rm._update(it.name(), p)
      state = rm.lastTransitionName
#      done = False
      if Subtransition==0:
        if state == Command:
          time_needed = rm.transitionTime   
          done = False
      if (Subtransition==1) or (Subtransition==-1):
        if len(rm.lastTransitionArgs)>0 and rm.lastTransitionArgs[0]=="SUB_TRANSITION" and rm.lastTransitionArgs[1] == Command:
          time_needed = rm.transitionTime   
          done = False
  time_transition.append(float(time_needed))
  which_transition.append(Command)
  x_time_transition.append(CommandNumber)     
  #print "\t" + str(Command) + " transition done in " +str(time_needed)
  print "%15s %s done in %d ms" % (" ", Command, time_needed) 


# Get path to log files from IS
def getLogFilePaths(p, node, appname):
  key = "%s\\|%s" % (node, appname)
  it = ISInfoIterator(p, 'PMG', ISCriteria(key))
  out = ""
  while it.next():
    pmg = ISInfoAny()
    it.value(pmg)
    pmg._update(it.name(), p)
    out = pmg.std_out
  return out

# From IS get the host where an application is running
def getRcdHost(p, appName):
  key = "Supervision.%s" % appName
  it = ISInfoIterator(p, 'RunCtrl', ISCriteria(key))
  out = ""
  while it.next():
    rc = ISInfoAny()
    it.value(rc)
    rc._update(it.name(), p)
    out = rc.host
  return out

# Grab the files from the remote host to the current directory
def grab(node, fname, dryRun=False):
  cmd = "scp %s:%s ." % (node, fname)
  print "Executing:", cmd
  if dryRun:
    return 0
  ret = subprocess.call(cmd, shell=True)
  if ret != 0:
    print "ERROR: failed fetching the file from the remote node"
  return ret

# Write test results on log file
def WriteLogFile():
  output_filename = out_file + ".log"
  outfile = open(output_filename,"w")

  outfile.write('Test description: ' + description + '\n')
  
  for a in range(0, len(which_transition)):
    s = "Transition %-16s performed in %8.1f ms\n" % ( which_transition[a],  time_transition[a] )
    outfile.write(s)

  outfile.write("//////////////")
  outfile.write("Transition times")
  outfile.write("//////////////\n")
  cnt = 0;
  for a in range(0, len(traTime_vec)):
    msg = "end"
    if cnt%2==0: msg = "begin " + which_transition[a/2]
    s = "%8.1f \t %s\n" % ( traTime_vec[a], msg )
    outfile.write(s)
    cnt+=1

  outfile.write("//////////////")
  outfile.write("Memory Usage")
  outfile.write("//////////////\n")
  for a in range(0, len(mem_vec)):
    s = "%8.1f \t %9d\n" % ( resTime_vec[a],  mem_vec[a] )
    outfile.write(s)

  outfile.write("//////////////")
  outfile.write("CPU Usage")
  outfile.write("//////////////\n")
  for a in range(0, len(cpu_vec)):
    s = "%8.1f \t %9d\n" % ( resTime_vec[a],  cpu_vec[a] )
    outfile.write(s)

  outfile.close()


# Calculate y offset of the "transition lines banner"
def lowBannerOffset(v):
  d = max(v) - min(v)
  below = min(v) - d/20.
  return below 
  
# Create and dump the monitoring plots (mem and cpu)
def WriteMonPlot(vec, title, ytitle):
  off = lowBannerOffset(vec)
  c1 = TCanvas( title, title, 900, 700)
  gr1 = TGraph( len(vec), resTime_vec, vec)
  gr1.SetLineColor( 2 )
  gr1.SetLineWidth( 4 )
  gr1.SetMarkerColor( 4 )
  gr1.SetMarkerStyle( 21 )
  gr1.SetTitle( title )
  gr1.GetXaxis().SetTitle( 'Time' )
  gr1.GetYaxis().SetTitle( ytitle )
  gr1.GetYaxis().SetTitleOffset(1.2)
  gr1.Draw( 'ALP' )

  # Add transition lines: when transitions are occurring
  tmp=[] 
  for i in range(0,len(traTime_vec),2):
    l = TLine(traTime_vec[i], off, traTime_vec[i+1], off)
    l.SetLineWidth(10)
    c = 16 if i%4==0 else 38  # Alternate color for better readability
    l.SetLineColor(c)
    l.Draw()
    tmp.append(l)
  c1.Update()
  output_filename = "%s_%s.pdf" % (out_file, title.replace(" ",""))
  c1.SaveAs(output_filename)


# Create and dump the transition plot
def WriteTransitionsPlot():
  c = TCanvas("TransitionTime", "TransitionTime", 900, 700)    
  gr = TGraph( len(time_transition), x_time_transition, time_transition)
  gr.SetLineColor( 2 )
  gr.SetLineWidth( 4 )
  gr.SetMarkerColor( 4 )
  gr.SetMarkerStyle( 21 )
  gr.SetTitle( 'Transition Time' )
  c.SetBottomMargin(0.25)
  #gr.GetXaxis().SetTitle( 'Transition' )
  gr.GetYaxis().SetTitle( 'Transition Time (ms)' )
  gr.GetYaxis().SetRangeUser(0.5, max(time_transition))
  c.SetLogy()
  for a in range(0, len(Command)):
    gr.GetXaxis().SetBinLabel(gr.GetXaxis().FindBin(a+1), Command[a]);
  gr.Draw( 'AP' )
  output_filename = out_file + "_TransTime.pdf"
  c.Update()
  c.SaveAs(output_filename)

# Archive the results
#  1. go 1 directory up
#  2. create a tarball
#  3. identify the location
#  3.1 at P1 scp the tarball to p1ArchNode:p1ArchPath 
#  3.2 alt lab4 cp the tarball to eos
def archive(dryRun=True):
  # Create the tarball
  os.chdir("..")
  tgz = directory + ".tgz"
  cmd = "tar czf %s %s " % (tgz, directory)
  print "Executing:", cmd
  ret = subprocess.call(cmd, shell=True)
  if ret != 0:
    print "ERROR: failed creating the tarball"
    return False
  
  ip = socket.gethostbyname(socket.gethostname())
  print ip
  if ip.startswith('10.'): # P1
    print 'IP(', ip, ') starting with 10. => we are at P1.'
    print '@P1: Archiving in %s:%s' % (p1ArchNode, p1ArchPath) 
    cmd = "scp %s %s:%s" % (tgz, p1ArchNode, p1ArchPath)
    print "Executing:", cmd
    if dryRun:
      return True
    ret = subprocess.call(cmd, shell=True)
    if ret != 0:
      print "ERROR: failed sending tar file to", p1ArchNode
      return False
  
  else: #lab4 
    print 'IP(', ip, ') not starting with 10. => we are outside P1'
    print '@lab4: Archiving in eos: %s' % eosArchPath 
    cmd = "cp %s %s" % (tgz, eosArchPath)
    print "Executing:", cmd
    if dryRun:
      return True
    ret = subprocess.call(cmd, shell=True)
    if ret != 0:
      print "ERROR: failed copying tar file to eos"
      return False

        
#############################################################################################################
#                                         Memory and CPU check                                              #
#############################################################################################################
def MonitorResource():
  global time0
  mem = 0
  p = IPCPartition(part_name)
  if not p.isValid():
    print 'Partition', part_name, 'is not valid, abort.'
  else:  
    while ThreadRun:
      node_app = "DF.AppResourceMonitoring." + app_name 
      it = ISInfoIterator(p, 'DF', ISCriteria('.*'))
      rm = ISInfoAny()
      while it.next():
        if it.name()==node_app:
        
      # Read the IS server
          it.value(rm)
          rm._update(it.name(), p)
          mem = rm.MemoryUsage
          cpu = rm.CPUUsage
          mem_vec.append(mem)
          cpu_vec.append(cpu)
          resTime_vec.append(time.time()-time0)
          time.sleep(5)



#############################################################################################################
#                                               Main                                                        #
#############################################################################################################

os.system('clear')

if __name__ == '__main__':
  try:
    logsDone = False
    plotDone = False
    
    if app_name=='RootController':
      print "WARNING: Subtransitions will be skipped and no CPU/memory monitoring will be performed"
    d = threading.Thread(name='MonitorResource', target=MonitorResource)
    d.start()
     

    ############# Transitions #############
    if (mem_only==False):

      print 'Partition =', part_name
      p = IPCPartition(part_name)
      if not p.isValid():
        print 'Partition', part_name, 'is not valid, abort.'
        sys.exit(2)
       

      #SubTransition = [0, 0, 0, 1,1 ,0, 0, 0, 0]          
      SubTransition = [-1, 0, 0, 1,1 ,0, 0, 0, 0, 0, 0, 0, 0, 0]          
      # StatusReached = ["INITIAL","CONFIGURED", "CONNECTED", "FTK_CHECKCONNECT","FTK_startNoDF" ,"RUNNING", "ROIBSTOPPED", "DISCONNECT", "UNCONFIGURE"]
      # StatusReached = ["INIT_FSM","CONFIGURE", "CONNECT", "FTK_CHECKCONNECT","FTK_startNoDF" ,"START", "STOP", "DISCONNECT", "UNCONFIGURE"]   
      #Command = ["INITIALIZE", "CONFIGURE", "CONNECT", "FTK_CHECKCONNECT","FTK_startNoDF" ,"START", "STOP", "DISCONNECT", "UNCONFIGURE"]    
      Command = ["checkConfig", "CONFIGURE", "CONNECT", "checkConnect","startNoDF" ,"START", "STOPROIB", "STOPDC", "STOPHLT" , "STOPRECORDING", "STOPGATHERING", "STOPARCHIVING", "DISCONNECT", "UNCONFIGURE"]    
      #CommandNumber = [1,2,3,4,5,6,7,8,9]    
      CommandNumber = [1,2,3,4,5,6,7,8,9,10,11,12,13, 14]    
      # Command = ["INITIALIZE", "CONFIGURE", "CONNECT", "START", "STOP", "DISCONNECT", "UNCONFIGURE"]
      # Status  = ["INITIAL", "CONFIGURED", "CONNECTED", "RUNNING", "CONNECTED", "CONFIGURED", "INITIAL"]
      
      for x in range (0, cycle):
        for a,b,c in zip(Command, SubTransition, CommandNumber): 
          if manual:
            raw_input('Press enter to proceed with the next transition...')
          traTime_vec.append(time.time()-time0) # Log begin time
          transition(a, b, c)
          traTime_vec.append(time.time()-time0) # Log end time
          if a == "START":
            time.sleep(runTime)
          else:
            time.sleep(1)
        cmd = "SHUTDOWN"
        #os.system('rc_sender -p %s -n %s -c %s' % (part_name, app_name, cmd))
        time.sleep(10)

      ThreadRun = False      

      #Create a new directory for the results
      now = datetime.datetime.now()
      dt = now.strftime("%y%m%d.%Hh%Mm%Ss")
      directory = "%s.%s.%s" % (out_file, part_name, dt)
      r = os.mkdir(directory, 755)
      r = os.chmod(directory, 0755) # Be sure to prevent sticky bit
      os.chdir(directory)
     
    
    ############# Dump log #############
      for a in range(0, len(which_transition)):
        s = "Transition %-16s performed in %8.1f ms" % ( which_transition[a],  time_transition[a] )
        print s
      WriteLogFile()
      logsDone = True

    ############# Plots #############
      WriteTransitionsPlot()
      if app_name!='RootController':
        WriteMonPlot(mem_vec, 'MemUsage', 'Memory Usage (kB)')
        WriteMonPlot(cpu_vec, 'CpuUsage', 'CPU (%)')
      plotDone = True
      
    ############# Fetch and save application log files #############
      n = getRcdHost(p, app_name)
      logfilebase=getLogFilePaths(p, n, app_name)
      r = grab(n, logfilebase[:-4]+".*")
      if r != 0:
        print "ERROR fetching the log files,", r

    ############# Archive data  #############
      archive(dryRun=False) # i.e. Default is dryRun
      #archive()

      time.sleep(5)
      sys.exit(0)
    else:
      print "Memory only monitoring, to close press Control+C"
      print "Histograms will be saved in the current directory"
      while(ThreadRun):
        time.sleep(1)


  except (KeyboardInterrupt, SystemExit):
    if not logsDone: 
      WriteLogFile()
    if not plotDone:
      WriteMonPlot(mem_vec, 'MemUsage', 'Memory Usage (kB)')
      WriteMonPlot(cpu_vec, 'CpuUsage', 'CPU (%)')
    os.chdir("..")

    print "quitting... cleaning threads"
    ThreadRun = False

