#!/bin/bash
#################################################################################
##
##   Script for the checkout of a FTK PU
##      Check communication with board, update the firmware to a standard version
##      Run a partition, take a SoloRun, copy the log files 
##      Parse log files and determine if run was sucessful
##
##   Author: Lesya Horyn (lesya.anna.horyn@cern.ch)
##           Paolo Mastrandrea (paolo.mastrandrea@cern.ch)
##
##################################################################################

##### TO BE CHECKED
#
# in FW reprogramming
#
# ## 1 ##  AUX FW version to be updated
# ## 2 ##  ABSOLUTE PATH TO BE REMOVED ONCE THE SCRIPT WILL BE AVAILABLE DIRECTLY FROM THE RELEASE
#


####### Service function - ftk_PU_checkout.sh help
#========================================================================

function ftk_PU_checkout_help {
    echo ""
    echo "Options for ftk_PU_checkout.sh: "
    echo ""
    echo "     -h               : Print this help"
    echo ""
    echo "     -C <#sbc>        : Specify the number of one sbc [P1: 1..8, LAB4: 1,2]"
    echo "     -s <slot>        : Specify the slots (comma separated - no space); if no slot is specified the full Run3 crate is assumed"
    echo ""
    echo "     -D               : Run dataflow test"
    echo "     -p <pname>       : Specify a partition; default is ProcessingUnit"
    echo "     -S <segname>     : Specify the segment name; default is FTK"
    echo "     -t <time (sec)>  : Specify time to run partition; default is 60s"
    echo "     -n <iters>       : Specify number of times to run a partition; default is 1"
    echo "     -l               : Keep local the output from SoloRun"
    echo ""
    echo " - EXPERT ONLY "
    echo ""
    echo "     -r               : Reconfigure AUX"
    echo "     -u               : Update AUX firmware"
    echo "     -v <ver>         : Set AUX firmware version to <ver>; default is 4.2"
    echo "     -m               : Update AMB firmware"
    echo "     -x               : Set LAMB version to 4; default is 5 - ATTENTION: it's user responsibility to check LAMB version in advance !!"
    echo ""
}


####### Service function - AUX reconfigure & PU FW update
#========================================================================

function ftk_PU_reconfigure_and_update {

    F1_SBC=$1
    F1_SLOT=$2
    F1_AUX_RECONFIGURE=$3
    F1_AUX_FW_UPDATE=$4
    F1_AMB_FW_UPDATE=$5
    F1_LAMB_VERSION=$6

    RUNNING_DIR=`pwd`

    ## 2 ##  ABSOLUTE PATH TO BE REMOVED ONCE THE SCRIPT WILL BE AVAILABLE DIRECTLY FROM THE RELEASE
#    CODE_DIR="/atlas-home/1/mastrand/development/FTK-04-00-01-update_01_PUcheckout/PUCheckout"


    F1_AMB_OPT="-s ${F1_SLOT}"
    [[ $F1_LAMB_VERSION == 4 ]]  &&  F1_AMB_OPT=$F1_AMB_OPT" -x"

    echo ""
    echo " reconfigure AUX and update PU FW "
    echo ""

    F1_LOG="ftk_PU_reconfigure_and_update-slot-${F1_SLOT}.log"
    echo "" > $F1_LOG
    echo "Log file for PU reconfigure and update - sbc ${F1_SBC} - slot ${F1_SLOT}" >> $F1_LOG
    echo "" >> $F1_LOG


    ### Setup

    F1_SCRIPT="ftk_PU_reconfigure_and_update-slot-${F1_SLOT}"
    echo "" > $F1_SCRIPT
    echo "cd ${RUNNING_DIR}" >> $F1_SCRIPT
    echo "" >> $F1_SCRIPT

    if [[ $F1_SBC == sbc-tbed-ftk* ]]
    then
	echo "source /tbed/oks/tdaq-07-01-00/FTK/${REL}/ftk/setup_partition_lab4_tdaq7.sh" >> $F1_SCRIPT
	F1_AMB_OPT=$F1_AMB_OPT" -l"
    fi

    if [[ $F1_SBC == sbc-ftk-rcc* ]]
    then
#	echo "source /det/ftk/ftk_setup.sh FTK-04-00-01" >> $F1_SCRIPT    # NOTE: no partition defined at this level
	echo "source /det/ftk/ftk_setup.sh ${REL}" >> $F1_SCRIPT    # NOTE: no partition defined at this level
    fi

    ### AUX - reconfigure
    if [ $F1_AUX_RECONFIGURE = true ]
    then
	echo "" >> $F1_SCRIPT
        echo "echo \"[ftk_PU_checkout.sh] Going to reflash AUX FW in slot ${F1_SLOT}\" " >> $F1_SCRIPT
        echo "reconfigureBoardAux.py --slot ${F1_SLOT}" >> $F1_SCRIPT
        echo "sleep 1" >> $F1_SCRIPT
    fi

    ### AUX - update FW & reconfigure
    if [ $F1_AUX_FW_UPDATE = true ]
    then
	echo "" >> $F1_SCRIPT
#        echo "echo \"[ftk_PU_checkout.sh] Going to change AUX FW in slot ${F1_SLOT} to fw04.2\" "  >> $F1_SCRIPT   ## 1 ## AUX FW version to be updated
        echo "echo \"[ftk_PU_checkout.sh] Going to change AUX FW in slot ${F1_SLOT} to ${AUX_FW_VER}\" "  >> $F1_SCRIPT   ## 1 ## AUX FW version to be updated
#        echo "upgradeFirmwareAux.py --slot ${F1_SLOT} --fw 04.2" >> $F1_SCRIPT
        echo "upgradeFirmwareAux.py --slot ${F1_SLOT} --fw ${AUX_FW_VER}" >> $F1_SCRIPT
        echo "reconfigureBoardAux.py --slot ${F1_SLOT}" >> $F1_SCRIPT
        echo "sleep 1" >> $F1_SCRIPT
    fi

    ### AMB - update FW
    if [ $F1_AMB_FW_UPDATE = true ]
    then
	echo "" >> $F1_SCRIPT
        echo "echo \"[ftk_PU_checkout.sh] Going to reprogram AMB FW in slot ${F1_SLOT}\" " >> $F1_SCRIPT
        echo "${CODE_DIR}/amb/ftk_reprogramAMB.sh ${F1_AMB_OPT}" >> $F1_SCRIPT
        echo "sleep 1" >> $F1_SCRIPT
    fi

    echo ""
    cat $F1_SCRIPT
    echo ""

    ssh $F1_SBC  < $F1_SCRIPT 2>&1 | tee $F1_LOG
}


####### Service function - check error summary
#========================================================================

function ftk_PU_error_summary {

    INPUTFILE=$1

    TMP_ERR_FILE="tmp_error_file.to_be_removed"

    grep ERROR $INPUTFILE | grep -v "was caused by" > $TMP_ERR_FILE

    printf '\n'
    printf '%5s %10s %10s %10s %13s \n' "Slot" "AMB" "AUX" "PU" "tot_slot"

    for S in ${PU_SLOTS[@]}
    do

        FTK_AMB="FTK-AMB-${SBC_NUMBER}-${S}"
        FTK_AUX="FTK-AUX-${SBC_NUMBER}-${S}"
        FTK_PU="PU-${SBC_NUMBER}-0${S}"
        [[ $S -gt 9 ]] && FTK_PU="PU-${SBC_NUMBER}-${S}"

        SLOT_ERRS=`cat $TMP_ERR_FILE | grep "${SBC_NUMBER}-${S}\|${SBC_NUMBER}-0${S}" | wc -l`
        AMB_ERRS=` cat $TMP_ERR_FILE | grep ${FTK_AMB} | wc -l`
        AUX_ERRS=` cat $TMP_ERR_FILE | grep ${FTK_AUX} | wc -l`
        PU_ERRS=`  cat $TMP_ERR_FILE | grep ${FTK_PU}  | wc -l`

        printf '%5s %10i %10i %10i %10i\n' "${SBC_NUMBER}-${S}" ${AMB_ERRS} ${AUX_ERRS} ${PU_ERRS} ${SLOT_ERRS}

    done


    TOT_ERRS=`cat $TMP_ERR_FILE | wc -l`
    OTH_ERRS=`cat $TMP_ERR_FILE | grep -v FTK-AMB | grep -v FTK-AUX | grep -v FTK:PU | wc -l`
    printf '\n'
    printf ' Other ERRORS :    %10i \n' ${OTH_ERRS}
    printf ' TOT          :    %10i \n' ${TOT_ERRS}
    printf '\n'

    rm -f $TMP_ERR_FILE

}     # ftk_PU_error_summary



####### Service function - run PU dataflow test
#========================================================================

function ftk_PU_run_dataflow {

    # Possible improvement : get SBC's and slots directly from partition

    F2_SBC=$1
    F2_PNAME=$2
    F2_RUNTIME=$3
    F2_SEGNAME=$4
    F2_ITERS=$5

    F2_HERE=`pwd`

    echo ""
    echo " ----------------------------- > RUN DATAFLOW TEST "
    echo " ---> crate       ${F2_SBC}     "
    echo " ---> partition   ${F2_PNAME}   "
    echo " ---> segment     ${F2_SEGNAME} "
    echo " ---> runtime     ${F2_RUNTIME} "
    echo " ---> iterations  ${F2_ITERS}   "
    echo ""


    logDir="/logs/${CMTRELEASE}/${F2_PNAME}/"


#    for i in $(seq 1 $F2_ITERS);
#    do
#	echo
#	echo "[ftk_PU_checkout.sh] Running test $i"
#
#        ### Setup a dir for our testing  ###  WORKINGDIR to be redefined as a passed parameter
#	outDir="$WORKINGDIR/run_$i"
    
	echo
	echo "[ftk_PU_checkout.sh] Running test "

        ### Setup a dir for our testing  ###  WORKINGDIR to be redefined as a passed parameter
	outDir="$WORKINGDIR/run"
    
	echo "[ftk_PU_checkout.sh] creating $outDir"
	mkdir $outDir
	mkdir $outDir/logFiles

	cd $outDir


	### Setup script to retrieve log files
	F2_SCRIPT="retrieve_log_files"
	echo "" > $F2_SCRIPT

	echo "errFile=\$(ls -t ${logDir}*.err | head -${F2_ITERS})" >> $F2_SCRIPT
	echo "outFile=\$(ls -t ${logDir}*.out | head -${F2_ITERS})" >> $F2_SCRIPT
       
	echo "cp \$errFile ${outDir}/logFiles/." >> $F2_SCRIPT
	echo "cp \$outFile ${outDir}/logFiles/." >> $F2_SCRIPT


        ### Setup and run the partition for specified amount of time, take a SoloRun, TODO: check rate to ensure board is running

	if [ $LOCAL_SOLORUN = true ]
	then
	    echo "../../launch_partition.py -p ${F2_PNAME} -t ${F2_RUNTIME} -S ${F2_SEGNAME} -l"
	    ../../launch_partition.py -p ${F2_PNAME} -t ${F2_RUNTIME} -S ${F2_SEGNAME} -n ${F2_ITERS} -l
	else
	    echo "../../launch_partition.py -p ${F2_PNAME} -t ${F2_RUNTIME} -S ${F2_SEGNAME}"
 	    ../../launch_partition.py -p ${F2_PNAME} -t ${F2_RUNTIME} -S ${F2_SEGNAME} -n ${F2_ITERS}
	fi

        ### get most recent log and error files
        ##NOTE: working for a single host/sbc
	echo "[ftk_PU_checkout.sh] Getting log files from $host"
   
	echo "s-s-h ${F2_SBC} < ${F2_SCRIPT}"
	ssh ${F2_SBC} < ${F2_SCRIPT}


	### Check AUX results
#	AUX_RES=$(checkErrors_aux.py $outDir/logFiles/*.err $outDir/logFiles/*.out)
#	echo "[ftk_PU_checkout.sh] AUX: Run $i : $AUX_RES"

	### Check AMB results
	#AMB_RES=##to be implemented
	#echo "[ftk_PU_checkout.sh] AMB: Run $i : $AMB_RES"


	ERR_LOG_FILE=`ls logFiles/*.err`

        ftk_PU_error_summary $ERR_LOG_FILE

	cd $F2_HERE

#    done
}    # ftk_PU_run_dataflow



#######################################################################
### MAIN
#######################################################################

if [[ $# -lt 1 ]]
then
    echo ""
    echo "[ftk_PU_checkout.sh]  This script needs options! Please run  \" ./ftk_PU_checkout.sh -h \"  for help!"
    echo ""
    exit
fi


### Configuration variables
#===========================================================

dateTime=`date +%Y%m%d_%H%M%S`
HERE=$(pwd)
CODE_DIR=$HERE

LAB="NO_LAB"
[[ $HOSTNAME == pc-tbed* ]]  && LAB="LAB4"
[[ $HOSTNAME == pc-atlas* ]] && LAB="P1"

SLOT=0
SBC_NAME="NONE"
SBC_NUMBER=0
AUX_RECONFIGURE=false
AUX_FW_UPDATE=false
AMB_FW_UPDATE=false
AUX_FW_VER="fw04.2"   ## 1 ## AUX FW version to be updated

AMB_LAMB_VERSION=5

RUN_DATAFLOW=false
PNAME="ProcessingUnit"
SEGNAME="FTK"
REL=$FTK_RELEASE
RUNTIME=60
ITERS=1
LOCAL_SOLORUN=false


P1_SBCS=(1 2 3 4 5 6 7 8)
LAB4_SBCS=(1 2)

PU_SLOTS_RUN3=(2 3 4 5 7 8 9 10 12 13 14 15 17 18 19 20)
PU_SLOTS_RUN2=(2 4 7 9 12 14 17 19)

PU_SLOTS=(${PU_SLOTS_RUN3[@]})
SBCS=(0)

[[ $LAB == "P1" ]]   && SBCS=(${P1_SBCS[@]})
[[ $LAB == "LAB4" ]] && SBCS=(${LAB4_SBCS[@]})

if [[ $LAB == "NO_LAB" ]]
then
    echo "[ftk_PU_checkout.sh] Running on invalib machine : ${HOSTNAME}  --  running in P1 or LAB4  *ONLY* "
    exit
fi


### Option parsing
#===========================================================

WRONG_CONFIGURATION=false

while getopts ":humDlC:xs:v:p:S:t:rn:" opt; do
    case $opt in
        h)
	    ftk_PU_checkout_help
            exit
            ;;  
        C)
            SBC_NUMBER=$OPTARG
            ;;
        s)
	    PU_SLOTS=(`echo $OPTARG | sed -e s/,/\ /g`)
            ;;
        r)
            AUX_RECONFIGURE=true
            ;;
        u)
            AUX_FW_UPDATE=true
            ;;
        v)
            AUX_FW_VER=$OPTARG
            ;;
        m)
            AMB_FW_UPDATE=true
            ;;
        r)
            AUX_RECONFIGURE=true
            ;;
        x)
            AMB_LAMB_VERSION=4
            ;;
        D)
            RUN_DATAFLOW=true
            ;;
        p)
            PNAME=$OPTARG
            ;;
        S)  
            SEGNAME=$OPTARG
            ;;
        t)
            RUNTIME=$OPTARG
            ;;
        n)
            ITERS=$OPTARG
            ;;
        l)
            LOCAL_SOLORUN=true
            ;;
        \?)
            echo "[ftk_PU_checkout.sh] Invalid option: -$OPTARG"
	    WRONG_CONFIGURATION=true
            ;;
    esac
done

[ $WRONG_CONFIGURATION = true ] && exit


### Update configuration variables & range check
#===========================================================

if [[ " ${SBCS[@]} " =~ " ${SBC_NUMBER} " ]]
then
    [[ $LAB == "P1" ]]   && SBC="sbc-ftk-rcc-0"$SBC_NUMBER
    [[ $LAB == "LAB4" ]] && SBC="sbc-tbed-ftk-0"$SBC_NUMBER
else
    echo -e "*** sbc  ${SBC}  is NOT a good sbc for FTK in ${LAB} ! "
    exit
fi


### Checking slots validity
#===========================================================

for SLOT in ${PU_SLOTS[@]}
do
    if [[ ! " ${PU_SLOTS_RUN3[@]} " =~ " ${SLOT} " ]]
    then
	echo -e "*** Slot  ${SLOT}  is NOT a good PU slot! "
	exit
    fi
done



### Setup directory for outputs
#===========================================================

WORKINGDIR="$(pwd)/ftk_PU_checkout_${SBC}_${dateTime}"

if [ -d $WORKINGDIR ];
then
    echo "[ftk_PU_checkout.sh]  Working directory  ${WORKINGDIR}  already present -> exit!"
    exit
fi

mkdir $WORKINGDIR
cd $WORKINGDIR


### Configuration summary
#===========================================================

SUMMARY_FILE="PUcheckout_configuration_summary.txt"

echo "[ftk_PU_checkout.sh] --- Configuration summary ---"             > $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  LAB              =  $LAB"                >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  FTK release      =  $REL"                >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  sbc              =  $SBC"                >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  slot             =  ${PU_SLOTS[@]}"      >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  # slots          =  ${#PU_SLOTS[@]}"     >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  "                                        >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  AUX reconfigure  =  $AUX_RECONFIGURE"    >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  AUX FW update    =  $AUX_FW_UPDATE"      >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  AMB FW update    =  $AMB_FW_UPDATE"      >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  LAMB version     =  $AMB_LAMB_VERSION"   >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  "                                        >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  Run dataflow     =  $RUN_DATAFLOW"       >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  Partition        =  $PNAME"              >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  Segment          =  $SEGNAME"            >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  Runtime          =  $RUNTIME"            >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  Iterations       =  $ITERS"              >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  Local SoloRun    =  $LOCAL_SOLORUN"      >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  "                                        >> $SUMMARY_FILE
echo "[ftk_PU_checkout.sh]  WORKINGDIR       =  $WORKINGDIR"         >> $SUMMARY_FILE
echo ""                                                              >> $SUMMARY_FILE

echo ""
cat $SUMMARY_FILE



### Reconfigure and reprogram
#===========================================================

if [ $AUX_RECONFIGURE = true ] || [ $AUX_FW_UPDATE = true ] || [ $AMB_FW_UPDATE = true ]
then
    for I_SLOT in ${PU_SLOTS[@]}
    do
	echo ""
	echo "[ftk_PU_checkout.sh] --- Going to reconfigure & update  $SBC  slot  $I_SLOT "
	echo ""
	ftk_PU_reconfigure_and_update $SBC  $I_SLOT  $AUX_RECONFIGURE  $AUX_FW_UPDATE  $AMB_FW_UPDATE  $AMB_LAMB_VERSION
    done
else
	echo "[ftk_PU_checkout.sh] --- No reconfiguration and/or reprogram requested "
fi



### Prepare Partition OKS
#===========================================================

mkdir oks
cp -r ../oks_PUCheckout/$LAB/ftk/ oks/.
cp ../oks_PUCheckout/setup_partition oks/.

PARTITION_FILE=oks/ftk/partitions/FTK-PU.data.xml
TEMP_PARTITION_FILE=.partition.tmp

DISABLED_STRING=""
DISABLED_N=0

for J_CRATE in ${SBCS[@]}
do
    if [[ $J_CRATE == ${SBC_NUMBER} ]]
    then
	# Check if any slot in this crate needs to be disabled
	for J_SLOT in ${PU_SLOTS_RUN3[@]}
	do
            if [[ ! " ${PU_SLOTS[@]} " =~ " ${J_SLOT} " ]]
	    then
		# Disable the ReadoutModule for this PU
		if [[ $J_SLOT -lt 10 ]]
		then
		    DISABLED_STRING=$DISABLED_STRING'  "ReadoutModule_PU" "PU-'${J_CRATE}'-0'${J_SLOT}'"\n'
		else
		    DISABLED_STRING=$DISABLED_STRING'  "ReadoutModule_PU" "PU-'${J_CRATE}'-'${J_SLOT}'"\n'
		fi
		DISABLED_N=$(( ${DISABLED_N} + 1 ))
	    fi
	done
    else
	# Disable the RCD for this crate
	DISABLED_STRING=$DISABLED_STRING'  "RCD" "FTK-RCD-VME-'${J_CRATE}'"\n'
	DISABLED_N=$(( ${DISABLED_N} + 1 ))
    fi
done

DISABLED_STRING=' <rel name="Disabled" num="'${DISABLED_N}'">\n'${DISABLED_STRING}' <\/rel>'

#cat $PARTITION_FILE | sed -e s/' <rel name="Disabled" num="0"><\/rel>'/"${DISABLED_STRING}"/g > $TEMP_PARTITION_FILE
#cat $TEMP_PARTITION_FILE | sed -e s/'_PU_PARTITION_NAME_'/"${PNAME}"/g > $TEMP_PARTITION_FILE


cat $PARTITION_FILE | sed -e s/' <rel name="Disabled" num="0"><\/rel>'/"${DISABLED_STRING}"/g | sed -e s/'_PU_PARTITION_NAME_'/"${PNAME}"/g > $TEMP_PARTITION_FILE


mv -f $TEMP_PARTITION_FILE $PARTITION_FILE

source oks/setup_partition

#echo " ----------- rel = " $CMTRELEASE



### Run dataflow test
#===========================================================

if [ $RUN_DATAFLOW = true ]
then
    ftk_PU_run_dataflow $SBC  $PNAME  $RUNTIME  $SEGNAME  $ITERS
else
    echo "[ftk_PU_checkout.sh] --- No dataflow test requested "
fi


cd $HERE

echo "[ftk_PU_checkout.sh] All done! Remember to kill the parition if you are done and everything completed successfully"

exit


