#!/bin/bash
#
# Script used to program the AMB boards remotely. 
#
#Author: Simone Sottocornola, Paolo Mastrandrea
#mail: simone.sottocornola@cern.ch, paolo.mastrandrea@cern.ch
#CERN, 26/01/2018, 02/04/2019
#
# - For LAMB FW merged (1 file for 4 LAMBs) is expected
# - Specific version supported for 1 FW at a time ONLY
#

######################################################################

function reprogram_fw_ambslp {

    RFW_FPGA=$1
    RFW_FW_VERS=$2
    RFW_FW_TARGET=$3
    RFW_FORCE=$4

    RFW_FW_FILE=none

    RFW_FW_ENTRY=0

    RFW_FW_PRE=none

    FW_PRE_VME=0
    FW_PRE_CTR=0
    FW_PRE_ROA=0
    FW_PRE_HIT=0
    FW_PRE_LAM=0

    FW_POST_VME=0
    FW_POST_CTR=0
    FW_POST_HIT=0
    FW_POST_LAM=0
    FW_POST_ROA=0

    echo ""
    echo " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   $RFW_FPGA  FPGA"
    echo ""

    for RFW_FW_ENTRY in ${FWS_BIT[@]}
    do
	RFW_NAME=`echo $RFW_FW_ENTRY | awk -F: ' { print $1 } '`
	RFW_VERS=`echo $RFW_FW_ENTRY | awk -F: ' { print $2 } '`
	RFW_TARG=`echo $RFW_FW_ENTRY | awk -F: ' { print $3 } '`
	RFW_FILE=`echo $RFW_FW_ENTRY | awk -F: ' { print $4 } '`

	if [ $RFW_NAME = $RFW_FPGA ] && [ $RFW_VERS = $RFW_FW_VERS ] && [ $RFW_TARG = $RFW_FW_TARGET ]
	then
#	    echo "FW found  :  $RFW_NAME $RFW_VERS $RFW_FILE"
	    RFW_FW_FILE=$RFW_FILE
	    break
	fi
    done


    CAN_REPROGRAM=true


    if [ $RFW_FW_FILE = none ]
    then
	echo "NO FW file found in DB for  $RFW_FPGA  $RFW_FW_VERS "
	CAN_REPROGRAM=false
    elif [[ ! -f $SVF_DIR/$RFW_FW_FILE ]]
    then
	echo "NO FW file found :  $SVF_DIR/$RFW_FW_FILE "
	CAN_REPROGRAM=false
    fi


    if [ $CAN_REPROGRAM = true ]
    then
	ambslp_status_main -s $SLOT > $TMPF 2>&1

	FW_PRE_VME=`grep Firmware $TMPF | grep VME     | awk '{ print $4 }'`
	FW_PRE_CTR=`grep Firmware $TMPF | grep CONTROL | awk '{ print $4 }'`
	FW_PRE_ROA=`grep Firmware $TMPF | grep ROAD    | awk '{ print $4 }'`
	FW_PRE_HIT=`grep Firmware $TMPF | grep Hit     | awk '{ print $4 }'`
	FW_PRE_LAM=`grep Firmware $TMPF | grep LAMB    | awk '{ print $8 }'`

	rm -f $TMPF

	[ $RFW_FPGA = CTR ] && RFW_FW_PRE=$FW_PRE_CTR
	[ $RFW_FPGA = ROA ] && RFW_FW_PRE=$FW_PRE_ROA
	[ $RFW_FPGA = HIT ] && RFW_FW_PRE=$FW_PRE_HIT
	[ ${RFW_FPGA:0:3} = LAM ] && RFW_FW_PRE=$FW_PRE_LAM

	if [ $RFW_FW_PRE != $RFW_FW_VERS ] || [ $RFW_FORCE = true ]
	then
	    echo "Reprogram   $RFW_FPGA   $RFW_FW_VERS   with   $RFW_FW_FILE " | tee $LOGDIR/$RFW_FPGA.log
	    echo "Slot $SLOT " | tee -a $LOGDIR/$RFW_FPGA.log
	    echo "" | tee -a $LOGDIR/$RFW_FPGA.log

	    if [ ${RFW_FPGA:0:3} = LAM ]
	    then
		ambslp_svfplayer --lamb --slot $SLOT -S $SVF_DIR/$RFW_FW_FILE 2>&1 | tee -a $LOGDIR/$RFW_FPGA.log
	    else
		ambslp_svfplayer --bt   --slot $SLOT -S $SVF_DIR/$RFW_FW_FILE 2>&1 | tee -a $LOGDIR/$RFW_FPGA.log
	    fi


	    ambslp_status_main -s $SLOT > $TMPF 2>&1

	    FW_POST_VME=`grep Firmware $TMPF | grep VME     | awk '{ print $4 }'`
	    FW_POST_CTR=`grep Firmware $TMPF | grep CONTROL | awk '{ print $4 }'`
	    FW_POST_HIT=`grep Firmware $TMPF | grep Hit     | awk '{ print $4 }'`
	    FW_POST_LAM=`grep Firmware $TMPF | grep LAMB    | awk '{ print $8 }'`
	    FW_POST_ROA=`grep Firmware $TMPF | grep ROAD    | awk '{ print $4 }'`

	    rm -f $TMPF

	else
	    echo "FW  $RFW_FW_VERS  already installed on  $RFW_FPGA  and NO Force option requested. Skip reprogram. "
	fi

	echo ""
	printf ' FW version:       %12s %12s %12s %12s %12s \n' "VME" "CTR" "ROA" "HIT" "LAM"
	printf ' Slot     %-5s  pre  %12s %12s %12s %12s %12s \n' "$SLOT" "$FW_PRE_VME" "$FW_PRE_CTR" "$FW_PRE_ROA" "$FW_PRE_HIT" "$FW_PRE_LAM"

	[ $FW_POST_VME != 0 ] && printf ' Slot     %-5s  post %12s %12s %12s %12s %12s \n' "$SLOT" "$FW_POST_VME" "$FW_POST_CTR" "$FW_POST_ROA" "$FW_POST_HIT" "$FW_POST_LAM"

	echo ""
	echo " ========================================================================= "
	echo ""
    fi
}

######################################################################

AMB_SLOTS=(2 3 4 5 7 8 9 10 12 13 14 15 17 18 19 20)
LAB="P1"
LOAD_BIT="true"
LOAD_TARGET="fpga"
FORCE=false

REPROGRAM_CTR=false
REPROGRAM_ROA=false
REPROGRAM_HIT=false
REPROGRAM_LAM=false
REPROGRAM_ALL=true

FPGA_TO_REPROGRAM=none
VERS_TO_REPROGRAM=0

LAMB_VERSION=5
LAMB_FILE_TYPE="merged"

AMB_FW_SET=fw05
SVF_DIR_P1=/det/ftk/repo/fw/AMB/$AMB_FW_SET
SVF_DIR_LAB4=/afs/cern.ch/user/m/mastrand/workspace/FTK/FW/FW_programming/ftk_SVF_files

SLOT=0


# Available FWs ---------------------------------------------------------------
# note: the second field in FWS_BIT entry needs to match the baseline FWs (case sensitive)

FWS_BIT=()

### fpga

FWS_BIT+=('CTR:0x0000000a:fpga:CTRL/CTRL_v0000000A_20180705.svf')

FWS_BIT+=('ROA:0x24a50b93:fpga:ROAD/ROADv4v5_v24A50B93_20180904.svf')
FWS_BIT+=('ROA:0xc4251481:fpga:ROAD/ROADv4v5_vC4251481_20180824.svf')

FWS_BIT+=('HIT:0x3ca5296c:fpga:HIT/HITv4v5_v3CA5296C_2018.svf')

FWS_BIT+=('LAM_4_LAMB0:0x06060606:fpga:LAMB/LAMBv3v4_v06_20180329_LAMB0.svf')
FWS_BIT+=('LAM_4_LAMB1:0x06060606:fpga:LAMB/LAMBv3v4_v06_20180329_LAMB1.svf')
FWS_BIT+=('LAM_4_LAMB2:0x06060606:fpga:LAMB/LAMBv3v4_v06_20180329_LAMB2.svf')
FWS_BIT+=('LAM_4_LAMB3:0x06060606:fpga:LAMB/LAMBv3v4_v06_20180329_LAMB3.svf')
FWS_BIT+=('LAM_4_merged:0x06060606:fpga:LAMB/LAMBv3v4_v06_20180329_merged.svf')

FWS_BIT+=('LAM_4_merged:0x07070707:fpga:LAMB/LAMBv3v4_v07_201903_merged.svf')

FWS_BIT+=('LAM_5_LAMB0:0x06060606:fpga:LAMB/LAMBv5_v06_20180329_LAMB0.svf')
FWS_BIT+=('LAM_5_LAMB1:0x06060606:fpga:LAMB/LAMBv5_v06_20180329_LAMB1.svf')
FWS_BIT+=('LAM_5_LAMB2:0x06060606:fpga:LAMB/LAMBv5_v06_20180329_LAMB2.svf')
FWS_BIT+=('LAM_5_LAMB3:0x06060606:fpga:LAMB/LAMBv5_v06_20180329_LAMB3.svf')
FWS_BIT+=('LAM_5_merged:0x06060606:fpga:LAMB/LAMBv5_v06_20180329_merged.svf')

FWS_BIT+=('LAM_5_merged:0x07070707:fpga:LAMB/LAMBv5_v07_201903_merged.svf')

### flash_memory

FWS_BIT+=('CTR:0x0000000a:flash_memory:CTRL/CTRL_v0000000A_20180705_flash.svf')

FWS_BIT+=('ROA:0xc4251481:flash_memory:ROAD/ROADv4v5_vC4251481_20180824_flash.svf')
FWS_BIT+=('ROA:0x24a50b93:flash_memory:ROAD/ROADv4v5_v24A50B93_20180904_flash.svf')

FWS_BIT+=('HIT:0x3ca5296c:flash_memory:HIT/HITv4v5_v3CA5296C_2018_flash.svf')

FWS_BIT+=('LAM_4_LAMB0:0x06060606:flash_memory:LAMB/LAMBv3v4_v06_20180329_LAMB0_flash.svf')
FWS_BIT+=('LAM_4_LAMB1:0x06060606:flash_memory:LAMB/LAMBv3v4_v06_20180329_LAMB1_flash.svf')
FWS_BIT+=('LAM_4_LAMB2:0x06060606:flash_memory:LAMB/LAMBv3v4_v06_20180329_LAMB2_flash.svf')
FWS_BIT+=('LAM_4_LAMB3:0x06060606:flash_memory:LAMB/LAMBv3v4_v06_20180329_LAMB3_flash.svf')

FWS_BIT+=('LAM_4_LAMB0:0x07070707:flash_memory:LAMB/LAMBv3v4_v07_201903_LAMB0_flash.svf')
FWS_BIT+=('LAM_4_LAMB1:0x07070707:flash_memory:LAMB/LAMBv3v4_v07_201903_LAMB1_flash.svf')
FWS_BIT+=('LAM_4_LAMB2:0x07070707:flash_memory:LAMB/LAMBv3v4_v07_201903_LAMB2_flash.svf')
FWS_BIT+=('LAM_4_LAMB3:0x07070707:flash_memory:LAMB/LAMBv3v4_v07_201903_LAMB3_flash.svf')
FWS_BIT+=('LAM_4_merged:0x07070707:flash_memory:LAMB/LAMBv3v4_v07_201903_merged_flash.svf')

FWS_BIT+=('LAM_5_LAMB0:0x06060606:flash_memory:LAMB/LAMBv5_v06_20180329_LAMB0_flash.svf')
FWS_BIT+=('LAM_5_LAMB1:0x06060606:flash_memory:LAMB/LAMBv5_v06_20180329_LAMB1_flash.svf')
FWS_BIT+=('LAM_5_LAMB2:0x06060606:flash_memory:LAMB/LAMBv5_v06_20180329_LAMB2_flash.svf')
FWS_BIT+=('LAM_5_LAMB3:0x06060606:flash_memory:LAMB/LAMBv5_v06_20180329_LAMB3_flash.svf')

FWS_BIT+=('LAM_5_LAMB0:0x07070707:flash_memory:LAMB/LAMBv5_v07_201904_LAMB0_flash.svf')
FWS_BIT+=('LAM_5_LAMB1:0x07070707:flash_memory:LAMB/LAMBv5_v07_201904_LAMB1_flash.svf')
FWS_BIT+=('LAM_5_LAMB2:0x07070707:flash_memory:LAMB/LAMBv5_v07_201904_LAMB2_flash.svf')
FWS_BIT+=('LAM_5_LAMB3:0x07070707:flash_memory:LAMB/LAMBv5_v07_201904_LAMB3_flash.svf')
FWS_BIT+=('LAM_5_merged:0x07070707:flash_memory:LAMB/LAMBv5_v07_201904_merged_flash.svf')


# Baseline FWs

CTR_FW=0x0000000a
ROA_FW=0x24a50b93
HIT_FW=0x3ca5296c
LAM_FW=0x07070707


# Parsing options --------------------------------------------------------------

while getopts ":s:hlfFCRHLxn:v:" opt; do
    case $opt in
        h)
            echo ""
            echo "Options for reprogramAMB.sh: "
            echo "     -s <slot>  : Slot to reprogram"
	    echo "     -h         : print this help"
	    echo "     -l         : set laboratory to Lab4 (otherwise P1 is assumed)"
	    echo "     -f         : program flash memory only (otherwise FPGA only will be programmed)"
	    echo "     -F         : force reprogramming even if target FW version is already on the FPGA"
	    echo "     -C         : program CTRL"
	    echo "     -R         : program ROAD"
	    echo "     -H         : program HIT"
	    echo "     -L         : program LAMB"
	    echo "     -x         : set LAMB version to v4 (otherwise v5 is assumed)"
	    echo "     -n <type>  : set LAMB file type to <type> - type being: 0, 1, 2, 3, merged (default)"
	    echo "     -v <F:V>   : program FPGA F with specific version V (obsolete)"
	    exit
            ;;  
        s)
            SLOT=$OPTARG
            ;;
        l)
            LAB="LAB4"
            ;;
        f)
            LOAD_TARGET="flash_memory"
            ;;
        F)
            FORCE=true
            ;;
        C)
	    REPROGRAM_CTR=true
	    REPROGRAM_ALL=false
            ;;
        R)
	    REPROGRAM_ROA=true
	    REPROGRAM_ALL=false
            ;;
        H)
	    REPROGRAM_HIT=true
	    REPROGRAM_ALL=false
            ;;
        L)
	    REPROGRAM_LAM=true
	    REPROGRAM_ALL=false
            ;;
        x)
	    LAMB_VERSION=4
            ;;
        n)
	    LAMB_FILE_TYPE=$OPTARG
            ;;
        v)
	    FPGA_TO_REPROGRAM=`echo $OPTARG | awk -F: ' { print $1 } '`
	    VERS_TO_REPROGRAM=`echo $OPTARG | awk -F: ' { print $2 } '`
	    echo "+$FPGA_TO_REPROGRAM+  +$VERS_TO_REPROGRAM+"
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            ;;
    esac
done

if [[ ! " ${AMB_SLOTS[@]} " =~ " ${SLOT} " ]]
then
    echo -e "*** Slot not in the list !  ${SLOT} "
    exit
fi

if [ $FPGA_TO_REPROGRAM != none ]
then
    REPROGRAM_ALL=false
    REPROGRAM_CTR=false
    REPROGRAM_ROA=false
    REPROGRAM_HIT=false
    REPROGRAM_LAM=false
    if   [ $FPGA_TO_REPROGRAM = CTR ] ; then  REPROGRAM_CTR=true ; CTR_FW=$VERS_TO_REPROGRAM ;  echo "CTR" ;
    elif [ $FPGA_TO_REPROGRAM = ROA ] ; then  REPROGRAM_ROA=true ; ROA_FW=$VERS_TO_REPROGRAM ;  echo "ROA" ;
    elif [ $FPGA_TO_REPROGRAM = HIT ] ; then  REPROGRAM_HIT=true ; HIT_FW=$VERS_TO_REPROGRAM ;  echo "HIT" ;
    elif [ ${FPGA_TO_REPROGRAM:0:3} = LAM ] ; then  REPROGRAM_LAM=true ; LAM_FW=$VERS_TO_REPROGRAM ;  echo "LAM" ;
    fi
fi

if [ $REPROGRAM_ALL = true ]
then
    REPROGRAM_CTR=true
    REPROGRAM_ROA=true
    REPROGRAM_HIT=true
    REPROGRAM_LAM=true
fi

SVF_DIR=$SVF_DIR_P1
[ $LAB = LAB4 ] && SVF_DIR=$SVF_DIR_LAB4

NODENAME=`basename $HOSTNAME .cern.ch`

LOGDIR=fwAMB_${NODENAME}_${SLOT}_${USER}_$(date +%Y%m%d_%H%M%S)

TMPF=tmp_$NODENAME.status

if [[ ! " 0 1 2 3 merged " =~ " ${LAMB_FILE_TYPE} " ]]
then
    echo -e "*** Wrong configuration for LAMB !  ${LAMB_FILE_TYPE} "
    exit
fi


# Configuration summary

echo ""
echo -e "Set to reprogram AMB in slot :  ${SLOT} "
echo -e "Laboratory                   :  ${LAB} "
echo -e "Logdir                       :  ${LOGDIR} "
echo -e "Load FW in                   :  ${LOAD_TARGET} "
echo -e "Force reprogramming is       :  ${FORCE} "
echo ""
[ $REPROGRAM_CTR = true ] && echo -e "-> Reprogram  CONTROL  FW  $CTR_FW "
[ $REPROGRAM_ROA = true ] && echo -e "-> Reprogram  ROAD     FW  $ROA_FW "
[ $REPROGRAM_HIT = true ] && echo -e "-> Reprogram  HIT      FW  $HIT_FW "
[ $REPROGRAM_LAM = true ] && echo -e "-> Reprogram  LAMB v$LAMB_VERSION  FW  $LAM_FW  $LAMB_FILE_TYPE "

echo ""

########################################################

mkdir $LOGDIR

[ $REPROGRAM_CTR = true ] && reprogram_fw_ambslp CTR $CTR_FW $LOAD_TARGET $FORCE
[ $REPROGRAM_ROA = true ] && reprogram_fw_ambslp ROA $ROA_FW $LOAD_TARGET $FORCE
[ $REPROGRAM_HIT = true ] && reprogram_fw_ambslp HIT $HIT_FW $LOAD_TARGET $FORCE
[ $REPROGRAM_LAM = true ] && reprogram_fw_ambslp LAM_$LAMB_VERSION"_"$LAMB_FILE_TYPE $LAM_FW $LOAD_TARGET $FORCE

echo ""

exit

