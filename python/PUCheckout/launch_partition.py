#!/usr/bin/env tdaq_python

"""
  Python script to launch a partition and to drive it through all the states, and to finally close it 
  Author: Simone Sottocornola
          simone.sottocornola@cern.ch

  Upated: 3/7/18 to work on the pub machines, run for a minute, and take a SoloRun
  Author: Lesya Horyn

  Upated: 16/09/19 Added option for keeping the output of the SoloRun local - default set to false
  Author: Paolo Mastrandrea

  Upated: 17/09/19 Added printed message for starting transition
  Author: Paolo Mastrandrea

"""

import os,sys
import subprocess
from ispy import IPCPartition, ISInfoIterator, ISInfoAny, ISCriteria, ISServerIterator
import re
import time, datetime
from optparse import OptionParser 

parser = OptionParser()
parser.add_option("-p", dest="part_name", default="FTK", help="Name of the partition to test")
parser.add_option("-S", dest="seg_name", default="FTK", help="Name of the segment to test")
parser.add_option("-a", dest="app_name", default="RootController", help="Name of the RC_app to which the command is sent")
parser.add_option("-t", dest="time_s", default=60, type="int", help="Length of time to be in the running state")
parser.add_option("-n", dest="n_iters", default=1, type="int", help="Number of iterations")
parser.add_option("-l", dest="local_solorun", default=False, action="store_true", help="Local run option")

(options, args) = parser.parse_args()
part_name     = options.part_name
seg_name      = options.seg_name
app_name      = options.app_name
time_s        = options.time_s
n_iters       = options.n_iters
local_solorun = options.local_solorun


#############################################################################################################
#                                             Transition                                                    #
#############################################################################################################
def transition(Command, status):
  start = time.time()
  os.system('rc_sender -p %s -n %s -c %s' % (part_name, app_name, Command))
#  if (Command != "SHUTDOWN"):
  print "Transition  %12s  started" %(Command)
  transition_done=False
  while(not transition_done):
    time.sleep(0.1)
    it = ISInfoIterator(p, 'RunCtrl', ISCriteria(app_name))
    while it.next():
      # Read the IS server
      rm = ISInfoAny()
      it.value(rm)
      rm._update(it.name(), p)
      state = rm.state
      if state == status:
        print "Transition  %12s  completed" %(Command)
        if status == "RUNNING":
          print "Running for %i s"%(time_s)
          time.sleep(time_s)
#            print "Taking a SoloRun"
          print '[%s] Taking a SoloRun' % time.asctime()
          solorun_cmd_string = 'FTK_Solo.sh -S %s -p %s'
          if local_solorun :
            solorun_cmd_string = 'FTK_Solo.sh -l -S %s -p %s'
          try: os.system(solorun_cmd_string%(seg_name, part_name))
          except: print "problem taking solorun!"
          print '[%s] SoloRun taken' % time.asctime()
        transition_done=True
#  else :
#    print "Going to  %12s " %(Command)

            
#############################################################################################################
#                                               Main                                                        #
#############################################################################################################

os.system('clear')

if __name__ == '__main__':

  os.system("setup_daq -p %s" %(part_name))
  #os.system("setup_daq -p %s -ng" %(part_name))
  #subprocess.check_output(['setup_daq', '-ng', '-p', part_name],stderr=subprocess.STDOUT)

  print 'Partition            = ', part_name
  print 'Segment              = ', seg_name
  print 'App name             = ', app_name
  print 'Running time         = ', time_s
  print 'SoloRun local output = ', local_solorun

  p = IPCPartition(part_name)
  if not p.isValid():
    print 'Partition', part_name, 'is not valid, abort.'
    sys.exit(2)

  Command = ["INITIALIZE", "CONFIGURE",  "CONNECT",   "START",   "STOP",      "DISCONNECT", "UNCONFIGURE", "SHUTDOWN"]
  Status  = ["INITIAL",    "CONFIGURED", "CONNECTED", "RUNNING", "CONNECTED", "CONFIGURED", "INITIAL",     "NONE"]

#  zipped_list = zip(Command, Status)
#  print_list  = list(zipped_list)
#  print print_list

  for i in range(1, 1+n_iters):

    print "-----------------------------> ITERATION  %i / %i " % (i,n_iters)

    for a,b in zip(Command,Status): 
      transition(a, b)

  #subprocess.check_output(['pmg_kill_partition', '-p', part_name], stderr=subprocess.STDOUT)
#  os.system('pmg_kill_partition -p %s'%part_name) ## dont want to do this incase there is a problem

  sys.exit(0)
