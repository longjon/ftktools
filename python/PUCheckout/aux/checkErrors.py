import sys
import os
import logging

if __name__ == '__main__':
    
    logging.basicConfig(filename="aux.log", level=logging.INFO)
        
    #Check error file
    errFile = sys.argv[1]
    outFile = sys.argv[2]

    allGood = False

    if not os.path.isfile(errFile): 
        logging.error("Invalid .err file")
        print "Failure"
        sys.exit(1)

    if not os.path.isfile(outFile): 
        logging.error("Invalid .out file")
        print "Failure"
        sys.exit(1)

    
    # error file empty but log file not empty (aka ran with no errors)
    if os.path.getsize(errFile) == 0 and os.path.getsize(outFile) > 0: allGood = True
    logging.info("No RC errors found")


    if allGood: print "Success"
    else: print "Failure"
