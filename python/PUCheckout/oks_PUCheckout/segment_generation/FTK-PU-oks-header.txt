<?xml version="1.0" encoding="ASCII"?>

<!-- oks-data version 2.0 -->


<!DOCTYPE oks-data [
  <!ELEMENT oks-data (info, (include)?, (comments)?, (obj)+)>
  <!ELEMENT info EMPTY>
  <!ATTLIST info
      name CDATA #REQUIRED
      type CDATA #REQUIRED
      num-of-items CDATA #REQUIRED
      oks-format CDATA #FIXED "extended"
      oks-version CDATA #REQUIRED
      created-by CDATA #REQUIRED
      created-on CDATA #REQUIRED
      creation-time CDATA #REQUIRED
      last-modified-by CDATA #REQUIRED
      last-modified-on CDATA #REQUIRED
      last-modification-time CDATA #REQUIRED
  >
  <!ELEMENT include (file)+>
  <!ELEMENT file EMPTY>
  <!ATTLIST file
      path CDATA #REQUIRED
  >
  <!ELEMENT comments (comment)+>
  <!ELEMENT comment EMPTY>
  <!ATTLIST comment
      creation-time CDATA #REQUIRED
      created-by CDATA #REQUIRED
      created-on CDATA #REQUIRED
      author CDATA #REQUIRED
      text CDATA #REQUIRED
  >
  <!ELEMENT obj (attr | rel)*>
  <!ATTLIST obj
      class CDATA #REQUIRED
      id CDATA #REQUIRED
  >
  <!ELEMENT attr (#PCDATA)*>
  <!ATTLIST attr
      name CDATA #REQUIRED
      type (bool|s8|u8|s16|u16|s32|u32|s64|u64|float|double|date|time|string|uid|enum|class|-) "-"
      num CDATA "-1"
  >
  <!ELEMENT rel (#PCDATA)*>
  <!ATTLIST rel
      name CDATA #REQUIRED
      num CDATA "-1"
  >
]>

<oks-data>


<info name="" type="" num-of-items="26" oks-format="extended" oks-version="oks-06-10-11 built &quot;May 22 2017&quot;" created-by="mastrand" created-on="pc-atlas-pub-03.cern.ch" creation-time="20190522T134041" last-modified-by="mastrand" last-modified-on="pc-atlas-pub-03.cern.ch" last-modification-time="20190522T134050"/>


