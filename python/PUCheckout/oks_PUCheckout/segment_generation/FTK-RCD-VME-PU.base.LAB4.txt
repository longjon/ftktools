<?xml version="1.0" encoding="ASCII"?>

<!-- oks-data version 2.0 -->


<!DOCTYPE oks-data [
  <!ELEMENT oks-data (info, (include)?, (comments)?, (obj)+)>
  <!ELEMENT info EMPTY>
  <!ATTLIST info
      name CDATA #REQUIRED
      type CDATA #REQUIRED
      num-of-items CDATA #REQUIRED
      oks-format CDATA #FIXED "extended"
      oks-version CDATA #REQUIRED
      created-by CDATA #REQUIRED
      created-on CDATA #REQUIRED
      creation-time CDATA #REQUIRED
      last-modified-by CDATA #REQUIRED
      last-modified-on CDATA #REQUIRED
      last-modification-time CDATA #REQUIRED
  >
  <!ELEMENT include (file)+>
  <!ELEMENT file EMPTY>
  <!ATTLIST file
      path CDATA #REQUIRED
  >
  <!ELEMENT comments (comment)+>
  <!ELEMENT comment EMPTY>
  <!ATTLIST comment
      creation-time CDATA #REQUIRED
      created-by CDATA #REQUIRED
      created-on CDATA #REQUIRED
      author CDATA #REQUIRED
      text CDATA #REQUIRED
  >
  <!ELEMENT obj (attr | rel)*>
  <!ATTLIST obj
      class CDATA #REQUIRED
      id CDATA #REQUIRED
  >
  <!ELEMENT attr (#PCDATA)*>
  <!ATTLIST attr
      name CDATA #REQUIRED
      type (bool|s8|u8|s16|u16|s32|u32|s64|u64|float|double|date|time|string|uid|enum|class|-) "-"
      num CDATA "-1"
  >
  <!ELEMENT rel (#PCDATA)*>
  <!ATTLIST rel
      name CDATA #REQUIRED
      num CDATA "-1"
  >
]>

<oks-data>

<info name="" type="" num-of-items="26" oks-format="extended" oks-version="oks-06-10-11 built &quot;May 22 2017&quot;" created-by="mastrand" created-on="pc-atlas-pub-03.cern.ch" creation-time="20190522T134041" last-modified-by="mastrand" last-modified-on="pc-atlas-pub-03.cern.ch" last-modification-time="20190522T134050"/>

<include>
 <file path="ftk/sw/FtkSwRepository.data.xml"/>
 <file path="ftk/segments/FTK-common.data.xml"/>
 <file path="ftk/hw/hosts-ftk.data.xml"/>
 <file path="daq/hw/hosts.data.xml"/>
 <file path="ftk/schema/Readout_Ssb.schema.xml"/>
 <file path="ftk/schema/ReadoutModule_Dummy.schema.xml"/>
 <file path="ftk/schema/ReadoutModule_PU.schema.xml"/>
 <file path="daq/segments/setup.data.xml"/>
 <file path="daq/schema/core.schema.xml"/>
 <file path="daq/hw/hosts-ros.data.xml"/>
 <file path="ftk/schema/FTKCommon.schema.xml"/>
 <file path="ftk/segments/FTK-PU-tbed01.data.xml"/>
 <file path="ftk/segments/FTK-PU-tbed02.data.xml"/>
</include>

<comments>
 <comment creation-time="20190522T134050" created-by="mastrand" created-on="pc-atlas-pub-03.cern.ch" author="mastrand" text="C++ oksconfig plug-in user: &apos;mastrand&apos;, host: &apos;pc-atlas-pub-03.cern.ch&apos;, log message: &apos;python plug-in&apos;"/>
</comments>


<obj class="AuxConstants" id="Aux1-Mod0-tower22">
 <attr name="Index" type="u8">0</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_module_tower_22_plane_0.hex"</attr>
 <attr name="Checksum" type="u32">0x85e816b0</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-Mod1-tower22">
 <attr name="Index" type="u8">1</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_module_tower_22_plane_1.hex"</attr>
 <attr name="Checksum" type="u32">0x307081f0</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-Mod2-tower22">
 <attr name="Index" type="u8">2</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_module_tower_22_plane_2.hex"</attr>
 <attr name="Checksum" type="u32">0xe00c20</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-Mod3-tower22">
 <attr name="Index" type="u8">3</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_module_tower_22_plane_3.hex"</attr>
 <attr name="Checksum" type="u32">0x66e046e0</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-Mod4-tower22">
 <attr name="Index" type="u8">4</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_module_tower_22_plane_4.hex"</attr>
 <attr name="Checksum" type="u32">0x64b06930</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-Mod5-tower22">
 <attr name="Index" type="u8">5</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_module_tower_22_plane_5.hex"</attr>
 <attr name="Checksum" type="u32">0x95e0ef10</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-Mod6-tower22">
 <attr name="Index" type="u8">6</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_module_tower_22_plane_6.hex"</attr>
 <attr name="Checksum" type="u32">0x6090ef10</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-Mod7-tower22">
 <attr name="Index" type="u8">7</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_module_tower_22_plane_7.hex"</attr>
 <attr name="Checksum" type="u32">0x2e600260</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-SS0-tower22">
 <attr name="Index" type="u8">0</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_ssid_tower_22_plane_0.hex"</attr>
 <attr name="Checksum" type="u32">0xa9fd0000</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-SS1-tower22">
 <attr name="Index" type="u8">1</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_ssid_tower_22_plane_1.hex"</attr>
 <attr name="Checksum" type="u32">0xa9fd0000</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-SS2-tower22">
 <attr name="Index" type="u8">2</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_ssid_tower_22_plane_2.hex"</attr>
 <attr name="Checksum" type="u32">0xa9fd0000</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-SS3-tower22">
 <attr name="Index" type="u8">3</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_ssid_tower_22_plane_3.hex"</attr>
 <attr name="Checksum" type="u32">0x22c04580</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-SS4-tower22">
 <attr name="Index" type="u8">4</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_ssid_tower_22_plane_4.hex"</attr>
 <attr name="Checksum" type="u32">0x22c04580</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-SS5-tower22">
 <attr name="Index" type="u8">5</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_ssid_tower_22_plane_5.hex"</attr>
 <attr name="Checksum" type="u32">0x22c04580</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-SS6-tower22">
 <attr name="Index" type="u8">6</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_ssid_tower_22_plane_6.hex"</attr>
 <attr name="Checksum" type="u32">0x22c04580</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-SS7-tower22">
 <attr name="Index" type="u8">7</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/ssmaps/auxTV_ssid_tower_22_plane_7.hex"</attr>
 <attr name="Checksum" type="u32">0x22c04580</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-TF1-tower22">
 <attr name="Index" type="u8">1</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/constants/corrgen_raw_8L_reg22.gcon"</attr>
 <attr name="Checksum" type="u32">0x18c928df</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-TF1-tower22-low-dataflow">
 <attr name="Index" type="u8">1</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/lowDataflow/AUX/constants/corrgen_raw_8L_reg22.gcon"</attr>
 <attr name="Checksum" type="u32">0x846bad26</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-TF2-tower22">
 <attr name="Index" type="u8">2</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/constants/corrgen_raw_8L_reg22.gcon"</attr>
 <attr name="Checksum" type="u32">0x5576303a</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-TF2-tower22-low-dataflow">
 <attr name="Index" type="u8">2</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/lowDataflow/AUX/constants/corrgen_raw_8L_reg22.gcon"</attr>
 <attr name="Checksum" type="u32">0x6b18cd18</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-TF3-tower22">
 <attr name="Index" type="u8">3</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/constants/corrgen_raw_8L_reg22.gcon"</attr>
 <attr name="Checksum" type="u32">0x4168dbb8</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-TF3-tower22-low-dataflow">
 <attr name="Index" type="u8">3</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/lowDataflow/AUX/constants/corrgen_raw_8L_reg22.gcon"</attr>
 <attr name="Checksum" type="u32">0x799c59aa</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-TF4-tower22">
 <attr name="Index" type="u8">4</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/latest/AUX/constants/corrgen_raw_8L_reg22.gcon"</attr>
 <attr name="Checksum" type="u32">0x296c309d</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxConstants" id="Aux1-TF4-tower22-low-dataflow">
 <attr name="Index" type="u8">4</attr>
 <attr name="Path" type="string">"/det/ftk/repo/condDB/lowDataflow/AUX/constants/corrgen_raw_8L_reg22.gcon"</attr>
 <attr name="Checksum" type="u32">0xa09a9cc2</attr>
 <attr name="LoadAllSectors" type="bool">0</attr>
</obj>

<obj class="AuxTestParams" id="data_tower22_1Evt_TRACKS">
 <attr name="Loop" type="bool">1</attr>
 <attr name="RoadPath" type="string">"/det/ftk/repo/condDB/latest/AUX/testvectors/data_tower22_1Evt_TRACKS/ttbarTV_roads_proc"</attr>
 <attr name="HitPath" type="string">"/det/ftk/repo/condDB/latest/AUX/testvectors/data_tower22_1Evt_TRACKS/ttbarTV"</attr>
 <attr name="SSPath" type="string">""</attr>
</obj>


<obj class="PatternBankConf" id="PBank-tower22">
 <attr name="NumPattPerChip" type="u32">131072</attr>
 <attr name="FilePath" type="string">"/det/ftk/repo/condDB/lowDataflow/pbanks/user.sschmitt.patterns_DataAlignment2017_xm05_ym09_Reb64_v9.HW2.SSEL3.180511-64PU_30x128x72Ibl-NB7-NE4-BM0_partitioning4_180601-095309_reg22_sub0.pbank.root"</attr>
 <attr name="BankType" type="u32">0</attr>
 <attr name="SSOffset" type="bool">0</attr>
 <attr name="InChipOff" type="u32">0</attr>
 <attr name="AMBChecksum" type="u32">0x22e12e0d</attr>
 <attr name="Proc1Checksum" type="u32">3308684123</attr>
 <attr name="Proc2Checksum" type="u32">4012826650</attr>
 <attr name="Proc3Checksum" type="u32">273953746</attr>
 <attr name="Proc4Checksum" type="u32">2413269329</attr>
</obj>


<obj class="firmware" id="fw04">
 <attr name="Input1" type="u32">0x1d843da1</attr>
 <attr name="Input2" type="u32">0x2d84c8a2</attr>
 <attr name="Processor1" type="u32">0xf284e7a3</attr>
 <attr name="Processor2" type="u32">0xf284e7a4</attr>
 <attr name="Processor3" type="u32">0xf284e7a5</attr>
 <attr name="Processor4" type="u32">0xf284e7a6</attr>
</obj>

