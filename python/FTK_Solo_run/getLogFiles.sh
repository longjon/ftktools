logDir="/logs/tdaq-07-01-00/$2/"
echo $logDir

errFile=$(ls -t ${logDir}*.err | head -1)
outFile=$(ls -t ${logDir}*.out | head -1)

if [ ! -d $1/logFiles ]; then
    echo "Creating $1/logFiles"
    mkdir $1/logFiles
fi

cp $errFile $1/logFiles
cp $outFile $1/logFiles
echo -e "copying $errFile and $outFile to $1/logFiles"

