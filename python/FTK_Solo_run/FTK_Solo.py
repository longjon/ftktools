#!/usr/bin/env python

import socket
import os
import sys
import errno
import subprocess
import datetime
from distutils.dir_util import copy_tree
import shutil
import glob
import distutils.dir_util as ddu
import config
import dal_algo
import pm
import pm.project
from ipc import IPCPartition
import trace

#******************************************************************************************************************
#  Global variables definition (will be filled up in read_oks function)
#******************************************************************************************************************
slot=""
crate=""
shelf=""
rcd_name=""
partition=""
df_name=""
im_df_script="SaveAllSpyISEMon_IMDF_v1.sh"
configPath=""


#******************************************************************************************************************
#	The the function checks/creates any folders required by the dump spybuffers commands.
#
#	Date:            UPDATE:
#	-----------	 -------------------------------------------------------------------------------------------------
#	2017-06-22:	 At this moment it only creates the "spy_output" folder for the AUX spybuffers.
#
#   2017-07-10   There seems to be no other directory prerequicite directories for the other boards or outputs.
#                The function is called by the AUX_board_commands function since it is only need there at this moment.
#*****************************************************************************************************************

def necessary_dirs():
        path = './spy_output'
        try:
            os.makedirs(path)
            print('file was created')
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                print('File exists')
                raise
        print('make_sure_directory_exists ran')

#******************************************************************************************************************
#       The the function reads oks to retrieve all the nodes in which the RCDs of the given partition are running
#*****************************************************************************************************************
def read_oks_hosts(part_name, segment_name):
    partition=part_name
    db = config.Configuration('rdbconfig:RDB@' + partition)
    partition = db.get_dal('Partition', partition)
    hosts=[]
    segment = partition.get_segment(db, segment_name, False, None)
    print "Going to retrieve informations from segment %s" %segment.seg_id
    if segment.is_disabled:
        print "segment %s is disabled! exiting.." %(segment.seg_id)
        sys.exit(2)
    print "Retrieving applications from segment %s" %(segment.seg_id)
    for p in segment.get_applications(db):
        application = p.get_base_app()
        print "Application class: %s" %application.className()
        if application.className()=="RCD":
          print "%s" %application.RunsOn.id
          if not application.RunsOn.id in hosts:
            hosts.append(application.RunsOn.id)
    #print "%s" %hosts
    return hosts

#******************************************************************************************************************
#       The the function reads oks to retrieve all the informations about the RMs running on the node in which the function is called
#       This function will also call the proper board functions for the mretrieve of the informations
#*****************************************************************************************************************
def read_oks(my_str, isLab4, part_name, segment_name, nobuffers):
    print "Entered read_oks"

#    print "Writing the PID in /tmp/"
#    out_file = open("/tmp/SoloRunID","w")
#    out_file.write(str(os.getpid())+"\n")
#    out_file.close()

    global slot
    global crate
    global shelf
    global df_name
    global partition
    global configPath
    partition=part_name
    print "Going to start the RDB"
    #partition = IPCPartition(partition)
    db = config.Configuration('rdbconfig:RDB@' + partition)
    print "RDB ok..Going to retrieve the Partition"
    partition = db.get_dal('Partition', partition)
    print "Going to retrieve segment"
    segment = partition.get_segment(db, segment_name, False, None)
    print "Going to retrieve informations from segment %s" %segment.seg_id
    if segment.is_disabled:
        print "segment %s is disabled! exiting.." %(segment.seg_id)
        sys.exit(2)
    print "Retrieving applications from segment %s" %(segment.seg_id)
    for p in segment.get_applications(db):
        application = p.get_base_app()
        hostname = os.getenv('HOSTNAME')
        if application.className()=="RCD":
            if application in partition.Disabled:
                print "Application %s is disabled. Skipping it.." %application.id
                continue
            if application.RunsOn.id[:-8] == hostname:
                print "found RCD %s running on this host (%s)" %(application.id, hostname)
                print application.Contains
                for RMD in application.Contains:
                    print "found ReadoutModule %s" %(RMD.id)
                    if RMD in partition.Disabled:
                      print "RM %s is disabled. Skipping it..." %RMD.id
                      continue
                    RMClass = RMD.className()
                    print "RM class= %s" %RMClass
                    rcd_name = application.id
                    print '---------------> RCD is %s '%(rcd_name)
                    if RMClass == "ReadoutModule_DataFormatter":
                        shelf = RMD.DF_Shelf
                        slot = RMD.DF_Slot
                        df_name = RMD.id
                        print "ReadoutModule DF name: %s" %RMD.id
                        configPath = RMD.ConfigPath
                        DF_IM_commands(my_str, nobuffers, rcd_name)
                    elif RMClass == "ReadoutModule_PU":
                        crate = RMD.Crate
                        slot = RMD.Slot
                        AUX_board_commands(my_str, nobuffers)
                        AMB_commands(my_str, nobuffers)
                    elif RMClass == "ReadoutModule_Ssb":
                        crate = RMD.Crate
                        slot = RMD.Slot

                        enabled_fpgas = list()
                        print RMD.FPGAs
                        for fpga in RMD.FPGAs:
                            if "EXTF" in fpga.id and fpga.Enable:
                                enabled_fpgas.append(fpga.Id)

                        SSB_commands(my_str, nobuffers, enabled_fpgas)
                    else:
                        print "No know RM class (%s) for object %s" %(RMClass, RMD.id)
    #removing PID file
#    subprocess.call("rm /tmp/SoloRunID", shell = True)
#******************************************************************************************************************
#	The the function checks/creates any folders required by the dump spybuffers commands.
#
#	Date:            UPDATE:
#	-----------	 -------------------------------------------------------------------------------------------------
#	2017-07-18:	 The function will take a file created from the different board/card commands and check for a freeze.
#
#**********************************************************************************************************************

def freeze_check(filename, listwords):
    try:
        file = open(filename, "r")
        read = file.readlines()
        file.close()
        for word in listwords:
            lower = word.lower()
            for sentance in read:
                line = sentance.split()
                for each in line:
                    line2 = each.lower()
                    line2 = line2.strip("!@#$%^&*()_-?+=")
                    if lower == line2:
                        print("\nA freeze has occured in %s...\n"%(filename))
    except FileExistsError:
        print("\nThe file %s is not there...\n"%(filename))


#***************************************************************************************************************
#
#	Date:            UPDATE:
#	-----------	 ---------------------------------------------------------------------------$
#	2017-06-22    The function contains a call to the different commands used for debugging AUX card.
#                 It takes the address of the current run directory created by the bash file
#                 /atlas-home/1/jsolorio/FTK_Solo.sh as a argument, to create a directory for AUX.
#                 It currently only dumps the spybuffers into the directory "spy_output" and it then
#                 moves the directory into the appropiate spybuffers directory in the directory tree
#                 created for the Current run from the file /atlas-home/1/jsolorio/FTK_Solo.sh
#
#   2017-07-12    commands list appended to contain the commands to be dumped. Commands added require for
#                 the screen output to be pipped into appropiately named files. Conditional statement added
#                 to distinguish between commands that need the pipping and aux_dump_buffers which doesn't.
#*****************************************************************************************************************

def AUX_board_commands(str, nobuffers):
        print("Running AUX commands.")
        dump_buffers = True
        if nobuffers=="true": dump_buffers=False
        commands = ["aux_linkstatus_main","aux_i2_fifo_status_main","aux_linkerrors_main","aux_proc_fifo_status_main","aux_fifo_status_main","aux_proc_status_main","aux_status_main", "aux_error_count_main", "aux_input_status_main"]
        if dump_buffers: commands += ["aux_dump_buffers"]
        fromDirectory = "./spy_output"

        #   Loop runs through the commands in the list of commands for the AUX.
        for cmd in commands:
            toDir = str + "/AUX/crate-%s/slot-%s"%(crate,slot)               #   Creating the directory for AUX within the current run directory tree
            print "Checking aux in crate %s, slot %s"%(crate, slot)
            subprocess.check_call(['mkdir','-p',toDir])                     #   subprocess.check_call allows to create the toDir in the current run directory tree.
            if(cmd == "aux_linkstatus_main"):
                print("Running command: %s"%(cmd))
                subprocess.call("%s --slot %s > %s.dat"%(cmd,slot,cmd), shell = True)
                freeze_check("%s.dat"%(cmd), "freeze")
                try:
                    shutil.move("%s.dat"%(cmd), toDir)
                except:
                    print "content not copied"
            elif(cmd == "aux_dump_buffers"):
                print("Running command: %s"%(cmd))
                necessary_dirs()
                subprocess.call("%s --slot %s --all --over"%(cmd,slot), shell = True)  #   The subprocess.call is a funtion that allows the python program to run the commands from the original bash commands.
                try:
                    shutil.move(fromDirectory, toDir)                           #   shutil.move is a function that allows for the "spy_output directory to be moved to the AUX directory in the current run directory.
                except:
                    print "content not copied"
            elif(cmd == "aux_proc_status_main"):
                print("Running command: %s"%(cmd))
                subprocess.call("%s --slot %s --rate --details > %s.dat"%(cmd,slot,cmd), shell = True)
                try:
                    shutil.move("%s.dat"%(cmd), toDir)
                except:
                    print "content not copied"
            else:
                print("Running command: %s"%(cmd))
                subprocess.call("%s --slot %s > %s.dat"%(cmd,slot,cmd), shell = True)
                try:
                    shutil.move("%s.dat"%(cmd), toDir)
                except:
                  print "content not copied"

        print("You've got to the end AUX")


#********************************************************************************************************************
#
#	Date:            UPDATE:
#	-----------	 -------------------------------------------------------------------------------------------
#
#	2017-06-22    The function runs the commands for the DF and IM. It currently takes it takes the higher directory tree
#                 for the current run created in the bash file /atlas-home/1/jsolorio/FTK_Solo.sh to then create
#                 the remaining directories under the DF-IM directory.
#********************************************************************************************************************

def DF_IM_commands(str, nobuffers, rcd_name ):
        print("\nWe are in DF-IM commands...   starting commands")
        print("export DF_IPUBUS_CONFIG")
        os.environ['DF_IPUBUS_CONFIG'] = configPath
        dump_buffers = True
        if nobuffers=="true": dump_buffers=False

        #   List holding commands for DF-IM, the shelf/crate, and slots
        commands = ["%s %s %s %s %s %s"]

        #   Nested loops running list of commands for the SSB boards in all the shelfs and slots
        for cmd_template in commands:
            cmd = cmd_template%(im_df_script, partition.id, shelf, slot, df_name, rcd_name)
            print("Running command: %s"%(cmd))
            subprocess.call("bash %s"%(cmd), shell = True)    #    Using subprocess.call to use the commands from the original bash script
            toDir = str + "/DF-IM/crtShelf-%s/slot-%s"%(shelf, slot)                           #    Creating the variable to hold the directory for the DF-IM's crate and slot
            print toDir
            subprocess.check_call(['mkdir','-p',toDir])                                        #    Creating the directory for the current run process for the DF-IM slot

        #     The output from the SSB is the form of files that start with 'ISIMAllSpy_'
        for files in glob.glob("ISIMAllSpy_*"):                                                               #    glob.glob function collects all the files in the current directory with the given condition
          try:
              shutil.move(files, toDir)                                                                          #    shutil.move will move the files to the current run directory for the SSB in the current slot
          except:
              print "content not copied"
        print("You've reach the end of DF-IM")



#***************************************************************************************************$
#
#	Date:            UPDATE:
#	-----------	 ---------------------------------------------------------------------------$
#	2017-07-03    The funtion contains the commands for the debugging statuses for the SSb board.
#                 To this point it takes the higher directory tree for the current run created in the
#                 bash file /atlas-home/1/jsolorio/FTK_Solo.sh to then create the remaining directories
#                 under the SSB directory.
#
#   2017-07-10    Funtion now runs the command to dump the spybuffers for the SSB, storing the output
#                 under the directory for the spybuffers, in the current run directory for SSB.
#                 This dumps the spybuffers for the SSB and puts them into the file named "spybuffers.log"
#
#   2017-07-17    Correction to the call to the fpga list, don't know yet if it has to be a list or the 0 is
#                 permanent. Also, now the output to the screen is piped to a file with the name of the command
#                 with the ".log" tag. The file is then moved to the SSB subdirectory.
#***************************************************************************************************$

def SSB_commands(path, nobuffers, enabled_fpgas):
    print("we are at the SSB commands...    starting commands")
    dump_buffers = True
    if nobuffers=="true": dump_buffers=False
    filename = "ssb_*"
    commands = ["ssb_status_main"]
    if dump_buffers: commands += ["ssb_spybuffer_dump_main"]
    fpga = enabled_fpgas + [4]

    fpga_mask = 0
    if (0 in fpga): fpga_mask = fpga_mask | 0b1
    if (1 in fpga): fpga_mask = fpga_mask | 0b10
    if (2 in fpga): fpga_mask = fpga_mask | 0b100
    if (3 in fpga): fpga_mask = fpga_mask | 0b1000

    for cmd in commands:
        print("Running command: %s"%(cmd))
        toDir = path + "/SSB/crate-%s/slot-%s"%(crate, slot)
        print toDir
        subprocess.check_call(['mkdir','-p',toDir])


        if(cmd == "ssb_status_main"):
            subprocess.call("%s --slot %s --EXTF_mask %s > %s.dat"%(cmd,slot,hex(fpga_mask),cmd), shell = True)

        for i_fpga in fpga:
            if i_fpga < 4: # EXTF

                if(cmd == "ssb_spybuffer_dump_main"):
                    subprocess.call("%s --fpga %s --slot %s --nSPY=10 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga)), shell = True)

                    # EXTF histograms
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x880 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_tracks_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x840 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_AUX_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x800 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_DF_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x8c0 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_outputlen_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xc00 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_skew_histo"), shell = True)

                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xd00 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_DFAtimeout_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xd40 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_DFBtimeout_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xd80 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_AUXAtimeout_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xdc0 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_AUXBtimeout_histo"), shell = True)

                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xe00 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_globall1idskip_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xe40 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_TFFIFOl1idskip_histo"), shell = True)

                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xf00 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_DFAlvl1idskip_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xf40 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_DFBlvl1idskip_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xf80 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_AUXAlvl1idskip_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xfc0 --hmode --nBLT 64 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_AUXBlvl1idskip_histo"), shell = True)

                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x900 --hmode --nBLT 256 --nWords 16 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga) + "_EXTF_LayerMap_histo"), shell = True)

            else: # hitwarrior
                if(cmd == "ssb_spybuffer_dump_main"):
                    subprocess.call("%s --fpga %s --slot %s --nSPY=10 > %s_FPGA%s.dat"%(cmd,str(i_fpga),slot,cmd,str(i_fpga)), shell = True)

                    # Hitwarrior Histograms
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x900 --hmode --nBLT 16 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWNTrack_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x940 --hmode --nBLT 64 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWl1idskip_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x980 --hmode --nBLT 64 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWchi2_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0x9c0 --hmode --nBLT 64 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWd0_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xa00 --hmode --nBLT 64 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWz0_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xa40 --hmode --nBLT 64 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWcotth_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xa80 --hmode --nBLT 64 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWphi0_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xac0 --hmode --nBLT 64 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWcurv_histo"), shell = True)
                    subprocess.call("%s --fpga %s --slot %s --nSPY=1 --addr 0xb00 --hmode --nBLT 256 --nWords 16 > %s_out.dat"%(cmd,str(i_fpga),slot,cmd + "_HWLayerMap_histo"), shell = True)


        # Run spybuffer parsing -- TODO this needs updating for multiple FPGAs etc.  and updating in general
        # subprocess.call("Parse_SSB_dump.py -o %s_out.dat -p"%(cmd), shell = True)


    for files in glob.glob("%s"%(filename)):
        try:
            shutil.move(files, toDir)
        except:
            print "content not copied"

    print("You've got to the end of SSB")


#***************************************************************************************************$
#
#	Date:            UPDATE:
#	-----------	 ---------------------------------------------------------------------------$
#	2017-07-03    Basic structure for AMB funtion created. It receives a string parameter from the Bash file,
#                 the parameter is the current run directory address. The function then creates a subdirectory
#                 for the AMB in the current run directory tree.
#
#   2017-07-17
#***************************************************************************************************$

def AMB_commands(str, nobuffers):
        print("we are at the AMB commands...    starting commands")
        dump_buffers=True
        if nobuffers=="true": dump_buffers=False
        commands = ["ambslp_status_main"]
        if dump_buffers: commands += ["ambslp_spybuffer_dumper_main"]

        for cmd in commands:
            print("Running command: %s"%(cmd))
            toDir = str + "/AMB/crate-%s/slot-%s"%(crate,slot)
            #print toDir
            subprocess.check_call(['mkdir','-p',toDir])

            if cmd == "ambslp_spybuffer_dumper_main":
                subprocess.call("%s --slot %s  --keepFreeze 1 --inp --tag ambslp_spybuffer --method 0 > %s_inp_meth0.dat 2>&1 "%(cmd,slot,cmd), shell = True)
                subprocess.call("%s --slot %s  --keepFreeze 1 --out --tag ambslp_spybuffer --method 0 > %s_out_meth0.dat 2>&1 "%(cmd,slot,cmd), shell = True)
                subprocess.call("%s --slot %s  --keepFreeze 1 --inp --tag ambslp_spybuffer --method 1 > %s_inp_meth1.dat 2>&1 "%(cmd,slot,cmd), shell = True)
                subprocess.call("%s --slot %s  --keepFreeze 1 --out --tag ambslp_spybuffer --method 1 > %s_out_meth1.dat 2>&1 "%(cmd,slot,cmd), shell = True)
                subprocess.call("%s --slot %s  --keepFreeze 1 --inp --tag ambslp_spybuffer --method 2 > %s_inp_meth2.dat 2>&1 "%(cmd,slot,cmd), shell = True)
                subprocess.call("%s --slot %s  --keepFreeze 1 --out --tag ambslp_spybuffer --method 2 > %s_out_meth2.dat 2>&1 "%(cmd,slot,cmd), shell = True)
#                subprocess.call("%s --slot %s --tag ambslp_spybuffer --method 4"%(cmd,slot), shell = True)
                for f in os.listdir("."):
                    if "ambslp_spybuffer" in f:
                        try:
                            shutil.move(f, toDir)
                        except:
                            print "Content not copied"
            else:
                subprocess.call("%s --slot %s > %s.dat"%(cmd,slot,cmd), shell = True)
                try:
                    shutil.move("%s.dat"%(cmd), toDir)
                except:
                    print "Content not copied"

        print("You've got to the end of AMB")
