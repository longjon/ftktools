#!/usr/bin/env python

import socket
import os
import sys
import errno
import subprocess
import datetime
from distutils.dir_util import copy_tree
import shutil
import glob
import distutils.dir_util as ddu

import argparse

#******************************************************************************************************************
#
#   The purpose of this script is to perform post-processing on the output of SoloRun, producing human readable
#   summaries of what's going on with the boards.
#
#*****************************************************************************************************************

# A few variables to be filled as you go(Have to be filled before getCounterRates is called)
aux_n_events = -1
aux_rate = -1
ssb_n_events = -1

# General formatting functions
#*****************************************************************************************************************
# Gives a string that looks nice for floats and ints
def intOrFloat(num):
    num_str = ""
    if num%1==0: num_str = "%d"%num
    else: num_str = "%.2f"%num
    return num_str

# Prints out a series of numbers with fixed width columns
def niceNums(nums):
    output = ""
    for num in nums:
        num_str = intOrFloat(num)
        output += "%18s"%num_str
    return output

# Set colors for printouts (format: print color.PURPLE + "text" + color.END)
class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

def printWithSlot(b,c,s,statement=''):
    print "\t" + color.DARKCYAN + "%s-%s-%s:"%(b,c,s), statement + color.END

def printHeader(statement):
    print '\n' + color.GREEN + statement + color.END
#*****************************************************************************************************************

# Special functions for different boards
#*****************************************************************************************************************
# Function that formats AUX rates
def getCounterRates(counts, name):
    info = []
    rate_ev = [0 for x in counts]
    if aux_n_events>0:
        rate_ev = [float(x)/aux_n_events for x in counts]
    rate_s = [x*aux_rate for x in rate_ev]
    average_count = float(sum(counts))/len(counts)
    info += [name]
    info += ["\tRaw           : "+niceNums(counts)  +"\t |  Avg: %13s"%intOrFloat(average_count)                       + "\t Tot: %13s"%intOrFloat(sum(counts))]
    info += ["\tPer Event     : "+niceNums(rate_ev) +"\t |  Avg: %13s"%intOrFloat(float(sum(rate_ev))/len(rate_ev))    + "\t Tot: %13s"%intOrFloat(sum(rate_ev))]
    info += ["\tRate (kHz)    : "+niceNums(rate_s)  +"\t |  Avg: %13s"%intOrFloat(float(sum(rate_s))/len(rate_s))      + "\t Tot: %13s"%intOrFloat(sum(rate_s))]
    return info

# Look for outputs and use this to define which boards we care about
def getBoardsAndSlots(log_dir):
    boards = {}
    # Get all subdirectories (ignore other files)
    dirs = [x for x in os.listdir( log_dir ) if os.path.isdir( os.path.join(log_dir, x) )]
    for board in dirs:
        if board == "logFiles": continue
        boards[board] = {}

        board_dir = os.path.join(log_dir, board)
        for crate in os.listdir( board_dir ):
            boards[board][crate] = []

            crate_dir = os.path.join(board_dir, crate)
            for slot in os.listdir( crate_dir ):
                boards[board][crate].append(slot)

    return boards

# Figure out which EXTF should be used
# Default to just enabling EXTF0 and HW when the method fails
def getFPGAs(full_path):
    fpgas = []
    output = os.listdir(full_path)
    for fpga in xrange(5):
        if "ssb_spybuffer_dump_main_FPGA%d.log"%fpga in output:
            fpgas.append(fpga)
    if len(fpgas) == 0: fpgas = [0,4]
    return fpgas

# Finds the file name for IM/DF (changes because of timestamp)
def getIMDFFileName(im_df_path, crate, slot):
    files = os.listdir(im_df_path)
    for f in files:
        if f.startswith("ISIMAllSpy"):
            if 'tbed' in socket.gethostname(): return f
            elif "%s-%02d"%(crate.split("-")[1], int(slot.split("-")[1])) in f: return f
    print color.RED + "Couldn't find IM/DF output!" + color.END
    return -1

# Gets the text block that gives link status and rate for DF channels
def getDFLinksBlock(output_path):
    links_text = []
    with open(output_path, "r") as f:
        writing = False
        line_count=0
        for line in f:
            line_count += 1
            if line.startswith("        |        FMCIN"): writing = True
            if line.startswith("[Device:")              : writing = False
            if writing == True:
                links_text.append(line)
    return links_text
#*****************************************************************************************************************

# Functions that go through the boards and print some type of info about all of them
#*****************************************************************************************************************

# Prints FW for each of the boards
def printFW(log_dir, board_dic):
    all_fw_versions = {}
    all_fw_versions['DF-IM'] = {}
    all_fw_versions['AUX'] = {}
    all_fw_versions['AMB'] = {}
    all_fw_versions['SSB'] = {}


    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

		fw_versions_parsed = []

                if board == 'DF-IM':
                    fname = getIMDFFileName(full_path, crate, slot)
                    if fname==-1: continue

                    fw_versions = []
                    adding_fw = False
                    with open(full_path+"/"+fname, "r") as f:
                        for line in f:
                            if ("irmware" in line) or ('IM FPGA' in line) :
                                fw_versions.append(line)


                if board == 'AUX':
                    fw_versions = []
                    adding_fw = False
                    with open(full_path+"/aux_status_main.dat", "r") as f:
                        for line in f:
                            if "--- FIRMWARE VERSIONS ---" in line:
                                adding_fw = True
                            if line=="\n":
                                adding_fw = False
                            if adding_fw:
				fw_versions.append(line)
				fw_versions_parsed.append(line.translate(None, '\t').split("(")[0])

                if board == 'AMB':
                    fw_versions = []
                    with open(full_path+"/ambslp_status_main.dat", "r") as f:
                        for line in f:
                            if 'Firmware ' in line:
                                fw_versions.append(line)

                if board == 'SSB':
                    fw_versions = []
                    with open(full_path+"/ssb_status_main.dat", "r") as f:
                        for line in f:
                            if 'Firmware' in line:
                                fw_versions.append(line)

                printWithSlot(board,crate,slot)
                for line in fw_versions:
                    print "\t\t", line,

		if(board == "AUX"):
			all_fw_versions[board][crate+slot] = fw_versions_parsed
		else:
			all_fw_versions[board][crate+slot] = fw_versions

    # Check that all FW versions are the same
    print ""
    for board in all_fw_versions.keys():
        fwMismatch = False
	print "\tNumber of " + board + "s included is ", len(all_fw_versions[board])

    if len(all_fw_versions[board]) > 1:
	    for slot in all_fw_versions[board]:
	        if not all(set(val) == set(all_fw_versions[board][slot]) for val in all_fw_versions[board].values()):
	            fwMismatch = True
    if fwMismatch:
        print color.RED + "\t!!! ALERT: " + board + " firmware is not the same on each board" + color.END

# Finds the number of total events processed for each board
# When numbers are available for multiple channels, averages over them
def eventsProcessed(log_dir, board_dic):

    global aux_n_events
    global ssb_n_events

    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

                events_out = 0

                if board == 'DF-IM':
                    n_events = []
                    links_block = getDFLinksBlock(full_path + "/" + getIMDFFileName(full_path, crate, slot))
                    for line in links_block:
                        if "FMCIN" in line: continue
                        if not "E" in line: continue
                        n_events.append(int(line.split('|')[1].split()[1]) / 3)
                    if len(n_events)>0: events_out = sum(n_events)/len(n_events)

                if board == 'AUX':
                    with open(full_path+"/aux_linkstatus_main.dat", "r") as f:
                        for line in f:
                            if "| Input 2 SFP" in line:
                                events_out = int(line.split("|")[8])-1
                                if events_out<0: events_out = 0
                                aux_n_events = events_out

                if board == 'AMB':
                    with open(full_path+"/ambslp_status_main.dat", "r") as f:
                        for line in f:
                            if "Number of completed events:" in line:
                                events_out = int(line.split("Number of completed events:")[1].split("-")[0])

                if board == 'SSB':
                    with open(full_path+"/ssb_status_main.dat", "r") as f:
                        for line in f:
                            if "# Events out of the HW=" in line:
                                events_out = int(line.split("HW=")[1].split("@")[0], 16)
                                ssb_n_events = events_out

                printWithSlot(board,crate,slot)
                print "\t\t %s events out"%intOrFloat(events_out)

# Finds event rates measured by different boards
def getRates(log_dir, board_dic):

    global aux_rate
    global ssb_rate

    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

                final_rate = ""

                if board == 'DF-IM':
                    #TODO
                    continue
                if board == 'AUX':

                    rates = []
                    with open(full_path+"/aux_linkstatus_main.dat", "r") as f:
                        for line in f:
                            if line.startswith("| Input") or line.startswith("| Processor"):
                                rate = line.split("|")[10]
                                rates.append(float(rate))
                        final_rate = str(1.*sum(rates)/len(rates)) + " kHz"
                        aux_rate = 1.*sum(rates)/len(rates)

                if board == 'AMB':
                    with open(full_path+"/ambslp_status_main.dat", "r") as f:
                        for line in f:
                            if "approximate rate " in line:
                                final_rate = line.split("approximate rate ")[1].strip("\n")

                if board == 'SSB':
                    with open(full_path+"/ssb_status_main.dat", "r") as f:
                        for line in f:
                            if "Events in the EXP" in line and "@" in line:
                                final_rate = line.split("@ ")[1]
                                ssb_rate = float(final_rate.split()[0])
                                break

                printWithSlot(board,crate,slot)
                print "\t\t", final_rate

# Prints out any additional errors not included in other functions
def additionalErrors(log_dir, board_dic):

    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

                found_errors = False
                error_info = []

                if board == 'DF-IM':

                    with open(full_path + "/" + getIMDFFileName(full_path, crate, slot), "r") as f:
                        lines = f.readlines()
                        ch_info = ""    # Keep track of which channel this is
                        for i_line, line in enumerate(lines):
                            if "reg.input_packet_l1id_error" in line or "reg.input_timedout_latch" in line or "reg.input_mod_header_error" in line:
                                errs = int(line.split(":")[len(line.split(":"))-1].strip("\n"),16)
                                if errs>0:
                                    found_errors = True
                                error_info += [line]

                            # Add IM L1ID skips too
                            if "IM Status Monitoring Tool" in line:
                                ch_info = lines[i_line+1]
                            if "L1ID Information" in line:
                                # Skips are in next 3 lines
                                for n_line in xrange(1, 4):
                                    n_skips = lines[i_line + n_line].split("|")[2].strip()
                                    if n_skips != "0x0":
                                        found_errors = True
                                        error_info += [ch_info]
                                        error_info += lines[i_line:i_line+4]

                if board == 'AUX':
                    # Nothing that doesn't involve a freeze
                    continue

                if board == 'AMB':
                    #TODO
                    continue

                if board == 'SSB':
                    ignore_errors = ["Dispar Err", "Nonint Err"]  # Ignore until more AUX plugged in
                    with open(full_path+"/ssb_status_main.dat") as f:
                        fpga_num = -1
                        for line in f:
                            if line.startswith("EXTF = ") or line.startswith("HW = "):
                                fpga_num = int(line.split("=")[1].split(",")[0])
                                error_info += ["\n"+line.split(",")[0]+"\n",]
                            if "Err" in line:
                                if "Failed" in line: continue
                                errs = 0
                                n_channels = 1
                                if "per ch" in line or "Err:" in line: n_channels = 4  # It would be nice if this were less hacky
                                words = line.split()[-1*n_channels:]
                                for word in words: errs += int(word, 16)
                                error_info += [line]

                                # Flag as an issue
                                if fpga_num not in getFPGAs(full_path): continue
                                ignore = False
                                for err in ignore_errors:
                                    if err in line: ignore = True
                                if errs > 0 and not ignore:
                                    print fpga_num, getFPGAs(full_path)
                                    print line, errs
                                    found_errors = True

                printWithSlot(board,crate,slot)
                if not found_errors: print "\t\t", "No additional errors found."
                else:
                    for line in error_info:
                        print "\t\t", color.RED+line+color.END,

# Prints out any additional info not included in other functions
def additionalInfo(log_dir, board_dic):

    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

                found_errors = False
                error_info = []
                general_info = []

                if board == 'DF-IM':
                    general_info += ["Last L1ID at the input to the DF in each ENABLED channel:\n"]
                    links_block =  getDFLinksBlock(full_path + "/" + getIMDFFileName(full_path, crate, slot))
                    sep=(' |')
                    ilink = -1
                    link_l1ID = []
                    link_fmcin = []

                    for line in links_block:
                        if 'I' in line.split(sep)[1] or 'R' in line.split(sep)[1]: ilink += 1
                        if 'E' in line.split(sep)[0]:
                            if '(' in line.split(sep)[2]:
                                if ilink > 15: continue
                                l1ID = line.split('(',1)[1].split(')')[0]
                                decl1ID = int(l1ID, 16)
                                fmcin   = int((line.split(sep)[1]).split()[1])
                                link_l1ID  += [(ilink, decl1ID)]
                                link_fmcin += [(ilink, fmcin)]
                                general_info += ["ch"+str(ilink)+": "+"0x"+l1ID+", dec: "+str(decl1ID)]
                                #ilink += 1

                    link_l1ID.sort(key = lambda x: x[1])
                    slowest_ch = link_l1ID[0][0]
                    fastest_ch = link_l1ID[-1][0]
                    general_info += ["Slowest channel: "+str(slowest_ch)]
                    general_info += ["Fastest channel: "+str(fastest_ch)+"\n"]
                    
                    general_info += ["FMC input header/trailer counts:\n"]
                    unique_fmcin = set(item[1] for item in link_fmcin)
                    for y in unique_fmcin:
                        imatch=0
                        for x in link_fmcin:
                            if x[1]==y: imatch += 1
                        if imatch==1: 
                            unique_ch = [item[0] for item in link_fmcin if item[1] == y]
                            general_info += ["ch"+ str(unique_ch[0]) + " has a different number of FMCIN counts than all other channels! FMCIN: "+str(y)]
                                                
                if board == 'AUX':
                    general_info += ["Cluster, Module, and SSID counts in AUX (Note: these counters often turn over):\n"]
                    with open(full_path+"/aux_input_status_main.dat") as f:
                        for line in f: general_info += [line.strip("\n")]
                    general_info += [""]

                    road_counts = []
                    with open(full_path+"/aux_proc_status_main.dat") as f:
                        lines = f.readlines()
                        for (i, line) in enumerate(lines):
                            if "Road Count:" in line:
                                road_counts = [int(x) for x in line.split(":")[1].split("0x")[0].split()]
                                average_roads = sum(road_counts)/len(road_counts)
                                general_info += getCounterRates(road_counts, "Road counts:")
                            if "Fits:" in line:
                                fit_counts = [int(x) for x in lines[i+1].split(":")[1].split()]
                                general_info += getCounterRates(fit_counts, "Fit counts:")
                            if "Tracks:" in line:
                                track_counts = [int(x) for x in lines[i+1].split(":")[1].split()]
                                general_info += getCounterRates(track_counts, "Track counts:")

                        for (i, count) in enumerate(road_counts):
                            if average_roads==0:
                                pass
                            elif count==0:
                                found_errors = True
                                error_info += [color.RED + "No roads found in Proc %d"%i + color.END]
                            elif 1.*abs(average_roads - count)/average_roads > 1:
                                found_errors = True
                                error_info += ["Skew detected in Proc %d roads"%i]

                    with open(full_path+"/aux_status_main.dat") as f:
                        lines = [x.strip("\n") for x in f.readlines()]
                        for (i, line) in enumerate(lines):
                            if "--- OUTPUT TRACKS ---" in line:
                                general_info += [""]
                                general_info += lines[i:i+2]

                if board == 'AMB':
                    #TODO
                    continue

                if board == 'SSB':
                    with open(full_path+"/ssb_status_main.dat") as f:
                        for line in f:
                            if line.startswith("EXTF = ") or line.startswith("HW = "): general_info += ["", line.split(",")[0]]
                            if "Total tracks" in line:
                                for place in ["Fitter", "(tf lmt)", "HW="]:
                                    if place in line:
                                        n_tracks = int(line.split(place)[1].split("@")[0].split()[0], 16)
                                        output = "%40s"%(line.split("# ")[1].split(place)[0]+place)
                                        trks_per_evt = 0
                                        if ssb_n_events > 0: trks_per_evt = float(n_tracks)/ssb_n_events
                                        output += "\t%15s  \t%10s / event  \t%10s kHz"%(str(n_tracks),intOrFloat(1.*trks_per_evt), intOrFloat(1.*ssb_rate*trks_per_evt))
                                        general_info += [output]

                printWithSlot(board,crate,slot)
                for line in general_info: print "\t\t", line
                if found_errors:
                    for line in error_info:
                        print "\t\t", line

# Reports all back pressure in the system
def describeBP(log_dir, board_dic):

    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

                found_bp = False
                bp_info = []

                if board == 'DF-IM':

                    links_block = getDFLinksBlock(full_path + "/" + getIMDFFileName(full_path, crate, slot))

                    output = "Channels     "
                    for line in links_block:
                        output += line[0:2] + "  "
                    bp_info += [output+"\n"]

                    sep=' |'
                    output = "Enabled?      "
                    for line in links_block:
                        if 'E' in line.split("|")[0]:
                            output += "E   "
                        else:
                            output += "-   "
                    bp_info += [output+"\n"]

                    output = "X-off?        "
                    for line in links_block:
                        if 'X' in line.split(sep)[0]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]


                    output = "INPUTSW IN?   "
                    for line in links_block:
                        if 'X' in line.split(sep)[2]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "ODO IN?       "
                    for line in links_block:
                        if 'X' in line.split(sep)[3]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "OUTSW IN?     "
                    for line in links_block:
                        if 'X' in line.split(sep)[4]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "OUTSW OUT?    "
                    for line in links_block:
                        if 'X' in line.split(sep)[5]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "PACKER IN?    "
                    for line in links_block:
                        if 'X' in line.split(sep)[7]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "SLINK OUT?    "
                    for line in links_block:
                        if 'X' in line.split(sep)[8]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "CENTSW IN?    "
                    for line in links_block:
                        if 'X' in line.split(sep)[9]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "CENTSW OUT?   "
                    for line in links_block:
                        if 'X' in line.split(sep)[10]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "INTOUTSW IN?  "
                    for line in links_block:
                        if 'X' in line.split(sep)[11]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "INTOUTSW OUT? "
                    for line in links_block:
                        if 'X' in line.split(sep)[12]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                    output = "IntLink TX?   "
                    for line in links_block:
                        if line.startswith("        |        FMCIN"): continue
                        if 'X' in line.split(sep)[13]:
                            output += "X   "
                            found_bp = True
                        else:
                            output += "    "
                    bp_info += [output+"\n"]

                if board == 'AUX':

                    with open(full_path+"/aux_linkstatus_main.dat", "r") as f:
                        for line in f:
                            if "f_{HOLD}" in line: bp_info.append(line)
                            cols = line.split("|")
                            if len(cols)==15:
                                if "1." in cols[4]:
                                    found_bp = True
                                    bp_info.append(line)

                if board == 'AMB':
                    with open(full_path+"/ambslp_status_main.dat", "r") as f:
                        for line in f:
                            if "hold" in line:
                                bp_info.append(line)
                                if "1" in line.split("status:")[1]: found_bp = True

                if board == 'SSB':

                    with open(full_path+"/ssb_status_main.dat", "r") as f:
                        lines = f.readlines()
                        for (i, line) in enumerate(lines):
                            if line.startswith("Links"): bp_info.append(line)
                            if ("BP to:" in line and "Sum BP to:" not in line):
                                bp_info.append(lines[i-1])
                                bp_info.append(lines[i])
                            if line.startswith("Full reg (for now only bp from flic)"): bp_info.append(line)

                    for line in bp_info:
                        if line.startswith("Links"):
                            status=int(line.split("(")[1].split(")")[0], 16)
                            if not (status >> 1)&1:
                                found_bp = True
                                bp_info += ["Sending BP to DFA\n"]
                            if not (status >> 4)&1:
                                found_bp = True
                                bp_info += ["Sending BP to DFB\n"]
                            if not (status >> 7)&1:
                                found_bp = True
                                bp_info += ["Sending BP to AUXA\n"]
                            if not (status >> 10)&1:
                                found_bp = True
                                bp_info += ["Sending BP to AUXB\n"]
                        elif "Sum" in line or "Frc" in line or "Max" in line:
                            pass
                        else:
                            if "1" in line: found_bp = True


                printWithSlot(board,crate,slot)
                if not found_bp: print "\t\t", "No back-pressure being sent."
                else:
                    for line in bp_info:
                        print "\t\t", line,

# Reports all freezes in the system
def findFreezes(log_dir, board_dic):

    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

                found_freeze = False
                freeze_info = []

                if board == 'DF-IM':
                    #TODO
                    continue
                if board == 'AUX':

                    writing_freeze = False
                    with open(full_path+"/aux_status_main.dat", "r") as f:
                        for line in f:
                            if "-- FREEZE STATUS --" in line: writing_freeze = True
                            if "--- SLINK STATUS ---" in line: writing_freeze = False
                            if "Freeze Info:" in line: writing_freeze = True
                            if writing_freeze: freeze_info.append(line)

                    for line in freeze_info:
                        if "Freeze Status:" in line or "Hit Warrior:" in line:
                            nums = line.strip("\n").split(":")[1].split("0x")
                            for num in nums:
                                if not (num.strip() == "0" or num.strip() == ""): found_freeze = True

                if board == 'AMB':
                    with open(full_path+"/ambslp_status_main.dat", "r") as f:
                        for line in f:
                            if "Freeze" in line and ":" in line:
                                freeze_val = line.split()[-1]
                                if freeze_val != "0":
                                    found_freeze = True
                                    freeze_info += [line]
                if board == 'SSB':
                    #TODO
                    continue

                printWithSlot(board,crate,slot)
                if not found_freeze: print "\t\t", "No freezes found."
                else:
                    for line in freeze_info:
                        print "\t\t", line,

# Reports all FIFO overflows in the system
def getOverflow(log_dir, board_dic):

    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

                found_overflow = False
                overflow_info = []

                if board == 'DF-IM':
                    #TODO
                    continue
                if board == 'AUX':

                    with open(full_path+"/aux_fifo_status_main.dat", "r") as f:
                        for line in f:
                            if "f_{HOLD}" in line: overflow_info.append(line)
                            cols = line.split("|")
                            if len(cols)==9:
                                if "BAD" in cols[6]:
                                    found_overflow = True
                                    overflow_info.append(line)

                if board == 'AMB':
                    #TODO
                    continue
                if board == 'SSB':
                    #TODO
                    continue

                printWithSlot(board,crate,slot)
                if not found_overflow: print "\t\t", "No overflows found."
                else:
                    for line in overflow_info:
                        print "\t\t", color.RED+line+color.END,

# Determines whether all links are up
def linkStatus(log_dir, board_dic):

    for board in board_dic:
        for crate in board_dic[board]:
            for slot in board_dic[board][crate]:
                full_path = os.path.join(log_dir,board,crate,slot)

                found_error = False
                error_info = []

                if board == 'DF-IM':
                    with open(full_path+"/"+getIMDFFileName(full_path, crate, slot)) as f:
                        for line in f:
                            if line.startswith("GT=["):
                                link        = int(line.split("GT=[")[1].split("]")[0])
                                force_ready = line.split("FORCE_READY=[")[1].split("]")[0].strip()
                                lderr       = line.split("LDERR=[")[1].split("]")[0].strip()
                                lup         = line.split("LUP=[")[1].split("]")[0].strip()

                                link_type = "UNKNOWN"
                                if link < 8: link_type = "AUX"
                                if link in [16, 17, 34, 35]: link_type = "SSB"
                                
                                if force_ready=="OFF" and (lderr=="BAD" or lup=="BAD"):
                                    found_error = True
                                    error_info += [color.RED+"Found link problem on channel %d. (%s link)\n"%(link,link_type)+color.END]

                if board == 'AUX':
                    with open(full_path+"/aux_linkstatus_main.dat", "r") as f:
                        for line in f:
                            if "f_{HOLD}" in line: error_info.append(line)
                            if "Input" in line or "Processor" in line:
                                if not line.split("|")[2].strip()=="ON":
                                    if "Input 2 SFP" in line and "SSB" not in board_dic: continue # Don't show SSB link down when no SSB
                                    found_error = True
                                    error_info += [line]
                if board == 'AMB':
                    with open(full_path+"/ambslp_status_main.dat", "r") as f:
                        lines = f.readlines()
                        for i, line in enumerate(lines):
                            if line.startswith("GTP align QUAD216"):
                                error_info += ["HIT\n"]
                                error_info += lines[i:i+9]
                        for i, line in enumerate(lines):
                            if line.startswith("ROAD input link UP QUAD216"):
                                error_info += ["ROAD\n"]
                                error_info += lines[i:i+9]
                        for line in error_info:
                            if ":" in line:
                                status = line.split(":")[1].strip('\n')
                                if not (status=="  1 - 1 - 1" or status=="  f - TX f" or status=="  f"):
                                    found_error = True

                if board == 'SSB':
                    with open(full_path+"/ssb_status_main.dat", "r") as f:
                        fpga_num = -1
                        for line in f:
                            if line.startswith("EXTF = ") or line.startswith("HW = "):
                                fpga_num = int(line.split("=")[1].split(",")[0])
                            if line.startswith("Links"):
                                error_info.append(line)
                                aux_up = line.split("(")[3].split(")")[0]
                                aux_up = aux_up.split(",")[0]           # For now, only considering AUX A, should be removed when we have 2 AUXen
                                df_up = line.split("(")[5].split(")")[0]
                                if '0' in aux_up or '0' in df_up:
                                    if fpga_num in getFPGAs(full_path):
                                        found_error = True

                printWithSlot(board,crate,slot)
                if not found_error: print "\t\t", "All links up."
                else:
                    for line in error_info:
                        print "\t\t", color.RED+line+color.END,
#*****************************************************************************************************************

def main():

    parser = argparse.ArgumentParser(description='Add input information.')
    parser.add_argument('-l', dest='log_dir')
    args = parser.parse_args()

    printHeader("Starting post-processing...")
    try: release = os.environ["FTK_RELEASE"]
    except: release = "NONE"

    printHeader("FTK release: %s" %release)
    log_dir = str(args.log_dir)
    if not os.path.exists(log_dir):
        print "Couldn't find directory with output."
        exit(1)
    print "Working with outputs in dir", log_dir

    board_dic = getBoardsAndSlots(log_dir)
    printHeader("Boards included in this dump:")
    print board_dic

    printHeader("Firmware of included boards:")
    printFW(log_dir, board_dic)

    printHeader("Link status for included boards:")
    linkStatus(log_dir, board_dic)

    printHeader("Freezes in the system:")
    findFreezes(log_dir, board_dic)

    printHeader("Back-pressure in the system:")
    describeBP(log_dir, board_dic)

    printHeader("FIFO overflows in the system:")
    getOverflow(log_dir, board_dic)

    printHeader("Any additional errors in the system:")
    additionalErrors(log_dir, board_dic)

    printHeader("Rates measured by different boards:")
    getRates(log_dir, board_dic)

    printHeader("Events through each board:")
    eventsProcessed(log_dir, board_dic)

    printHeader("Additional information:")
    additionalInfo(log_dir, board_dic)

    print ""
    print "Output stored in summary.txt"

    print ""

main()
