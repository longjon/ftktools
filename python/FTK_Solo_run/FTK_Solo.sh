#!/bin/bash

# Define colors
RED='\033[1;31m'
YELLOW='\033[1;33m'
NC='\033[0m'

# Set default options
SEGNAME="FTK"
PNAME=$TDAQ_PARTITION
ISLAB4=false
if echo "$HOSTNAME" | grep -q "tbed"; then
    ISLAB4=true
fi

# Define a few options for the script to run
while getopts ":hnslp:f:o:S:" opt; do
    case $opt in
        h)
            echo "Options for FTK_Solo.sh: " >&2
            echo "     -n           : Run without dumping spybuffers" >&2
            echo "     -p <pname>   : Specify a partition; default is \$TDAQ_PARTITION" >&2
			echo "     -S <segname> : Specify the segment name; default is FTK" >&2
            echo "     -s           : Run without printing a summary" >&2
            echo "     -l           : Keep all outputs local; don't transfer to the calibration node" >&2
			echo "     -o <output>  : Specify the name of the folder in which the output should be stored (used by the watchdog)" >&2
            echo ""
            exit
            ;;  
        n)
            echo -e "${YELLOW}Running without dumping buffers.${NC}"
            NOBUFFERS=true
            ;;
        p)
						echo -e "${YELLOW}Changed the partition name to ${OPTARG}.${NC}"
            PNAME=$OPTARG
            ;;
				S)  
						echo -e "${YELLOW}Changed the segment name to ${OPTARG}.${NC}"
            SEGNAME=$OPTARG
            ;;
				o)	
						echo -e "${YELLOW}Forcing the output to be placed in the ${OPTARG} folder .${NC}"
            OUTPUTDIR=$OPTARG
            ;;
        s)
            NOSUMMARY=true
            ;;
        l)
            echo -e "${YELLOW}Keeping outputs local -- nothing will be transferred to the calibration node.${NC}"
            NOTRANSFER=true
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            ;;
    esac
done

# Make sure no one's using the old interface without options
shift $((OPTIND -1))
if [ -n "$1" ]; then
    echo -e "${RED}Unrecognized argument $1${NC}"
    echo "Run with option -h for more information on proper usage."
    echo ""
    exit
fi
echo ""

# Determine lab4 / p1 specific values
if [ $ISLAB4 = true ] 
then
    OUTPUTPATH=$PWD
    MYPYTHONPATH=/afs/cern.ch/user/f/ftk/inst/releases/${FTK_RELEASE}/installed/share/lib/python/FTK_Solo_run/
    SETUP_SCRIPT=/afs/cern.ch/user/f/ftk/bin/setup_ftk.sh
else
    OUTPUTMACHINE=pc-tdq-calib-13
    SETUP_SCRIPT=/det/ftk/ftk_setup.sh
    OUTPUTPATH=/detwork/ftk/solo_run/
    TRANSFIRDIR=/data/SoloRun/
    MYPYTHONPATH=/det/ftk/repo/releases/${FTK_RELEASE}/installed/share/lib/python/FTK_Solo_run/
fi

export PYTHONPATH="${PYTHONPATH}:${MYPYTHONPATH}"


echo "SoloRun options set:"
echo "Using Lab4:     $ISLAB4"
echo "Partition name: $PNAME"
echo "Segment name:	  $SEGNAME"
echo "Release:        $FTK_RELEASE"
#echo "Output path:  $OUTPUTPATH"   #Not really used now that we transfer @ p1
echo ""

function main {

# Calling the funtion currentDirTree to set directory tree created by the funtion as the value of dirT
dirT=$(currentDirTree)
cd $OUTPUTDIR
echo "Working Directory: $dirT"

# Call the FTK_Solo.read_oks_hosts function to obtain an array with all the nodes on which the RCDs are running
echo "Going to collect the hosts on which the RCDs are running"
hosts=$(python -c 'from __future__ import print_function 
import sys, FTK_Solo 
print(FTK_Solo.read_oks_hosts("'$PNAME'","'$SEGNAME'"), file=sys.stderr)
' 2>&1 1>/dev/null)

if [[ $hosts == *"Traceback"* ]];
then
    echo "Failed to retrieve hosts:"
    echo $hosts
    exit 1
fi

for pc in $hosts:
do 
  echo "pc: $pc"
done



declare -a ARRAY
COUNTER=0
for pc in $hosts:
do
  ARRAY[$COUNTER]=`echo ${pc} | tr -d \'\[\],:`
  COUNTER=$((COUNTER+1))
done

host_connect=''
for host in "${ARRAY[@]}"; 
do 
	echo "going to connect to host $host"
  host_connect=$host
	ssh -T $host << EOF
        cd $OUTPUTPATH
	echo \$PWD
        source $SETUP_SCRIPT $FTK_RELEASE 
        dir="$1"
        `# Using PYTHONPATH to reach the directory where the Solo_test.py module is to be found`
        PYTHONPATH=$PYTHONPATH python -c 'import FTK_Solo as fs; fs.necessary_dirs()'
        echo "Starting the board reading..."
        PYTHONPATH=$PYTHONPATH python -c 'import FTK_Solo as fs; fs.read_oks("$dirT", "$ISLAB4", "$PNAME", "$SEGNAME", "$NOBUFFERS")'
        echo "Copying log files"
        getLogFiles.sh $dirT $PNAME 
EOF
host_connect=''

done

echo $PWD

# Run post-processing script to summarize outputs
if [ ! -n "$NOSUMMARY" ]
then
    echo "Running post-processing:"
    echo "python SoloRun_post.py -l $dirT"
      SoloRun_post.py -l $dirT | tee $dirT/summary.txt
fi

# If at P1, transfer output to the calibration node
if [ $ISLAB4 = false ] 
then
    if [ ! -n "$NOTRANSFER" ]; then
        TRANSFER "${dirT}" 
    fi
else
    if [ ! -n "$NOTRANSFER" ]; then
      echo "Full output of SoloRun: ${dirT}"
      echo "Lab4: Moving output to eos rep in /eos/atlas/atlascerngroupdisk/det-ftk/Lab4_SoloRun/"
      TRANSFERLAB4_TOEOS "${dirT}"
    fi
fi
}

`#********************************************************************************************************************
#
#	Date:            UPDATE:
#	-----------	 -------------------------------------------------------------------------------------------
#
#	2017-07-10    	The funtion currentDirTree, creates a directory tree in the current working directory to store the spybuffers
#               	from the boards/cards. The directory tree is of the form:
#                             ~/spybuffers/(date: YYYY-mm-dd)/(time: HH:MM:SS)/
#********************************************************************************************************************`

function currentDirTree {
if [ -n "$OUTPUTDIR" ];
then
	tempDir=$OUTPUTPATH/$OUTPUTDIR
else
	timeDate=$(date "+%Y-%m-%d")
	timeHour=$(date "+%Hh%Mm%Ss")
	tempDir=$OUTPUTPATH/$timeDate/$timeHour/$PNAME
fi
echo "${tempDir}"
mkdir -p "${tempDir}"
}

function TRANSFER {
partDir=$(basename "$1")
#echo "partDir= $partDir"
fullDateDir=$(dirname "$1")
#echo "fullDateDir= $fullDateDir"
timeDir=$(basename "$fullDateDir")
#echo "timeDir= $timeDir"
temp=$(dirname "$fullDateDir")
dateDir=$(basename "$temp")
#echo "dateDir= $dateDir"
echo "Transferring output to $TRANSFIRDIR/moveToEOS/$dateDir/$timeDir/$partDir on $OUTPUTMACHINE" 

ssh -T $OUTPUTMACHINE << EOF
    # Check if there's an output directory for today's date already and make if not
    if [ ! -d $TRANSFIRDIR/$dateDir ]; then
        #echo "Creating directory $TRANSFIRDIR/$dateDir"  #This will probably just confuse people
        mkdir $TRANSFIRDIR/$dateDir
    fi

    # Move the output to that location
    mv $fullDateDir $TRANSFIRDIR/$dateDir

    # Then move do the same thing over in a subdir and change permissions... 
    # Final atomic operation to let the copying script move the files to EOS
    if [ ! -d $TRANSFIRDIR/moveToEOS/$dateDir ]; then               
        echo "Creating directory $TRANSFIRDIR/moveToEOS/$dateDir"   
        mkdir $TRANSFIRDIR/moveToEOS/$dateDir 
		chmod -R a+w $TRANSFIRDIR/moveToEOS/$dateDir
    fi  
	
    mv $TRANSFIRDIR/$dateDir/$timeDir $TRANSFIRDIR/moveToEOS/$dateDir/
		echo "Changing the rights to the files"
	chmod -R a+w $TRANSFIRDIR/moveToEOS/$dateDir/$timeDir/$partDir
    #rm -r $fullDateDir
    rm -r $TRANSFIRDIR/$dateDir/
        
    echo -e "${YELLOW}This output will be copied to eos and eventually deleted from P1. Please be aware of this when you e-log the location.${NC}"
    echo -e "${YELLOW}It should appear on eos within the hour, here: /eos/atlas/atlascerngroupdisk/det-ftk/SoloRun/$dateDir/$timeDir${NC}"
EOF
}

function TRANSFERLAB4_TOEOS {

partDir=$(basename "$1")
#echo "partDir= $partDir"
fullDateDir=$(dirname "$1")
#echo "fullDateDir= $fullDateDir"
timeDir=$(basename "$fullDateDir")
#echo "timeDir= $timeDir"
temp=$(dirname "$fullDateDir")
dateDir=$(basename "$temp")
out="/eos/atlas/atlascerngroupdisk/det-ftk/Lab4_SoloRun/$dateDir/$timeDir/$partDir"
out_to="/eos/atlas/atlascerngroupdisk/det-ftk/Lab4_SoloRun/$dateDir"
if [ "$dirT" == "$out" ]
then
echo "Already in the eos output directory"
else
    if [ ! -d $out_to ]; then
        echo "Creating directory $out_to"   
        mkdir $out_to
    fi
    echo "Copying data to $out_to"
    cp -r $fullDateDir $out_to
fi
}

#function trap_ctrlc ()
#{
    # perform cleanup here
#  echo "Ctrl-C caught...performing clean up"

#  echo "Doing cleanup"
#  if [ "$host_connect" != "" ]; then
#  ssh -T $host_connect << EOF
#  cd /tmp/
#  if [ -f /tmp/SoloRunID ]; then
#  while IFS= read -r line; do
#    echo "Text read from file: \$line"
#    a=\$line
#  done < SoloRunID
#  echo Killing processes on the node having PID: \$a
#  kill -9 -\$(ps -o pgid= \$a | grep -o '[0-9]*')
#  rm /tmp/SoloRunID
#  fi
#EOF
#fi
  # exit shell script with error code 2
  # if omitted, shell script will continue execution
#exit 2
#}
#trap "trap_ctrlc" 2

main
