# e.g. FTK_DF, IM-DF-AUX-AMB-SSB-FLIC
partition=${1}
# e.g. 3 (DF22 at lab4)
shelf=${2}
# e.g. 9 (DF22 at lab4)
slot=${3}
# e.g. DF22, DF-3-08 etc
df=${4}
# e.g. FTK-RCD-DF-Slice2, FTK-RCD-DF-MultiBoards, FTK-RCD-DF-Shelf3-4, FTK-RCD-DF
MonitoringOutput=${5}

#echo "aaa "${partition}" "${df}" "${shelf}" "${slot}

date=`date +%Y%m%d%H%M`
file="ISIMAllSpy_"$df"_"$date".dat"

if [ $# -lt 4 ];  then
    echo ""
    echo "usage : bash SaveAllSpyISEMon_IMDF.sh [partition] [shelf] [slot] [dfID] [RCD]"
    echo "example of Lab 4 by DF22"
    echo ">bash SaveAllSpyISEMon_IMDF.sh [IM-DF-AUX-AMB] [3] [3] [DF22] [RCD]"
    echo "ERROR  4 arguments required"
    echo ""
    exit 1
fi


#### Write the Firmware version
test_df -D ${df} -M 21 >> ${file}
im_monitor -p ${partition} -D ${df} -M 3 -R ${shelf} -l ${slot} -n ${MonitoringOutput} >> ${file}

echo "" >> ${file}
echo "" >> ${file}

#### Run DF Monitoring
echo -n "Dumping DF monitoring information (~3 sec)... "
test_df -p ${partition} -D  ${df} -M 2 >> ${file} 
echo " done"

echo -n "Checking for errors in DF (~long)..."
checkerrors-DF.sh ${df} >> ${file}
echo " done"

#### Write IM monitoring information
echo -n "IM monitoring (~5 sec)..."
echo "" >> ${file}
echo "" >> ${file}
# echo "============ IM Monitoring =============" >> ${file}
im_monitor -p ${partition} -D ${df} -M 2 -R ${shelf} -l ${slot} -n ${MonitoringOutput} >> ${file}
echo " done"

#### Write IM and DF spy buffers
echo "" >> ${file}
echo "" >> ${file}
echo -n "Dumping IM and DF spy buffers...)"
im_monitor -p ${partition} -D ${df} -M 1 -R ${shelf} -l ${slot} -n ${MonitoringOutput} >> ${file}
echo " done"

# This checks that the spy buffer is at least 10,000 lines long
# If it isn't then it's assumed that the IM+DF spys weren't read, and complains
file_length=$(wc $file | awk '{print $1}')
if [ $file_length -lt 10000 ]
then
	echo "ERROR! IM+DF Spy Buffers not saved!"
	echo "This probably means you have an atypical RCD name"
	echo "Please rerun, pass the RCD name as an argument:"
	echo ">bash SaveAllSpyISEMon_IMDF.sh [IM-DF-AUX-AMB] [3] [3] [DF22] [FTK-RCD-DF-Slice2]"
fi
