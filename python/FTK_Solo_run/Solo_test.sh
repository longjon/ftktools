#!/bin/bash

function main {
PYTHONPATH=/atlas-home/1/jsolorio/ python -c 'import FTK_Solo as fs; print fs.necessary_dirs()'
timeDate=$(date "+%Y-%m-%d")
timeHour=$(date "+%H:%M:%S")
echo "$timeDate"
echo "$timeHour"
echo "passing the timedir"
dirT=$(currentDirTree)
echo "${dirT}"
AUX-SSB-AMB "${dirT}"
DF-IM "${dirT}"
}

function currentDirTree {
timeDate=$(date "+%Y-%m-%d")
timeHour=$(date "+%H:%M:%S")
tempDir=./StatusOutput/$timeDate/$timeHour/
echo "${tempDir}"
mkdir -p "${tempDir}"
}

function AUX-SSB-AMB {
ssh -T sbc-ftk-rcc-04 << EOF
        source /det/ftk/setup_SliceA_04.sh
        dir="$1"
        echo "in AUX bash func"
        echo "$dir/AUX"
       	PYTHONPATH=/atlas-home/1/jsolorio/ python -c 'import FTK_Solo as fs; fs.AUX_board_commands("$1")'
	PYTHONPATH=/atlas-home/1/jsolorio/ python -c 'import FTK_Solo as fs; fs.SSB_commands("$1")'
	PYTHONPATH=/atlas-home/1/jsolorio/ python -c 'import FTK_Solo as fs; fs.AMB_commands("$1")'
EOF
}

function DF-IM {
ssh -T pc-ftk-tdq-01 << EOF
        export DF_IPUBUS_CONFIG=/det/tdaq/ftk/repo/condDB/latest/DataFormatter/ver1.5/config
        dir="$1"
        echo "in DF-IM bash func"
        echo "$dir + /DF-IM"
        PYTHONPATH=/atlas-home/1/jsolorio/ python -c 'import FTK_Solo as fs; fs.DF_IM_commands("$1")'
EOF
}

main
