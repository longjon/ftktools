import os
import json
import glob


def getDictJsonPath():
    if os.path.isfile('/det/ftk/tools/DF_cabling/dfDict.json'):
        # Check if central dictionary exists (will only be at p1)
        dictionary_path = '/det/ftk/tools/DF_cabling/dfDict.json'
    else:
        # Else grab the one where the ftkmaker module lives
        script_path = os.path.abspath(__file__)
        script_dir  = os.path.split(script_path)[0]
        dictionary_path = script_dir+"/dfDict/dfDict.json"
        if not os.path.exists( dictionary_path ):
            dictionary_path = ""
            for dataDir in os.environ['TDAQ_DB_PATH'].split(':'):
                csDir = dataDir + "/ftktools/"
                dictionary_tmp  = os.path.join(csDir, "dfDict/dfDict.json")
                if os.path.exists( dictionary_tmp ):
                    dictionary_path = dictionary_tmp
                    break
            if not dictionary_path:
                print "ERROR! No dfDict.json was found by df_tools.getDictJsonPath()"
    return dictionary_path

def getDictInfo(shelf,slot,channel,idx):
    """ Given shelf/slot, returns other information
    idx = 0 -> ROB id
    idx = 1 -> cable id
    idx = 2 -> IM channel
    idx = 3 -> Subdetector (ibl,pix,sct)
    """
    dictionary_path = getDictJsonPath()
    cableDictionary = json.loads(open(dictionary_path).read())

    i=0
    try: cableDictionary["%i-%i" % (int(shelf),int(slot))] #Check to make sure this df exists/is mapped
    except KeyError:
        print "A requested DF doesn't exist in the mapping - check df-shelves input"
        return 'KeyError'
    while i < len(cableDictionary["%i-%i" % (int(shelf),int(slot))])-3:
        if int(cableDictionary["%i-%i" % (int(shelf),int(slot))][i][2]) == channel: return cableDictionary["%i-%i" % (int(shelf),int(slot))][i][idx]
        i+=1
    return 'NONE'

def makePseudoDataConfig(pseudodataDir,shelf,slot):
    """ Makes a IM_PseudoData.txt file per board """

    if not os.path.exists(pseudodataDir):
        print "Pseudodata directory does not exist."
        return

    else:
        # Creates directory for board if it doesn't exist
        if not os.path.exists("ftk/DFconfigFiles/DF-%s-%s/" % (shelf,slot)):
            os.makedirs("ftk/DFconfigFiles/DF-%s-%s/" % (shelf,slot))

        #if os.path.exists('ftk/DFconfigFiles/DF-%s-%s/IM_PseudoData' % (shelf,slot)):
        #    os.unlink('ftk/DFconfigFiles/DF-%s-%s/IM_PseudoData' % (shelf,slot))

        os.system('ln -s %s ftk/DFconfigFiles/DF-%s-%s/IM_PseudoData' % (pseudodataDir,shelf,slot))
        f = open('ftk/DFconfigFiles/DF-%s-%s/IM_PseudoData.txt' % (shelf,slot), 'w')
        # Loop over lanes per DF
        for i in range(16):
            robID = getDictInfo(shelf,slot,i,0)[-6:] # gets ROB info from shelf/slot

            if robID == 'KeyError': #Catch problems
                f.write('This DF does not exist in our mapping')
                break

            elif robID == 'NONE': f.write('%i ###\n' % i) # write '###' if no cable
            else:
                pseudodataFile = glob.glob(pseudodataDir+'/*'+robID+'*')
                if len(pseudodataFile) > 1:
                        print "Problem! More than one file matches this ROB ID - writing #"
                        f.write('%i #\n' % i)
                elif len(pseudodataFile) < 1:
                        print "No matching pseudodata file! writing # for this lane"
                        f.write('%i #\n' % i)
                else:
                    pseudodataFile = 'IM_PseudoData/'+pseudodataFile[0].split('/')[-1]
                    f.write('%i %s\n' % (i,pseudodataFile))
        f.close()

def makel4PseudoDataConfig(pseudodataDir):
    """function to deal with lab4 case"""

    if not os.path.exists(pseudodataDir):
        print "Pseudodata directory does not exist."
        return

    else:
        # Creates directory for board if it doesn't exist
        if not os.path.exists("ftk/DFconfigFiles/DF22/"):
            os.makedirs("ftk/DFconfigFiles/DF22/")

        #if os.path.exists('ftk/DFconfigFiles/DF22/IM_PseudoData'):
        #    os.unlink('ftk/DFconfigFiles/DF22/IM_PseudoData')

        os.system('ln -s %s ftk/DFconfigFiles/DF22/IM_PseudoData' % (pseudodataDir))
        f = open('ftk/DFconfigFiles/DF22/IM_PseudoData.txt', 'w')

        # hard coded ROB ids
        robIDs = ['00140103',
                  'NONE',
                  '00111750',
                  'NONE',
                  '00112521',
                  '00220100',
                  '00112559',
                  '00220101',
                  '00140093',
                  'NONE',
                  '00130109',
                  '00220102',
                  '00111710',
                  'NONE',
                  '00130108',
                  '00220103']

        # Loop over lanes per DF
        for i in range(16):

            robID = robIDs[i]

            if robID == 'NONE': f.write('%i ###\n' % i) # write '###' if no cable
            else:
                pseudodataFile = glob.glob(pseudodataDir+'/*'+robID+'*')
                if len(pseudodataFile) > 1:
                        print "Problem! More than one file matches this ROB ID - writing #"
                        f.write('%i #\n' % i)
                elif len(pseudodataFile) < 1:
                        print "No matching pseudodata file! writing # for this lane"
                        f.write('%i #\n' % i)
                else:
                    pseudodataFile = 'IM_PseudoData/'+pseudodataFile[0].split('/')[-1]
                    f.write('%i %s\n' % (i,pseudodataFile))


def binary_string_to_hexmask(binary_string):
    """
    Takes a 16 digit string of binary and turns it into a hex mask
    :param binary_string: 16 digit binary
    :return: mask - 4 digit mask in hex
    """
    return (str(hex(int(binary_string[0:4], 2)))[2:]  +
            str(hex(int(binary_string[4:8], 2)))[2:]  +
            str(hex(int(binary_string[8:12], 2)))[2:] +
            str(hex(int(binary_string[12:16], 2)))[2:] )

def get_slink_mode(shelf,slot):
    """
    Sets mode for SLINK in form of mask
    :param shelf: int - DF shelf no
    :param slot: int - DF slot no
    :return: string - slink mask for board
    """
    slink_mode=""
    for channel in range(16):

        rob_id = getDictInfo(shelf, slot, channel, 0)[-6:]
        """
         11 = Barrel (new readout)
         12 = Disk (old readout)
         13 = B-Layer (old readout)
         14 = IBL (new readout)
         TO BE CHANGED IN 2018!
         """
        if rob_id.startswith("14") or rob_id.startswith("11") or rob_id.startswith("12"):
            channel_slink_mode = 1
        else: channel_slink_mode = 0
        slink_mode+=str(channel_slink_mode)

    # channels go 0-15, reverse that
    slink_mode = slink_mode[::-1]

    return "0x" + binary_string_to_hexmask(slink_mode)

def makeLUTsConfig(lutsDir,shelf,slot):
    """ Makes an IM_LUTs.txt file per board"""

    if not os.path.exists(lutsDir):
        print "LUTs directory does not exist."
        return

    else:
        # Creates directory for board if it doesn't exist
        if not os.path.exists("ftk/DFconfigFiles/DF-%s-%s/" % (shelf,slot)):
            os.makedirs("ftk/DFconfigFiles/DF-%s-%s/" % (shelf,slot))

        #if os.path.exists('ftk/DFconfigFiles/DF-%s-%s/IM_LUTs' % (shelf,slot)):
        #    os.unlink('ftk/DFconfigFiles/DF-%s-%s/IM_LUTs' % (shelf,slot))

        os.system('ln -s %s ftk/DFconfigFiles/DF-%s-%s/IM_LUTs' % (lutsDir,shelf,slot))
        f = open('ftk/DFconfigFiles/DF-%s-%s/IM_LUTs.txt' % (shelf,slot), 'w')
        # Loop over lanes per DF
        for i in range(16):
            robID = getDictInfo(shelf,slot,i,0) # gets ROB info from shelf/slot
            if robID == 'KeyError': #Catch problems
                f.write('This DF does not exist in our mapping')
                return

            elif robID == 'NONE': f.write('%i ###\n' % i) # write '###' if no cable
            else:
                lutsFile = glob.glob(lutsDir+'/*'+robID+'*')
                if len(lutsFile) > 1:
                        print "Problem! More than one file matches this ROB ID - writing #"
                        f.write('%i #\n' % i)
                elif len(lutsFile) < 1:
                        print "No matching LUTs file! writing # for this lane"
                        f.write('%i #\n' % i)
                else:
                    lutsFile = 'IM_LUTs/'+lutsFile[0].split('/')[-1]
                    f.write('%i %s\n' % (i,lutsFile) )
        f.close()

def makel4LUTsConfig(lutsDir):
    """ Makes an IM_LUTs.txt file per board"""

    if not os.path.exists(lutsDir):
        print "LUTs directory does not exist."
        return
    else:
        # Creates directory for board if it doesn't exist
        if not os.path.exists("ftk/DFconfigFiles/DF22/" ):
            os.makedirs("ftk/DFconfigFiles/DF22/" )

        #if os.path.exists('ftk/DFconfigFiles/DF22/IM_LUTs'):
        #    os.unlink('ftk/DFconfigFiles/DF22/IM_LUTs' )

        os.system('ln -s %s ftk/DFconfigFiles/DF22/IM_LUTs' % (lutsDir))
        f = open('ftk/DFconfigFiles/DF22/IM_LUTs.txt' , 'w')

        # hard coded ROB ids
        robIDs = ['0x140103',
                  'NONE',
                  '0x111750',
                  'NONE',
                  '0x112521',
                  '0x220100',
                  '0x112559',
                  '0x220101',
                  '0x140093',
                  'NONE',
                  '0x130109',
                  '0x220102',
                  '0x111710',
                  'NONE',
                  '0x130108',
                  '0x220103']

        # Loop over lanes per DF
        for i in range(16):
            robID = robIDs[i]

            if robID == 'NONE': f.write('%i ###\n' % i) # write '###' if no cable
            else:
                lutsFile = glob.glob(lutsDir+'/*'+robID+'*')
                if len(lutsFile) > 1:
                        print "Problem! More than one file matches this ROB ID - writing #"
                        f.write('%i #\n' % i)
                elif len(lutsFile) < 1:
                        print "No matching LUTs file! writing # for this lane"
                        f.write('%i #\n' % i)
                else:
                    lutsFile = 'IM_LUTs/'+lutsFile[0].split('/')[-1]
                    f.write('%i %s\n' % (i,lutsFile) )
        f.close()



def makeDfDelayValues():
    """Generates df_board_delay_values.txt
        Delays in general no longer needed, so no options implemented here
    """
    f = open('ftk/DFconfigFiles/df_board_delay_values.txt', 'w')
    f.write('####################\n#key lane inv delay\n')
    for i in range(8):
        f.write('LaneDelayVal %i %i %i\n' % (i,0,0)) # All fixed to 0
    f.close()


def isBoardEnabled(slot,shelfMap):
    """ Returns 1 if board enabled, 0 otherwise"""
    logicalMapping = [13,11,9,7,5,3,1,2,4,6,8,10,12,14] # logical mapping (boards left to right)

    for stringPosition in range(0, len(shelfMap)):
        if shelfMap[stringPosition] == "D":
            thisSlot = logicalMapping[stringPosition]
            if slot == thisSlot: return 1
    return 0


def makeMultiboardConfig(activeShelves,isLab4):
    """ Makes df_multiboard_config.txt """
    dictionary_path = getDictJsonPath()
    cableDictionary = json.loads(open(dictionary_path).read())

    if not os.path.exists("ftk/DFconfigFiles/"):
        os.makedirs("ftk/DFconfigFiles/")
    f = open('ftk/DFconfigFiles/df_multiboard_config.txt','w')

    if not isLab4:
        # Make first header
        f.write('################################\n')
        f.write('##Key BoardNumber Enabled?(0 = no, 1 = yes)\n')
        f.write('################################\n')

        slotAll  = range(3,11) # Valid slots 3-10
        for shelf in activeShelves:
            shelfName=shelf[0][-1:]
            shelfMap =shelf[2]
            for slot in slotAll:
                boardIndex = (4*slot-3)+int(shelfName)-10 # gets "DFNumber" or "BoardNumber" from shelf/slot
                f.write("### shelf %i slot %i\n" % (int(shelfName),slot))
                f.write('boardEnable %i %i\n' % ( boardIndex, isBoardEnabled(slot,shelfMap)))

        # Make second header
        f.write('################################\n')
        f.write('##KEY RobID DFNumber IMLane\n')

        for shelf in activeShelves:
            shelfName=shelf[0][-1:]
            shelfMap =shelf[2]
            for slot in slotAll:
                boardIndex = (4*slot-3)+int(shelfName)-10
                f.write('########################\n')
                for key in cableDictionary:
                    if key == 'FTK Shelf-Slot': continue # Only loop over dictionary values
                    elif  int(key.split('-')[0]) == int(shelfName) and int(key.split('-')[1]) == slot:
                        for element in cableDictionary[key]:
                            if type(element) is list:
                                f.write('rodToBoard %s %i %s\n' % (element[0],boardIndex,element[2]))

    else: # Standardized for lab4
        f.write('################################\n')
        f.write('##Key BoardNumber Enabled?(0 = no, 1 = yes)\n')
        f.write('################################\n')
        f.write('### shelf 2 slot 4\n')
        f.write('boardEnable 5  1\n')
        f.write('################################\n')
        f.write('##KEY RobID DFNumber IMLane\n')
        f.write('########################\n')
        f.write('########################\n')
        f.write('rodToBoard 0x220100 5 5\n')
        f.write('rodToBoard 0x220101 5 7\n')
        f.write('rodToBoard 0x220102 5 11\n')
        f.write('rodToBoard 0x220103 5 15\n')
        f.write('rodToBoard 0x112559 5 6\n')
        f.write('rodToBoard 0x130108 5 14\n')
        f.write('rodToBoard 0x112521 5 4\n')
        f.write('rodToBoard 0x111750 5 2\n')
        f.write('rodToBoard 0x140103 5 0\n')
        f.write('rodToBoard 0x140093 5 8\n')
        f.write('rodToBoard 0x111710 5 12\n')
        f.write('rodToBoard 0x130109 5 10\n')
        f.write('########################\n')

    f.close()

def linkModuleList(moduleListFile,moduleListName):
    """Links the actual module list file to the path set in the RCD
    moduleListFile should be the centralized module list, maintained by IM+DF experts
    moduleListName is the local name of the module list, as given in the RCD
    """
    if not os.path.isfile(moduleListFile):
        print "Module List File does not exist"
        return

    else:
        if not os.path.exists("ftk/DFconfigFiles/" ):
            os.makedirs("ftk/DFconfigFiles/" )

        # get rid of any old links that may interfere
        #if os.path.exists('ftk/DFconfigFiles/%s' %moduleListName):
        #    os.unlink('ftk/DFconfigFiles/%s' % moduleListName)

        # link
        os.system('ln -s %s ftk/DFconfigFiles/%s' % (moduleListFile,moduleListName))
        return

def makeAddressFile(addressPath):
    """makes address_df.xml"""

    if not os.path.isfile(addressPath):
        print "address_df file does not exist"
        return
    else:
        #if os.path.exists('ftk/DFconfigFiles/address_df.xml'):
        #    os.unlink('ftk/DFconfigFiles/address_df.xml')
        # link
        os.system('ln -s %s ftk/DFconfigFiles/address_df.xml' % (addressPath))
        return


def makeTxConfig(excludeAUX,excludeAMB,excludeSSB):
    """ Makes tx_configuration.txt
    excludeAMB equivalent to excludeAUX
    Currently accepts 3 options: noSSB, noAuxSSB, default
    This will need some tightening scaling to full system... need to set per board - will have to implement some sort of mapping
    """

    if excludeSSB and not excludeAUX and not excludeAMB:
        option = "noSSB"
    elif excludeAUX or excludeAMB: # these  exclude SSB implicitly
        option = "noAuxSSB"
    else: option = "default"

    f = open('ftk/DFconfigFiles/tx_configuration.txt','w')

    f.write('### This version adds the two more GT channels for the slinks and we also re-map the channel numbers\n\n')
    f.write('###################################################################\n')
    f.write('#### <SLINK OUT Summary>\n')
    f.write('#### ch 0   SLINK OUT AUX-OUT Upper Tower 1st AUX 1st QSFP+ ch0\n')
    f.write('#### ch 1   SLINK OUT AUX-OUT Upper Tower 1st AUX 1st QSFP+ ch1\n')
    f.write('#### ch 2   SLINK OUT AUX-OUT Upper Tower 1st AUX 1st QSFP+ ch2\n')
    f.write('#### ch 3   SLINK OUT AUX-OUT Upper Tower 1st AUX 1st QSFP+ ch3\n')
    f.write('#### ch 4   SLINK OUT AUX-OUT Upper Tower 1st AUX 2nd QSFP+ ch0\n')
    f.write('#### ch 5   SLINK OUT AUX-OUT Upper Tower 1st AUX 2nd QSFP+ ch1\n')
    f.write('#### ch 6   SLINK OUT AUX-OUT Upper Tower 1st AUX 2nd QSFP+ ch2\n')
    f.write('#### ch 7   SLINK OUT AUX-OUT Upper Tower 1st AUX 2nd QSFP+ ch3\n')
    f.write('#### ch 8   SLINK OUT AUX-OUT Upper Tower 2nd AUX 1st QSFP+ ch0\n')
    f.write('#### ch 9   SLINK OUT AUX-OUT Upper Tower 2nd AUX 1st QSFP+ ch1\n')
    f.write('#### ch 10  SLINK OUT AUX-OUT Upper Tower 2nd AUX 1st QSFP+ ch2\n')
    f.write('#### ch 11  SLINK OUT AUX-OUT Upper Tower 2nd AUX 1st QSFP+ ch3\n')
    f.write('#### ch 12  SLINK OUT AUX-OUT Upper Tower 2nd AUX 1st QSFP+ ch0\n')
    f.write('#### ch 13  SLINK OUT AUX-OUT Upper Tower 2nd AUX 1st QSFP+ ch1\n')
    f.write('#### ch 14  SLINK OUT AUX-OUT Upper Tower 2nd AUX 1st QSFP+ ch2\n')
    f.write('#### ch 15  SLINK OUT AUX-OUT Upper Tower 2nd AUX 1st QSFP+ ch3\n')
    f.write('#### ch 16  SLINK OUT SSB-OUT Upper Tower                      \n')
    f.write('#### ch 17  SLINK OUT SSB-OUT Upper Tower                      \n')
    f.write('#### ch 18  SLINK OUT AUX-OUT Lower Tower 1st AUX 1st QSFP+ ch0\n')
    f.write('#### ch 19  SLINK OUT AUX-OUT Lower Tower 1st AUX 1st QSFP+ ch1\n')
    f.write('#### ch 20  SLINK OUT AUX-OUT Lower Tower 1st AUX 1st QSFP+ ch2\n')
    f.write('#### ch 21  SLINK OUT AUX-OUT Lower Tower 1st AUX 1st QSFP+ ch3\n')
    f.write('#### ch 22  SLINK OUT AUX-OUT Lower Tower 1st AUX 2nd QSFP+ ch0\n')
    f.write('#### ch 23  SLINK OUT AUX-OUT Lower Tower 1st AUX 2nd QSFP+ ch1\n')
    f.write('#### ch 24  SLINK OUT AUX-OUT Lower Tower 1st AUX 2nd QSFP+ ch2\n')
    f.write('#### ch 25  SLINK OUT AUX-OUT Lower Tower 1st AUX 2nd QSFP+ ch3\n')
    f.write('#### ch 26  SLINK OUT AUX-OUT Lower Tower 2nd AUX 1st QSFP+ ch0\n')
    f.write('#### ch 27  SLINK OUT AUX-OUT Lower Tower 2nd AUX 1st QSFP+ ch1\n')
    f.write('#### ch 28  SLINK OUT AUX-OUT Lower Tower 2nd AUX 1st QSFP+ ch2\n')
    f.write('#### ch 29  SLINK OUT AUX-OUT Lower Tower 2nd AUX 1st QSFP+ ch3\n')
    f.write('#### ch 30  SLINK OUT AUX-OUT Lower Tower 2nd AUX 1st QSFP+ ch0\n')
    f.write('#### ch 31  SLINK OUT AUX-OUT Lower Tower 2nd AUX 1st QSFP+ ch1\n')
    f.write('#### ch 32  SLINK OUT AUX-OUT Lower Tower 2nd AUX 1st QSFP+ ch2\n')
    f.write('#### ch 33  SLINK OUT AUX-OUT Lower Tower 2nd AUX 1st QSFP+ ch3\n')
    f.write('#### ch 34  SLINK OUT SSB-OUT Lower Tower\n')
    f.write('#### ch 35  SLINK OUT SSB-OUT Lower Tower\n')
    f.write('#### ch 36  ATCA FABRIC ch3 port0\n')
    f.write('#### ch 37  ATCA FABRIC ch4 port0\n')
    f.write('#### ch 38  ATCA FABRIC ch5 port0\n')
    f.write('#### ch 39  ATCA FABRIC ch6 port0\n')
    f.write('#### ch 40  ATCA FABRIC ch7 port0\n')
    f.write('#### ch 41  ATCA FABRIC ch8 port0\n')
    f.write('#### ch 42  ATCA FABRIC ch9 port0\n')
    f.write('#### ch 43  ATCA FABRIC ch10 port0\n')
    f.write('#### ch 44  ATCA FABRIC ch11 port0\n')
    f.write('#### ch 45  ATCA FABRIC ch12 port0\n')
    f.write('#### ch 46  INTER-CRATE ch0 port0\n')
    f.write('#### ch 47  INTER-CRATE ch1 port0\n')
    f.write('#### ch 48  ATCA FABRIC ch3 port1\n')
    f.write('#### ch 49  ATCA FABRIC ch4 port1\n')
    f.write('#### ch 50  ATCA FABRIC ch5 port1\n')
    f.write('#### ch 51  ATCA FABRIC ch6 port1\n')
    f.write('#### ch 52  ATCA FABRIC ch7 port1\n')
    f.write('#### ch 53  ATCA FABRIC ch8 port1\n')
    f.write('#### ch 54  ATCA FABRIC ch9 port1\n')
    f.write('#### ch 55  ATCA FABRIC ch10 port1\n')
    f.write('#### ch 56  ATCA FABRIC ch11 port1\n')
    f.write('#### ch 57  ATCA FABRIC ch12 port1\n')
    f.write('#### ch 58  INTER-CRATE ch0 port1\n')
    f.write('#### ch 59  INTER-CRATE ch1 port1\n')
    f.write('###################################################################\n')
    f.write('\n')
    f.write('#### gtch2rxpolarity <uint32_t (GT ID)> <uint32_t (1 or 0)>\n')
    f.write('#### POLARITY FLIP (N/P) for individual Transceiver Parameters.\n')
    f.write('#### This parameters are based on the schematics. Do not change.\n')
    f.write('gtch2rxpolarity   0 0\n')
    f.write('gtch2rxpolarity   1 0\n')
    f.write('gtch2rxpolarity   2 0\n')
    f.write('gtch2rxpolarity   3 0\n')
    f.write('gtch2rxpolarity   4 0\n')
    f.write('gtch2rxpolarity   5 0\n')
    f.write('gtch2rxpolarity   6 0\n')
    f.write('gtch2rxpolarity   7 0\n')
    f.write('gtch2rxpolarity   8 0\n')
    f.write('gtch2rxpolarity   9 0\n')
    f.write('gtch2rxpolarity  10 0\n')
    f.write('gtch2rxpolarity  11 0\n')
    f.write('gtch2rxpolarity  12 0\n')
    f.write('gtch2rxpolarity  13 0\n')
    f.write('gtch2rxpolarity  14 0\n')
    f.write('gtch2rxpolarity  15 0\n')
    f.write('gtch2rxpolarity  16 0\n')
    f.write('gtch2rxpolarity  17 0\n')
    f.write('gtch2rxpolarity  18 0\n')
    f.write('gtch2rxpolarity  19 0\n')
    f.write('gtch2rxpolarity  20 0\n')
    f.write('gtch2rxpolarity  21 1\n')
    f.write('gtch2rxpolarity  22 0\n')
    f.write('gtch2rxpolarity  23 0\n')
    f.write('gtch2rxpolarity  24 0\n')
    f.write('gtch2rxpolarity  25 0\n')
    f.write('gtch2rxpolarity  26 0\n')
    f.write('gtch2rxpolarity  27 0\n')
    f.write('gtch2rxpolarity  28 0\n')
    f.write('gtch2rxpolarity  29 0\n')
    f.write('gtch2rxpolarity  30 0\n')
    f.write('gtch2rxpolarity  31 0\n')
    f.write('gtch2rxpolarity  32 1\n')
    f.write('gtch2rxpolarity  33 1\n')
    f.write('gtch2rxpolarity  34 1\n')
    f.write('gtch2rxpolarity  35 0\n')
    f.write('gtch2rxpolarity  36 0\n')
    f.write('gtch2rxpolarity  37 0\n')
    f.write('gtch2rxpolarity  38 0\n')
    f.write('gtch2rxpolarity  39 0\n')
    f.write('gtch2rxpolarity  40 1\n')
    f.write('gtch2rxpolarity  41 0\n')
    f.write('gtch2rxpolarity  42 0\n')
    f.write('gtch2rxpolarity  43 0\n')
    f.write('gtch2rxpolarity  44 0\n')
    f.write('gtch2rxpolarity  45 0\n')
    f.write('gtch2rxpolarity  46 1\n')
    f.write('gtch2rxpolarity  47 0\n')
    f.write('gtch2rxpolarity  48 0\n')
    f.write('gtch2rxpolarity  49 0\n')
    f.write('gtch2rxpolarity  50 0\n')
    f.write('gtch2rxpolarity  51 0\n')
    f.write('gtch2rxpolarity  52 0\n')
    f.write('gtch2rxpolarity  53 0\n')
    f.write('gtch2rxpolarity  54 0\n')
    f.write('gtch2rxpolarity  55 0\n')
    f.write('gtch2rxpolarity  56 0\n')
    f.write('gtch2rxpolarity  57 0\n')
    f.write('gtch2rxpolarity  58 1\n')
    f.write('gtch2rxpolarity  59 0\n')
    f.write('\n')
    f.write('#### gtch2txpolarity <uint32_t (GT ID)> <uint32_t (1 or 0)>\n')
    f.write('#### POLARITY FLIP (N/P) for individual Transceiver Parameters. \n')
    f.write('#### This parameters are based on the schematics. Do not change.\n')
    f.write('gtch2txpolarity   0 0\n')
    f.write('gtch2txpolarity   1 0\n')
    f.write('gtch2txpolarity   2 0\n')
    f.write('gtch2txpolarity   3 0\n')
    f.write('gtch2txpolarity   4 0\n')
    f.write('gtch2txpolarity   5 0\n')
    f.write('gtch2txpolarity   6 0\n')
    f.write('gtch2txpolarity   7 0\n')
    f.write('gtch2txpolarity   8 0\n')
    f.write('gtch2txpolarity   9 0\n')
    f.write('gtch2txpolarity  10 0\n')
    f.write('gtch2txpolarity  11 0\n')
    f.write('gtch2txpolarity  12 0\n')
    f.write('gtch2txpolarity  13 0\n')
    f.write('gtch2txpolarity  14 0\n')
    f.write('gtch2txpolarity  15 0\n')
    f.write('gtch2txpolarity  16 0\n')
    f.write('gtch2txpolarity  17 0\n')
    f.write('gtch2txpolarity  18 0\n')
    f.write('gtch2txpolarity  19 0\n')
    f.write('gtch2txpolarity  20 1\n')
    f.write('gtch2txpolarity  21 0\n')
    f.write('gtch2txpolarity  22 0\n')
    f.write('gtch2txpolarity  23 0\n')
    f.write('gtch2txpolarity  24 0\n')
    f.write('gtch2txpolarity  25 1\n')
    f.write('gtch2txpolarity  26 0\n')
    f.write('gtch2txpolarity  27 0\n')
    f.write('gtch2txpolarity  28 0\n')
    f.write('gtch2txpolarity  29 0\n')
    f.write('gtch2txpolarity  30 0\n')
    f.write('gtch2txpolarity  31 0\n')
    f.write('gtch2txpolarity  32 1\n')
    f.write('gtch2txpolarity  33 1\n')
    f.write('gtch2txpolarity  34 0\n')
    f.write('gtch2txpolarity  35 0\n')
    f.write('gtch2txpolarity  36 0\n')
    f.write('gtch2txpolarity  37 0\n')
    f.write('gtch2txpolarity  38 0\n')
    f.write('gtch2txpolarity  39 0\n')
    f.write('gtch2txpolarity  40 0\n')
    f.write('gtch2txpolarity  41 0\n')
    f.write('gtch2txpolarity  42 0\n')
    f.write('gtch2txpolarity  43 0\n')
    f.write('gtch2txpolarity  44 0\n')
    f.write('gtch2txpolarity  45 0\n')
    f.write('gtch2txpolarity  46 1\n')
    f.write('gtch2txpolarity  47 1\n')
    f.write('gtch2txpolarity  48 0\n')
    f.write('gtch2txpolarity  49 0\n')
    f.write('gtch2txpolarity  50 0\n')
    f.write('gtch2txpolarity  51 0\n')
    f.write('gtch2txpolarity  52 0\n')
    f.write('gtch2txpolarity  53 0\n')
    f.write('gtch2txpolarity  54 0\n')
    f.write('gtch2txpolarity  55 0\n')
    f.write('gtch2txpolarity  56 0\n')
    f.write('gtch2txpolarity  57 0\n')
    f.write('gtch2txpolarity  58 1\n')
    f.write('gtch2txpolarity  59 0\n')
    f.write('\n')
    f.write('####  <uint32_t gtch2force_ready_mode (GT ID)> <uint32_t (1 or 0)>\n')
    f.write('#### \'0\' : normal mode\n')
    f.write('#### \'1\' : force_ready mode ignoring the hold & error signals from transceiver.\n')
    f.write('####       (used for gt channels where we do not have connection (e.g. in commissioning))\n')
    f.write('#### PLEASE UPDATE as the hardware configuration changed accordingly\n')
    if option == 'noAuxSSB':
        f.write('gtch2force_ready_mode   0         1\n')
        f.write('gtch2force_ready_mode   1         1\n')
        f.write('gtch2force_ready_mode   2         1\n')
        f.write('gtch2force_ready_mode   3         1\n')
        f.write('gtch2force_ready_mode   4         1\n')
        f.write('gtch2force_ready_mode   5         1\n')
        f.write('gtch2force_ready_mode   6         1\n')
        f.write('gtch2force_ready_mode   7         1\n')
        f.write('gtch2force_ready_mode   8         1\n')
        f.write('gtch2force_ready_mode   9         1\n')
        f.write('gtch2force_ready_mode  10         1\n')
        f.write('gtch2force_ready_mode  11         1\n')
        f.write('gtch2force_ready_mode  12         1\n')
        f.write('gtch2force_ready_mode  13         1\n')
        f.write('gtch2force_ready_mode  14         1\n')
        f.write('gtch2force_ready_mode  15         1\n')
    else:
        f.write('gtch2force_ready_mode   0         0\n')
        f.write('gtch2force_ready_mode   1         0\n')
        f.write('gtch2force_ready_mode   2         0\n')
        f.write('gtch2force_ready_mode   3         0\n')
        f.write('gtch2force_ready_mode   4         0\n')
        f.write('gtch2force_ready_mode   5         0\n')
        f.write('gtch2force_ready_mode   6         0\n')
        f.write('gtch2force_ready_mode   7         0\n')
        f.write('gtch2force_ready_mode   8         0\n')
        f.write('gtch2force_ready_mode   9         0\n')
        f.write('gtch2force_ready_mode  10         0\n')
        f.write('gtch2force_ready_mode  11         0\n')
        f.write('gtch2force_ready_mode  12         0\n')
        f.write('gtch2force_ready_mode  13         0\n')
        f.write('gtch2force_ready_mode  14         0\n')
        f.write('gtch2force_ready_mode  15         0\n')
    if option == 'noAuxSSB' or option == 'noSSB':
        f.write('gtch2force_ready_mode  16         1\n')
        f.write('gtch2force_ready_mode  17         1\n')
    else:
        f.write('gtch2force_ready_mode  16         0\n')
        f.write('gtch2force_ready_mode  17         0\n')

    f.write('gtch2force_ready_mode  18         1\n')
    f.write('gtch2force_ready_mode  19         1\n')
    f.write('gtch2force_ready_mode  20         1\n')
    f.write('gtch2force_ready_mode  21         1\n')
    f.write('gtch2force_ready_mode  22         1\n')
    f.write('gtch2force_ready_mode  23         1\n')
    f.write('gtch2force_ready_mode  24         1\n')
    f.write('gtch2force_ready_mode  25         1\n')
    f.write('gtch2force_ready_mode  26         1\n')
    f.write('gtch2force_ready_mode  27         1\n')
    f.write('gtch2force_ready_mode  28         1\n')
    f.write('gtch2force_ready_mode  29         1\n')
    f.write('gtch2force_ready_mode  30         1\n')
    f.write('gtch2force_ready_mode  31         1\n')
    f.write('gtch2force_ready_mode  32         1\n')
    f.write('gtch2force_ready_mode  33         1\n')

    if option == 'noAuxSSB' or option == 'noSSB':
        f.write('gtch2force_ready_mode  34         1\n')
        f.write('gtch2force_ready_mode  35         1\n')
    else:
        f.write('gtch2force_ready_mode  34         0\n')
        f.write('gtch2force_ready_mode  35         0\n')

    f.write('gtch2force_ready_mode  36         1\n')
    f.write('gtch2force_ready_mode  37         1\n')
    f.write('gtch2force_ready_mode  38         1\n')
    f.write('gtch2force_ready_mode  39         1\n')
    f.write('gtch2force_ready_mode  40         1\n')
    f.write('gtch2force_ready_mode  41         1\n')
    f.write('gtch2force_ready_mode  42         1\n')
    f.write('gtch2force_ready_mode  43         1\n')
    f.write('gtch2force_ready_mode  44         1\n')
    f.write('gtch2force_ready_mode  45         1\n')
    f.write('gtch2force_ready_mode  46         1\n')
    f.write('gtch2force_ready_mode  47         1\n')
    f.write('gtch2force_ready_mode  48         1\n')
    f.write('gtch2force_ready_mode  49         1\n')
    f.write('gtch2force_ready_mode  50         1\n')
    f.write('gtch2force_ready_mode  51         1\n')
    f.write('gtch2force_ready_mode  52         1\n')
    f.write('gtch2force_ready_mode  53         1\n')
    f.write('gtch2force_ready_mode  54         1\n')
    f.write('gtch2force_ready_mode  55         1\n')
    f.write('gtch2force_ready_mode  56         1\n')
    f.write('gtch2force_ready_mode  57         1\n')
    f.write('gtch2force_ready_mode  58         1\n')
    f.write('gtch2force_ready_mode  59         1\n')
    f.write('\n')
    f.write('####  <uint32_t gtch2to_altera_fpga (GT ID)> <uint32_t (1 or 0)>\n')
    f.write('#### switch for IDLE word definition.\n')
    f.write('#### \'0\' : to xilinx mode (idle word = 0XC5C5C5BC : 0XBC=K28.5 sent as K-char)\n')
    f.write('#### \'1\' : to altera mode (idle word = 0X0000BCC5 : 0XBC=K28.5 sent as K-char)\n')
    f.write('#### \'1\' is for AUX channels\n')
    f.write('gtch2to_altera_fpga   0     1\n')
    f.write('gtch2to_altera_fpga   1     1\n')
    f.write('gtch2to_altera_fpga   2     1\n')
    f.write('gtch2to_altera_fpga   3     1\n')
    f.write('gtch2to_altera_fpga   4     1\n')
    f.write('gtch2to_altera_fpga   5     1\n')
    f.write('gtch2to_altera_fpga   6     1\n')
    f.write('gtch2to_altera_fpga   7     1\n')
    f.write('gtch2to_altera_fpga   8     1\n')
    f.write('gtch2to_altera_fpga   9     1\n')
    f.write('gtch2to_altera_fpga  10     1\n')
    f.write('gtch2to_altera_fpga  11     1\n')
    f.write('gtch2to_altera_fpga  12     1\n')
    f.write('gtch2to_altera_fpga  13     1\n')
    f.write('gtch2to_altera_fpga  14     1\n')
    f.write('gtch2to_altera_fpga  15     1\n')
    f.write('gtch2to_altera_fpga  16     0\n')
    f.write('gtch2to_altera_fpga  17     0\n')
    f.write('gtch2to_altera_fpga  18     1\n')
    f.write('gtch2to_altera_fpga  19     1\n')
    f.write('gtch2to_altera_fpga  20     1\n')
    f.write('gtch2to_altera_fpga  21     1\n')
    f.write('gtch2to_altera_fpga  22     1\n')
    f.write('gtch2to_altera_fpga  23     1\n')
    f.write('gtch2to_altera_fpga  24     1\n')
    f.write('gtch2to_altera_fpga  25     1\n')
    f.write('gtch2to_altera_fpga  26     1\n')
    f.write('gtch2to_altera_fpga  27     1\n')
    f.write('gtch2to_altera_fpga  28     1\n')
    f.write('gtch2to_altera_fpga  29     1\n')
    f.write('gtch2to_altera_fpga  30     1\n')
    f.write('gtch2to_altera_fpga  31     1\n')
    f.write('gtch2to_altera_fpga  32     1\n')
    f.write('gtch2to_altera_fpga  33     1\n')
    f.write('gtch2to_altera_fpga  34     0\n')
    f.write('gtch2to_altera_fpga  35     0\n')
    f.write('gtch2to_altera_fpga  36     0\n')
    f.write('gtch2to_altera_fpga  37     0\n')
    f.write('gtch2to_altera_fpga  38     0\n')
    f.write('gtch2to_altera_fpga  39     0\n')
    f.write('gtch2to_altera_fpga  40     0\n')
    f.write('gtch2to_altera_fpga  41     0\n')
    f.write('gtch2to_altera_fpga  42     0\n')
    f.write('gtch2to_altera_fpga  43     0\n')
    f.write('gtch2to_altera_fpga  44     0\n')
    f.write('gtch2to_altera_fpga  45     0\n')
    f.write('gtch2to_altera_fpga  46     0\n')
    f.write('gtch2to_altera_fpga  47     0\n')
    f.write('gtch2to_altera_fpga  48     0\n')
    f.write('gtch2to_altera_fpga  49     0\n')
    f.write('gtch2to_altera_fpga  50     0\n')
    f.write('gtch2to_altera_fpga  51     0\n')
    f.write('gtch2to_altera_fpga  52     0\n')
    f.write('gtch2to_altera_fpga  53     0\n')
    f.write('gtch2to_altera_fpga  54     0\n')
    f.write('gtch2to_altera_fpga  55     0\n')
    f.write('gtch2to_altera_fpga  56     0\n')
    f.write('gtch2to_altera_fpga  57     0\n')
    f.write('gtch2to_altera_fpga  58     0\n')
    f.write('gtch2to_altera_fpga  59     0\n')
    f.write('####  <uint32_t gtch_ignore_freeze (GT ID)> <uint32_t (1 or 0)>\n')

    if option == 'noAuxSSB' or option == 'noSSB':
        f.write('gtch_ignore_freeze   0      1\n')
        f.write('gtch_ignore_freeze   1      1\n')
        f.write('gtch_ignore_freeze   2      1\n')
        f.write('gtch_ignore_freeze   3      1\n')
        f.write('gtch_ignore_freeze   4      1\n')
        f.write('gtch_ignore_freeze   5      1\n')
        f.write('gtch_ignore_freeze   6      1\n')
        f.write('gtch_ignore_freeze   7      1\n')
        f.write('gtch_ignore_freeze   8      1\n')
        f.write('gtch_ignore_freeze   9      1\n')
        f.write('gtch_ignore_freeze  10      1\n')
        f.write('gtch_ignore_freeze  11      1\n')
        f.write('gtch_ignore_freeze  12      1\n')
        f.write('gtch_ignore_freeze  13      1\n')
        f.write('gtch_ignore_freeze  14      1\n')
        f.write('gtch_ignore_freeze  15      1\n')
        f.write('gtch_ignore_freeze  16      1\n')
        f.write('gtch_ignore_freeze  17      1\n')
        f.write('gtch_ignore_freeze  18      1\n')
        f.write('gtch_ignore_freeze  19      1\n')
        f.write('gtch_ignore_freeze  20      1\n')
        f.write('gtch_ignore_freeze  21      1\n')
        f.write('gtch_ignore_freeze  22      1\n')
        f.write('gtch_ignore_freeze  23      1\n')
        f.write('gtch_ignore_freeze  24      1\n')
        f.write('gtch_ignore_freeze  25      1\n')
        f.write('gtch_ignore_freeze  26      1\n')
        f.write('gtch_ignore_freeze  27      1\n')
        f.write('gtch_ignore_freeze  28      1\n')
        f.write('gtch_ignore_freeze  29      1\n')
        f.write('gtch_ignore_freeze  30      1\n')
        f.write('gtch_ignore_freeze  31      1\n')
        f.write('gtch_ignore_freeze  32      1\n')
        f.write('gtch_ignore_freeze  33      1\n')
        f.write('gtch_ignore_freeze  34      1\n')
        f.write('gtch_ignore_freeze  35      1\n')
        f.write('gtch_ignore_freeze  36      1\n')
        f.write('gtch_ignore_freeze  37      1\n')
        f.write('gtch_ignore_freeze  38      1\n')
        f.write('gtch_ignore_freeze  39      1\n')
        f.write('gtch_ignore_freeze  40      1\n')
        f.write('gtch_ignore_freeze  41      1\n')
        f.write('gtch_ignore_freeze  42      1\n')
        f.write('gtch_ignore_freeze  43      1\n')
        f.write('gtch_ignore_freeze  44      1\n')
        f.write('gtch_ignore_freeze  45      1\n')
        f.write('gtch_ignore_freeze  46      1\n')
        f.write('gtch_ignore_freeze  47      1\n')
        f.write('gtch_ignore_freeze  48      1\n')
        f.write('gtch_ignore_freeze  49      1\n')
        f.write('gtch_ignore_freeze  50      1\n')
        f.write('gtch_ignore_freeze  51      1\n')
        f.write('gtch_ignore_freeze  52      1\n')
        f.write('gtch_ignore_freeze  53      1\n')
        f.write('gtch_ignore_freeze  54      1\n')
        f.write('gtch_ignore_freeze  55      1\n')
        f.write('gtch_ignore_freeze  56      1\n')
        f.write('gtch_ignore_freeze  57      1\n')
        f.write('gtch_ignore_freeze  58      1\n')
        f.write('gtch_ignore_freeze  59      1\n')
    else:
        f.write('gtch_ignore_freeze   0      0\n')
        f.write('gtch_ignore_freeze   1      0\n')
        f.write('gtch_ignore_freeze   2      0\n')
        f.write('gtch_ignore_freeze   3      0\n')
        f.write('gtch_ignore_freeze   4      0\n')
        f.write('gtch_ignore_freeze   5      0\n')
        f.write('gtch_ignore_freeze   6      0\n')
        f.write('gtch_ignore_freeze   7      0\n')
        f.write('gtch_ignore_freeze   8      0\n')
        f.write('gtch_ignore_freeze   9      0\n')
        f.write('gtch_ignore_freeze  10      0\n')
        f.write('gtch_ignore_freeze  11      0\n')
        f.write('gtch_ignore_freeze  12      0\n')
        f.write('gtch_ignore_freeze  13      0\n')
        f.write('gtch_ignore_freeze  14      0\n')
        f.write('gtch_ignore_freeze  15      0\n')
        f.write('gtch_ignore_freeze  16      0\n')
        f.write('gtch_ignore_freeze  17      0\n')
        f.write('gtch_ignore_freeze  18      0\n')
        f.write('gtch_ignore_freeze  19      0\n')
        f.write('gtch_ignore_freeze  20      0\n')
        f.write('gtch_ignore_freeze  21      0\n')
        f.write('gtch_ignore_freeze  22      0\n')
        f.write('gtch_ignore_freeze  23      0\n')
        f.write('gtch_ignore_freeze  24      0\n')
        f.write('gtch_ignore_freeze  25      0\n')
        f.write('gtch_ignore_freeze  26      0\n')
        f.write('gtch_ignore_freeze  27      0\n')
        f.write('gtch_ignore_freeze  28      0\n')
        f.write('gtch_ignore_freeze  29      0\n')
        f.write('gtch_ignore_freeze  30      0\n')
        f.write('gtch_ignore_freeze  31      0\n')
        f.write('gtch_ignore_freeze  32      0\n')
        f.write('gtch_ignore_freeze  33      0\n')
        f.write('gtch_ignore_freeze  34      0\n')
        f.write('gtch_ignore_freeze  35      0\n')
        f.write('gtch_ignore_freeze  36      0\n')
        f.write('gtch_ignore_freeze  37      0\n')
        f.write('gtch_ignore_freeze  38      0\n')
        f.write('gtch_ignore_freeze  39      0\n')
        f.write('gtch_ignore_freeze  40      0\n')
        f.write('gtch_ignore_freeze  41      0\n')
        f.write('gtch_ignore_freeze  42      0\n')
        f.write('gtch_ignore_freeze  43      0\n')
        f.write('gtch_ignore_freeze  44      0\n')
        f.write('gtch_ignore_freeze  45      0\n')
        f.write('gtch_ignore_freeze  46      0\n')
        f.write('gtch_ignore_freeze  47      0\n')
        f.write('gtch_ignore_freeze  48      0\n')
        f.write('gtch_ignore_freeze  49      0\n')
        f.write('gtch_ignore_freeze  50      0\n')
        f.write('gtch_ignore_freeze  51      0\n')
        f.write('gtch_ignore_freeze  52      0\n')
        f.write('gtch_ignore_freeze  53      0\n')
        f.write('gtch_ignore_freeze  54      0\n')
        f.write('gtch_ignore_freeze  55      0\n')
        f.write('gtch_ignore_freeze  56      0\n')
        f.write('gtch_ignore_freeze  57      0\n')
        f.write('gtch_ignore_freeze  58      0\n')
        f.write('gtch_ignore_freeze  59      0\n')



#### Done one once and for all - creates new copy of file
def makeConnections(isLab4):
    """ Makes connections.xml file, which is entirely set in stone"""
    f = open('ftk/DFconfigFiles/connections.xml', 'w')

    if isLab4:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        f.write('\n')
        f.write('<connections>\n')
        f.write('  <connection id="DF20"  uri="chtcp-2.0://10.193.20.102:10203?target=10.193.64.20:50001"   address_table="file://address_df_DF20.xml" />\n')
        f.write('  <connection id="DF20D" uri="ipbusudp-2.0://10.193.64.20:50001"                           address_table="file://address_df_DF20D.xml" />\n')
        f.write('  <connection id="DF21"  uri="chtcp-2.0://10.193.20.102:10203?target=10.193.64.21:50001"   address_table="file://address_df_DF21.xml" />\n')
        f.write('  <connection id="DF21D" uri="ipbusudp-2.0://10.193.64.21:50001"                           address_table="file://address_df_DF21D.xml" />\n')
        f.write('  <connection id="DF22"  uri="chtcp-2.0://10.193.20.102:10203?target=10.193.64.22:50001"   address_table="file://address_df_DF22.xml" />\n')
        f.write('  <connection id="DF22D" uri="ipbusudp-2.0://10.193.64.22:50001"                           address_table="file://address_df_DF22D.xml" />\n')
        f.write('  <connection id="DF22-tower11"  uri="chtcp-2.0://10.193.20.102:10203?target=10.193.64.22:50001" address_table="file://address_df_DF22-tower11.xml" />\n')
        f.write('  <connection id="DF22-tower11D" uri="ipbusudp-2.0://10.193.64.22:50001"                   address_table="file://address_df_DF22-tower11D.xml" />\n')
        f.write('</connections>\n')

    else:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n\n')
        f.write('<connections>')
        f.write('  <connection id="DF1D" uri="ipbusudp-2.0://10.145.95.18:50001"                        address_table="file://address_df_DF1D.xml" />\n')
        f.write('  <connection id="DF31D" uri="ipbusudp-2.0://10.145.95.31:50001"                       address_table="file://address_df_DF31D.xml" />\n')
        f.write('  <connection id="DF31" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.31:50001" address_table="file://address_df_DF31.xml" />\n')
        f.write('  <connection id="DF30" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.30:50001" address_table="file://address_df_DF30.xml" />\n')
        f.write('  <connection id="DF30D" uri="ipbusudp-2.0://10.145.95.30:50001"                       address_table="file://address_df_DF30D.xml" />\n')
        f.write('  <connection id="DF33" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.33:50001" address_table="file://address_df_DF33.xml" />\n')
        f.write('  <connection id="DF33D" uri="ipbusudp-2.0://10.145.95.33:50001"                       address_table="file://address_df_DF33D.xml" />\n')
        f.write('  <connection id="DF34" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.34:50001" address_table="file://address_df_DF34.xml" />\n')
        f.write('  <connection id="DF34D" uri="ipbusudp-2.0://10.145.95.34:50001"                       address_table="file://address_df_DF34D.xml" />\n')
        f.write('  <connection id="DF36" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.36:50001" address_table="file://address_df_DF36.xml" />\n')
        f.write('  <connection id="DF36D" uri="ipbusudp-2.0://10.145.95.36:50001"                       address_table="file://address_df_DF36D.xml" />\n')
        f.write('  <connection id="DF-3-09D" uri="ipbusudp-2.0://10.145.95.18:50001"                    address_table="file://address_df_DF-3-09D.xml" />\n')
        f.write('  <connection id="DF-3-10D" uri="ipbusudp-2.0://10.145.95.32:50001"                    address_table="file://address_df_DF-3-10D.xml" />\n')
        f.write('  <connection id="DF-2-04-slice" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.39:50001" address_table="file://address_df_DF-2-04-slice.xml" />\n')
        f.write('  <connection id="DF-2-04D" uri="ipbusudp-2.0://10.145.95.39:50001"                    address_table="file://address_df_DF-2-04D.xml" />\n')
        f.write('  <connection id="DF-3-03" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.18:50001" address_table="file://address_df_DF-3-03.xml" />\n')
        f.write('  <connection id="DF-3-04" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.31:50001" address_table="file://address_df_DF-3-04.xml" />\n')
        f.write('  <connection id="DF-3-05" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.32:50001" address_table="file://address_df_DF-3-05.xml" />\n')
        f.write('  <connection id="DF-3-06" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.33:50001" address_table="file://address_df_DF-3-06.xml" />\n')
        f.write('  <connection id="DF-3-07" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.34:50001" address_table="file://address_df_DF-3-07.xml" />\n')
        f.write('  <connection id="DF-3-09" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.36:50001" address_table="file://address_df_DF-3-09.xml" />\n')
        f.write('  <connection id="DF-4-03" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.54:50001" address_table="file://address_df_DF-4-03.xml" />\n')
        f.write('  <connection id="DF-4-04" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.55:50001" address_table="file://address_df_DF-4-04.xml" />\n')
        f.write('  <connection id="DF-4-05" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.56:50001" address_table="file://address_df_DF-4-05.xml" />\n')
        f.write('  <connection id="DF-4-06" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.57:50001" address_table="file://address_df_DF-4-06.xml" />\n')
        f.write('  <connection id="DF-4-07" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.58:50001" address_table="file://address_df_DF-4-07.xml" />\n')
        f.write('  <connection id="DF-4-08" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.59:50001" address_table="file://address_df_DF-4-08.xml" />\n')
        f.write('  <connection id="DF-4-09" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.60:50001" address_table="file://address_df_DF-4-09.xml" />\n')
        f.write('  <connection id="DF-4-10" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.61:50001" address_table="file://address_df_DF-4-10.xml" />\n')
        f.write('  <connection id="DF-1-03" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.46:50001" address_table="file://address_df_DF-1-03.xml" />\n')
        f.write('  <connection id="DF-1-04" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.47:50001" address_table="file://address_df_DF-1-04.xml" />\n')
        f.write('  <connection id="DF-1-05" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.48:50001" address_table="file://address_df_DF-1-05.xml" />\n')
        f.write('  <connection id="DF-1-06" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.49:50001" address_table="file://address_df_DF-1-06.xml" />\n')
        f.write('  <connection id="DF-1-07" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.50:50001" address_table="file://address_df_DF-1-07.xml" />\n')
        f.write('  <connection id="DF-1-08" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.51:50001" address_table="file://address_df_DF-1-08.xml" />\n')
        f.write('  <connection id="DF-1-09" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.52:50001" address_table="file://address_df_DF-1-09.xml" />\n')
        f.write('  <connection id="DF-1-10" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.53:50001" address_table="file://address_df_DF-1-10.xml" />\n')
        f.write('  <connection id="DF-2-03" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.38:50001" address_table="file://address_df_DF-2-03.xml" />\n')
        f.write('  <connection id="DF-2-04" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.39:50001" address_table="file://address_df_DF-2-04.xml" />\n')
        f.write('  <connection id="DF-2-05" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.40:50001" address_table="file://address_df_DF-2-05.xml" />\n')
        f.write('  <connection id="DF-2-06" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.41:50001" address_table="file://address_df_DF-2-06.xml" />\n')
        f.write('  <connection id="DF-2-07" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.42:50001" address_table="file://address_df_DF-2-07.xml" />\n')
        f.write('  <connection id="DF-2-08" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.43:50001" address_table="file://address_df_DF-2-08.xml" />\n')
        f.write('  <connection id="DF-2-09" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.44:50001" address_table="file://address_df_DF-2-09.xml" />\n')
        f.write('  <connection id="DF-2-10" uri="chtcp-2.0://10.145.95.19:10203?target=10.145.95.45:50001" address_table="file://address_df_DF-2-10.xml" />\n\n')
        f.write('</connections>')

    f.close()

def makeSystemConfig64():
    """ Makes system config file, which is set in stone"""

    f = open('ftk/DFconfigFiles/df_system_config_64towers.txt', 'w')

    f.write("####################################\n")
    f.write("#Number of FTK Towers\n")
    f.write("####################################\n")
    f.write("NumberOfFTKTowers 64\n")
    f.write("####################################\n")
    f.write("#Mapping between Board Number to Logical Location (ShelfAndSlot)\n")
    f.write("#Key Board Shelf Slot\n")
    f.write("####################################\n")
    f.write("BoardNToShelfAndSlot   0  1   3\n")
    f.write("BoardNToShelfAndSlot   1  2   3\n")
    f.write("BoardNToShelfAndSlot   2  3   3\n")
    f.write("BoardNToShelfAndSlot   3  4   3\n")
    f.write("BoardNToShelfAndSlot   4  1   4\n")
    f.write("BoardNToShelfAndSlot   5  2   4\n")
    f.write("BoardNToShelfAndSlot   6  3   4\n")
    f.write("BoardNToShelfAndSlot   7  4   4\n")
    f.write("BoardNToShelfAndSlot   8  1   5\n")
    f.write("BoardNToShelfAndSlot   9  2   5\n")
    f.write("BoardNToShelfAndSlot  10  3   5\n")
    f.write("BoardNToShelfAndSlot  11  4   5\n")
    f.write("BoardNToShelfAndSlot  12  1   6\n")
    f.write("BoardNToShelfAndSlot  13  2   6\n")
    f.write("BoardNToShelfAndSlot  14  3   6\n")
    f.write("BoardNToShelfAndSlot  15  4   6\n")
    f.write("BoardNToShelfAndSlot  16  1   7\n")
    f.write("BoardNToShelfAndSlot  17  2   7\n")
    f.write("BoardNToShelfAndSlot  18  3   7\n")
    f.write("BoardNToShelfAndSlot  19  4   7\n")
    f.write("BoardNToShelfAndSlot  20  1   8\n")
    f.write("BoardNToShelfAndSlot  21  2   8\n")
    f.write("BoardNToShelfAndSlot  22  3   8\n")
    f.write("BoardNToShelfAndSlot  23  4   8\n")
    f.write("BoardNToShelfAndSlot  24  1   9\n")
    f.write("BoardNToShelfAndSlot  25  2   9\n")
    f.write("BoardNToShelfAndSlot  26  3   9\n")
    f.write("BoardNToShelfAndSlot  27  4   9\n")
    f.write("BoardNToShelfAndSlot  28  1  10\n")
    f.write("BoardNToShelfAndSlot  29  2  10\n")
    f.write("BoardNToShelfAndSlot  30  3  10\n")
    f.write("BoardNToShelfAndSlot  31  4  10\n")
    f.write("####################################\n")
    f.write("#=========================================\n")
    f.write("# Mapping between FTK plane and output bit\n")
    f.write("#=========================================\n")
    f.write("#Key Plane OutBit\n")
    f.write("####################################\n")
    f.write("PlaneToOutBit  0 8\n")
    f.write("PlaneToOutBit  1 0\n")
    f.write("PlaneToOutBit  2 1\n")
    f.write("PlaneToOutBit  3 2\n")
    f.write("PlaneToOutBit  4 3\n")
    f.write("PlaneToOutBit  5 8\n")
    f.write("PlaneToOutBit  6 4\n")
    f.write("PlaneToOutBit  7 8\n")
    f.write("PlaneToOutBit  8 5\n")
    f.write("PlaneToOutBit  9 6\n")
    f.write("PlaneToOutBit 10 7\n")
    f.write("PlaneToOutBit 11 8\n")
    f.write("####################################\n")
    f.write("#======================================================================\n")
    f.write("# Mapping between DF board ID and tower assignment\n")
    f.write("#======================================================================\n")
    f.write("# Key BoardN TowerN\n")
    f.write("####################################\n")
    f.write("BoardNToTopTower 0 2\n")
    f.write("BoardNToBotTower 0 18\n")
    f.write("BoardNToTopTower 1 7\n")
    f.write("BoardNToBotTower 1 23\n")
    f.write("BoardNToTopTower 2 10\n")
    f.write("BoardNToBotTower 2 26\n")
    f.write("BoardNToTopTower 3 14\n")
    f.write("BoardNToBotTower 3 30\n")
    f.write("BoardNToTopTower 4 3\n")
    f.write("BoardNToBotTower 4 19\n")
    f.write("BoardNToTopTower 5 22\n")
    f.write("BoardNToBotTower 5 6\n")
    f.write("BoardNToTopTower 6 11\n")
    f.write("BoardNToBotTower 6 27\n")
    f.write("BoardNToTopTower 7 15\n")
    f.write("BoardNToBotTower 7 31\n")
    f.write("BoardNToTopTower 8 34\n")
    f.write("BoardNToBotTower 8 50\n")
    f.write("BoardNToTopTower 9 38\n")
    f.write("BoardNToBotTower 9 54\n")
    f.write("BoardNToTopTower 10 42\n")
    f.write("BoardNToBotTower 10 58\n")
    f.write("BoardNToTopTower 11 46\n")
    f.write("BoardNToBotTower 11 62\n")
    f.write("BoardNToTopTower 12 35\n")
    f.write("BoardNToBotTower 12 51\n")
    f.write("BoardNToTopTower 13 39\n")
    f.write("BoardNToBotTower 13 55\n")
    f.write("BoardNToTopTower 14 43\n")
    f.write("BoardNToBotTower 14 59\n")
    f.write("BoardNToTopTower 15 47\n")
    f.write("BoardNToBotTower 15 63\n")
    f.write("BoardNToTopTower 16 4\n")
    f.write("BoardNToBotTower 16 20\n")
    f.write("BoardNToTopTower 17 8\n")
    f.write("BoardNToBotTower 17 24\n")
    f.write("BoardNToTopTower 18 12\n")
    f.write("BoardNToBotTower 18 28\n")
    f.write("BoardNToTopTower 19 0\n")
    f.write("BoardNToBotTower 19 16\n")
    f.write("BoardNToTopTower 20 5\n")
    f.write("BoardNToBotTower 20 21\n")
    f.write("BoardNToTopTower 21 9\n")
    f.write("BoardNToBotTower 21 25\n")
    f.write("BoardNToTopTower 22 13\n")
    f.write("BoardNToBotTower 22 29\n")
    f.write("BoardNToTopTower 23 1\n")
    f.write("BoardNToBotTower 23 17\n")
    f.write("BoardNToTopTower 24 36\n")
    f.write("BoardNToBotTower 24 52\n")
    f.write("BoardNToTopTower 25 40\n")
    f.write("BoardNToBotTower 25 56\n")
    f.write("BoardNToTopTower 26 44\n")
    f.write("BoardNToBotTower 26 60\n")
    f.write("BoardNToTopTower 27 32\n")
    f.write("BoardNToBotTower 27 48\n")
    f.write("BoardNToTopTower 28 37\n")
    f.write("BoardNToBotTower 28 53\n")
    f.write("BoardNToTopTower 29 41\n")
    f.write("BoardNToBotTower 29 57\n")
    f.write("BoardNToTopTower 30 45\n")
    f.write("BoardNToBotTower 30 61\n")
    f.write("BoardNToTopTower 31 33\n")
    f.write("BoardNToBotTower 31 49\n")
    f.write("####################################\n")
    f.write("#======================================================================\n")
    f.write("# Mapping between ATCA Fabric Channel and Output bit\n")
    f.write("#======================================================================\n")
    f.write("# Key Channel Bit\n")
    f.write("####################################\n")
    f.write("ChanToOutBit  3  0\n")
    f.write("ChanToOutBit  4  8\n")
    f.write("ChanToOutBit  5  4\n")
    f.write("ChanToOutBit  6 12\n")
    f.write("ChanToOutBit  7  2\n")
    f.write("ChanToOutBit  8 10\n")
    f.write("ChanToOutBit  9  6\n")
    f.write("ChanToOutBit 10  3\n")
    f.write("ChanToOutBit 11  5\n")
    f.write("ChanToOutBit 12  7\n")
    f.write("####################################\n")
    f.write("#======================================================================\n")
    f.write("# Mapping between DF DF Fiber Connection\n")
    f.write("#======================================================================\n")
    f.write("####################################\n")
    f.write("# Key Board1 Board2\n")
    f.write("DFFiberConnection 0 14\n")
    f.write("DFFiberConnection 1 16\n")
    f.write("DFFiberConnection 3 18\n")
    f.write("DFFiberConnection 4 23\n")
    f.write("DFFiberConnection 7 22\n")
    f.write("DFFiberConnection 8 27\n")
    f.write("DFFiberConnection 10 25\n")
    f.write("DFFiberConnection 11 26\n")
    f.write("DFFiberConnection 12 31\n")
    f.write("DFFiberConnection 19 29\n")
    f.write("DFFiberConnection 15 30\n")
    f.write("DFFiberConnection 9 24\n")
    f.write("DFFiberConnection 13 28\n")
    f.write("#DFFiberConnection 5 20\n")
    f.write("DFFiberConnection 5 10001\n")
    f.write("DFFiberConnection 20 10002\n")
    f.write("DFFiberConnection 2 17\n")
    f.write("DFFiberConnection 6 21\n")
