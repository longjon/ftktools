''' Module used to extend the option_FTK dictionary
'''
import os
def extend(option_FTK):
  ''' Function to be used to extend the option_FTK dictionary
  To be used by DF board expert
  '''

  # Configuration of the DF shelves
  # NB: The possibility to use a collections.namedtuple was explored but it looks impossible
  #     since the "import collections" is not propagated to the user option file
  option_FTK['df-shelves']  = {
    'short': '','arg': True,
    'default': [],
    'description':
    'Python list describing each DF shelf. Each element in the list is a python'
    + '\n# a tuple composed of 3 strings: shelf number, pc name, shelf map. The latter'
    + '\n# is string where the index represents the slot and the characters means:'
    + '\n# "D"=DF board, "x"=board disabled, "_"=unused slot. [NB: slot 0 does not exist, 1 is switch, 2 is blocked] '
    + '\n# Full rack configuration: \' _ _ D D D D _ _ D D D D _ _ \' '
    + '\n# Physical to logical map: \'     9 7 5 3 1 2 4 6 8 10    \''
    + '\n# For Lab4 just pass 1 DF (see below)'
    + '\n# E.g:'
    + '\n# option[\'df-shelves\'] = ['
    + '\n# ("1","pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__"),'   #full
    + '\n# ("2","pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__"),'   #full
    + '\n# ("3","pc-ftk-tdq-01.cern.ch","__DxxD__DDxx__"),'   #A and D slices, partially full
    + '\n# ("4","pc-ftk-tdq-01.cern.ch","__xxxx__DDDD__") ]'  #Partially full,
    + '\n#'
    + '\n# option[\'df-shelves\'] = [ #pc-ftk-tdq-01.cern.ch P1, pc-tbed-ftk-01.cern.ch Lab4 '
    + '\n# ("1","pc-tbed-ftk-01.cern.ch","__xxxx__Dxxx__")] # If lab 4, pass this',
    'group':'df'
    }

  # Example of option for DF
  option_FTK['DFFoobar'] = {
    'short':'',
    'arg': True,
    'default':'FooBar',
    'description': 'Just an example',
    'group':'df'
  }

  option_FTK['DFConfigFolder'] = {
    'short':'',
    'arg': True,
    'default':'%s/ftk/DFconfigFiles/'%(os.getcwd()),
    'description': 'Base folder containing DF configuration',
    'group':'df'
  }

  option_FTK['IM_IS_APPLY_TRUNCATION'] = {
    'default':hex(0x1),
    'short':'',
    'arg': True,
    'description': 'IM Apply truncation',
    'group':'df'
    }
  option_FTK['IM_L1TTPSMask'] = {
    'default':hex(0x0),
    'short':'',
    'arg': True,
    'description': 'IM L1TT Mask',
    'group':'df'
    }
  option_FTK['IM_PSEUDO_DATA_XOFF'] = {
    'default':hex(0x1),
    'short':'',
    'arg': True,
    'description': 'IM Pseudodata XOFF',
    'group':'df'
    }
  option_FTK['IM_FILLGAP_CONFIG'] = {
    'default':hex(0x0),
    'short':'',
    'arg': True,
    'description': 'IM Fillgap Value',
    'group':'df'
    }
  option_FTK['IM_RECOVER_CONFIG'] = {
    'default':hex(0xe0000400),
    'short':'',
    'arg': True,
    'description': 'IM Recover Value',
    'group':'df'
    }
  option_FTK['IM_L1ID_PS_VALUE'] = {
    'default':hex(0x0),
    'short':'',
    'arg': True,
    'description': 'IM L1ID Prescale Value',
    'group':'df'
    }
  option_FTK['IM_L1ID_PS_MASK'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': 'IM L1ID Prescale Mask',
    'group':'df'
    }
  option_FTK['IM_L1ID_FREEZE_VALUE'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': 'IM L1ID Freeze Value',
    'group':'df'
    }
  option_FTK['IM_L1ID_FREEZE_MASK'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': 'IM L1ID Freeze Mask',
    'group':'df'
    }
  option_FTK['IM_ERR_FREEZE_MASK1'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': 'IM Error Freeze Mask 1',
    'group':'df'
    }
  option_FTK['IM_ERR_FREEZE_MASK2'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': 'IM Error Freeze Mask 2',
    'group':'df'
    }
  option_FTK['IM_START_L1ID_MASK'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': 'IM Sync Start L1ID Mask (was 28bit sync)',
    'group':'df'
    }
  option_FTK['IM_START_L1ID_VALUE'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': 'IM Sync Start L1ID Value',
    'group':'df'
    }
  option_FTK['IM_HIT_PER_MODULE_IBL'] = {
    'default':hex(0x800000FF),
    'short':'',
    'arg': True,
    'description': 'IM Hit/Module removal for IBL (31:enable bit, 15:0 limit of fillgap event)',
    'group':'df'
    }
  option_FTK['IM_HIT_PER_MODULE_PIX'] = {
    'default':hex(0x800000FF),
    'short':'',
    'arg': True,
    'description': 'IM Hit/Module removal for PIX (31:enable bit, 15:0 limit of fillgap event)',
    'group':'df'
    }
  option_FTK['IM_HIT_PER_MODULE_SCT'] = {
    'default':hex(0x800000FF),
    'short':'',
    'arg': True,
    'description': 'IM Hit/Module removal for SCT (31:enable bit, 15:0 limit of fillgap event)',
    'group':'df'
    }
  option_FTK['IM_SPECIAL5'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': 'IM General used register, ask Masahiro',
    'group':'df'
    }
  option_FTK['IM_PSEUDO_INPUT_RATE'] = {
    'default':hex(0x00000064),
    'short':'',
    'arg': True,
    'description': 'IM Pseudo data input rate value in kHz (default is 100 kHz)',
    'group':'df'
    }
  option_FTK['IM_DUPLICATED_MODULE'] = {
    'default':hex(0x0),
    'short':'',
    'arg': True,
    'description': 'IM Enable bit (just 1bit) for enabling duplicated module removal',
    'group':'df'
    }
  option_FTK['IM_IS_APPLY_SELECTPS'] = {
    'default':hex(0x0),
    'short':'',
    'arg': True,
    'description': 'IM Enable bit (just 1bit) for enabling Select PreScale for L1ID',
    'group':'df'
    }
  option_FTK['IM_SLINK_MODE_MASK'] = {
    'default':hex(0x0),
    'short':'',
    'arg': True,
    'description': 'Channel mask for enabling SLINK, SLINK is disabled by default',
    'group':'df'
    }
  option_FTK['IM_PSEUDO_DATA_INITIAL_DELAY'] = {
    'default':hex(0x1),
    'short':'',
    'arg': True,
    'description': 'IM enable just 1 bit for enabling Pseudo data initial delay fix',
    'group':'df'
    }
  option_FTK['DF_MaxCyclesWaitedForData'] = {
    'default':hex(0x4fff),
    'short':'',
    'arg': True,
    'description': 'DF_MaxCyclesWaitedForData',
    'group':'df'
    }
  option_FTK['DF_use_auto_delay_setting'] = {
    'default':1,
    'short':'',
    'arg': True,
    'description': 'DF_use_auto_delay_setting',
    'group':'df'
    }
  option_FTK['DF_doFWCheck'] = {
    'default':1,
    'short':'',
    'arg': True,
    'description': 'DF_doFWCheck',
    'group':'df'
    }
  option_FTK['DF_FwVersion'] = {
    'default':hex(0x0001900f),
    'short':'',
    'arg': True,
    'description': 'DF_FwVersion (default fw release 02)',
    'group':'df'
    }
  option_FTK['IM_FwVersion'] = {
    'default':hex(0x0),
    'short':'',
    'arg': True,
    'description': 'IM FW version which should be loaded',
    'group':'df'
    }

  option_FTK['GenerateConfiguration']={
    'default': False,
    'short':'',
    'arg':True,
    'description':'If true, generates all configuration files',
    'group':'df'
    }

  option_FTK['IM_ModuleListFile']={
    #'default': '/det/tdaq/ftk/repo/condDB/latest/DataFormatter/ver1.6/config/modulelist_Data2017_64T.txt\' #p1',
    'default': '/det/tdaq/ftk/repo/condDB/latest/DataFormatter/ver1.6/config/modulelist_Data2017_64T.txt',
    'short':'',
    'arg':True,
    'description':'Give full path of Module List. Use defaults if not an expert!!!'
    +'\n#option[\'IM_ModuleListFile\']=\'/tbed/ftk/condDB/latest/DataFormatter/ver1.5/config/modulelist_Data2017_64T.txt\' #Lab4',
    'group':'df'
    }

  option_FTK['PseudoData_directory']={
    'default': '',
    'short':'',
    'arg':True,
    'description':'Indicate name of directory containing pseudodata to use'
    +'\n#option[\'PseudoData_directory\']=\'/det/ftk/repo/condDB/latest/DataFormatter/ver1.6/config/pseudo_data_20170624\'',
    'group':'df'
    }

  option_FTK['LUTs_directory']={
    #'default': '/det/tdaq/ftk/repo/condDB/latest/DataFormatter/ver1.6/config/IM_LUTs_Data2017\' #p1',
    'default': '/det/tdaq/ftk/repo/condDB/latest/DataFormatter/ver1.6/config/IM_LUTs_Data2017',
    'short':'',
    'arg':True,
    'description':'Indicate name of directory containing LUTs'
    +"\n#option[\'LUTs_directory\']=\'/tbed/ftk/condDB/latest/DataFormatter/ver1.5/config/IM_LUTs_Data2017/\' #Lab4",
    'group':'df'
    }

  option_FTK['usePseudoData']={
    'default': False,
    'short':'',
    'arg': True,
    'description':'Use pseudodata for this run. Default OKS value is False.',
    'group': 'df'
    }

  option_FTK['use_df22']={
    'default': False,
    'short':'',
    'arg': True,
    'description':'Use DF22, the DF in lab4. If generating a partition for p1, False; if for lab4, true.',
    'group': 'df'
    }


  option_FTK['address_df_location']={
    #'default': '/det/ftk/repo/condDB/latest/DataFormatter/ver1.6/config/address_df.xml\' #p1',
    'default': '/det/ftk/repo/condDB/latest/DataFormatter/ver1.6/config/address_df.xml',
    'short':'',
    'arg': True,
    'description':'Location of address_df.xml'
    +'\n#option[\'address_df_location\']=\'/tbed/ftk/condDB/latest/DataFormatter/ver1.5/config/address_df.xml\' #l4',
    'group': 'df'
    }

  option_FTK['IPBusEmulatorMode']={
    'default': 0,
    'short':'',
    'arg': True,
    'description':'uhal IPBus SW emulator mode. (default) 0=no emulator. 1=spy on IPBus packets, forward to DF hardware. 2=no hardware, RC ignores IPBus responses.',
    'group': 'df'
    }

  option_FTK['failOnBadLink']={
    'default': hex(0x0),
    'short':'',
    'arg': True,
    'description':'A mask to toggle whether RC should fail on a link having bad status. If 0 for a given link, then just print a warning. Default = 0x0 (never fail)',
    'group': 'df'
    }

  option_FTK['IM_L1TT_PS_MASK']={
    'default': hex(0x20),
    'short':'',
    'arg': True,
    'description':'Trigger type prescale mask. Default 32 = 0x20 = FTK trigger type',
    'group': 'df'
    }

  option_FTK['DF_skewTimeoutThresholdMax']={
    'default': hex(0x30000),
    'short':'',
    'arg': True,
    'description':'Maximum timeout threshold to drop IM input lane. Default 0x30000 = ~1 ms',
    'group': 'df'
    }

  option_FTK['DF_skewTimeoutThresholdMin']={
    'default': hex(0xff),
    'short':'',
    'arg': True,
    'description':'Minimum timeout threshold to drop IM input lane. Default 0xff = ~1 us',
    'group': 'df'
    }

  option_FTK['DF_packetHandlerTimeoutThreshold']={
    'default': hex(0x4e20),
    'short':'',
    'arg': True,
    'description':'Timeout threshold for the packet handler to complete a partial received packet. Default 0x4e20 = 100 us',
    'group': 'df'
    }
