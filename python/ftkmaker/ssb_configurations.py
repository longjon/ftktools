########################################################################
### Dictionaries for different SSB configurations.
########################################################################
### Assumes a default of running the full slice at P1
### In current set up, each option should be identical to the name of
### the attribute to set in the xml file. This is applied *only* to AUX
### so no conflicts with other boards should arise.
########################################################################

# Default options (only need to be set if different from those defined in the schema)
fpga_configs = {
        "VMEIF"     : {
			"Id":		0xf,
			"Type":		'"VMEIF"',
			"Enable":		1,
            },
        "EXTF0"     : {
			"Id":		0x0,
			"Type":		'"EXTF"',
			"Enable":		'"%s"%(ssb_extf_enabled)',
                        "FWHash":               '"%s"%(ssb_extf_hash)',
                        "FWDate":               '"%s"%(ssb_extf_date)',
			"EXPPath":		'"%s%d.txt"%(ssb_exp_path,ssb_tower)',
			"TFPath":		'"%s%d.txt"%(ssb_tf_path,ssb_tower)',
			"EXPChecksum":		'"%s"%(ssb_exp_checksum)',
			"TFChecksum":		'"%s"%(ssb_tf_checksum)',
			"SSMapAUXPix":		'"%s%d.txt"%(ssb_sspixaux_path,ssb_tower)',
			"SSMapAUXSCT":		'"%s%d.txt"%(ssb_ssdfpix_path,ssb_tower)',
			"SSMapDFPix" :		'"%s%d.txt"%(ssb_ssauxsct_path,ssb_tower)',
			"SSMapDFSCT" :		'"%s%d.txt"%(ssb_ssdfsct_path,ssb_tower)',
                        "FreezeMask":           '"%s"%(ssb_freezemask)',
                        "AUXADelay":            0x0100,
                        "AUXBDelay":            0x0100,
                        "DFBDelay":             0x0000,
                        "TFDelay":              0x0200,
                        "DFPacketFreezeLength": 0x0,
                        "AUXPacketFreezeLength":0x0,
                        "TFPacketFreezeLength": 0x0,
                        "FPGAReset":            '"%s"%(ssb_fpga_reset)',
                        "TimeoutThreshold":     0xffffffff,
                        "IgnoreDFB":            '"%s"%(ssb_ignoreDFB)',
                        "IgnoreAUXB":           '"%s"%(ssb_ignoreAUXB)',
                        "IgnoreBP":             0,
                        "AUXDupRemoval":        0x0,
                        "NColSpy":              10,
                        "AUXTrackLimit":        '"%s"%(ssb_AUXLimit)',
                        "EXPTrackLimit":        '"%s"%(ssb_EXPLimit)',
                        "Chi2_Cut":             '"%s"%(ssb_chi2)',
            },
        "EXTF1"     : {
			"Id":		0x1,
			"Type":		'"EXTF"',
			"Enable":		'"%s"%(ssb_extf_enabled)',
                        "FWHash":               '"%s"%(ssb_extf_hash)',
                        "FWDate":               '"%s"%(ssb_extf_date)',
			"EXPPath":		'"%s%d.txt"%(ssb_exp_path,ssb_tower)',
			"TFPath":		'"%s%d.txt"%(ssb_tf_path,ssb_tower)',
			"EXPChecksum":		'"%s"%(ssb_exp_checksum)',
			"TFChecksum":		'"%s"%(ssb_tf_checksum)',
			"SSMapAUXPix":		'"%s%d.txt"%(ssb_sspixaux_path,ssb_tower)',
			"SSMapAUXSCT":		'"%s%d.txt"%(ssb_ssdfpix_path,ssb_tower)',
			"SSMapDFPix" :		'"%s%d.txt"%(ssb_ssauxsct_path,ssb_tower)',
			"SSMapDFSCT" :		'"%s%d.txt"%(ssb_ssdfsct_path,ssb_tower)',
                        "FreezeMask":           '"%s"%(ssb_freezemask)',
                        "AUXADelay":            0x0100,
                        "AUXBDelay":            0x0100,
                        "DFBDelay":             0x0000,
                        "TFDelay":              0x0200,
                        "DFPacketFreezeLength": 0x0,
                        "AUXPacketFreezeLength":0x0,
                        "TFPacketFreezeLength": 0x0,
                        "FPGAReset":            '"%s"%(ssb_fpga_reset)',
                        "TimeoutThreshold":     0xffffffff,
                        "IgnoreDFB":            '"%s"%(ssb_ignoreDFB)',
                        "IgnoreAUXB":           '"%s"%(ssb_ignoreAUXB)',
                        "IgnoreBP":             0,
                        "AUXDupRemoval":        0x0,
                        "NColSpy":              10,
                        "AUXTrackLimit":        '"%s"%(ssb_AUXLimit)',
                        "EXPTrackLimit":        '"%s"%(ssb_EXPLimit)',
                        "Chi2_Cut":             '"%s"%(ssb_chi2)',
            },
        "EXTF2"     : {
			"Id":		0x0,
			"Type":		'"EXTF"',
			"Enable":		0x0,
                        "FWHash":               '"%s"%(ssb_extf_hash)',
                        "FWDate":               '"%s"%(ssb_extf_date)',
			"EXPPath":		'"%s%d.txt"%(ssb_exp_path,ssb_tower)',
			"TFPath":		'"%s%d.txt"%(ssb_tf_path,ssb_tower)',
			"EXPChecksum":		'"%s"%(ssb_exp_checksum)',
			"TFChecksum":		'"%s"%(ssb_tf_checksum)',
			"SSMapAUXPix":		'"%s%d.txt"%(ssb_sspixaux_path,ssb_tower)',
			"SSMapAUXSCT":		'"%s%d.txt"%(ssb_ssdfpix_path,ssb_tower)',
			"SSMapDFPix" :		'"%s%d.txt"%(ssb_ssauxsct_path,ssb_tower)',
			"SSMapDFSCT" :		'"%s%d.txt"%(ssb_ssdfsct_path,ssb_tower)',
                        "FreezeMask":           '"%s"%(ssb_freezemask)',
                        "AUXADelay":            0x0100,
                        "AUXBDelay":            0x0100,
                        "DFBDelay":             0x0000,
                        "TFDelay":              0x0200,
                        "DFPacketFreezeLength": 0x0,
                        "AUXPacketFreezeLength":0x0,
                        "TFPacketFreezeLength": 0x0,
                        "FPGAReset":            '"%s"%(ssb_fpga_reset)',
                        "TimeoutThreshold":     0xffffffff,
                        "IgnoreDFB":            '"%s"%(ssb_ignoreDFB)',
                        "IgnoreAUXB":           '"%s"%(ssb_ignoreAUXB)',
                        "IgnoreBP":             0,
                        "AUXDupRemoval":        0x0,
                        "NColSpy":              10,
                        "AUXTrackLimit":        '"%s"%(ssb_AUXLimit)',
                        "EXPTrackLimit":        '"%s"%(ssb_EXPLimit)',
                        "Chi2_Cut":             '"%s"%(ssb_chi2)',
            },
        "EXTF3"     : {
			"Id":		0x3,
			"Type":		'"EXTF"',
			"Enable":		0x0,
                        "FWHash":               '"%s"%(ssb_extf_hash)',
                        "FWDate":               '"%s"%(ssb_extf_date)',
			"EXPPath":		'"%s%d.txt"%(ssb_exp_path,ssb_tower)',
			"TFPath":		'"%s%d.txt"%(ssb_tf_path,ssb_tower)',
			"EXPChecksum":		'"%s"%(ssb_exp_checksum)',
			"TFChecksum":		'"%s"%(ssb_tf_checksum)',
			"SSMapAUXPix":		'"%s%d.txt"%(ssb_sspixaux_path,ssb_tower)',
			"SSMapAUXSCT":		'"%s%d.txt"%(ssb_ssdfpix_path,ssb_tower)',
			"SSMapDFPix" :		'"%s%d.txt"%(ssb_ssauxsct_path,ssb_tower)',
			"SSMapDFSCT" :		'"%s%d.txt"%(ssb_ssdfsct_path,ssb_tower)',
                        "FreezeMask":           '"%s"%(ssb_freezemask)',
                        "AUXADelay":            0x0100,
                        "AUXBDelay":            0x0100,
                        "DFBDelay":             0x0000,
                        "TFDelay":              0x0200,
                        "DFPacketFreezeLength": 0x0,
                        "AUXPacketFreezeLength":0x0,
                        "TFPacketFreezeLength": 0x0,
                        "FPGAReset":            '"%s"%(ssb_fpga_reset)',
                        "TimeoutThreshold":     0xffffffff,
                        "IgnoreDFB":            '"%s"%(ssb_ignoreDFB)',
                        "IgnoreAUXB":           '"%s"%(ssb_ignoreAUXB)',
                        "IgnoreBP":             0,
                        "AUXDupRemoval":        0x0,
                        "NColSpy":              10,
                        "AUXTrackLimit":        '"%s"%(ssb_AUXLimit)',
                        "EXPTrackLimit":        '"%s"%(ssb_EXPLimit)',
                        "Chi2_Cut":             '"%s"%(ssb_chi2)',
                },
        "HITWARRIOR": {
			"Id":		0x4,
			"Type":		'"HITWARRIOR"',
			"Enable":		1,
                        "FWHash":               '"%s"%(ssb_hw_hash)',
                        "FWDate":               '"%s"%(ssb_hw_date)',
                        "NColSpy":               10,
                        "IgnoreBP":             'ignore_flic',
                        "HWInputMask":            'n_enabled_extf',
                },
}

#######################################################################
### Configurations to be turned on by various options
#######################################################################

# Dictionary for options specific to Lab 4
lab4_dic = {
    #'EXPPath': '"/afs/cern.ch/work/v/vcavalie/public/constants/64tower_config/data_alignment/EXPConstants_reg%d.txt"%ssb_tower',
    #'EXPPath': '"/det/ftk/repo/condDB/lowDataflow/ssb_constants/EXPConstants_reg%d.txt"%ssb_tower',
    }

# Dictionary for SSB when FLIC is excluded
ignore_flic_dic = {
    'IgnoreBP':1,
}


#######################################################################
### Other default values to be set
#######################################################################

# For crate name, slot, gives slot of connected AUX
enhanced_slice_slot_map = { "4" : {
            16: {"auxA": 19, "auxC": 20} } }

# Note: these should be the same for each crate, but specifying them individually in case
# we ultimately need to tinker with them.
final_config_slot_map = {
        "3": {
            6: {"auxC": 2, "auxA": 4},
            11: {"auxC": 7, "auxA": 9},
            16: {"auxC": 12, "auxA": 14},
            21: {"auxC": 17, "auxA": 19},
            },
        "4": {
            6: {"auxC": 2, "auxA": 4},
            11: {"auxC": 7, "auxA": 9},
            16: {"auxC": 12, "auxA": 14},
            21: {"auxC": 17, "auxA": 19},
            },
        "2": {
            6: {"auxC": 2, "auxA": 4},
            11: {"auxC": 7, "auxA": 9},
            16: {"auxC": 12, "auxA": 14},
            21: {"auxC": 17, "auxA": 19},
            },
        "1": {
            6: {"auxC": 2, "auxA": 4},
            11: {"auxC": 7, "auxA": 9},
            16: {"auxC": 12, "auxA": 14},
            21: {"auxC": 17, "auxA": 19},
            },
        "8": {
            6: {"auxC": 2, "auxA": 4},
            11: {"auxC": 7, "auxA": 9},
            16: {"auxC": 12, "auxA": 14},
            21: {"auxC": 17, "auxA": 19},
            },
        "7": {
            6: {"auxC": 2, "auxA": 4},
            11: {"auxC": 7, "auxA": 9},
            16: {"auxC": 12, "auxA": 14},
            21: {"auxC": 17, "auxA": 19},
            },
        "6": {
            6: {"auxC": 2, "auxA": 4},
            11: {"auxC": 7, "auxA": 9},
            16: {"auxC": 12, "auxA": 14},
            21: {"auxC": 17, "auxA": 19},
            },
        "5": {
            6: {"auxC": 2, "auxA": 4},
            11: {"auxC": 7, "auxA": 9},
            16: {"auxC": 12, "auxA": 14},
            21: {"auxC": 17, "auxA": 19},
            },
        }
