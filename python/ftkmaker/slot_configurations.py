# This file contains the slot configurations for special set-ups, such as pre-defined slices
# to be tested as well as the final configuration. They are intended to be used to overwrite
# whatever slot assignments are set in the options file when these configurations are used.

class Configs():
    ENHANCED = 1
    ALBERTO = 2
    SLICE35 = 3
    SLICE35_DFS = 4
    FINAL = 5
    ENHANCED40 = 6
    MULTIDFPU = 7

def findConfig(opts):
    config_enum = 0
    if opts["create_enhanced_slice"]: config_enum = Configs.ENHANCED
    if opts["create_enhanced_slice40"]:
        if not config_enum==0: return -1
        config_enum = Configs.ENHANCED40
    if opts["create_alberto_slice"]:
        if not config_enum==0: return -1
        config_enum = Configs.ALBERTO
    if opts["create_slice_35"]:
        if not config_enum==0: return -1
        config_enum = Configs.SLICE35
    if opts["create_slice_35_allDF"]:
        if not config_enum==0: return -1
        config_enum = Configs.SLICE35_DFS
    if opts["create_final_config"]:
        if not config_enum==0: return -1
        config_enum = Configs.FINAL
    if opts["create_multi_DF_PU_slice"]:
        if not config_enum==0: return -1
        config_enum = Configs.MULTIDFPU
    if config_enum < 0: print WARN, "More than one config has been defined, so it's being ignored."
    return config_enum

def getCratesAndShelves(opts, config):
    if config < 1: # Use whatever's defined in the options
        slots = {'vme-crates': opts['vme-crates'],
                 'df-shelves': opts['df-shelves']}
        return slots
    if config==Configs.ENHANCED: return enhanced_slice_slots
    if config==Configs.ALBERTO: return alberto_slice_slots
    if config==Configs.MULTIDFPU: return multi_DF_PU_slots
    if config==Configs.SLICE35: return slice_35_slots
    if config==Configs.SLICE35_DFS: return slice_35_allDFs_slots
    if config==Configs.FINAL: return final_config_slots
    if config==Configs.ENHANCED40: return enhanced_slice40_slots

enhanced_slice_slots = {
        'vme-crates': [ ("4", "sbc-ftk-rcc-04.cern.ch","________________S__P") ],
        'df-shelves': [ ("2", "pc-ftk-tdq-01.cern.ch","__xxxx__Dxxx__")],
        }

enhanced_slice40_slots = {
        'vme-crates': [ ("3", "sbc-ftk-rcc-03.cern.ch","_______P____________") ],
        'df-shelves': [ ("2", "pc-ftk-tdq-01.cern.ch","__Dxxx__xxxx__")],
        }

multi_DF_PU_slots = {
        'vme-crates': [ ("7", "sbc-ftk-rcc-07.cern.ch","__P_P__P_P__P_P__P_P__"),
                        ("8", "sbc-ftk-rcc-08.cern.ch","__P_P__P_P__P_P__P_P__")],
        'df-shelves': [ ("4", "pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__")],
        }

alberto_slice_slots = {
        'vme-crates': [ ("1", "sbc-ftk-rcc-01.cern.ch","__P_P_SP_P_SP_P_SP_P_S"),
                        ("2", "sbc-ftk-rcc-02.cern.ch","__P_P_SP_P_SP_P_SP_P_S")],
        'df-shelves': [ ("1", "pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__")],
        }

slice_35_slots = {
        'vme-crates': [ ("4", "sbc-ftk-rcc-04.cern.ch","_________________P___S") ],
        'df-shelves': [ ("1", "pc-ftk-tdq-01.cern.ch","__xxxx__xDxx__")],
        }

slice_35_allDF_slots = {
        'vme-crates': [ ("4", "sbc-ftk-rcc-04.cern.ch","_________________P___S") ],
        'df-shelves': [ ("1", "pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__")],
        }

final_config_slots = {
        'vme-crates': [ ("3", "sbc-ftk-rcc-03.cern.ch","__P_P_SP_P_SP_P_SP_P_S"),
                        ("4", "sbc-ftk-rcc-04.cern.ch","__P_P_SP_P_SP_P_SP_P_S"),
                        ("1", "sbc-ftk-rcc-01.cern.ch","__P_P_SP_P_SP_P_SP_P_S"),
                        ("2", "sbc-ftk-rcc-02.cern.ch","__P_P_SP_P_SP_P_SP_P_S"),
                        ("8", "sbc-ftk-rcc-08.cern.ch","__P_P_SP_P_SP_P_SP_P_S"),
                        ("7", "sbc-ftk-rcc-07.cern.ch","__P_P_SP_P_SP_P_SP_P_S"),
                        ("6", "sbc-ftk-rcc-06.cern.ch","__P_P_SP_P_SP_P_SP_P_S"),
                        ("5", "sbc-ftk-rcc-05.cern.ch","__P_P_SP_P_SP_P_SP_P_S")],
        'df-shelves': [ ("2", "pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__"),
                        ("1", "pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__"),
                        ("4", "pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__"),
                        ("3", "pc-ftk-tdq-01.cern.ch","__DDDD__DDDD__"),],
        }

