########################################################################
### Dictionaries for different AUX configurations.
########################################################################
### Assumes a default of running the full slice at P1
### In current set up, each option should be identical to the name of
### the attribute to set in the xml file. This is applied *only* to AUX
### so no conflicts with other boards should arise.
########################################################################

# Default options (only need to be set if different from those defined in the schema)
#default_RCD
default_RCD_dic={
  "InterfaceName"  :  '"rc/commander"',
  "IPCName"  :  '""',
  "ActionTimeout"  :  30,
  "ProbeInterval"  :  20,
  "FullStatisticsInterval"  :120,
  "IfError"  :  '"Error"',
  "ControlsTTCPartitions"  :  0,
  "Logging"  :  1,
  "InitTimeout"  :  60,
  "ExitTimeout"  :  60,
  "RestartableDuringRun"  :  0,
  "IfExitsUnexpectedly"  :  '"Error"',
  "IfFailsToStart"  :  '"Error"',
  "Id"  :  0,
  "DataRequestDestination"  :  '"PC"',
  "config_version"  : "1.5",
}
#default for DFs
default_df_dic={
# "PhysAddress" : "0",
  "DryRun" : "0",
  "Dummy" : "0",
  "IPBus_CF" : "connections.xml",
#  "IPBus_TCF": "tx_configuration.txt",
  "DF_Global_CF": "df_multiboard_config.txt",
  #"DF_Global_CF" : "Slice2_04_config_Data2018_wIBL_EnhancedSlice.txt",
  "DF_System_CF" : "df_system_config_64towers.txt",
  "DF_DelayFile" : "df_board_delay_values.txt",
  "IM_ModuleListFile" : "moduleList_Data2018_64T.fixed.txt",
  "DF_MultiShelf_CF" : "df_multishelf_config.txt",
  "DF_OTF_Configuration" : 1,
  "IM_OTF_Configuration" : 0,
  "DF_validate_OTF" : 0,
  "DF_Write_OTF_Configuration" : 1,
  "DF_MaxCyclesWaitedForData" : 1048575,
  "DF_use_auto_delay_setting" : 1,
  "doFWCheck" : 0,
}

#######################################################################
### Configurations to be turned on by various options
#######################################################################

# Dictionary for options specific to Lab 4
lab4_dic = {

    }


#######################################################################
### Files to be included at P1 / Lab4
#######################################################################

# Files to be included at P1
default_inc = [
    "daq/segments/setup.data.xml",
   "daq/schema/core.schema.xml",
   "daq/hw/hosts-ros.data.xml",
   "ftk/schema/FTKCommon.schema.xml",
        ]

#Files to be included in Lab4
lab4_inc = [
      "daq/segments/setup.data.xml"
      "ftk/sw/FtkSwRepository.data.xml"
      "ftk/hw/hosts-ftk.data.xml"
      "daq/hw/hosts.data.xml"
      "daq/hw/workstations.data.xml"
      "daq/schema/core.schema.xml"
      "ftk/segments/FTK-RoA-Aux.data.xml"
      "ftk/segments/FTK-AMBoards.data.xml"
      "ftk/schema/Readout_Ssb.schema.xml"
      "ftk/schema/Readout_DataFormatter.schema.xml"
      "ftk/schema/Readout_flic.schema.xml"
        ]

#######################################################################
### Other default values to be set
#######################################################################

