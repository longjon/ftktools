""" This module hosts the fuction supposed to generate ReadoutModules (RMs) for DataFormatter
"""
# TODO
import os
import ftkmaker.df_configurations as df_configurations

from ftkmaker.df_tools import *


def optional_includes(self):
  return []

def execute(rcd, ftkProj, index):
  """ Creates the ReadoutModules (RMs) for DataFormatter.

  This function is called by the ftk_maker.py application that manages the
  creation of FTK partition and segments. This function is supposed to create
  the DataFormatter RMs and to append them to the rcd object received as parameter.
  The required OKS objects are accessible via the ftkProj that is an instance of
  the FTKProject class.
  Nota bene:
   - the user shall explicitly append the RMs to the rcd instance
   - the user could tweak the rcd object

  Input Parameters:
    rcd       An RCD instance. It will hosts all the RMs created by this function
    ftkProj   An instance of the FTKProject class instantiated by the caller
              The option dictionary is available via: ftkProj.args
    index     The index in the Dataformatter shelves lists. The function is
              supposed to access configuration data at row = index of the
              ftkProj.args["df-shelves"] dictionary entry, i.e.
              ftkProj.args["df-shelves"][index]

  Useful objects from ftkProj:
    args      The FTK option dictionary. E.g.: ftkProj.args["df-shelves"] provides
              a list of tuples with the format: ["rackName", "computer", "shelfMap"]
    dal_df    The dal module used to instantiate objects defined in the DF schema
  """

  s = "\033[1;37m" + "  [process_df.execute]" + "\033[0m"
  print s, "Making RCD", rcd.id, "running on", rcd.RunsOn.id

  # get the options
  opts  = ftkProj.args
  shelf = ftkProj.df_shelves[index]
  print s, "Processing crate:", shelf

  # Loop over the map
  shelfName=shelf[0]
  shelfMap =shelf[2]

  # Values to map physical slot to logical slot
  # In general, boards should only occupy 3-10
  logicalMapping = [13,11,9,7,5,3,1,2,4,6,8,10,12,14]


  for slot in range(0, len(shelfMap)):
    suffix = "%s-%02d" % (shelfName, logicalMapping[slot])
    # Occupied slots
    if shelfMap[slot] == "D":
      thisId = "DF-" + suffix # ReadoutModuleID (do not touch)

      slot=thisId.split('-')[2]    # get the slot number
      shelf=thisId.split('-')[1]   # get the shelf number

      if ftkProj.args['is_lab4'] and ftkProj.args['use_df22']:
        rm_id = 'DF-22'
        shelf = 2
        slot = 4  # DF22 mirrors DF-2-04
        if ftkProj.args['usePseudoData'] is False: print "Running on lab4 without Pseudodata! Make sure you want this"
      else:
        if slot < 10:
          rm_id = thisId
        else:
          rm_id = thisId
      print "%s Making %s@ReadoutModule_DataFormatter" % (s, rm_id)
      # Instantiate a DataFormatter configuration object
      RM_df = ftkProj.dal_df.ReadoutModule_DataFormatter(rm_id)

## ADD BOARD SPECIFIC CODE HERE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      if ftkProj.args['is_lab4'] and ftkProj.args['use_df22']: ipbus_name="DF22"
      else: ipbus_name="DF-%s-%s"%(shelf,slot)

      config_folder = ftkProj.args["DFConfigFolder"]

      df_default={}
#      df_default.update(default_RCD_dic)
      df_default.update(df_configurations.default_df_dic)
      for k in df_default:
          exec("RM_df.%s = '%s'"%(k,df_default[k]))


      # Configure the object
      RM_df.ConfigPath = config_folder
      #RM_df.DataPath = config_folder
      #RM_df.IPBus_CF = "connections.xml"
      RM_df.IPBus_TCF = "tx_configuration.txt"
      RM_df.DF_CF = "%s/configuration.txt" % (ipbus_name)
      RM_df.IM_LUTs = "%s/IM_LUTs.txt" % (ipbus_name)
      RM_df.IM_PseudoData = "%s/IM_PseudoData.txt" % (ipbus_name)
      RM_df.IPBus_DID = ipbus_name
#      RM_df.DF_MaxCyclesWaitedForData =  ftkProj.args["DF_MaxCyclesWaitedForData"]
#      RM_df.DF_use_auto_delay_setting =  ftkProj.args["DF_use_auto_delay_setting"]
#      RM_df.doFWCheck = ftkProj.args["DF_doFWCheck"]
      RM_df.DF_FwVersion = ftkProj.args["DF_FwVersion"]
      RM_df.IM_FwVersion = ftkProj.args["IM_FwVersion"]
      RM_df.usePseudoData = ftkProj.args["usePseudoData"]
      RM_df.IPBusEmulatorMode = ftkProj.args["IPBusEmulatorMode"]
      RM_df.failOnBadLink = ftkProj.args["failOnBadLink"]
      RM_df.IM_L1TT_PS_MASK = ftkProj.args["IM_L1TT_PS_MASK"]
      RM_df.IM_L1ID_PS_MASK = ftkProj.args["IM_L1ID_PS_MASK"]
      RM_df.IM_L1ID_PS_VALUE = ftkProj.args["IM_L1ID_PS_VALUE"]
      RM_df.IM_L1ID_FREEZE_VALUE= ftkProj.args["IM_L1ID_FREEZE_VALUE"]
      RM_df.IM_L1ID_FREEZE_MASK = ftkProj.args["IM_L1ID_FREEZE_MASK"]
      #RM_df.IM_ERR_FREEZE_MASK1 = ftkProj.args["IM_ERR_FREEZE_MASK1"]
      #RM_df.IM_ERR_FREEZE_MASK2 = ftkProj.args["IM_ERR_FREEZE_MASK2"]
      RM_df.IM_START_L1ID_MASK = ftkProj.args["IM_START_L1ID_MASK"]
      RM_df.IM_START_L1ID_VALUE = ftkProj.args["IM_START_L1ID_VALUE"]
      RM_df.IM_RECOVER_CONFIG = ftkProj.args["IM_RECOVER_CONFIG"]
      RM_df.IM_FILLGAP_CONFIG = ftkProj.args["IM_FILLGAP_CONFIG"]
      RM_df.IM_HIT_PER_MODULE_IBL = ftkProj.args["IM_HIT_PER_MODULE_IBL"]
      RM_df.IM_HIT_PER_MODULE_PIX = ftkProj.args["IM_HIT_PER_MODULE_PIX"]
      RM_df.IM_HIT_PER_MODULE_SCT = ftkProj.args["IM_HIT_PER_MODULE_SCT"]
      # RM_df.IM_IBL_KENTO_REMOVAL      = ftkProj.args["IM_IBL_KENTO_REMOVAL"]
      RM_df.IM_SPECIAL5               = ftkProj.args["IM_SPECIAL5"]
      RM_df.IM_PSEUDO_INPUT_RATE      = ftkProj.args["IM_PSEUDO_INPUT_RATE"]
      # RM_df.IM_FREEZE_WAIT_CLK        = ftkProj.args["IM_FREEZE_WAIT_CLK"]
      # RM_df.IM_GTP_RESET_WAIT_CLK     = ftkProj.args["IM_GTP_RESET_WAIT_CLK"]
      # RM_df.IM_XOFF_KEYWORD           = ftkProj.args["IM_XOFF_KEYWORD"]
      # RM_df.IM_XOFF_CONFIG            = ftkProj.args["IM_XOFF_CONFIG"]
      # RM_df.IM_IGNORE_HOLD            = ftkProj.args["IM_IGNORE_HOLD"]
      # RM_df.IM_XOFF_MODE              = ftkProj.args["IM_XOFF_MODE"]
      # RM_df.IM_GANGED_PIXEL           = ftkProj.args["IM_GANGED_PIXEL"]
      # RM_df.IM_REDUNDANCY             = ftkProj.args["IM_REDUNDANCY"]
      RM_df.IM_DUPLICATED_MODULE      = ftkProj.args["IM_DUPLICATED_MODULE"]
      RM_df.IM_IS_APPLY_SELECTPS      = ftkProj.args["IM_IS_APPLY_SELECTPS"]
      RM_df.IM_SLINK_MODE_MASK        = ftkProj.args["IM_SLINK_MODE_MASK"]
      # RM_df.IM_IS_APPLY_REJECTPS      = ftkProj.args["IM_IS_APPLY_REJECTPS"]
      # RM_df.IM_IS_APPLY_L1TTPS        = ftkProj.args["IM_IS_APPLY_L1TTPS"]
      RM_df.IM_IS_APPLY_TRUNCATION    = ftkProj.args["IM_IS_APPLY_TRUNCATION"]
      # RM_df.IM_IS_APPLY_FREEZE        = ftkProj.args["IM_IS_APPLY_FREEZE"]
      # RM_df.IM_IS_APPLY_L1ID_FREEZE   = ftkProj.args["IM_IS_APPLY_L1ID_FREEZE"]
      # RM_df.IM_IS_APPLY_DF_FREEZE     = ftkProj.args["IM_IS_APPLY_DF_FREEZE"]
      # RM_df.IM_IS_APPLY_ERR_FREEZE    = ftkProj.args["IM_IS_APPLY_ERR_FREEZE"]
      RM_df.IM_PSEUDO_DATA_XOFF       = ftkProj.args["IM_PSEUDO_DATA_XOFF"]
      RM_df.IM_PSEUDO_DATA_INITIAL_DELAY  = ftkProj.args["IM_PSEUDO_DATA_INITIAL_DELAY"]
      RM_df.DF_skewTimeoutThresholdMax = ftkProj.args["DF_skewTimeoutThresholdMax"]
      RM_df.DF_skewTimeoutThresholdMin = ftkProj.args["DF_skewTimeoutThresholdMin"]
      RM_df.DF_packetHandlerTimeoutThreshold = ftkProj.args["DF_packetHandlerTimeoutThreshold"]
      RM_df.DF_Shelf = shelf
      RM_df.DF_Slot = slot
      #RM_df.Name = ipbus_name

      moduleListName = 'moduleList.txt'
      RM_df.IM_ModuleListFile = moduleListName
      linkModuleList(ftkProj.args["IM_ModuleListFile"],moduleListName)

      if ftkProj.args["GenerateConfiguration"]:
        if ftkProj.args['is_lab4'] and ftkProj.args['use_df22']:
          if ftkProj.args['usePseudoData'] : makel4PseudoDataConfig(ftkProj.args["PseudoData_directory"])
          makel4LUTsConfig(ftkProj.args["LUTs_directory"])
        else:
          if ftkProj.args['usePseudoData'] : makePseudoDataConfig(ftkProj.args["PseudoData_directory"],shelf,slot)
          makeLUTsConfig(ftkProj.args["LUTs_directory"],shelf,slot)

      if ftkProj.args["UseMultithreading"]:
        RM_df.ParallelConfigure = True
        RM_df.ParallelConnect = True
        RM_df.ParallelPrepareForRun = True
        RM_df.ParallelStopROIB = True
        RM_df.ParallelStopDC = True
        RM_df.ParallelStopHLT = True
        RM_df.ParallelStopRecording = True
        RM_df.ParallelStopGathering = True
        RM_df.ParallelStopArchiving = True
        RM_df.ParallelDisconnect = True
        RM_df.ParallelUnconfigure = True
        RM_df.ParallelMonitoring = True
        RM_df.SeparatePublishThreads = True
        RM_df.ProtectStart = True
        RM_df.ProtectStop = True
        RM_df.SerializeRCDMonitoring = True

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ADD BOARD SPECIFIC CODE HERE

      # Add the ReadoutModule to the RCD
      rcd.Contains.append(RM_df)
    # Empty slots
    else:
      if logicalMapping[slot] < 11 and logicalMapping[slot] > 2:
        print s, "Skip slot", logicalMapping[slot]
