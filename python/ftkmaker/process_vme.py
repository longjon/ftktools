""" This module hosts the fuction supposed to generate ReadoutModules (RMs) for flic
"""
# TODO
# - ......

import os
import commands
import subprocess
from ftkmaker.aux_configurations import *
import ftkmaker.amb_configurations as amb_configurations
import ftkmaker.ssb_configurations as ssb_configurations
import ftkmaker.slot_configurations as slot_configurations

def optional_includes(ftkProj):
    if ftkProj.args['is_lab4']:
        return lab4_inc
    else:
        return default_inc

def execute(rcd, ftkProj, index):

  '''
  Creates the ReadoutModules (RMs) for VME: ambm aux and ssb.

  This function is called by the ftk_maker.py application that manages the
  creation of FTK partition and segments. This function is supposed to create
  the VME RMs and to append them to the rcd object received as parameter.
  The required OKS objects are accessible via the ftkProj that is an instance
  of the FTKProject class.
  N.B.:
  - the user shall explicitly append the RMs to the rcd instance
  - the user could tweak the rcd object

  Input Parameters:
  rcd       An RCD instance. It will hosts all the RMs created by this function
  ftkProj   An instance of the FTKProject class instantiated by the caller
    The option dictionary is available via: ftkProj.args
  index     The index in the Dataformatter shelves lists. The function is
    supposed to access configuration data at row = index of the
    ftkProj.args["vme-crates"] dictionary entry, i.e.
    ftkProj.args["vme-crates"][index]

  Useful objects from ftkProj:
  args      The FTK option dictionary. E.g.: ftkProj.args["vme-crates"] provides
    a list of tuples with the format: ["RackName", "computer", "crateMap"]
  dal_amb   The dal module used to instantiate objects defined in the amb schema
  dal_aux   The dal module used to instantiate objects defined in the amb schema
  dal_ssb   The dal module used to instantiate objects defined in the amb schema
  dal_RMD   The dal module used to instantiate objects defined in the amb schema
  '''

  pId = "\033[1;37m" + "  [process_vme.execute]" + "\033[0m"

  #import option_dependent_configurations # Needs access to ftkproj to set up some paths
  # Set up checksum paths
  subdir = "checksums/data/"
  if not ftkProj.args["use_data_alignment"]: sub_dir = "checksums/mc/"

  script_path = os.path.abspath(__file__)
  script_dir  = os.path.split(script_path)[0]
  print pId, "Checksum files are taken by default locally from ", script_dir+"/"+subdir

  mod_path = subdir+"mod_checksum.json"
  ss_path  = subdir+"ss_checksum.json"
  tf_path  = subdir+"tf_checksum.json"
  pb_path  = subdir+"pb_checksum.json"
  ss_path  = os.path.join(script_dir, ss_path)
  if not os.path.exists( ss_path ):
      for dataDir in os.environ['TDAQ_DB_PATH'].split(':'):
          csDir = dataDir + "/ftktools/"
          ss_path  = os.path.join(csDir, subdir+"ss_checksum.json")
          if os.path.exists( ss_path ):
              script_dir = csDir
              break
      print pId, "Checksum was not found locally and the location has been changed to ", script_dir+subdir
  tf_path  = os.path.join(script_dir, tf_path)
  pb_path  = os.path.join(script_dir, pb_path)
  mod_path = os.path.join(script_dir, mod_path)

  # Import checksums
  with open(mod_path, 'r') as f:
      mod_checksum = json.load(f)
  with open(ss_path, 'r') as f:
      ss_checksum = json.load(f)
  with open(tf_path, 'r') as f:
      tf_checksum = json.load(f)
  with open(pb_path, 'r') as f:
      pbank_checksum = json.load(f)

  print pId, "Making RCD", rcd.id, "running on", rcd.RunsOn.id

  # Change RCD id needed.
  # e.g.: rcd.ProbeInterval=77

  # get the options
  opts  = ftkProj.args

  tower = opts["tower"]
  is_lab4 = opts["is_lab4"]
  exclude_amb = opts["exclude_amb"]
  exclude_ssb = opts["exclude_ssb"]
  ignore_flic = opts["ignore_flic"]
  empty_tvs = opts["empty_tvs"]
  use_m14_opts = opts["use_m14_opts"]
  fwversions = opts["AUX_firmwareversions"]
  print pId, "Default tower", tower
  print pId, "Creating partition for Lab 4?", is_lab4
  print pId, "Including AMB?", not exclude_amb
  print pId, "Including SSB?", not exclude_ssb
  print pId, "Using m14 config?", use_m14_opts
  print pId, "Using data alignment?", opts["use_data_alignment"]

  # Find out if we're working in a special configuration
  config_enum = slot_configurations.findConfig(opts)
  crate = ftkProj.vme_crates[index]

  tower_map = final_config_tower_assignments
  ssb_map = ssb_configurations.final_config_slot_map
  if config_enum == slot_configurations.Configs.ENHANCED or config_enum == slot_configurations.Configs.ENHANCED40:
      tower_map = enhanced_slice_tower_assignments
      ssb_map = ssb_configurations.enhanced_slice_slot_map
  print pId, "Processing crate:", crate
  print pId, "Configuration:", config_enum

  # Loop over the map
  crateName=crate[0]
  print pId, crateName
  crateMap =crate[2]
  print pId, tower_map

  #Loop over slots, and save towers for PUs that are included as enabled towers
  enabled_towers = []
  for slot in range(0, len(crateMap)):
    towerID = None
    if crateMap[slot] == "P":       
       if tower != None: towerID = tower
       elif crateName in tower_map and slot in tower_map[crateName]:
         towerID = tower_map[crateName][slot]
         enabled_towers.append(towerID)

  for slot in range(0, len(crateMap)):
    suffix = "%s-%02d" % (crateName, slot)
    if tower != None: towerID = tower
    elif crateName in tower_map and slot in tower_map[crateName]:
        towerID = tower_map[crateName][slot]
    else :
      towerID = None
    # Overwrite tower if LUT is set

    #if crateMap[slot] == "P":
    #    print "Tower ID:", towerID
    #    print "Using tower:", tower

    ## SSB  ###########################################
    if crateMap[slot] == "S":
      rm_id = "SSB-" + suffix
      print "%s Making %s@ReadoutModule_Ssb" % (pId, rm_id)
      # Create the ReadoutModule
      RM_ssb = ftkProj.dal_ssb.ReadoutModule_Ssb(rm_id)


## ADD SSB SPECIFIC CODE HERE >>>>>>>>>>>>>>>>>>>>>>>>>

      # Get tower info
      ssb_towers = [-1,-1,-1,-1]
      if crateName in ssb_map and slot in ssb_map[crateName]:
        if 'auxA' in ssb_map[crateName][slot]:
            aux_slot = ssb_map[crateName][slot]['auxA']
            if crateName in tower_map and aux_slot in tower_map[crateName]:
                ssb_towers[0] = tower_map[crateName][aux_slot]
                ssb_towers[2] = tower_map[crateName][aux_slot]
        if 'auxC' in ssb_map[crateName][slot]:
            aux_slot = ssb_map[crateName][slot]['auxC']
            if crateName in tower_map and aux_slot in tower_map[crateName]:
                ssb_towers[1] = tower_map[crateName][aux_slot]
                ssb_towers[3] = tower_map[crateName][aux_slot]

      # Create FPGA and Constants object with unique ID
      fpgas = []
      ssb_tower = -1
      ssb_exp_path = opts["SSB_Constants_Base"] + "/EXPConstants_reg"
      ssb_tf_path  = opts["SSB_Constants_Base"] + "/TFConstants_reg"
      ssb_sspixaux_path = opts["SSB_SSMap_Base"] + "/ssmapPixAux_tower"
      ssb_ssdfpix_path = opts["SSB_SSMap_Base"] + "/ssmapPixDF_tower"
      ssb_ssauxsct_path = opts["SSB_SSMap_Base"] + "/ssmapSctAux_tower"
      ssb_ssdfsct_path = opts["SSB_SSMap_Base"] + "/ssmapSctDF_tower"

      if is_lab4:
        ssb_exp_path = "/det/ftk/repo/condDB/lowDataflow/ssb_constants/EXPConstants_reg"
        ssb_tf_path = "/det/ftk/repo/condDB/lowDataflow/ssb_constants/TFConstants_reg"
        ssb_sspixaux_path = "/det/ftk/repo/condDB/enhanced/configs/ssmap/SSB/ssmapPixAux_tower"
        ssb_ssdfpix_path =  "/det/ftk/repo/condDB/enhanced/configs/ssmap/SSB/ssmapPixDF_tower"
        ssb_ssauxsct_path = "/det/ftk/repo/condDB/enhanced/configs/ssmap/SSB/ssmapSctAux_tower"
        ssb_ssdfsct_path =  "/det/ftk/repo/condDB/enhanced/configs/ssmap/SSB/ssmapSctDF_tower"

      n_enabled_extf = 0 # this is now a mask of enabled EXTFs
      for fpga_name in sorted(ssb_configurations.fpga_configs.keys()):
        fpga = ftkProj.dal_ssb.Ssb_Fpga(fpga_name+"_"+rm_id)
        # Figure out which tower corresponds to this fpga
        if "EXT" in fpga_name:
            fpga_id = int(fpga_name.split("EXTF")[1])
            ssb_tower = ssb_towers[fpga_id]
            ssb_extf_enabled = str(int(ssb_tower in enabled_towers))
            ssb_exp_checksum = opts["SSB_EXP_Checksums"][ssb_tower][1] # it's (tower,checksum)
            ssb_tf_checksum = opts["SSB_TF_Checksums"][ssb_tower][1]
            ssb_extf_date = opts["SSB_EXTF_Hash"][0]
            ssb_extf_hash = opts["SSB_EXTF_Hash"][1]
            ssb_hw_date = opts["SSB_HW_Hash"][0]
            ssb_hw_hash = opts["SSB_HW_Hash"][1]
            ssb_freezemask = opts["SSB_FreezeMask"]
            ssb_fpga_reset = opts["SSB_Reset_Threshold"]
            ssb_ignoreDFB  = opts["SSB_IgnoreDFB"]
            ssb_ignoreAUXB  = opts["SSB_IgnoreAUXB"]
            ssb_AUXLimit = opts["SSB_AUXLimit"]
            ssb_EXPLimit = opts["SSB_EXPLimit"]
            ssb_chi2     = opts["SSB_Chi2"]

            #ssb_exp_checksum = commands.getoutput("crc_exp.py -i " + ssb_exp_path + str(ssb_tower) + ".txt")

        # Add all attributes from ssb_configurations
        for value in ssb_configurations.fpga_configs[fpga_name].keys():
          if 'EXT'  in fpga_name and value =="Enable":
            if ssb_configurations.fpga_configs[fpga_name][value]: # if enabled
              extf_num = int(fpga_name[-1:])
              n_enabled_extf += (0x1 << extf_num) # mask for enabled FPGAs
          exec("fpga.%s = %s"%(value, str(ssb_configurations.fpga_configs[fpga_name][value])))

        fpgas.append(fpga)
      # end fpga loop

      RM_ssb.Slot         = slot
      RM_ssb.FPGAs        = fpgas
      RM_ssb.Crate        = crateName
      RM_ssb.ReadHistosForPublish = 1
      RM_ssb.PublishOnlyNonZeroBins = 0

      if ftkProj.args["UseMultithreading"]:
        RM_ssb.ParallelConfigure = True
        RM_ssb.ParallelConnect = True
        RM_ssb.ParallelPrepareForRun = True
        RM_ssb.ParallelStopROIB = True
        RM_ssb.ParallelStopDC = True
        RM_ssb.ParallelStopHLT = True
        RM_ssb.ParallelStopRecording = True
        RM_ssb.ParallelStopGathering = True
        RM_ssb.ParallelStopArchiving = True
        RM_ssb.ParallelDisconnect = True
        RM_ssb.ParallelUnconfigure = True
        RM_ssb.ParallelMonitoring = True
        RM_ssb.SeparatePublishThreads = True
        RM_ssb.ProtectStart = True
        RM_ssb.ProtectStop = True
        RM_ssb.SerializeRCDMonitoring = False

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<< ADD SSB SPECIFIC CODE HERE

      # Add the ReadoutModule to the RCD
      rcd.Contains.append(RM_ssb)

    ## RMD  ###########################################
    if crateMap[slot] == "R":
      rm_id = "RMD-" + suffix
      print "%s Making %s@ReadoutModule_dummy" % (pId, rm_id)
      # Create the ReadoutModule
      RM_RMD = ftkProj.dal_RMD.ReadoutModule_Dummy(rm_id)
      RM_RMD.Slot = slot
      #RM_RMD.Name = rm_id

      # Add the ReadoutModule to the RCD
      rcd.Contains.append(RM_RMD)

    ## Processing Unit  ###############################
    if crateMap[slot] == "P":

      # Create PatternBankConf
      pb_id = "PBank-tower%d"%tower
      print "%s Making %s@PatternBankConf" % (pId, pb_id)
      pb    = ftkProj.dal_PU.PatternBankConf(pb_id)

## ADD PATTERNBANK SPECIFIC CODE HERE >>>>>>>>>>>>>>>>>>>>>

      # Example
      pb.NumPattPerChip = 2048

## <<<<<<<<<<<<<<<<<<<<< ADD PATTERNBANK SPECIFIC CODE HERE


      # Create ReadoutModule for PU
      rm_id = "PU-" + suffix
      print "%s Making %s@ReadoutModule_PU" % (pId, rm_id)
      RM_PU =ftkProj.dal_PU.ReadoutModule_PU(rm_id)
      RM_PU.PatternBankConf = pb

## ADD PU SPECIFIC CODE HERE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      # For example
      RM_PU.Slot = slot
      #RM_PU.Name = rm_id
      RM_PU.Crate = crateName
      RM_PU.towerID = tower

      if ftkProj.args["UseMultithreading"]:
        RM_PU.ParallelConfigure = False
        RM_PU.ParallelConnect = True
        RM_PU.ParallelPrepareForRun = True
        RM_PU.ParallelStopROIB = True
        RM_PU.ParallelStopDC = True
        RM_PU.ParallelStopHLT = True
        RM_PU.ParallelStopRecording = True
        RM_PU.ParallelStopGathering = True
        RM_PU.ParallelStopArchiving = True
        RM_PU.ParallelDisconnect = True
        RM_PU.ParallelUnconfigure = True
        RM_PU.ParallelMonitoring = True
        RM_PU.SeparatePublishThreads = True
        RM_PU.ProtectStart = True
        RM_PU.ProtectStop = True
        RM_PU.SerializeRCDMonitoring = False

      # Apply all block configurations
      # Note that the order applied matters -- the later ones will overwrite
      amb_values  = {}
      amb_values.update(amb_configurations.default_amb_dic)

#      if opts["create_enhanced_slice"]:    amb_values.update(amb_configurations.sliceA_amb_dic)
#      if opts["create_enhanced_slice40"]:  amb_values.update(amb_configurations.slice2_amb_dic)
      if is_lab4: amb_values.update(lab4_dic)

      for k in amb_values.keys():
          exec("RM_PU.%s = %s"%(k, str(amb_values[k])))

      # Get directory for constants:
      const_dir = "/det/ftk/repo/condDB/lowDataflow/"
      if is_lab4: const_dir = "/tbed/user/pbryant/"
      #TODO: get a path for data in lab 4 (and do everything else)
      # could just use same dir -- /det/ftk/condDB/enhanced/

      # Add pattern bank
      pb.NumPattPerChip = 131072
      pb.InChipOff = 0
      try:
	pb.Proc1Checksum = hex(int(pbank_checksum[str(tower)][0], 16))
      	pb.Proc2Checksum = hex(int(pbank_checksum[str(tower)][1], 16))
      	pb.Proc3Checksum = hex(int(pbank_checksum[str(tower)][2], 16))
      	pb.Proc4Checksum = hex(int(pbank_checksum[str(tower)][3], 16))
      except:
	pb.Proc1Checksum = 0x1
	pb.Proc2Checksum = 0x2
	pb.Proc3Checksum = 0x3
	pb.Proc4Checksum = 0x4
      #TODO: these paths will only work for P1 -- need to work on lab4
      #fname = "pbanks/patterns_user.sschmitt.DataAlignment_xm05_ym05_Reb64_v2.HW2.170217-64PU_30x128x72Ibl-NB9-NE6-BM0_partitioning2_170219-215620_reg%s_sub0.pbank.root"%tower
      fname = "pbanks/user.sschmitt.patterns_DataAlignment2017_xm05_ym09_Reb64_v9.HW2.SSEL3.180511-64PU_30x128x72Ibl-NB7-NE4-BM0_partitioning4_180601-095309_reg%s_sub0.pbank.root"%tower
      pb.FilePath = os.path.join(const_dir, fname)
      amb_cs_cmd = "ambslp_calculate_checksum -B %s 2>&1 > /dev/null"%os.path.join(const_dir, fname)
      subprocess.call(amb_cs_cmd, shell = True)
      file = open("./AM_checksum.txt", "r")
      pb.AMBChecksum = file.readline()
      #pb.FilePath = os.path.join(const_dir, "pbanks/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg%d_sub0.pbank.root"%tower)
      RM_PU.PatternBankConf = pb

      #TODO: If you want this in Lab4 we need to change all these paths

      # Add ModuleMap

      mod_maps = []
      print "%s Making Module for tower %d" % (pId, tower)
      for x in xrange(8):
          mod_maps += [ftkProj.dal_PU.AuxConstants("Aux1-Mod%d-tower%d"%(x, tower))]
          mod_maps[x].Path = os.path.join(const_dir,"AUX/ssmaps/auxTV_module_tower_%d_plane_%d.hex"%(tower, x))

	  try:
	    mod_maps[x].Checksum = hex(int(mod_checksum[str(tower)][x], 16))
	  except:
	    mod_maps[x].Checksum = 0x1
          mod_maps[x].Index = x
      RM_PU.ModuleMap = mod_maps

      # Add SSMapPath
      print "%s Making SSMap for tower %d" % (pId, tower)
      ss_maps = []
      for x in xrange(8):
          ss_maps += [ftkProj.dal_PU.AuxConstants("Aux1-SS%d-tower%d"%(x, tower))]
          ss_maps[x].Path = os.path.join(const_dir,"AUX/ssmaps/auxTV_ssid_tower_%d_plane_%d.hex"%(tower, x))
          try:
            ss_maps[x].Checksum = hex(int(ss_checksum[str(tower)][x], 16))
          except:
            ss_maps[x].Checksum = 0x2
          ss_maps[x].Index = x
      RM_PU.SSMapPath = ss_maps

      # Add TFConstants#
      print "%s Making TFConstants for tower %d" % (pId, tower)
      tf_consts = []
      fname = "AUX/constants/corrgen_raw_8L_reg%d.gcon"%tower
      for x in xrange(4):
          tf_consts += [ftkProj.dal_PU.AuxConstants("Aux1-TF%d-tower%d"%(x+1, tower))]
          tf_consts[x].Path = os.path.join(const_dir, fname)
          try:
	    tf_consts[x].Checksum = hex(int(tf_checksum[str(tower)][x], 16))
	  except:
            tf_consts[x].Checksum = 0x3
          tf_consts[x].Index = x+1
      RM_PU.TFConstants = tf_consts

      # Set up TVs
      #TODO: I don't think these first two actually exist
      if empty_tvs:
          tv = ftkProj.dal_PU.AuxTestParams("AuxTVEmpty")
          tv.Loop         = 1
          tv.RoadPath     = os.path.join(const_dir, "AUX/testvectors/empty/empty_roads_proc")
          tv.HitPath      = os.path.join(const_dir, "AUX/testvectors/empty/InputHits_empty")
          RM_PU.AuxTestSetup = tv

      elif not opts["use_data_alignment"]:
          tv = ftkProj.dal_PU.AuxTestParams("data_tower%d_1Evt"%tower)
          tv.Loop         = 1
          tv.RoadPath     = os.path.join(const_dir, "AUX/testvectors/data_tower%d_1Evt/ttbarTV_roads_proc"%tower)
          tv.HitPath      = os.path.join(const_dir, "AUX/testvectors/data_tower%d_1Evt/ttbarTV"%tower)
          RM_PU.AuxTestSetup = tv

      else:
          tv = ftkProj.dal_PU.AuxTestParams("data_tower%d_1Evt"%tower)
          tv.Loop         = 1
          tv.RoadPath     = os.path.join(const_dir, "AUX/testvectors/data_tower%d_1Evt_TRACKS/ttbarTV_roads_proc"%tower) #dont have these at p1, dont need em
          tv.HitPath      = os.path.join(const_dir, "/det/ftk/repo/condDB/auxInTV/Shelf4_1ev_r355877_1143502/AUX_%d/auxTV"%tower)
          RM_PU.AuxTestSetup = tv

      #setup firmware versions
      auxfirmware = ftkProj.dal_PU.firmware(fwversions["fwtag"])
      auxfirmware.Input1      = fwversions["Input1"]
      auxfirmware.Input2      = fwversions["Input2"]
      auxfirmware.Processor1  = fwversions["Processor1"]
      auxfirmware.Processor2  = fwversions["Processor2"]
      auxfirmware.Processor3  = fwversions["Processor3"]
      auxfirmware.Processor4  = fwversions["Processor4"]
      RM_PU.firmwareVersions = auxfirmware


      # Check for special configurations
      #tmp_exclude_amb = exclude_amb
      #tmp_exclude_ssb =

      # Apply all block configurations
      # Note that the order applied matters -- the later ones will overwrite
      aux_values  = {}
      aux_values.update(default_dic)
      if is_lab4: aux_values.update(lab4_dic)
      if exclude_amb: aux_values.update(exclude_amb_dic)
      if exclude_ssb: aux_values.update(exclude_ssb_dic)

      if use_m14_opts:
        if crateName in m14_slot_overwrites.keys():
          if slot in m14_slot_overwrites[crateName].keys():
            if 'exclude_amb' in m14_slot_overwrites[crateName][slot].keys() and m14_slot_overwrites[crateName][slot]['exclude_amb']:
              print pId, "Excluding AMB for this slot."
              aux_values.update(exclude_amb_dic)
            if 'exclude_ssb' in m14_slot_overwrites[crateName][slot].keys() and m14_slot_overwrites[crateName][slot]['exclude_ssb']:
              print pId, "Excluding SSB for this slot."
              aux_values.update(exclude_ssb_dic)

      for k in aux_values.keys():
          exec("RM_PU.%s = %s"%(k, str(aux_values[k])))


## <<<<<<<<<<<<<<<<<<<<<<<<<<<<< ADD AUX SPECIFIC CODE HERE


      # Add the ReadoutModules to the RCD
      rcd.Contains.append(RM_PU)


    else:
      print pId, "Skip slot", slot
