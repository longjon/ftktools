''' Module used to extend the option_FTK dictionary
'''

def extend(option_FTK):
  ''' Function to be used to extend the option_FTK dictionary
  To be used by FLIC board expert
  '''

  option_FTK['flic-shelves'] = {
    'short': '',
    'arg': True, 
    'default': [],
    'description':
    'Python list describing each FLIC shelf. Each element in the list is a python'
    + '\n# a tuple composed of 3 strings: shelf name, pc number, shelf map. The latter'
    + '\n# is string where the index represents the slot and the characters means:'
    + '\n# "F"=FLIC board and "_"=unused slot. [NB: slot 0 does not exist]. E.g.:'
    + '\n#  [ ("1","pc-tbed-ftk-01.cern.ch","___F_") ] for Lab4'
    + '\n#      -- or --'
    + '\n#  [ ("1","pc-ftk-tdq-04.cern.ch","_FF__") ] for P1'
    + '\n#'
    + '\n# option[\'flic-shelves\'] = [( "1" , "pc-ftk-tdq-04.cern.ch" , "_F___" ) ]', #SliceA
    'group': 'flic'
    }

  option_FTK['flic-ips'] = {
    'short': '',
    'arg': True,
    'default': [( "10.153.37.25" , "50000" , "10.153.37.17" ) ],
    'description':
    'Python list with IP information for each readout module (or board) in the shelf.'
    + '\n# Each tuple has 3 strings: flic IP, port, and host IP. The number of tuples'
    + '\n# must match the number of boards in the shelf (i.e. the number of F\'s written'
    + '\n# in each shelf). E.g.:'
    + '\n#  [ ("10.153.37.18","50000","10.153.37.16") ] for Lab4'
    + '\n#      -- or --'
    + '\n#  [ ("10.153.37.25","50000","10.153.37.17") ]'
    + '\n# option[\'flic-ips\'] = ['
    + '\n# ( "10.153.37.25" , "50000" , "10.153.37.17" ) ]',
    'group': 'flic'
    }
  
  option_FTK['Verbose'] = {
    'default':2,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['LoopbackMode'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['UseAsSSBe'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['TracksPerRecord'] = {
    'default':13,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['DelayBetweenRecords'] = {
    'default':13,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['DelayBetweenRecords'] = {
    'default':1000,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['TotalRecords'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['DelayBetweenSyncAttempts'] = {
    'default':5,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['TotalSyncAttempts'] = {
    'default':10,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['LinksInUse'] = {
    'default':1,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['TotalROSLinkResets'] = {
    'default':1,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['IgnoreROSLinks'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['ReloadFirmware'] = {
    'default':1,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['ReloadSRAMs'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['CaptureCondition'] = {
    'default':12,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }

  option_FTK['MonitoringChannel'] = {
    'default':0,
    'short':'',
    'arg': True,
    'description': '',
    'group':'flic'
    }
