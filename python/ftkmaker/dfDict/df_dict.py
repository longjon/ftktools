######################################
#  This code originally by Jim Muoa  #
#  Summer 2017                       #
#  Maintained by Tyler James Burch   #
#  tyler.james.burch@cern.ch         #
######################################

#!usr/bin/env python
import csv
import pprint
import sys
import json
from collections import defaultdict

def getDFInfo():
    # Asking for user input to display ROB ID's for user inputted DF
    dfInfo = raw_input("Type in which DF's information you would like to find i.e. '1-3', '1-8', or '1-9': ")

    # outputting ROB ID's associated to user inputted DF
    print '=============================='
    print "ROB ID's to DF", dfInfo, "are"
    print '=============================='
    iterVar = 0
    while iterVar < (len(dfDict[dfInfo]) - 4) :
        print dfDict[dfInfo][iterVar][0]
        iterVar += 1

    # Oututting cable ID's associated to user inputted DF
    print '=============================='
    print "Cable ID's to DF", dfInfo, "are"
    print '=============================='
    iterVar = 0
    while iterVar < (len(dfDict[dfInfo]) - 4) :
        print dfDict[dfInfo][iterVar][1]
        iterVar += 1

    # outputting channel mask associated to user inputted DF
    print '=============================='
    print "Channel mask for DF", dfInfo, "is", dfDict[dfInfo][len(dfDict[dfInfo]) - 3]
    print '=============================='

    main()

def getRobInfo():
    # outputting DF and cable ID for user inputted ROB ID
    robInfo = raw_input("Enter the ROB ID whose connections you would like to know: ")

    # finding key then cable ID, and IM lane associated to user inputted DOB ID
    print '======================='
    for key, value in dfDict.items():
        for sublist in value:
            if robInfo == sublist[0]:
                robDF = key
                robCable = sublist[1]
                robIM = sublist[2]
                print 'ROB', robInfo, 'is connected to DF', robDF, 'through cable', robCable, 'and IM Lane', robIM
    print '======================='

    main()

def getCableInfo():
    # outputting info for user inputted Cable ID
    robCableInfo = raw_input("Enter the Cable ID whose connections you would like to know: ")

    # finding key, ROB ID, and IM lane associated to user inputted cable ID
    print '======================='
    for key, value in dfDict.items():
        for sublist in value:
            if sublist[0] != sublist[-1]:
                if robCableInfo == sublist[1]:
                    robDF = key
                    robInfo = sublist[0]
                    robIM = sublist[2]
                    print 'Cable', robCableInfo, 'is connecting ROB', robInfo, 'to IM lane', robIM, 'on DF', robDF
    print '======================='

    main()

def dictionary():
    # using pprint module for output readability
    pprint.pprint(dict(dfDict))

    main()

def sdMask():
    pprint.pprint(dict(subdetectorMask))

    main()

def write2json():
    # output dictionary to json file
    s = json.dumps(dfDict, sort_keys = True)
    with open('dfDict.json', 'w') as f:
        f.write(s)

def exit():
    sys.exit()

def info():
    print 'Type in "dictionary" to display DF dictionary.'
    print 'Type in "df" to get DF information.'
    print 'Type in "rob" to get ROB information.'
    print 'Type in "cable" to get cable information.'
    print 'Type in "exit" to exit.'

    main()

def main():
    print 'Type in "info" for commands'
    k = raw_input()
    if k == 'dictionary':
        dictionary()
    elif k == 'df':
        getDFInfo()
    elif k == 'rob':
        getRobInfo()
    elif k == 'cable':
        getCableInfo()
    elif k == 'exit':
        exit()
    elif k == 'info':
        info()
    elif k == 'json':
        write2json()
    elif k == 'sub':
        sdMask()

# using defaultdict to allow for multiple lists per key
dfDict = defaultdict(list)

# page 1 of cabling info
with open('PIX_SCT_AsInstalledAtP1_1.csv') as f:
    readCSV = csv.reader(f)

    for column in readCSV:
        # this line disregards inputting the separating rows in the CSV file into the dictionary
        if column[4] != '':
            # removing tower number from DF info(row4)
            dashToSplitString = 2
            groups = column[4].split('-')
            column[4] = '-'.join(groups[:dashToSplitString])

            # determining channel type from ROB ID
            if ('0x11' or '0x12' or '0x13') in column[2]:
                channelType = 'Pixel'
            elif '0x2' in column[2]:
                channelType = 'SCT'
            elif '0x14' in column[2]:
                channelType = 'IBL'
            elif 'Rob ID' in column[2]:
                channelType = 'Subdetector'
            # this if statement gets rid of duplicating the header row for each page from the excel file
            if column[4] != 'FTK Shelf-Slot':
                # adding information to dictionary
                dfDict[column[4]].append((column[2], column[3], column[5], channelType))

# page 2 of cabling info
with open('PIX_SCT_AsInstalledAtP1_2.csv') as f:
    readCSV = csv.reader(f)

    for column in readCSV:
        # this line disregards inputting the separating rows in the CSV file into the dictionary
        if column[4] != '':
            # removing tower number from DF info(row4)
            dashToSplitString = 2
            groups = column[4].split('-')
            column[4] = '-'.join(groups[:dashToSplitString])
            # determining channel type from ROB ID
            if ('0x11' or '0x12' or '0x13') in column[2]:
                channelType = 'Pixel'
            elif '0x2' in column[2]:
                channelType = 'SCT'
            elif '0x14' in column[2]:
                channelType = 'IBL'
            elif 'Rob ID' in column[2]:
                channelType = 'Subdetector'
            if column[4] != 'FTK Shelf-Slot':
                # adding information to dictionary
                dfDict[column[4]].append((column[2], column[3], column[5], channelType))

# page 3 of cabling info
with open('PIX_SCT_AsInstalledAtP1_3.csv') as f:
    readCSV = csv.reader(f)

    for column in readCSV:
        # this line disregards inputting the separating rows in the CSV file into the dictionary
        if column[4] != '':
            # removing tower number from DF info(row4)
            dashToSplitString = 2
            groups = column[4].split('-')
            column[4] = '-'.join(groups[:dashToSplitString])
            # determining channel type from ROB ID
            if ('0x11' or '0x12' or '0x13') in column[2]:
                channelType = 'Pixel'
            elif '0x2' in column[2]:
                channelType = 'SCT'
            elif '0x14' in column[2]:
                channelType = 'IBL'
            elif 'Rob ID' in column[2]:
                channelType = 'Subdetector'
            if column[4] != 'FTK Shelf-Slot':
                # adding information to dictionary
                dfDict[column[4]].append((column[2], column[3], column[5], channelType))

# page 4 of cabling info
with open('PIX_SCT_AsInstalledAtP1_4.csv') as f:
    readCSV = csv.reader(f)

    for column in readCSV:
        # this line disregards inputting the separating rows in the CSV file into the dictionary
        if column[4] != '':
            # removing tower number from DF info(row4)
            dashToSplitString = 2
            groups = column[4].split('-')
            column[4] = '-'.join(groups[:dashToSplitString])
            # determining channel type from ROB ID
            if ('0x11' or '0x12' or '0x13') in column[2]:
                channelType = 'Pixel'
            elif '0x2' in column[2]:
                channelType = 'SCT'
            elif '0x14' in column[2]:
                channelType = 'IBL'
            elif 'Rob ID' in column[2]:
                channelType = 'Subdetector'
            # adding information to dictionary
            dfDict[column[4]].append((column[2], column[3], column[5], channelType))

# Retrieve value from dictionary lists using 'data['key' i.e. DF Shelf-Slot][list index][value index]'
# Example: print('FTK IM Lane for DF 1-3 ROB ID', dfDict['1-3'][2][0], 'is',  dfDict['1-3'][2][2])

# creating channel mask
for key, value in dfDict.items():
    IMchannel = []
    IMchannelBinary = []
    for sublist in value:
        IMchannel.append(sublist[2])
    for i in range(16):
        if str(i) in IMchannel:
            IMchannelBinary.append('1')
        else:
            IMchannelBinary.append('0')
    # reversing binary number to be indexed right to left
    reverseBinary = ("".join(IMchannelBinary))[::-1]
    # converting to hex
    IMint = int(reverseBinary, 2)
    IMhex = ('%x' % IMint).zfill(4)
    # adding in channel mask name for FTK Shelf-Slot key containing type of data in dictionary
    if key == 'FTK Shelf-Slot':
        IMhex = 'Channel Mask'
    dfDict[key].append(IMhex)

subdetectorMask = defaultdict(list)

for key, value in dfDict.items():
    pixelChannel = []
    iblChannel = []
    sctChannel = []
    pixelChannelBinary = []
    iblChannelBinary = []
    sctChannelBinary = []
    for sublist in value:
        if sublist[3] == 'Pixel':
            pixelChannel.append(sublist[2])
        if sublist[3] == 'IBL':
            iblChannel.append(sublist[2])
        if sublist[3] == 'SCT':
            sctChannel.append(sublist[2])
    for i in range(16):
        if str(i) in pixelChannel:
            pixelChannelBinary.append('1')
        else:
            pixelChannelBinary.append('0')
    for i in range(16):
        if str(i) in iblChannel:
            iblChannelBinary.append('1')
        else:
            iblChannelBinary.append('0')
    for i in range(16):
        if str(i) in sctChannel:
            sctChannelBinary.append('1')
        else:
            sctChannelBinary.append('0')
    # reversing binary number to be indexed right to left
    pixReverseBinary = ("".join(pixelChannelBinary))[::-1]
    iblReverseBinary = ("".join(iblChannelBinary))[::-1]
    sctReverseBinary = ("".join(sctChannelBinary))[::-1]
    # converting to hex
    pixInt = int(pixReverseBinary, 2)
    pixHex = ('%x' % pixInt).zfill(4)
    iblInt = int(iblReverseBinary, 2)
    iblHex = ('%x' % iblInt).zfill(4)
    sctInt = int(sctReverseBinary, 2)
    sctHex = ('%x' % sctInt).zfill(4)
    # adding in channel mask name for FTK Shelf-Slot key containing type of data in dictionary
    if key == 'FTK Shelf-Slot':
        pixHex = 'Pixel'
        iblHex = 'IBL'
        sctHex = 'SCT'
    subdetectorMask[key].append((pixHex, iblHex, sctHex))

# adding in FTK Tower numbers to associated DF's from txt file
with open('df_system_config.txt') as g:
    # this dictionary stores the board number with the respective DF
    boardDict = {}
    for line in g:
        if 'BoardNToShelfAndSlot' in line:
            board = (line[22] + line[23]).strip()
            dfShelfSlot = line[26] + '-' + ((line[29] + line[30]).strip())
            boardDict[dfShelfSlot] = board

        # This block associates the top towers with their respective board
        if 'BoardNToTopTower' in line:
            for key, value in boardDict.items():
                if value == (line[17] + line[18]).strip():
                    if line[20] == line[-1]:
                        topTower = (line[19] + line [20]).strip()
                    else:
                        topTower = (line[19] + line[20] + line[21]).strip()
                    dfDict[key].append(topTower)

        # This block associates the bottom towers with their respective board
        if 'BoardNToBotTower' in line:
            for key, value in boardDict.items():
                if value == (line[17] + line[18]).strip():
                    if line[20] == line[-1]:
                        botTower = (line[19] + line [20]).strip()
                    else:
                        botTower = (line[19] + line[20] + line[21]).strip()
                    dfDict[key].append(botTower)

# adding in tower names for FTK Shelf-Slot key containing type of data in dictionary
for key, value in dfDict.items():
    if key == 'FTK Shelf-Slot':
        dfDict[key].append('Top Tower')
        dfDict[key].append('Bottom Tower')

info()
main()
