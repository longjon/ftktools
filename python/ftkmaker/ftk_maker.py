#!/usr/bin/env tdaq_python

# TODO
#  - check ReadoutApplication configuration at P1

# ================== IMPORTS =========================
import os
import re
import sys
import pm.project
from pm.dal import dal
from config.dal import module as dal_module
from pm.partition.utils import parse_cmdline #, post_process
from pm.common import create_partition, create_datafiles
from pm.farm import local_computer, network_computer          # To find hosts
from pm.multinode import gen_hlt_segment, gen_ros_segment     # To create HLT and ROS segments
from pm.partition import default_farm

from ftkmaker.options import option_FTK, doc_FTK           # FTK options

import ftkmaker.process_vme
import ftkmaker.process_df
import ftkmaker.process_flic
from ftkmaker.df_tools import *
import ftkmaker.slot_configurations
#import collections
#Crate = collections.namedtuple('Crate', 'name computer stringmap')


#some color definitions
col_red = "\033[31m"
col_def = "\033[0m"
col_gre = "\033[32m"
col_bol = "\033[1m"
col_bla = "\033[30m"
col_blu = "\033[34m"
col_yel = "\033[33m"
col_whi = "\033[37m"
WARN=col_yel + col_bol + "[WARNING]" + col_def
ERRO=col_red + col_bol + "[ERROR]"   + col_def


class FTKProject:
  """ Main class of the ftkmaker package
  It contains the basic configuration variables and it drives the execution
  """
  def __init__(self):

    # Configuration
    self.args              = parse_cmdline(doc_FTK, option_FTK)

    # Sanity check
    self.checkEnv()

    # Alert if breaking naming convention
    if "_" in self.args["file_suffix"]:
      print "Underscore in file suffix! File convention uses dashes(-)"

    # Output file name
    self.oksfile_ftkSwRepo = 'ftk/sw/FtkSwRepository.data.xml'
    self.oksfile_hosts     = ['ftk/hw/hosts-ftk.data.xml','daq/hw/hosts.data.xml']
    self.oksfile_ros       = 'daq/segments/ROS/ROS-FTK-robin-dc.data.xml'
    self.oksfile_gnam      = "ftk/segments/FTK-GNAM.data.xml"
    self.outfile_ftkCommon = "ftk/segments/FTK-common.data.xml"
    self.outfile_vme       = "ftk/segments/FTK-RCD-VME"+self.args["file_suffix"]+".data.xml"
    self.outfile_df        = "ftk/segments/FTK-RCD-DF"+self.args["file_suffix"]+".data.xml"
    self.outfile_flic      = "ftk/segments/FTK-RCD-FLIC"+self.args["file_suffix"]+".data.xml"
    self.outfile_ma        = "ftk/segments/FTK-RCD-Manager"+self.args["file_suffix"]+".data.xml"
    self.outfile_segment   = "ftk/segments/FTK-segment"+self.args["file_suffix"]+".data.xml"
    self.outfile_partition = "ftk/partitions/FTK"+self.args["file_suffix"]+".data.xml"

    #DQAgent file name (taken from the release)
    self.oksfile_dqmf      = "ftk/segments/FTK-DQMF.data.xml"

    # Includes
    baseInc = [self.oksfile_ftkSwRepo, self.outfile_ftkCommon ]
    baseInc += self.oksfile_hosts
    self.includes_ftkCommon= [ 'ftk/schema/FTKCommon.schema.xml', self.args['setup-file'], ]
    self.includes_vme      = baseInc + [
                               'ftk/schema/Readout_Ssb.schema.xml',
                               #'ftk/schema/Readout_Aux.schema.xml',
                               #'ftk/schema/Readout_Ambslp.schema.xml',
                               'ftk/schema/ReadoutModule_Dummy.schema.xml',
                               'ftk/schema/ReadoutModule_PU.schema.xml'
                             ]  + ftkmaker.process_vme.optional_includes(self) + self.args['vme-extra-inc']
    self.includes_df       = baseInc + [
                               'ftk/schema/Readout_DataFormatter.schema.xml',
                             ] + ftkmaker.process_df.optional_includes(self) + self.args['df-extra-inc']
    self.includes_flic     = baseInc + [
                               'ftk/schema/Readout_flic.schema.xml',
                             ] + ftkmaker.process_flic.optional_includes(self) + self.args['flic-extra-inc']
    self.includes_ma      = baseInc + ['ftk/schema/ReadoutModule_Manager.schema.xml',]
    self.includes_segment  = baseInc + ["daq/segments/setup.data.xml", "daq/schema/RODBusy.schema.xml"]

    #check if P1 (in this case HLT_SW_Repository should be added)
    if not self.args['is_lab4']:
        baseInc.append('daq/sw/HLT_SW_Repository.data.xml')
    self.includes_partition= baseInc + [self.outfile_segment]

    # check for any master configs
    config_enum = ftkmaker.slot_configurations.findConfig(self.args)
    crates_dictionary = ftkmaker.slot_configurations.getCratesAndShelves(self.args, config_enum)
    self.vme_crates = crates_dictionary['vme-crates']
    self.df_shelves = crates_dictionary['df-shelves']

    # Projects
    self.proj_swRepo    = pm.project.Project("daq/sw/repository.data.xml")
    self.proj_ftkSwRepo = pm.project.Project(self.oksfile_ftkSwRepo)
    self.proj_hosts     = []
    self.proj_segment   = None
    self.proj_ros       = None
    self.proj_common    = None
    self.proj_vme       = None
    self.proj_df        = None
    self.proj_flic      = None
    self.proj_ma        = None

    for f in self.oksfile_hosts:
        try: self.proj_hosts.append( pm.project.Project(f) )
        except RuntimeError:
           print ERRO, 'hw file ', f, 'not found'
           sys.exit(2)


    # Other stuff
    self.readoutApp     = 0
    self.dal            = dal_module('dal',     'daq/schema/core.schema.xml' )
    self.DFdal          = dal_module('DFdal',   'daq/schema/df.schema.xml',                    [dal])
    self.RODBusy        = dal_module('RODBusy', 'daq/schema/RODBusy.schema.xml',               [self.dal, self.DFdal])
    self.dal_ssb        = dal_module('dal_ssb', 'ftk/schema/Readout_Ssb.schema.xml',           [dal])
    self.dal_COM        = dal_module('dal_COM', 'ftk/schema/FTKCommon.schema.xml',	           [dal])
#    self.dal_amb        = dal_module('dal_amb', 'ftk/schema/Readout_Ambslp.schema.xml',        [dal])
#    self.dal_aux        = dal_module('dal_aux', 'ftk/schema/Readout_Aux.schema.xml',           [dal])
    self.dal_PU         = dal_module('dal_PU',  'ftk/schema/ReadoutModule_PU.schema.xml',      [dal])
    self.dal_df         = dal_module('dal_df',  'ftk/schema/Readout_DataFormatter.schema.xml', [dal])
    self.dal_flic       = dal_module('dal_flic','ftk/schema/Readout_flic.schema.xml',          [dal])
    self.dal_RMD        = dal_module('dal_RMD', 'ftk/schema/ReadoutModule_Dummy.schema.xml',   [dal])
    self.dal_MA         = dal_module('dal_MA',  'ftk/schema/ReadoutModule_Manager.schema.xml', [dal])
    self.gnamFTK        = dal_module('dal_gnam', 'ftk/segments/FTK-GNAM.data.xml')
    self.ftkSwRepo      = self.proj_ftkSwRepo.getObject("SW_Repository", "FTK")
    self.RCDs           = []
    self.segm           = None
    self.detector       = None
    self.mem_pool       = None
    self.emu_dataout    = None
    self.FTKEDO         = None

  # Check running environment
  def checkEnv(self):
    """ Check the shell environment
    """
    if not os.environ.get('FTK_RELEASE'):
      print ERRO, 'Enviroment variable $FTK_RELEASE not set. Please source the FTK release'
      sys.exit(1)
    # Add local path to $TDAQ_DB_PATH
    os.environ['TDAQ_DB_PATH'] = os.getcwd() + ":" + os.environ['TDAQ_DB_PATH']
    print "TDAQ_DB_PATH =\t", os.environ['TDAQ_DB_PATH'].replace(":","\n\t\t")

  # Create the directory tree in the current working directory
  def createDirTree(self):
    """ Create the directory tree that will host the OKS file
    """
    print "Creating tree ..."
    if not os.path.exists("ftk/partitions"):
      os.makedirs("ftk/partitions")

    if not os.path.exists("ftk/segments"):
      os.makedirs("ftk/segments/")

    if not os.path.exists("ftk/DFconfigFiles"):
      os.makedirs("ftk/DFconfigFiles/")

  # Create common stuffs. E.g.: Detector object
  def createCommon(self):
    """ Create the shared objects to be hosted in the common oks file.
    If the file is available in the $TDAQ_DB_PATH, it loads the objects
    from it, otherwise it makes a brand new file
    """
    if (not os.path.exists(self.outfile_ftkCommon)
         or os.path.getsize(self.outfile_ftkCommon) == 0):
      print "Creating brand new common file:", self.outfile_ftkCommon
      self.proj_common = pm.project.Project(self.outfile_ftkCommon, self.includes_ftkCommon)

      # detector
      self.detector = dal.Detector("FTK")
      self.detector.State = 0
      self.detector.LogicalId = 127 # 0x7f, the FTK logical ID
      self.proj_common.addObjects([self.detector])

      # Memory Pool
      self.mem_pool = self.DFdal.MemoryPool("FtkMemoryPool")
      self.mem_pool.NumberOfPages = 8
      self.mem_pool.PageSize = 512
      self.proj_common.addObjects([self.mem_pool])

      # Emulated data out
      self.emu_dataout = self.DFdal.EmulatedDataOut("FTKEmuDataOut")
      self.emu_dataout.OutputDelay = 0
      self.proj_common.addObjects([self.emu_dataout])

      #FTKEMonDataOut
      self.FTKEDO = self.dal_COM.FTKEMonDataOut("FTKEDO")
      self.FTKEDO.key = "ReadoutApplication"
      self.proj_common.addObjects([self.FTKEDO])

    # If FTK common exists, the script will use it
    else:
      self.proj_common = pm.project.Project(self.outfile_ftkCommon)
      self.detector    = self.proj_common.getObject("Detector", "FTK")
      self.mem_pool    = self.proj_common.getObject("MemoryPool", "FtkMemoryPool")
      self.FTKEDO     = self.proj_common.getObject("FTKEMonDataOut", "FTKEDO")
      print "Using available common file:", self.outfile_ftkCommon


  # get computer object by name
  def getComputer(self, computerName):
    """ Try to get the computer 'computerName' from the hw database
    If the computer is not found, the program exits providing a list
    of the computers available in the DB
    """
    pc=None
    for i,pj in enumerate(self.proj_hosts):
        try:
            pc = pj.getObject("Computer", computerName)
            break
        except RuntimeError:
            if i == len(self.proj_hosts)-1 :
                print "Available computers:"
                includedHosts = [".*ftk.*", ".*-pub-.*"]
                combinedIncludedHosts = "(" + ")|(".join(includedHosts) + ")"
                for p in self.proj_hosts:
                    for c in p.getObject("Computer"):
                        if re.match(combinedIncludedHosts, c.id):
                            print "\t", c.id
                msg = "%s Computer %s not found in %s and their includes (only `ftk` and `pub` machines are listed) \n\tThe available computers are listed above " % (
                      ERRO, computerName, self.oksfile_hosts )
                print msg
                sys.exit(2)
    return pc


  # Create RCD object
  def makeRCD(self, rcd_id, computerName=""):
    """ It creates an RCD named 'rcd_id' and running on 'computerName'
    """
    print "Making RCD:", rcd_id

    rcd =  self.DFdal.RCD(rcd_id)
    rcd.Program = self.readoutApp
    rcd.Uses    = [self.ftkSwRepo]
    rcd.Detector= self.detector
    if computerName:
      rcd.RunsOn = self.getComputer(computerName)

    rcd.MemoryConfiguration = self.mem_pool
    rcd.DebugOutput         = self.emu_dataout
    rcd.MonitoringOutput    = self.FTKEDO
    rcd.InterfaceName       = "rc/commander"
    rcd.InitTimeout         = 60
    rcd.ActionTimeout       = self.args['ActionTimeout']
    rcd.ProbeInterval       = self.args['ProbeInterval']
    rcd.FullStatisticsInterval = self.args['FullStatisticsInterval']

    # readout configuration. TODO: it could be move to common, but this will make
    # impossible to set ThreadedConfigure for each RCD
    readout_config = self.dal_COM.ReadoutConfiguration_FTK("ReadoutConfiguration_FTK_" + rcd_id)
    readout_config.TraceLevel        = 10
    readout_config.TracePackage      = 0
    readout_config.ThreadedConfigure = self.args['ThreadedConfigure_opt']      # NB. needed to have concurrent readout modules execution
    readout_config.ReleaseDelay      = 0
    readout_config.RequestTimeOut    = 0
    rcd.Configuration = readout_config

    self.RCDs.append(rcd)
    return rcd


  # Create configuration for vme crates
  def makeVme(self):
    """ It calls the function ftkmaker.process_vme.execute() for each RCD requested
    The function is called providing the index in the 'vme-crates' list, i.e. the
    crate to be processed
    """
    print "Making project", self.outfile_vme
    self.proj_vme = pm.project.Project(self.outfile_vme, self.includes_vme)
    self.includes_segment.append(self.outfile_vme)

    # Get crates for this configuration
    idx = 0
    for crate in self.vme_crates:
      crateId = crate[0]
      nodeName= crate[1]
      crateMap= crate[2]
      # Sanity check
      if len(crateMap) > 22:
        print WARN, "Too many slots in the VME create map:", crateMap
      rcdName = "FTK-RCD-VME-" + crateId
      rcd = self.makeRCD(rcdName, nodeName)
      ftkmaker.process_vme.execute(rcd, self, idx)
      idx = idx + 1
      self.proj_vme.addObjects([rcd])

  # Create configuration for DF shelves
  def makeDF(self):
    """ It calls the function ftkmaker.process_df.execute() for each RCD requested
    The function is called providing the index in the 'vme-shelves' list, i.e. the
    shelf to be processed
    """
    print "Making project", self.outfile_df
    self.proj_df = pm.project.Project(self.outfile_df, self.includes_df)
    self.includes_segment.append(self.outfile_df)

    if self.args["GenerateConfiguration"]: # Generate configuration if indicated
      if not os.path.exists("ftk/DFconfigFiles/"):
        os.makedirs("ftk/DFconfigFiles/")

      if self.args["PseudoData_directory"] == '' and self.args['usePseudoData'] is True:
        # Stop if using pseudodata and no pseudodata given
        sys.exit("No pseudodata directory given! Cannot generate configuration")
      if self.args["LUTs_directory"] == '':
        sys.exit("No LUTs directory given! Cannot generate configuration")

      makeDfDelayValues()
      makeConnections(self.args['is_lab4'])
      makeSystemConfig64()
      makeMultiboardConfig(self.df_shelves,self.args['is_lab4'])
      makeTxConfig(self.args['exclude_aux'],self.args['exclude_amb'],self.args['exclude_ssb'])
      makeAddressFile(self.args['address_df_location'])
    # Process DF shelves
    idx = 0
    for shelf in self.df_shelves:
      shelfId = shelf[0]
      nodeName= shelf[1]
      rcdName = "FTK-RCD-DF-" + shelfId
      if self.args['create_enhanced_slice']:
        rcdName = rcdName + "-SliceA"
      elif self.args['create_enhanced_slice40']:
        rcdName = rcdName + "-Slice2"
      rcd = self.makeRCD(rcdName, nodeName)
      ftkmaker.process_df.execute(rcd, self, idx)
      idx = idx + 1
      self.proj_df.addObjects([rcd])

  # Create configuration for FLIC shelf
  def makeFLIC(self):
    """ It calls the function ftkmaker.process_flic.execute() for each RCD requested
    The function is called providing the index in the 'vme-shelves' list, i.e. the
    shelf to be processed
    """
    print "Making project", self.outfile_flic
    self.proj_flic = pm.project.Project(self.outfile_flic, self.includes_flic)
    self.includes_segment.append(self.outfile_flic)
    # Process DF shelves
    idx = 0
    for shelf in self.args['flic-shelves']:
      shelfId = shelf[0]
      nodeName= shelf[1]
      rcdName = "FTK-RCD-FLIC-" + shelfId
      rcd = self.makeRCD(rcdName, nodeName)
      ftkmaker.process_flic.execute(rcd, self, idx)
      idx = idx + 1
      self.proj_flic.addObjects([rcd])

  # Create configuration for Manager Application
  def makeManager(self):
    """
      It generates one RCD containing one Manager Application.
    """
    print "Making project", self.outfile_ma
    self.proj_ma = pm.project.Project(self.outfile_ma, self.includes_ma)
    self.includes_segment.append(self.outfile_ma)
    rcdName = "FTK-RCD-Manager" + self.args["file_suffix"]
    rcd = self.makeRCD(rcdName, "pc-ftk-tdq-01.cern.ch")
    RM_ma = ftkProj.dal_MA.ReadoutModule_Manager("Manager" + self.args["file_suffix"])
    RM_ma.performRecovery=1
    rcd.Contains.append(RM_ma)
    self.proj_ma.addObjects([rcd])


  # Make the segment file
  def makeSegment(self):
    """ It generates an FTK segment containing all the generated RCDs
    """
    print "Making project", self.outfile_segment

    self.includes_segment.append(self.oksfile_gnam)
    self.includes_segment.append(self.oksfile_dqmf)

    useRos=False
    if len(self.args["ROSs"]) > 0:
      useRos = True

    if useRos:
      try:
        self.includes_segment.append(self.oksfile_ros)
        self.proj_ros = pm.project.Project(self.oksfile_ros)
      except RuntimeError:
        print ERRO, 'ROS file', self.oksfile_ros, 'not found in $TDAQ_DB_PATH'
        sys.exit(2)

    self.proj_segment = pm.project.Project(self.outfile_segment, self.includes_segment)
    self.segm = self.dal.Segment("FTK" + self.args["file_suffix"])
    self.segm.IsControlledBy = self.dal.RunControlTemplateApplication("DefRC-TTC")
    self.segm.Resources.extend(self.RCDs)
    if len(self.args["segmentHost"]) > 0:
      self.segm.Hosts = [self.getComputer(self.args["segmentHost"])]

    FTKWatchdog=[]
    if useRos:
      for rosName in self.args["ROSs"]:
        print "Adding", rosName
        ros = None
        try: ros = self.proj_ros.getObject("ROS", rosName)
        except RuntimeError:
          print ERRO, "ROS", rosName, "not found in", self.oksfile_ros
          sys.exit(2)
        self.segm.Resources.append(ros)
        #Creation of the FTKWatchdog
        if not self.args['is_lab4']:
          FTKWatchdog.append(self.makewatchdog(ros))
          self.segm.Applications = FTKWatchdog

          #Generation of the objects required by the Watchdog
          Resource = self.dal.Resource("FTK_Busy_Source_%s" %(ros.id))
          BusyChannel = self.RODBusy.BusyChannel("FTK_Busy_Channel_%s" %(ros.id))
          BusyChannel.BusySource=Resource
          ResourceSet = self.dal.ResourceSetOR("FTK_RCD_TO_ROL_%s" %(ros.id))
          if ros.id[-1:] == "0":
            for i in range (1,9):
              ResourceSet.Contains.append(self.proj_segment.getObject("RobinDataChannel", "ROL-TDQ-FTK-00-7f00%s" %('{:02x}'.format(i))))
          elif ros.id[-1:] == "1":
            for i in range (9,17):
              ResourceSet.Contains.append(self.proj_segment.getObject("RobinDataChannel", "ROL-TDQ-FTK-01-7f00%s" %('{:02x}'.format(i))))
          else:
            print "ERROR: Something changed in the ROL configuration. Please update the code"
            sys.exit(2)
          print "generation of the Stopless Removal infrastructure ready"
          ResourceSet.Contains.append(BusyChannel)
          self.proj_segment.addObjects([BusyChannel])
          self.segm.Resources.append(ResourceSet)

    #Adding gnamFTK and FTK_DQAgent
    self.proj_DQA  = pm.project.Project(self.oksfile_dqmf)
    self.proj_gnam = pm.project.Project(self.oksfile_gnam)
    DQMF    = self.proj_DQA.getObject("DQAgent", "FTK_DQAgent")
    GNAMFTK = self.proj_gnam.getObject("GnamApplication","gnamFTK")
    self.segm.Applications.append(DQMF)
    self.segm.Resources.append(GNAMFTK)

    #Generation of the checkConnect subtransition

    subTranCheckConfig = self.dal.SubTransition("FTK_checkConfig")
    subTranCheckConfig.MainTransition="CONFIGURE"
    subTranCheckConfig.Substeps.append("checkConfig")
    self.proj_segment.addObjects([subTranCheckConfig])
    self.segm.SubTransitions.append(subTranCheckConfig)

    subTran = self.dal.SubTransition("FTK_checkConnect")
    subTran.MainTransition="START"
    subTran.Substeps.append("checkConnect")
    self.proj_segment.addObjects([subTran])
    self.segm.SubTransitions.append(subTran)

    subTran1 = self.dal.SubTransition("FTK_startNoDF")
    subTran1.MainTransition="START"
    subTran1.Substeps.append("startNoDF")
    self.proj_segment.addObjects([subTran1])
    self.segm.SubTransitions.append(subTran1)

    self.proj_segment.addObjects([self.segm])


  # Make watchdog customlife application
  def makewatchdog(self,ros):
    print "Starting the generation of the Watchdog"
    self.CustomApp = self.dal.CustomLifetimeApplication("FTKWatchdog-%s"%(ros.id))
    self.Watchdog = self.proj_ftkSwRepo.getObject("Script","FTKWatchDogScript")
    self.CustomApp.Program = self.Watchdog
    self.CustomApp.Lifetime = "SOR_EOR"
    self.CustomApp.Parameters = "-s 60 -w 10 -v 1000 -o ROS.%s.ReadoutModule0 -r FTK%s -f FTK%s -b FTK_Busy_Channel_%s" %(ros.id, self.args["file_suffix"], self.args["file_suffix"], ros.id)
    self.CustomApp.RestartParameters = "-s 60 -w 10 -v 1000 -o ROS.%s.ReadoutModule0 -r FTK%s -f FTK%s -b FTK_Busy_Channel_%s" %(ros.id, self.args["file_suffix"], self.args["file_suffix"], ros.id)
    self.CustomApp.RestartableDuringRun = 1
    #self.CustomApp.RunsOn = self.getComputer(self.args["segmentHost"])
    self.CustomApp.RunsOn = ros.RunsOn
    self.CustomApp.ProcessEnvironment = [self.proj_segment.getObject("Variable","PYTHONPATH_NO_TAGS"),
		  			 self.proj_segment.getObject("Variable","TDAQ_PYTHON_HOME"),
					 self.proj_segment.getObject("Variable","DEF_FATAL_STREAM"),
					 self.proj_segment.getObject("Variable","DEF_WARNING_STREAM"),
				 	 self.proj_segment.getObject("Variable","DEF_INFO_STREAM"),
				 	 self.proj_segment.getObject("Variable","DEF_ERROR_STREAM"),
					 self.proj_segment.getObject("Variable","DEF_DEBUG_STREAM")]
    return self.CustomApp

  # Make the partition file
  def makePartition(self):
    """ It generates the FTK partition
    """
    print "Making project", self.outfile_partition
    p = pm.project.Project(self.outfile_partition, self.includes_partition )
    df_parameters = pm.common.create_df_parameters("DFParameters")

    #check if P1 (in that case the HLT-Parameters whould be added)
    part = create_partition(self.args["partitionName"],
                          [],
                          df_parameters = df_parameters,
                          environment=p.getObject("VariableSet", "CommonEnvironment"),
                          parameters=p.getObject("VariableSet", "CommonParameters"),
                          online_segment=p.getObject("OnlineSegment","setup"),
                          default_host=local_computer())

    #check if isLoadMode and in case, add the FTK_LOAD_MODE variable
    FTK_LOAD = dal.Variable('FTK_LOAD_MODE', Name='FTK_LOAD_MODE', Value='1', Description='If set it triggers the loading mode')
    if self.args["isLoadMode"]:
      part.ProcessEnvironment.append(FTK_LOAD)
    
#check if isLoadMode and in case, add the FTK_LOAD_MODE variable
    FTK_NO_FATAL = dal.Variable('FTK_NO_FATAL', Name='FTK_NO_FATAL', Value='0', Description='If set it does not allow fatals')
    if self.args["isNoFatalMode"]:
      part.ProcessEnvironment.append(FTK_NO_FATAL)

    #check if P1 (in that case the HLT-Parameters whould be added)
    if not self.args['is_lab4']:
      part.Parameters.append(p.getObject("VariableSet", "HLT-Parameters"))

    part.RepositoryRoot = self.args['repository-root']
    part.ProcessEnvironment.append(p.getObject("Variable", "TDAQ_VERSION"))
    ldir=self.args['logRoot']
    part.LogRoot=ldir+"/${TDAQ_VERSION}"
    part.WorkingDirectory=ldir
    part.Segments.append(self.segm)


    # IGUI Counters
    isRates_LV1 = self.dal.IS_EventsAndRates("ftk-lv1-counter")
    isRates_LV1.EventCounter = self.args['partitionCounters'][0][0]
    isRates_LV1.Rate         = self.args['partitionCounters'][0][1]
    isRates_LV1.Description  = "LVL1 counter"

    isRates_HLT = self.dal.IS_EventsAndRates("ftk-hlt-counters")
    isRates_HLT.EventCounter = self.args['partitionCounters'][1][0]
    isRates_HLT.Rate         = self.args['partitionCounters'][1][1]
    isRates_HLT.Description  = "HLT counter"

    isRates_SFO = self.dal.IS_EventsAndRates("ftk-rec-counters")
    isRates_SFO.EventCounter = self.args['partitionCounters'][2][0]
    isRates_SFO.Rate         = self.args['partitionCounters'][2][1]
    isRates_SFO.Description  = "Recording Counter"

    isInfo           = self.dal.IS_InformationSources("ftk-IS-info")
    isInfo.LVL1      = isRates_LV1
    isInfo.HLT       = isRates_HLT
    isInfo.Recording = isRates_SFO

    part.IS_InformationSource = isInfo


    # adding the partition to the project
    p.addObjects([part])



  # Main routine
  def make(self):
    """ The main routine
    """

    #Check if at least one RCD has been created, otherwise rise an advice
    count= len(self.args['flic-shelves'])+ len(self.df_shelves) + len(self.vme_crates)
    if count == 0:
      sys.exit("No RCD has been generated! \nPlease check your configuration file..")

    self.createDirTree()
    self.createCommon()

    # Get readoutapp object
    self.readoutApp       = self.proj_swRepo.getObject("Binary", "ReadoutApplication")
    self.readoutApp.Needs = [] # removing a resource that can't be shared. Necessary to
                               # execute two or more RCDs on the same machine

    # Process VME crates
    if len(self.vme_crates) > 0:
      self.makeVme()

    # Process DF shelves
    if len(self.df_shelves) > 0:
      self.makeDF()

    # Process FLIC shelf
    if len(self.args['flic-shelves']) > 0:
      self.makeFLIC()

    #Generate Manager Application
    if self.args['generate_manager']:
      self.makeManager()

    # FTK segment
    if self.args['generateSegment']:
      self.makeSegment()

      # FTK partition
      if len(self.args['partitionName'])>0:
        self.makePartition()

##################################################################################
############### BEGIN ###########################################################


ftkProj = FTKProject()  # Create an instance of FTKProject
ftkProj.make()          # Launch the generation
