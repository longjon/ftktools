#################################################
# Creating FTK documentation string
# It's printed as an introduction with -h option
doc_FTK = """
ftk_maker.py is a python script used to generate OKS files for FTK.
The documentation is available in this twiki page:
https://twiki.cern.ch/twiki/bin/view/Atlas/FtkMaker
"""

# TODO:
#  - Set defaults customized to P1
#  - Generation of the ROS segment for testing purpose
#  - Add watchdog setup.
#    - it requires the ROS segment

#################################################
# Creating FTK option dictionary
# each option key is a dictionary with the following key, values pairs:
#
# short = keyboard character representing the key in the command line
# arg = boolean flag true if the option can be set in the command line
# default = default value of the option
# description = key description
# group = used to collect keys

option_FTK = {}

from pm.option.common import option as option_FTK

# Tower being used
option_FTK['tower'] = {
  'short': '','arg': True,
  'default': 22 ,
  'description': 'This is the tower the slice is running with.'
    + '\n#option[\'tower\'] = 11',
  'group':'common'
  }

# Option for lab 4 testing
option_FTK['is_lab4'] = {
  'short': '','arg': True,
  'default': False ,
  'description': 'If true, will set up partition for Lab 4 testing.'
    + '\n#option[\'is_lab4\'] = True',
  'group':'common'
}

# True for generation of a configure partition
option_FTK['isLoadMode'] = {
  'short': '','arg': True,
  'default': True ,
  'description': 'If true, will generate t configuration partition (adding the FTK_LOAD_MODE variable).'
    + '\n#option[\'is_lab4\'] = False',
  'group':'common'
}

option_FTK['isNoFatalMode'] = {
  'short': '','arg': True,
  'default': True ,
  'description': 'If true, will generate a non-ATLAS partition (adding the FTK_NO_FATAL variable).',
  'group':'common'
}

# Option for set the ThreadedConfigure
option_FTK['ThreadedConfigure_opt'] = {
  'short': '','arg': True,
  'default': 0,
  'description': 'Flag to control whether or not a separate thread should be created for configuring each ReadOutModule.'
    + '\n#option[\'ThreadedConfigure_opt\'] = 1',
  'group':'common'
}

# Option for set the ProbeInterval
option_FTK['ProbeInterval'] = {
  'short': '','arg': True,
  'default': 20,
  'description': 'Value in seconds of the publish interval time.'
    + '\n#option[\'ProbeInterval\'] = 60',
  'group':'common'
}

# Option for set the FullStatisticsInterval
option_FTK['FullStatisticsInterval'] = {
  'short': '','arg': True,
  'default': 60,
  'description': 'Value in seconds of the publish_full_stat interval time.'
    + '\n#option[\'FullStatisticsInterval\'] = 9999',
  'group':'common'
}

# Option for lab 4 testing
option_FTK['use_m14_opts'] = {
  'short': '','arg': True,
  'default': False ,
  'description': 'If true, overwrites options based on an m14 configuration.'
    + '\n#option[\'use_m14_opts\'] = True',
  'group':'common'
}

# Option for lab 4 testing
option_FTK['file_suffix'] = {
  'short':'', 'arg': True, 'default':'',
  'description': 'Suffix to be added to the generated files name (e.g. -SliceA, -Slice2)',
  'group':'common'
}

# Option for using multithreading
option_FTK['UseMultithreading']={
  'default': 1,
  'short':'',
  'arg': True,
  'description':'Whether to use multithreading options',
  'group': 'common'
}  

# Option for manager application generation
option_FTK['generate_manager']={
  'default': True,
  'short':'',
  'arg': True,
  'description':'Whether to generate the Manager Application for this partition',
  'group': 'common'
}

# Option for manager application generation
option_FTK['ActionTimeout']={
  'default': 60,
  'short':'',
  'arg': True,
  'description':'Timeout of the RCD actions (e.g. transitions)',
  'group': 'common'
}

#########################################################
# Options for specific configurations of boards. These will overwrite any assignments of
# slots/shelves to turn on.

# Option for enhanced slice, tower 22
option_FTK['create_enhanced_slice'] = {
  'short': '','arg': True,
  'default': True ,
  'description': 'If true, uses the slot assignments and cabling for the enhanced slice.'
    + '\n#option[\'create_enhanced_slice\'] = False',
  'group':'common'
}

# Option for enhanced slice, tower 40
option_FTK['create_enhanced_slice40'] = {
  'short': '','arg': True,
  'default': False ,
  'description': 'If true, uses the slot assignments and cabling for the enhanced slice in tower 40.'
    + '\n#option[\'create_enhanced_slice40\'] = True',
  'group':'common'
}

# Option for "alberto" slice (all of DF shelf 1 + rack 05-07)
option_FTK['create_alberto_slice'] = {
  'short': '','arg': True,
  'default': False,
  'description': 'If true, uses the slot assignments and cabling for the full cabling and turns on DF shelf 1 and rack 05-07.'
    + '\n#option[\'create_alberto_slice\'] = True',
  'group':'common'
}

# Option for multiPU/multiDF slice (all of DF shelf 4 + rack 05-09)
option_FTK['create_multi_DF_PU_slice'] = {
  'short': '','arg': True,
  'default': False,
  'description': 'If true, uses the slot assignments and cabling for the full cabling and turns on DF shelf 4 and rack 05-09.'
    + '\n#option[\'create_multi_DF_PU_slice\'] = True',
  'group':'common'
}

# Option for a slice using tower 35
option_FTK['create_slice_35'] = {
  'short': '','arg': True,
  'default': False ,
  'description': 'If true, uses the slot assignments and cabling for the full configuration with only tower 35 enabled.'
    + '\n#option[\'create_slice_35\'] = True',
  'group':'common'
}

# Option for a slice using tower 35 + all DF in shelf 1
option_FTK['create_slice_35_allDF'] = {
  'short': '','arg': True,
  'default': False ,
  'description': 'If true, uses the slot assignments and cabling for the full configuration with only tower 35 enabled, but all shelf 1 DFs.'
    + '\n#option[\'create_slice_35_allDF\'] = True',
  'group':'common'
}

# Option for final config (turns on all slots!)
option_FTK['create_final_config'] = {
  'short': '','arg': True,
  'default': False ,
  'description': 'If true, uses the slot assignments and cabling for the full configuration with all towers enabled.'
    + '\n#option[\'create_final_config\'] = True',
  'group':'common'
}

###################################################################################

# Running without the AUX?
option_FTK['exclude_aux'] = {
'short': '', 'arg': True, 'default': False,
'description': 'Option used to enable running without AUX'
+'\n#option[\'exclude_aux\'] = True',
'group': 'common'
}

# Running without the AMB?
option_FTK['exclude_amb'] = {
'short': '', 'arg': True, 'default': False,
'description': 'Option used to enable running without AMB'
+'\n#option[\'exclude_amb\'] = True',
'group': 'common'
}

# Running without the SSB?
option_FTK['exclude_ssb'] = {
'short': '', 'arg': True, 'default': False,
'description': 'Option used to enable running without SSB'
+'\n#option[\'exclude_ssb\'] = True',
'group': 'common'
}


# Option to configure constants etc. for data or pseudodata from data (not MC)
option_FTK['use_data_alignment'] = {
  'short': '','arg': True,
  'default': True,
  'description': 'If true, makes sure we use all constants and pbanks for data, not MC.'
    + '\n#option[\'use_data_alignment\'] = False',
  'group':'common'
}

# Option to use empty test vectors
option_FTK['empty_tvs'] = {
  'short': '','arg': True,
  'default': False ,
  'description': 'If true, will use TVs without any hits.'
    + '\n#option[\'empty_tvs\'] = True',
  'group':'common'
}


# Dummy option used to dump "import collections"
option_FTK['PleaseIgnoreMe']  = {
  'short': '','arg': False,
  'default': None ,
  'description': 'This is just a dirty trick to dump import statements. IGNORE IT'
    + '\nimport collections'
    + '\nCrate = collections.namedtuple("Crate", "name computer stringmap")',
  'group':' ignore'
}


# Partition name.
option_FTK['partitionName'] = {
  'short':'', 'arg': True, 'default':'',
  'description': 'Partition name. If empty the partition object is not generated',
  'group':'common'
}

# Log directory.
option_FTK['logRoot'] = {
  'short':'', 'arg': True, 'default':'/logs',
  'description': 'Specify the directory for log files',
  'group':'common'
}

# Partition counters
option_FTK['partitionCounters'] = {
  'short':'', 'arg': True,
  'default':[
              ("DF.HLTSV.Events.LVL1Events",      "DF.HLTSV.Events.Rate"),
              ("DF.HLTSV.Events.ProcessedEvents", "DF.HLTSV.Events.Rate"),
              ("DF.SFO-1.EventsSaved",            "DF.SFO-1.CurrentEventSavedRate")
            ],
  'description': 'Partition counters: list of 3 tuples representing events and rates for L1, HLT and SFO',
  'group':'common'
}


# Switch to control the generation of the segment
option_FTK['generateSegment'] = {
  'short':'', 'arg': True, 'default':True,
  'description': 'If set to false the segment is not generated: i.e. only the RCD files are created',
  'group':'common'
}

# Switch to control the generation of the segment
option_FTK['segmentHost'] = {
  'short':'', 'arg': True, 'default':'',
  'description': 'Host where the segment controller will be execute. If empty the current PC is used',
  'group':'common'
}

# Link the ROS to the FTK segment
option_FTK['ROSs'] = {
  'short':'', 'arg': True, 'default':[],
  'description': 'List of ROS objects to be appended to the FTK segment. The objects are searched for in the file'
           + '\n# "ftk/segments/ROS/ROS-FTK-robin-dc.data.xml", that is considered as an input parameter.'
           + '\n# If the list is empty, no ROSs are linked to the FTK segment'
           + '\n# Eg: option[\'ROSs\'] = ["ROS-TDQ-FTK-00", "ROS-TDQ-FTK-01"]',
  'group':'common'
}



# Configuration of the vme crates
# NB: The possibility to use a collections.namedtuple was explored but it looks impossible
#     since the "import collections" is not propagated to the user option file
option_FTK['vme-crates']  = {
  'short': '','arg': True,
  'default': [],
  'description': 'Python list describing each vme crate. Each element in the list is a python'
    + '\n# tuple composed of 3 strings: crate number, sbc name, crate map. The latter is a '
    + '\n# string where the index represents the slot and the characters means: "P"=PU, "S"==ssb'
    + '\n# "R"=RMDummy, "_"=unused slot. [NB: slot 0 does not exist and 1 is the SBC]. E.g.:'
    + '\n#option[\'vme-crates\'] = ['
    + '\n#   ("4","sbc-tbed-ftk-04.cern.ch","__PPPPSPPPPSPPPSPPPS"),'
    + '\n#   ("2","sbc-tbed-ftk-02.cern.ch","__PPPPS_____PPPSPPPS") ]'
    + '\n#option[\'vme-crates\'] = [ ("4", "sbc-ftk-rcc-04.cern.ch","___________________P") ]'
    + '\n#option[\'vme-crates\'] = [ ("1","pc-tbed-pub-03.cern.ch","____RR______________")]',
  'group':'vme'
}


# Configuration of the DF shelves
# NB: The possibility to use a collections.namedtuple was explored but it looks impossible
#     since the "import collections" is not propagated to the user option file
option_FTK['df-shelves']  = {
  'short': '','arg': True,
  'default': [],
  'description':
          'Python list describing each DF shelf. Each element in the list is a python'
    + '\n# a tuple composed of 3 strings: shelf number, pc name, shelf map. The latter'
    + '\n# is string where the index represents the slot and the characters means:'
    + '\n# "D"=DF board, "_"=unused slot. [NB: slot 0 does not exist] E.g:'
    + '\n#option[\'df-shelves\'] = ['
    + '\n#       ("Shelf_Number","pc-tbed-pub-17.cern.ch","__D_D___"),'
    + '\n#       ("Shelf_Number","pc-tbed-pub-17.cern.ch","__D_DDDD") ]',
  'group':'df'
}


# Configuration of the FLIC shelves
# NB: The possibility to use a collections.namedtuple was explored but it looks impossible
#     since the "import collections" is not propagated to the user option file
option_FTK['flic-shelves']= {
  'short': '','arg': True,
  'default': [],
  'description':
          'Python list describing each DF shelf. Each element in the list is a python'
    + '\n# a tuple composed of 3 strings: shelf number, pc name, shelf map. The latter'
    + '\n# is string where the index represents the slot and the characters means:'
    + '\n# "F"=FLIC board and "_"=unused slot. [NB: slot 0 does not exist] E.g:'
    + '\n#option[\'flic-shelves\'] = [ ("Shelf_Number","pc-tbed-pub-17.cern.ch","_F_F_") ]'
    + '\n#'
    + '\n# option[\'flic-shelves\'] = ['
    + '\n# ( "1" , "pc-ftk-tdq-04.cern.ch" , "_F___" ) ]', #SliceA
  'group':'flic'
}



# Extra includes
option_FTK['vme-extra-inc'] = {
  'short': '','arg': True, 'default':[],
  'description':'Extra includes for the VME RCDs.',
  'group':'vme'
}

option_FTK['df-extra-inc'] = {
  'short': '','arg': True, 'default':[],
  'description':'Extra includes for the DF RCDs.',
  'group':'df'
}

option_FTK['flic-extra-inc'] = {
  'short': '','arg': True, 'default':[],
  'description':'Extra includes for the Flic RCDs.',
  'group':'flic'
}


# Extend the dictionary calling board specific code
import options_vme, options_df, options_flic
options_vme.extend(option_FTK)
options_df.extend(option_FTK)
options_flic.extend(option_FTK)
