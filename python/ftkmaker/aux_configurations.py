########################################################################
### Dictionaries for different AUX configurations.
########################################################################
### Assumes a default of running the full slice at P1
### In current set up, each option should be identical to the name of
### the attribute to set in the xml file. This is applied *only* to AUX
### so no conflicts with other boards should arise.
########################################################################

import os
import json

# Default options (only need to be set if different from those defined in the schema)
default_dic = {
    'DryRun':                           0,
    'DisableBlockWrites':               0,
    'HoldIgnoreProc':                   0,
    'HoldIgnoreSSB':                    0,
    'HoldIgnoreAMB':                    0,
    'AllowFreezeFromProcsToInputs':     1,
    'Ignore_Freeze':                    1,
    'ForceHoldProcOut':                 0,
    'EnableAMBFreeze':                  1,
    'Enable_hitsort':                   1,
    'SourceRoads':                      0xf,
    'SourceHits':                       '"df"',
    'EventRate':                        0,
    'ProcFreezeMask':                   0xFFFFFDFC,
    'I1FreezeMask':                     0x0FFFFFFC,
    'I2FreezeMask':                     0x0FFFFFFC,
    'HWFreezeMask':                     0x0FFFFFFC,
    'MaxFitsPerRoad':                   0x3e8,
    'MaxRoadsPerEvent':                 0x1f4,
    }


#######################################################################
### Configurations to be turned on by various options
#######################################################################

# Dictionary for options specific to Lab 4
lab4_dic = {
    }

# Dictionary for AUX options when the AMB is excluded
exclude_amb_dic = {
    'HoldIgnoreAMB': 1,     # If not all processors are sending roads from FIFOs, keep this 0.
    'SourceRoads': 0x0,     # Set the bits to zero corresponding to the processor(s) on which you are using road FIFOs
    }

# Dictionary for AUX options when the SSB isn't being used in the slice
exclude_ssb_dic = {
    'HoldIgnoreSSB': 1,
    }

#######################################################################
### Files to be included at P1 / Lab4
#######################################################################

# Files to be included at P1
default_inc = [
        "daq/segments/setup.data.xml",
        "daq/schema/core.schema.xml",
        "daq/hw/hosts-ros.data.xml",
        "ftk/sw/FtkSwRepository.data.xml",
        "ftk/hw/hosts-ftk.data.xml",
#        "ftk/schema/Readout_Ambslp.schema.xml",
        "ftk/schema/Readout_Ssb.schema.xml",
#        "ftk/schema/Readout_Aux.schema.xml",
        "ftk/segments/FTK-common.data.xml",
        "ftk/schema/FTKCommon.schema.xml",
        ]

#Files to be included in Lab4
lab4_inc = [
        "daq/segments/setup.data.xml",
        "daq/schema/core.schema.xml",
#       "ftk/schema/Readout_Aux.schema.xml",
        "ftk/segments/lab4-FTK-AuxInputFiles.data.xml",
        "ftk/segments/lab4-FTK-PatternBank.data.xml",
        ]

#######################################################################
### Other default values to be set
#######################################################################

# Tower assignments for AUX/AMB slots
# Note: crate names are based on SBC names, not the DCS names
# assigned to each crate. The correspondance (as well as a prettier
# map of slot assignments) can be found here:
# https://docs.google.com/spreadsheets/d/1SCpMPV6D_2F60uCM3l4JlEiv5ZPTxY_ts3aEMnBmQng/edit#gid=1781589287

enhanced_slice_tower_assignments ={"4": {19: 22}, "3": {7: 40},}

final_config_tower_assignments = {
        "3": # Crate index
            { #slot indices
                #1: sbc-ftk-rcc-03
                2:  8,
                #3:
                4:  24,
                #5:
                #6: ssb
                7:  40,
                #8:
                9:  56,
                #10:
                #11: ssb
                12: 9,
                #13:
                14: 25,
                #15:
                #16: ssb
                17: 41,
                #18:
                19: 57,
                #20:
                #21: ssb
            },
        "4": # Crate index
            { #slot indices
                #1: sbc-ftk-rcc-04
                2:  6,
                #3:
                4:  22,
                #5:
                #6: ssb
                7:  38,
                #8:
                9:  54,
                #10:
                #11: ssb
                12: 7,
                #13:
                14: 23,
                #15:
                #16: ssb
                17: 39,
                #18:
                19: 55,
                #20:
                #21: ssb
            },
        "2": # Crate index
            { #slot indices
                #1: sbc-ftk-rcc-02
                2:  2,
                #3:
                4:  18,
                #5:
                #6: ssb
                7:  34,
                #8:
                9:  50,
                #10:
                #11: ssb
                12: 3,
                #13:
                14: 19,
                #15:
                #16: ssb
                17: 35,
                #18:
                19: 51,
                #20:
                #21: ssb
            },
        "1": # Crate index
            { #slot indices
                #1: sbc-ftk-rcc-01
                2:  4,
                #3:
                4:  20,
                #5:
                #6: ssb
                7:  36,
                #8:
                9:  52,
                #10:
                #11: ssb
                12: 5,
                #13:
                14: 21,
                #15:
                #16: ssb
                17: 37,
                #18:
                19: 53,
                #20:
                #21: ssb
            },
        "8": # Crate index
            { #slot indices
                #1: sbc-ftk-rcc-08
                2:  14,
                #3:
                4:  30,
                #5:
                #6: ssb
                7:  46,
                #8:
                9:  62,
                #10:
                #11: ssb
                12: 15,
                #13:
                14: 31,
                #15:
                #16: ssb
                17: 47,
                #18:
                19: 63,
                #20:
                #21: ssb
            },
        "7": # Crate index
            { #slot indices
                #1: sbc-ftk-rcc-07
                2:  0,
                #3:
                4:  16,
                #5:
                #6: ssb
                7:  32,
                #8:
                9:  48,
                #10:
                #11: ssb
                12: 1,
                #13:
                14: 17,
                #15:
                #16: ssb
                17: 33,
                #18:
                19: 49,
                #20:
                #21: ssb
            },
        "6": # Crate index
            { #slot indices
                #1: sbc-ftk-rcc-06
                2:  10,
                #3:
                4:  26,
                #5:
                #6: ssb
                7:  42,
                #8:
                9:  58,
                #10:
                #11: ssb
                12: 11,
                #13:
                14: 27,
                #15:
                #16: ssb
                17: 43,
                #18:
                19: 59,
                #20:
                #21: ssb
            },
        "5": # Crate index
            { #slot indices
                #1: sbc-ftk-rcc-05
                2:  12,
                #3:
                4:  28,
                #5:
                #6: ssb
                7:  44,
                #8:
                9:  60,
                #10:
                #11: ssb
                12: 13,
                #13:
                14: 29,
                #15:
                #16: ssb
                17: 45,
                #18:
                19: 61,
                #20:
                #21: ssb
            },
        }


# Special configuration for M14
m14_slot_overwrites = {
        "4": # Crate index
            {
            19: { "exclude_ssb": False, },
            20: { "exclude_ssb": False,  },
            },
        "2":
            {
            9:  { "exclude_ssb": True, },
            10: { "exclude_ssb": True, },
            12: { "exclude_ssb": True, },
            13: { "exclude_ssb": True, },
            19: { "exclude_ssb": True, },
            20: { "exclude_ssb": True, },
            },
}
