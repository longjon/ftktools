''' Module used to extend the option_FTK dictionary
'''

def extend(option_FTK):
  ''' Function to be used to extend the option_FTK dictionary
  To be used by amb, aux and ssb board expert
  '''
  # Ignore BP from FLIC to SSB
  option_FTK['ignore_flic'] = {
    'short': '', 'arg': True, 'default': False,
    'description': 'Option used to ignore BP from FLIC to SSB'
        +'\n#option[\'ignore_flic\'] = True',
    'group': 'vme'
  }

  # Compute SSB checksums
  option_FTK['compute_SSB_checksums'] = {
    'short': '', 'arg': True, 'default': False,
    'description': 'Compute checksums of SSB EXP and TF constants.  This does not do anything',
    'group': 'vme'
  }

  # SSB SSMap
  option_FTK['SSB_SSMap_Base'] = {
    'short': '', 'arg': True, 'default': '/det/ftk/repo/condDB/latest/SSB/64Tower_config/',
    'description': 'Base path for SSB SSMaps',
    'group': 'vme'
  }

  # SSB Constants
  option_FTK['SSB_Constants_Base'] = {
    'short': '', 'arg': True, 'default': '/det/ftk/repo/condDB/latest/SSB/consts/',
    'description': 'Base path for SSB constants',
    'group': 'vme'
  }


  n_towers = 64
  dummy_array = [ [i,'0x0'] for i in range(0, n_towers+1) ]
  # SSB EXP_Checksums
  option_FTK['SSB_EXP_Checksums'] = {
    'short': '', 'arg': True, 'default': dummy_array,
    'description': 'EXP checksums',
    'group': 'vme'
  }

  # SSB TF_Checksums
  option_FTK['SSB_TF_Checksums'] = {
    'short': '', 'arg': True, 'default': dummy_array,
    'description': 'TF checksums',
    'group': 'vme'
  }

  # SSB EXTF-Hash
  option_FTK['SSB_EXTF_Hash'] = {
    'short': '', 'arg': True, 'default': ["0x0","0x0"],
    'description': 'EXTF date,hash',
    'group': 'vme'
  }

  # SSB HW-Hash
  option_FTK['SSB_HW_Hash'] = {
    'short': '', 'arg': True, 'default': ["0x0","0x0"],
    'description': 'HW date,hash',
    'group': 'vme'
  }

  # SSB Freeze Mask
  option_FTK['SSB_FreezeMask'] = {
    'short': '', 'arg': True, 'default': "0x0",
    'description': 'Freeze Mask',
    'group': 'vme'
  }

  # SSB FPGA Reset Threshold
  option_FTK['SSB_Reset_Threshold'] = {
    'short': '', 'arg': True, 'default': "0x19999",
    'description': 'FPGA Reset threshold',
    'group': 'vme'
  }

  # SSB Ignore DFB
  option_FTK['SSB_IgnoreDFB'] = {
    'short': '', 'arg': True, 'default': "1",
    'description': 'Ignore DFB channel',
    'group': 'vme'
  }

  # SSB Ignore AUXB
  option_FTK['SSB_IgnoreAUXB'] = {
    'short': '', 'arg': True, 'default': "1",
    'description': 'Ignore AUXB channel',
    'group': 'vme'
  }

  # SSB AUX track limit
  option_FTK['SSB_AUXLimit'] = {
    'short': '', 'arg': True, 'default': "0xd8",
    'description': 'Limit number of tracks considered from AUX',
    'group': 'vme'
  }

  # SSB EXP track limit
  option_FTK['SSB_EXPLimit'] = {
    'short': '', 'arg': True, 'default': "0x50",
    'description': 'Limit number of tracks from EXP',
    'group': 'vme'
  }

  # SSB Chi2 cut in TF
  option_FTK['SSB_Chi2'] = {
    'short': '', 'arg': True, 'default': "0x5640",
    'description': 'Chi2 cut on tracks out of TF, 0x5640=100',
    'group': 'vme'
  }


  option_FTK['AUX_firmwareversions'] = {
    'short': '', 'arg': True, 'default':
    {'fwtag':                      "",
    'Input1':                      0x0,
    'Input2':                      0x0,
    'Processor1':                  0x0,
    'Processor2':                  0x0,
    'Processor3':                  0x0,
    'Processor4':                  0x0, },
    'description': 'AUX Firmware versions',
    'group': 'vme' 
}
  

  


  # Crate/Slot to tower LUT, format ("Name", SlotID): TowerID
  # Crates: 05-07 corresponds to the rack with sbcs 1 and 2 (1 is on top)
  #         07-07 corresponds to the rack with sbcs 4 and 3 (4 is on top)
  '''
  option_FTK['VMECrateToTowerLUT'] = {
    'short': '', 'arg': True, 'default': {
        ("Y.05-07.A2-crate2", 9): 37,
        ("Y.05-07.A2-crate2", 10): 36,
        ("Y.05-07.A2-crate2", 12): 19,
        ("Y.05-07.A2-crate2", 13): 18,
        ("Y.05-07.A2-crate2", 14): 21,
        ("Y.05-07.A2-crate2", 15): 20,
        ("Y.05-07.A2-crate2", 17): 39,
        ("Y.05-07.A2-crate2", 18): 38,
        ("Y.05-07.A2-crate2", 19): 25,
        ("Y.05-07.A2-crate2", 20): 24,

        ("Y.07-07.A2-crate4", 19): 22,
        ("Y.07-07.A2-crate4", 20): 23,

        #("Y.05-07.A2-crate1", 19): 22,
        #("Y.05-07.A2-crate1", 20): 23,
        },
    'description': 'LUT used to convert Crate/Slot in an FTK tower',
    'group': 'vme'
  }
  '''

  # Example of option for vme
  option_FTK['AmbFoobar'] = {
    'short':'', 'arg': True, 'default':'FooBar',
    'description': 'Just an example',
    'group':'vme'
  }


